# pySCTST #

A simple and user-friendly python package for performing Semiclassical Transition State Theory calculations, by Timothy Burd.

### What is this repository for? ###

This is a code base for performing semiclassical transition state theory (SCTST) calculations.

A recent review of SCTST can be found  [here.](https://pubs.acs.org/doi/abs/10.1021/acs.jpca.9b01987 "2019 Review Article")

It was specifically written to perform the calculations used in the following papers:

* [Burd, T.A., Shan, X. and Clary, D.C., 2018. Tunnelling and the kinetic
isotope effect in CH3 + CH4 -> CH4 + CH3: An application of semiclassical
transition state theory. Chemical Physics Letters, 693, pp.88-94.](https://www.sciencedirect.com/science/article/pii/S0009261418300022 "Paper Link")
* [Burd, T.A., Shan, X. and Clary, D.C., 2018. Catalysis and tunnelling in the
unimolecular decay of Criegee intermediates. Physical Chemistry Chemical
Physics, 20(39), pp.25224-25234.](https://pubs.rsc.org/en/content/articlelanding/2018/cp/c8cp05021j#!divAbstract "Paper Link")
* [Shan, X., Burd, T.A. and Clary, D.C., 2019. New Developments in Semiclassical
Transition-State Theory. The Journal of Physical Chemistry A, 123(22),
pp.4639-4657.9](https://pubs.acs.org/doi/abs/10.1021/acs.jpca.9b01987 "Paper Link")
* [Burd, T.A., Shan, X. and Clary, D.C., 2019. Tunnelling in cyclocarbenes: 
An application of Semiclassical Transition State Theory in reduced dimensions. Chemical Physics Letters, p.136783.](https://www.sciencedirect.com/science/article/abs/pii/S000926141930764X?via%3Dihub "Paper Link")
* [Burd, T. A. H., Shan, X., & Clary, D. C. (2020). Hydrogen Tunnelling in the Rearrangements of Carbenes: The Role of Dynamical Calculations. Physical Chemistry Chemical Physics. 22, 962-965](https://pubs.rsc.org/en/content/articlelanding/2020/CP/C9CP06300E "paper link")
* [Burd, T. A. H., & Clary, D. C. (2020). Analytic Route to Tunneling Splittings Using Semiclassical Perturbation Theory. Journal of Chemical Theory and Computation, 16(6), 3486-3493.](https://pubs.acs.org/doi/10.1021/acs.jctc.0c00207 "paper link")

### How do I get set up? ###

The getting_started_guide.pdf in this repository contains detailed information on running calculations. This is sumarised below:

There are three software requirements to use the pySCST software:
* Python 3.5 or later
* C complier, such as gcc ( Not required to run the examples and tutorials in the repository)
* Electronic structure package ( Not required to run the examples in the repository)
  1. Currently, pySCTST is designed for use with Gaussian output files.

To perform your own calculations, you will need access to an electronic structure package. Currently, this code is set up for Gaussian.


For example, to install and run the 1D-SCTST tutorial you first need to add the source code to your python path, and ensure all the requirements are installed:


```bash
$ git clone https://timothyburd@bitbucket.org/timothyburd/pysctst.git
$ cd pysctst
$ export PYTHONPATH=$PYTHONPATH:$(pwd)"/source" 
$ pip3 install --user -r requirements.txt
$ cd projects
$ cd 1D_SCTST
$ python3 tutorial_1d.py
```

### Tutorials and examples ###

The code contains four 'projects' which each illustrate a type of SCTST calculation. 
Each project also contains data and scripts to reproduce many of the results of the above papers, illustrating more complex calculations. 
Each project contains the required Gaussian output files, so no electronic structure 
calculations are required to do the tutorials.
Each project also contains a test file to ensure the code is correctly set up to reproduce the results in the papers. 

* 1D-SCTST
    1. Contains a short tutorial on 1D-SCTST calculations, using the CH3 + CH4 reaction 
    as an example. The main results of [this paper](https://www.sciencedirect.com/science/article/pii/S0009261418300022 "Paper Link") 
    are reproduced by the thesis_results.py script, to illustrate its application.
* FD-SCTST
    1. Contains a short tutorial on FD-SCTST calculations, using the Criegee decay reactions as an example. 
    The main results of [this paper](https://pubs.rsc.org/en/content/articlelanding/2018/cp/c8cp05021j#!divAbstract "Paper Link") are reproduced by the thesis_results.py script, to illustrate their application.
    2. This project also illustrates using SCTST for microcannonical rate consatnts.
* RD-SCTST
    1. Contains a short tutorial on 2D-SCTST calculations, using the unimolecular 
    carbene rearrangement reaction as an example. The main results of [this paper](https://pubs.acs.org/doi/abs/10.1021/acs.jpca.9b01987 "Paper Link") paper are reproduced by the thesis_results.py 
    script, to illustrate their application.
* Tunnelling Splittings
    1. Contains a short tutorial on SPT calculations, using the Malonaldehyde isomerisation as an example.
     The main results of Chapter 9 are reproduced by the thesis_results.py script, to illustrate its application.

sctst.py is the main module, that contains all the important classes you need. Your script (to do rate calculations) 
only needs to directly interact with this module and not the other modules in the pySCTST code.

 


### Suggested Citation ###
@misc{burd_pysctst_2019,
author = {Burd, T.A.H.},
title = {py{SCTST}}, 
year = {2019},
publisher = {BitBucket}, 
journal = {BitBucket repository}, 
howpublished = {\url{https://bitbucket.com/timothyburd/pysctst}} }


### License ###
This project is fully open source, and lisenced under the MIT licence, as described in LICENSE.txt


### Who do I talk to? ###

Timothy Burd (timothy.burd@chem.ox.ac.uk / timothyahburd@googlemail.com) would be excited to hear about anyone using this code for whatever purpose, and is happy to answer any and all questions.


### Acknowldegements ###
The majority of the code was written by Timothy Burd whilst a DPhil student at the University of Oxford, under the supervision of Prof. Sir David Clary.
This work was thus supported by the EPSRC Centre for Doctoral Training in Theory and Modelling in Chemical Sciences (Project Grant No. EP/L015722/1).

Minor contributions to the code were from Tom Young and Karl-Michael Zeims.

Parts of the code are based on Mathematica and C code written by Xiao Shan and Samuel Greene.
