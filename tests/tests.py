import read_gaussian as rg
import extract_anharm_matrix as ex
import partiton_function as pf
import unittest
import physical_constants as pc
import numpy as np
import rotation_matrix as rm
import md_sctst as md
import sctst
import read_log as rl


class TestReadGaussian(unittest.TestCase):
    """
    Test functions in the read_gaussian.py module
    """

    def test_find_basis(self):
        basis = rg.find_basis_set('checkpoint_files/ts_freq.fchk')
        self.assertEqual(basis, 'Aug-CC-pVTZ')

    def test_std_numeric(self):
        inputs = ['1.039302927293E+07', '-1.039302927293E+07', '-0.461848D+02', '0.714188D+01', '-0.413628D+01', '0.714188D+01', '-0.771180D+00']

        answers = [10393029.27293, -10393029.27293, -46.1848, 7.14188, -4.13628, 7.14188, -0.77118]
        for i, a in zip(inputs, answers):
            self.assertAlmostEqual(rg.std_to_numeric(i), a, 5)

    def test_find_energy(self):
        energy = rg.find_energy('checkpoint_files/ts_freq.fchk')
        self.assertAlmostEqual(energy, -228.5054929124509, 6)

    def test_find_mass(self):
        mass = rg.find_mass('checkpoint_files/ts_freq.fchk')
        answer = [12.0, 1.00782504, 15.994914600000001, 15.994914600000001, 12.0, 1.00782504, 1.00782504, 1.00782504]
        for m, answer in zip(mass, answer):
            self.assertAlmostEqual(m, answer)

    def test_freq_corect_ordering(self):
        sorted = rg.freq_corect_ordering_in_log('FD_files/ch3_vpt2.log')
        self.assertFalse(sorted)

        sorted = rg.freq_corect_ordering_in_log('FD_files/water_vpt2.log')
        self.assertTrue(sorted)

    def test_repair_water(self):

        x = ex.extract_matrix('FD_files/water_vpt2.log', 3)
        answer = np.asarray([[-46.1848, -155.058, -20.1902],
                             [-155.058, -40.8484, -16.9253],
                             [-20.1902, -16.9253, -15.8567]])
        answer /= pc.hartree_to_cm
        x_fixed = rg.repair_x_matrix(x, 'FD_files/water_vpt2.log')  # sholdnt change anything as freqs in correct order
        self.assertTrue(np.array_equal(x_fixed, answer))

    def test_repair_c2h7(self):

        import extract_anharm_matrix as ex
        x = ex.extract_matrix('FD_files/c2h7_vpt2.log', 21)
        x_fixed = rg.repair_x_matrix(x, 'FD_files/c2h7_vpt2.log')

        # looking at the log file, i can pick out a few modes, and where they should end up
        # Then test if all combinations of these end up in the right place

        old_positions = [13, 20, 0, 10]
        new_positions = [-1, 19, 0, -6]

        for o1, n1 in zip(old_positions, new_positions):
            for o2, n2 in zip(old_positions, new_positions):
                self.assertEqual(x[o1, o2], x_fixed[n1, n2])

    def test_extract_gradient(self):

        grad = rg.extract_gradient_vector('checkpoint_files/m1_grad.fchk', 6)

        answer = [-2.31187845 * 10 ** -3, -1.72073426 * 10 ** -3, -2.81445093 * 10 ** -3, -2.93994722 * 10 ** -3, 1.02706265 * 10 ** -2,
                  -3.58549611 * 10 ** -4, -2.76886802 * 10 ** -3, -5.16674180 * 10 ** -3, 8.93855444 * 10 ** -3, 9.92564753 * 10 ** -3,
                  -1.96536657 * 10 ** -3, -3.44644113 * 10 ** -3, 2.10319907 * 10 ** -3, 1.56524748 * 10 ** -3, 2.56029166 * 10 ** -3,
                  -4.00815292 * 10 ** -3, -2.98303136 * 10 ** -3, -4.87940443 * 10 ** -3]

        for i, j in zip(grad, answer):
            self.assertEqual(i, j)

    def test_extract_forces(self):

        grad = rg.find_forces_vector('checkpoint_files/p3_grad.log', 8)

        answer = [-0.000893324, -0.006640329, 0.004788381,
                  -0.001068541, -0.000423133, 0.001018558,
                  -0.000450134, 0.006780081, -0.007780471,
                  -0.002077967, -0.012036093, 0.008096532,
                  -0.000124679, -0.004566729, -0.010915893,
                  0.005577507, 0.016332574, 0.002684367,
                  0.000738220, -0.000448613, -0.001464025,
                  -0.001701084, 0.001002243, 0.003572551
                  ]

        for i, j in zip(grad, answer):
            self.assertEqual(i, j)


class TestExtractAnharmMatrix(unittest.TestCase):
    """
    Tests for extract_anham_matrix.py
    """

    def test_convert_to_float(self):
        self.assertAlmostEqual(ex.convert_to_float('-0.461848D+02'), -46.1848, places=4)
        self.assertAlmostEqual(ex.convert_to_float('0.714188D+01'), 7.14188, places=4)
        self.assertAlmostEqual(ex.convert_to_float('-0.413628D+01'), -4.13628, places=4)
        self.assertAlmostEqual(ex.convert_to_float('0.714188D+01'), 7.14188, places=4)
        self.assertAlmostEqual(ex.convert_to_float('-0.771180D+00'), -0.77118, places=4)

    def test_extract_matrix_water(self):

        x = ex.extract_matrix('FD_files/water_vpt2.log', 3)

        answer = np.asarray([[-46.1848, -155.058, -20.1902],            # Read directly from Log file
                             [-155.058, -40.8484, -16.9253],
                             [-20.1902, -16.9253, -15.8567]])

        answer /= pc.hartree_to_cm

        pass_test_2 = np.array_equal(x, answer)

        self.assertTrue(np.array_equal(x, answer))

    def test_extract_matrix_c2h7(self):
        x = ex.extract_matrix('FD_files/c2h7_vpt2.log', 21)

        # Read correct answer directly from Log file
        answer = np.asarray([[-203.465, 3.5707699999999996, 3.52223, 3.4192199999999997, 3.52658, -7.711449999999999, -1.81276, -9.28324, 31.8647, -11.3629, -32.6525, 13.3069, -19.2027, -4.29328, 3.5796800000000006, 3.5193199999999996, -7.689510000000001, -1.81701, -9.51927, -32.6721, -19.4517], [3.5707699999999996, -15.361400000000001, -42.543, -34.6008, -34.6058, -6.3826, -6.65675, -3.55656, -4.7549, -2.1164, -6.58465, 0.609036, -0.547389, 1.30834, -18.7445, -18.8634, -6.95822, -7.808609999999999, -4.65487, -4.30587, -7.74636], [3.52223, -42.543, -19.3609, -34.6446, -34.6576, -3.42946, -0.567001, -3.22303, -5.05269, -1.66, -0.387451, 0.586278, -11.4336, 0.171372, -36.563, -39.1904, -8.92947, -14.837600000000002, -3.9071599999999997, -6.60169, 7.07274], [3.4192199999999997, -34.6008, -34.6446, -8.65111, -34.5308, -0.9137399999999999, -0.9950729999999999, -1.6704, -2.19009, 1.61006, -2.29143, 0.336331, -1.077, 0.541757, -34.6007, -34.6446, -0.9137520000000001, -0.9966770000000001, -1.67035, -0.902096, -1.07695], [3.52658, -34.6058, -34.6576, -34.5308, -8.65315, -0.812412, -0.738303, -1.53772, -1.44627, 0.9801119999999999, -2.39859, 0.344046, -0.16759, -45.3462, -34.6058, -34.6576, -0.843788, -0.738308, -1.4597, -2.39869, 1.25761], [-7.711449999999999, -6.3826, -3.42946, -0.9137399999999999, -0.812412, -2.28799, -10.8879, -1.96858, -3.12586, 28.8222, 4.82365, -1.32113, 5.14868, -4.98815, -9.45213, -7.3585, -1.33456, -4.47131, 0.114143, 9.90903, 38.3452], [-1.81276, -6.65675, -0.567001, -0.9950729999999999, -0.738303, -10.8879, -3.74666, -3.06879, -5.98322, -1.90724, 18.2037, -0.055272800000000004, -2.38896, 4.44275, -9.44386, -10.4023, -7.019759999999999, -4.04859, -0.0694, 19.984, 2.60306], [-9.28324, -3.55656, -3.22303, -1.6704, -1.53772, -1.96858, -3.06879, -0.721931, -7.1650100000000005, -2.23517, 6.64348, -2.42073, 14.1652, 2.2061, -3.76541, -2.99802, -1.5668, -2.7347, 0.483499, -1.67925, 0.061428800000000006], [31.8647, -4.7549, -5.05269, -2.19009, -1.44627, -3.12586, -5.98322, -7.1650100000000005, -0.485802, -9.39312, 4.54792, 6.63843, 4.24259, -1.68341, -4.52736, -5.0576, -3.4807999999999995, -5.99024, -7.14517, 4.51365, 4.3455], [-11.3629, -2.1164, -1.66, 1.61006, 0.9801119999999999, 28.8222, -1.90724, -2.23517, -9.39312, -5.81507, 4.57683, 1.53638, -28.605, 0.785715, -2.11595, -1.50298, 28.8189, -2.34802, -2.23482, 4.62724, -28.6079], [-32.6525, -6.58465, -0.387451, -2.29143, -2.39859, 4.82365, 18.2037, 6.64348, 4.54792, 4.57683, -2.67268, -0.18252, 4.96073, -3.06188, -2.9804, -3.62744, 5.94447, 17.1497, -1.76761, -14.4529, 38.4319], [13.3069, 0.609036, 0.586278, 0.336331, 0.344046, -1.32113, -0.055272800000000004, -2.42073, 6.63843, 1.53638, -0.18252, -2.02169, -7.75998, 0.569252, 0.609025, 0.587109, -1.32111, -0.0594595, -2.42071, -0.115179, -31.664500000000004], [-19.2027, -0.547389, -11.4336, -1.077, -0.16759, 5.14868, -2.38896, 14.1652, 4.24259, -28.605, 4.96073, -7.75998, 3.6288300000000002, 9.92456, -2.20163, -1.34975, 34.9659, -1.33766, 0.162766, 4.01935, 1.11647], [-4.29328, 1.30834, 0.171372, 0.541757, -45.3462, -4.98815, 4.44275, 2.2061, -1.68341, 0.785715, -3.06188, 0.569252, 9.92456, -10.7436, 0.994935, 0.171451, -3.78951, 4.4428, 1.88679, -3.05889, 9.11896], [3.5796800000000006, -18.7445, -36.563, -34.6007, -34.6058, -9.45213, -9.44386, -3.76541, -4.52736, -2.11595, -2.9804, 0.609025, -2.20163, 0.994935, -15.3628, -61.0212, -4.40073, -7.320450000000001, -4.45218, -4.16932, -1.31853], [3.5193199999999996, -18.8634, -39.1904, -34.6446, -34.6576, -7.3585, -10.4023, -2.99802, -5.0576, -1.50298, -3.62744, 0.587109, -1.34975, 0.171451, -61.0212, -15.423300000000001, -4.96087, -5.15689, -4.0371, -3.26487, -3.11905], [-7.689510000000001, -6.95822, -8.92947, -0.9137520000000001, -0.843788, -1.33456, -7.019759999999999, -1.5668, -3.4807999999999995, 28.8189, 5.94447, -1.32111, 34.9659, -3.78951, -4.40073, -4.96087, -2.28811, -11.2437, -1.60182, 6.80262, 3.19816], [-1.81701, -7.808609999999999, -14.837600000000002, -0.9966770000000001, -0.738308, -4.47131, -4.04859, -2.7347, -5.99024, -2.34802, 17.1497, -0.0594595, -1.33766, 4.4428, -7.320450000000001, -5.15689, -11.2437, -3.99535, -3.34228, -0.83235, -3.2092], [-9.51927, -4.65487, -3.9071599999999997, -1.67035, -1.4597, 0.114143, -0.0694, 0.483499, -7.14517, -2.23482, -1.76761, -2.42071, 0.162766, 1.88679, -4.45218, -4.0371, -1.60182, -3.34228, -0.721926, 7.00711, 14.063400000000001], [-32.6721, -4.30587, -6.60169, -0.902096, -2.39869, 9.90903, 19.984, -1.67925, 4.51365, 4.62724, -14.4529, -0.115179, 4.01935, -3.05889, -4.16932, -3.26487, 6.80262, -0.83235, 7.00711, 2.05678, 4.98275], [-19.4517, -7.74636, 7.07274, -1.07695, 1.25761, 38.3452, 2.60306, 0.061428800000000006, 4.3455, -28.6079, 38.4319, -31.664500000000004, 1.11647, 9.11896, -1.31853, -3.11905, 3.19816, -3.2092, 14.063400000000001, 4.98275, 9.60563]])
        answer = answer / pc.hartree_to_cm

        self.assertTrue(np.array_equal(x, answer))


class TestReadLog(unittest.TestCase):
    """
    Tests for read_log.py
    """

    def test_std_to_numeric(self):
        self.assertAlmostEqual(rl.std_to_numeric('1.039302927293E+07'), 10393029.27293, places = 6)
        self.assertAlmostEqual(rl.std_to_numeric('2.039302927293E+00'), 2.039302927293, places = 6)
        self.assertAlmostEqual(rl.std_to_numeric('0.039302927293E+07'), 393029.27293, places = 6)
        self.assertAlmostEqual(rl.std_to_numeric('4.039302927293E-03'), 0.004039302927293, places = 6)
        self.assertAlmostEqual(rl.std_to_numeric('-5.039302927293E+07'), -50393029.27293, places = 6)

    def test_get_ir_coords(self):

        coords = rl.get_ir_coords('FD_files/c2h7_vpt2.log', 1, 6)
        known_coords = [0.16406999999999999, -0.17916299999999999, -0.097982]
        self.assertTrue(np.array_equal(coords, known_coords))

        coords = rl.get_ir_coords('FD_files/c2h7_vpt2.log', 2, 6)
        self.assertEqual(-1, coords)

        coords = rl.get_ir_coords('FD_files/ch4_vpt2.log', 1, 6)
        known_coords = [ 2.981928,  0.933735,  0]
        self.assertTrue(np.array_equal(coords, known_coords))

        coords = rl.get_ir_coords('FD_files/ch4_vpt2.log', 2, 6)
        self.assertEqual(-1, coords)

    def test_get_bond_length(self):

        bond = rl.get_bond_length('FD_files/c2h7_vpt2.log', 1, 6, 2, 1)
        bond_length_from_gaussview = 3.9140551642165189
        self.assertEqual(bond, bond_length_from_gaussview)


        bond = rl.get_bond_length('FD_files/c2h7_vpt2.log', 1, 3, 6, 1)
        bond_length_from_gaussview = 1.0830061242827762
        self.assertEqual(bond, bond_length_from_gaussview)


        bond = rl.get_bond_length('FD_files/ch4_vpt2.log', 1, 2, 6, 1)
        bond_length_from_gaussview = 1.0853742063758471
        self.assertEqual(bond, bond_length_from_gaussview)


        bond = rl.get_bond_length('FD_files/ch4_vpt2.log', 1, 3, 6, 1)
        bond_length_from_gaussview = 1.0853741068111034
        self.assertEqual(bond, bond_length_from_gaussview)


class TestPartitionFunction(unittest.TestCase):

    def test_q_translational_a(self):
        p = pf.q_translational([0], [1])
        self.assertEqual(p, [0.0])


class TestRotationMatrix(unittest.TestCase):

    def test_rotations(self):
        x = np.asarray([1, 5, 2, 5, 8, 4, 2, 3, -2])
        y = np.asarray([4, 7, 2, 4, 5, -1, 5, 6, 6])

        x = x / np.linalg.norm(x)
        y = y / np.linalg.norm(y)

        R = rm.find_rot_matrix(x, y)

        test_y = R @ x

        for element1, element2, in zip(test_y, y):
            self.assertAlmostEqual(element1, element2, 10)

    def test_self_rotations(self):
        x = np.asarray([1, 5, 2, 5, 8, 4, 2, 3, -2])
        y = np.asarray([1, 5, 2, 5, 8, 4, 2, 3, -2])

        x = x / np.linalg.norm(x)
        y = y / np.linalg.norm(y)

        R = rm.find_rot_matrix(x, y)
        test_y = R @ x

        for element1, element2, in zip(test_y, y):
            self.assertAlmostEqual(element1, element2, 10)

    def test_rotations_random(self):

        for length in range(3, 50):
            for test in range(3):
                x = np.random.rand(length)
                y = np.random.rand(length)

                x = x / np.linalg.norm(x)
                y = y / np.linalg.norm(y)

                R = rm.find_rot_matrix(x, y)

                test_y = R @ x

                for element1, element2, in zip(test_y, y):
                    self.assertAlmostEqual(element1, element2, 10)


class TestCH3CH4(unittest.TestCase):

    def setUp(self):
        sctst.checkpoint_root = '../projects/1D_SCTST/checkpoint_files/'
        temperatures = np.linspace(250, 2000, num=3)
        ch4 = sctst.Chemical("ch4", 5, temperatures, sigma=12)
        ch3 = sctst.Chemical("ch3", 4, temperatures, sigma=6)
        ts_1_harmonic = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="Harmonic")
        ts_1_tanh = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="tanh", hr_groups=[[0, 1, 2, 3], [4, 5, 6, 7], [0, 4]])
        ts_1_eigen = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="eigenvalue_summation",
                                           eigen_list=[2.4169, 5.20564, 6.86944])
        self.R1_harmonic = sctst.Reaction(ch3, ch4, ts_1_harmonic, ch3, ch4, temperatures, label="Harmonic")
        self.R1_tanh = sctst.Reaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, label="tanh")
        self.R1_eigen = sctst.Reaction(ch3, ch4, ts_1_eigen, ch3, ch4, temperatures, label="Eigenvalue Summation")


    def test_tst_rate(self):
        expected_rate = [2.3185105875698448e-28,   3.2026376299673332e-15,   4.9610784421916212e-13]

        calculated_rate = self.R1_harmonic.rate_tst

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e, c, 10)

    def test_tst_rate_tanh(self):
        expected_rate = [1.5179134884120891e-28,   1.1318322798153234e-15,   1.3401089813366586e-13]
        calculated_rate = self.R1_tanh.rate_tst

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e, c, 10)


    def test_tst_rate_eigen(self):
        expected_rate = [1.5437625626861419e-28,   1.1521936204536552e-15,  1.3455615734919504e-13]
        calculated_rate = self.R1_eigen.rate_tst

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e, c, 10)

    def test_1d_sctst_rate(self):
        expected_rate = [6.1917228697275087e-25,   4.4171720439346114e-15,   5.6224538783433275e-13]
        calculated_rate = self.R1_harmonic.sctstRate

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e, c, 10)


    def test_1d_sctst_rate_tanh(self):
        expected_rate = [4.053679854151567e-25,   1.5610563799170807e-15,   1.5187627100269089e-13]
        calculated_rate = self.R1_tanh.sctstRate

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e, c, 10)

    def test_1d_sctst_rate_eigen(self):
        expected_rate = [4.1227113717137521e-25,   1.5891393399757202e-15,   1.5249422027053197e-13]
        calculated_rate = self.R1_eigen.sctstRate

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e, c, 10)

    def test_barrier(self):
        self.assertAlmostEqual(self.R1_harmonic.adiabatic_barrier_kj_per_mol, 77.06606899554555, 10)

    def test_barrier_tanh(self):
        self.assertAlmostEqual(self.R1_tanh.adiabatic_barrier_kj_per_mol, 77.06606899554555, 10)

    def test_barrier_eigen(self):
        self.assertAlmostEqual(self.R1_eigen.adiabatic_barrier_kj_per_mol, 77.06606899554555, 10)


class TestCriegee(unittest.TestCase):

    def setUp(self):
        sctst.checkpoint_root = '../projects/FD_SCTST/checkpoint_files/'

        temperatures = np.linspace(250, 2000, num=3)
        r = sctst.Chemical("r", 8, temperatures,  vpt2_log=True, run_dos=False)
        ts = sctst.TransitionState("ts", 8, temperatures, rich_step_size=0.05, vpt2_log=True,
                                   run_dos=False)
        ts_water = sctst.TransitionState("ts_water", 11, temperatures, rich_step_size=0.05,
                                         vpt2_log=True, run_dos=False)
        water = sctst.Chemical("water", 3, temperatures, sigma=2, vpt2_log=True, run_dos=False)
        p = sctst.Chemical("p", 8, temperatures, sigma=1, vpt2_log=True, run_dos=False)

        self.reaction = sctst.UnimolecularReaction(r, ts, p, temperatures, label="criegee_1D")
        self.reaction_FD = sctst.FD_Unimolecular_Reaction(r, ts, p, temperatures, label="criegee_FD", integration=False)
        self.catalysed_reaction_FD = sctst.FD_Bimolecular_Reaction(r, water, ts_water, p, water, temperatures, label='FD_water_mp2',
                                                    integration=False)

    def test_tst_rate(self):
        expected_rate = [0.018443110838264377,   2790180747.1615438,   78986492947.2146]
        calculated_rate = self.reaction.rate_tst

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e/c, 1, 10)


    def test_tst_rate_FD(self):
        expected_rate = [0.018443110838264377,   2790180747.1615438,   78986492947.2146]
        calculated_rate = self.reaction_FD.rate_tst

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e/c, 1, 10)


    def test_tst_rate_catalysed_FD(self):
        expected_rate = [2.3502305349671474e-21,   6.6606629281112257e-16,   1.1190439806075187e-14]
        calculated_rate = self.catalysed_reaction_FD.rate_tst

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e/c, 1, 10)


    def test_sctst_rate(self):
        expected_rate = [5.5628330779646582,   3326132919.9711876,   83923561101.127045]
        calculated_rate = self.reaction.sctstRate

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e/c, 1, 6)

    def test_sctst_rate_FD(self):
        expected_rate = [8.2079382712120736,   2230961889.6515784,   59778694843.207932]

        self.assertCountEqual([round(i) for i in self.reaction_FD.rate_sctst] , [round(i) for i in expected_rate])

    def test_sctst_rate_catalysed_FD(self):
        expected_rate = [7.5424472760159606e-19,   7.1455432412318698e-16,   1.387867565146656e-14]
        self.assertCountEqual([round(i, 5) for i in self.catalysed_reaction_FD.rate_sctst] , [round(i, 5) for i in expected_rate])

    def test_barrier(self):
        self.assertAlmostEqual(self.reaction.adiabatic_barrier_kj_per_mol, 67.986553686723312, 10)

    def test_barrier_tanh(self):
        self.assertAlmostEqual(self.reaction_FD.adiabatic_barrier_kj_per_mol, 67.986553686723312, 10)

    def test_barrier_eigen(self):
        self.assertAlmostEqual(self.catalysed_reaction_FD.adiabatic_barrier_kj_per_mol, 32.721423904483572, 10)


class TestRDSCTST(unittest.TestCase):

    def setUp(self):
        sctst.checkpoint_root = '../projects/RD_SCTST/checkpoint_files/'
        rec_temps = np.linspace(0.5, 5, num=3)
        temperatures = 1000 / rec_temps

        ts = sctst.TransitionState("ts_me", 11, temperatures, sigma=1, rich_step_size=0.05, vpt2_log=True,
                                   run_dos=False)
        r = sctst.Chemical("r_me", 11, temperatures, sigma=1, vpt2_log=True, run_dos=False)
        p = sctst.Chemical("p_me", 11, temperatures, vpt2_log=True, run_dos=False)

        self.twod_rate = md.md_sctst([r, ts, p], [6], temperatures, plot_barriers=False, spectator_coupling=True,
                                 max_quanta=4, deep_tunneling=True, verbose=False)

        # Calculate three d rate using the next-most strongly coupled mode. Expect the effect to be very small
        self.threed_rate = md.md_sctst([r, ts, p], [6, 22], temperatures, plot_barriers=False, max_quanta=4, spectator_coupling=True, deep_tunneling=True)

    def test_2d_rate(self):
        expected_rate = [130023062466.21338,   11779.050677835507,   2.1238441740228171]
        calculated_rate = self.twod_rate

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e/c, 1, 6)


    def test_3d_rate(self):
        expected_rate = [104134304443.72284,   11372.713488108739,   1.9345836997484038]

        calculated_rate = self.threed_rate

        for e, c in zip(expected_rate, calculated_rate):
            self.assertAlmostEqual(e/c, 1, 6)



if __name__ == '__main__':
    unittest.main()

