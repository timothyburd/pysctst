"""
CHEMICAL PHYSICS LETTERS
Semiclassical transition state theory.
A new perspective
Rigoberto Hernandez and William H. Miller
29 October 1993

This paper describes a different way of doing SCTST, where you integrate over theta ratehr than E.

This module contains functions to do this.

Note this is entirely equivalent to the 'normal' SCTST method, just uses a different route to get to the answers

"""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.integrate import quad
from sympy import *

import physical_constants as pc


def WagBar(s, freq, xff, bar_height, rev_bar_height):
    """
    Calculates the three piece assymetric eckart potential
    THEN RETURNS THE SQUARE ROOT OF THE POTENTIAL TIMES SQRT 2 TIMES SQRT AMU
    THIS IS USED TO CAUCLATE THE INTEGRAL IN EQUATION 22 OF BARKER - "IMPROVED MULTIDIMENSIONAL SEMICALLSICAL TUNNELING"
    This allows for calculating the value of theta at which E = 0.
    This funciton is integrated over by thetamax()
    """

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5

    f = (Db / bar_height) ** 0.5
    ur = (((rho * rho + rho + 1) ** 0.5) - (rho - 1)) / max(3 * f, 3)
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (f - rho * ((1 + f) ** 2) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (1 - rho) * f * f * f * ur * ur * ur + 105 * f * f * f * f * ur * ur * ur * ur)

    alphar = alphab * f * Rr
    umr1 = (((1 - rho) * (1 - rho) * (1 - Rr * f) * (1 - Rr * f) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) ** 0.5 + (1 - rho) * (1 - Rr * f)) / (2 * (1 - Rr * f * f))
    umr2 = (-1) * umr1

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab

    dr = -ybr + math.log(((1 - f * umr) / (rho + f * umr))) / alphar

    ###################
    up = (((rho ** 2 + rho + 1) ** 0.5) + rho - 1) / max(3 * f, 3)
    Rp = (210 * rho * rho - 168 * (1 + f) * rho * (1 - rho) * up + 140 * (f - (1 + f) ** 2 * rho + f * rho ** 2) * up * up + 120 * f * (1 + f) * (1 - rho) * up ** 3 + 105 * f ** 2 * up ** 4) / (
                210 * rho ** 2 - 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho ** 2) * f ** 2 * up ** 2 + 240 * (1 - rho) * f ** 3 * up ** 3 + 105 * f ** 4 * up ** 4)

    alphap = alphab * f * Rp

    ump1 = (((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    ump2 = (-((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1

    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab

    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp

    # overflow_max = 1900
    # exponent = alphar * (s + dr)
    # if exponent < -overflow_max or exponent > overflow_max:
    #     Vr_s = 0
    # else:
    #     Vr_s = bar_height * ((1 - rho ** 2) * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) +
    #                          (1 + rho) ** 2 * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) ** 2)
    #
    # exponent = alphap * (s + dp)
    # if exponent < -overflow_max or exponent > overflow_max:
    #     Vp_s = 0
    # else:
    #     Vp_s = bar_height * ((1 - rho ** 2) * math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) + (1 + rho) ** 2 *
    #                          math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) ** 2)
    #
    # exponent = alphab * s
    # if exponent < -overflow_max or exponent > overflow_max:
    #     Vb_s = 0
    # else:
    #     Vb_s = bar_height - Db + Db * ((1 - rho ** 2) * math.exp(alphab * s) / (1 + math.exp(alphab * s)) + (1 + rho) ** 2 *
    #                                    math.exp(alphab * s) / (1 + math.exp(alphab * s)) ** 2)
    #
    Vr_s = bar_height * ((1 - rho ** 2) * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) +
                         (1 + rho) ** 2 * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) ** 2)

    Vp_s = bar_height * ((1 - rho ** 2) * math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) + (1 + rho) ** 2 *
                         math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) ** 2)

    Vb_s = bar_height - Db + Db * ((1 - rho ** 2) * math.exp(alphab * s) / (1 + math.exp(alphab * s)) + (1 + rho) ** 2 *
                                   math.exp(alphab * s) / (1 + math.exp(alphab * s)) ** 2)

    if s <= ybr:
        if Vr_s > 0:
            return 2 ** 0.5 * Vr_s ** 0.5

        else:
            return 0
    if (s > ybr) and (s < ybp):
        if Vb_s > 0:
            return 2 ** 0.5 * Vb_s ** 0.5
        else:
            return 0
    if s >= ybp:
        if Vp_s > 0:
            return 2 ** 0.5 * Vp_s ** 0.5
        else:
            return 0
    else:
        return 0  # Probably lots of NANs here due to a sqrt of a negative being used or somethign
        # raise Exception("Strange error here!")


def plot_theta_of_E(freq, xff, bar_height, rev_bar_height):
    x = np.linspace(-100, 100, num=300)

    y = [WagBar(t, freq, xff, bar_height, rev_bar_height) for t in x]

    plt.plot(x, y)
    plt.show()


def thetamax(freq, xff, bar_height, rev_bar_height):
    """
    Evaluate equation 22 in Barker ("Improved multidimensional semicallical tunneling") at E = 0. This then feeds into equation 27, which is the energy as a function of theta

    This effectively calculates the 'upper bound' to the theta integral (if using miller VPT2 SCTST)
    """

    # print(freq*pc.hartree_to_cm, xff, bar_height/pc.hartree_to_joule, rev_bar_height/pc.hartree_to_joule)
    # plt.plot(np.linspace(-100, 100, num=1500), [WagBar(i, freq, xff, bar_height, rev_bar_height) for i in np.linspace(-500, 500, num=1500)])
    # plt.show()

    I = quad(WagBar, -500, 40, args=(freq, xff, bar_height, rev_bar_height))
    # return 20
    return I[0]


def integral(imaginary_frequency, xff, T, barrier, deep_tunneling, rev_barrier=None, vpt4=False, Wff=None, Yfff=None):
    """
    Does the integral 
    
    ﻿\int_{- \infty}^{- theta_C} d \theta \frac{1}{2} \mathrm{sech}^2(\theta)  \exp \left[ - \beta \left(  i \hbar \omega_F  \frac{\theta}{\pi}  - \hbar x_{FF} \left( \frac{\theta}{\pi} \right)^2 \right) \right]

    """

    # First we want to calcualte the upper bound to the integration

    if deep_tunneling:
        upper_bound = thetamax(imaginary_frequency, xff, barrier, rev_barrier)  # Must calculate theta max by integrating over the potential at E=0

        I = quad(integrand_deep_tunneling, -20, upper_bound, args=(imaginary_frequency, xff, T, upper_bound, barrier))

        # 'surface terms' frm the integration by parts:
        f = pc.boltzman * T / (1 + math.exp(2 * upper_bound))


    elif vpt4:
        print("Because im here", Wff, xff, Yfff, barrier)
        upper_bound = vpt4_max_theta(Wff, xff, Yfff, barrier)  # Must find theta max by finding the minimum of E(theta)
        I = quad(integrand_vpt4, -20, upper_bound, args=(Wff, xff, Yfff, T))

        # 'surface terms' frm the integration by parts:
        f = pc.boltzman * T / (1 + math.exp(2 * upper_bound))


    else:  # VPT2 SCTST
        # Must find theta max by solving a quadratic
        to_be_squareooted = imaginary_frequency ** 2 + 4 * xff * barrier
        if to_be_squareooted < 0:
            upper_bound = -math.pi * imaginary_frequency / 2 / xff
        else:
            upper_bound = math.pi * ((imaginary_frequency - math.sqrt(imaginary_frequency ** 2 + 4 * xff * barrier)) / (-2 * xff))

        # 'surface terms' frm the integration by parts:
        f = pc.boltzman * T / (1 + math.exp(2 * upper_bound))

        I = quad(integrand_vpt2, -20, upper_bound, args=(imaginary_frequency, xff, T))

    return I[0] + f


def integrand_vpt2(theta, imaginary_frequency, xff, T):
    tmp = (- imaginary_frequency * theta / math.pi) - xff * (theta / math.pi) ** 2
    exponent = (- pc.hartree_to_joule * tmp / (pc.boltzman * T))

    return 0.5 * (math.cosh(theta)) ** (-2) * math.exp(exponent)


def integrand_vpt4(theta, Wff, xff, Yfff, T):
    """
            tmp comes from
        Stanton's VPT4 paper    Equation 27

    """
    red_theta = theta / math.pi

    tmp = (- Wff * red_theta) - (xff * red_theta ** 2) - Yfff * red_theta ** 3

    exponent = (- pc.hartree_to_joule * tmp / (pc.boltzman * T))
    return 0.5 * (math.cosh(theta)) ** (-2) * math.exp(exponent)


def integrand_deep_tunneling(theta, imaginary_frequency, xff, T, theta_max, V1):
    """
    tmp comes from
    dx.doi.org/10.1021/jp409720s | J. Phys. Chem. A 2013, 117, 13089−13100
    Equation 27

    """
    red_theta = theta / math.pi

    red_theta_max = theta_max / math.pi

    tmp = (- imaginary_frequency * red_theta) - (xff * red_theta ** 2) \
          + (3 * imaginary_frequency * red_theta_max + 2 * xff * red_theta_max ** 2 - 4 * V1) * (theta / theta_max) ** 3 \
          - (2 * imaginary_frequency * red_theta_max + xff * red_theta_max ** 2 - 3 * V1) * (theta / theta_max) ** 4

    exponent = (- pc.hartree_to_joule * tmp / (pc.boltzman * T))
    return 0.5 * (math.cosh(theta)) ** (-2) * math.exp(exponent)


def E_of_theta_deep_tunneling(theta, imaginary_frequency, xff, T, theta_max, V1):
    """
    tmp comes from
    dx.doi.org/10.1021/jp409720s | J. Phys. Chem. A 2013, 117, 13089−13100
    Equation 27

    """
    red_theta = theta / math.pi

    red_theta_max = theta_max / math.pi

    tmp = (- imaginary_frequency * red_theta) - (xff * red_theta ** 2) \
          + (3 * imaginary_frequency * red_theta_max + 2 * xff * red_theta_max ** 2 - 4 * V1) * (theta / theta_max) ** 3 \
          - (2 * imaginary_frequency * red_theta_max + xff * red_theta_max ** 2 - 3 * V1) * (theta / theta_max) ** 4

    return tmp


def vpt4_max_theta(Wff, xff, Yfff, barrier):
    """ Calculate the maximum allowed value of theta within the VPT4 setup
    Do this by calcualteing E for a large range of theta, and find the place where there is a minimum"""

    theta = np.linspace(-20, 200, num=500)

    E = [barrier + (- Wff * (t / math.pi)) - (xff * (t / math.pi) ** 2) - Yfff * (t / math.pi) ** 3 for t in theta]

    max_i = len(E)
    for i in range(1, len(E) - 1):
        if E[i] < E[i - 1] and E[i] < E[i + 1]:
            max_i = i
    print(theta[max_i])
    return theta[max_i]


def plot_e_of_theta(imaginary_frequency, xff, bar_height, rev_bar_height, deep_tunneling, label, parabolic=False, vpt4=False, Wff=0.00851091210075, Yfff=-0.000253296199409):
    """
    NOTE the default values of Wff and Yfff are FOR CH3 + CH4 ONLY!!!!
    """
    theta = np.linspace(-100, 100, num=500)
    if vpt4:
        upper_bound = vpt4_max_theta(imaginary_frequency, xff, Yfff, bar_height)
        theta = np.linspace(-100, upper_bound, num=500)
        E = [bar_height + (- Wff * (t / math.pi)) - (xff * (t / math.pi) ** 2) - Yfff * (t / math.pi) ** 3
             for t in theta]

    elif deep_tunneling:

        theta_max = thetamax(imaginary_frequency, xff, bar_height, rev_bar_height)
        theta = np.linspace(-100, theta_max, num=500)

        red_theta_max = theta_max / math.pi

        E = [bar_height + (- imaginary_frequency * t / math.pi) - (xff * (t / math.pi) ** 2) \
             + (3 * imaginary_frequency * red_theta_max + 2 * xff * red_theta_max ** 2 - 4 * bar_height) * (t / theta_max) ** 3 \
             - (2 * imaginary_frequency * red_theta_max + xff * red_theta_max ** 2 - 3 * bar_height) * (t / theta_max) ** 4 \
             for t in theta]
    elif parabolic:
        upper_bound = bar_height * math.pi / imaginary_frequency
        theta = np.linspace(-100, upper_bound, num=500)

        E = [bar_height + (-imaginary_frequency * t / math.pi) for t in theta]
    else:
        to_be_squareooted = imaginary_frequency ** 2 + 4 * xff * bar_height
        if to_be_squareooted < 0:
            upper_bound = -math.pi * imaginary_frequency / 2 / xff
        else:
            upper_bound = math.pi * ((imaginary_frequency - math.sqrt(imaginary_frequency ** 2 + 4 * xff * bar_height)) / (-2 * xff))
        theta = np.linspace(-100, upper_bound, num=500)

        E = [bar_height + (-imaginary_frequency * t / math.pi) - xff * (t / math.pi) ** 2 for t in theta]

    plt.plot(theta, E, label=label)
    # plt.show()


def plot_p_of_e(imaginary_frequency, xff, bar_height, rev_bar_height, deep_tunneling, label, parabolic=False, vpt4=False):
    theta = np.linspace(0, 50, num=500)
    if vpt4:
        Wff = 0.00851091210075
        Yfff = -0.000253296199409  # THIS IS FOR CH3 + CH4 ONLY!!!!
        upper_bound = vpt4_max_theta(imaginary_frequency, xff, Yfff, bar_height)
        theta = np.linspace(-0, upper_bound, num=500)
        # IS THIS UPPER BOUND CALUCLATED CORRECTLY??

        E = [bar_height + (- Wff * (t / math.pi)) - (xff * (t / math.pi) ** 2) - Yfff * (t / math.pi) ** 3
             for t in theta]

    elif deep_tunneling:

        theta_max = thetamax(imaginary_frequency, xff, bar_height, rev_bar_height)
        theta = np.linspace(-0, theta_max, num=500)

        red_theta_max = theta_max / math.pi

        E = [bar_height + (- imaginary_frequency * t / math.pi) - (xff * (t / math.pi) ** 2) \
             + (3 * imaginary_frequency * red_theta_max + 2 * xff * red_theta_max ** 2 - 4 * bar_height) * (t / theta_max) ** 3 \
             - (2 * imaginary_frequency * red_theta_max + xff * red_theta_max ** 2 - 3 * bar_height) * (t / theta_max) ** 4 \
             for t in theta]
    elif parabolic:
        upper_bound = bar_height * math.pi / imaginary_frequency
        theta = np.linspace(-0, upper_bound, num=500)

        E = [bar_height + (-imaginary_frequency * t / math.pi) for t in theta]
    else:
        to_be_squareooted = imaginary_frequency ** 2 + 4 * xff * bar_height
        if to_be_squareooted < 0:
            upper_bound = -math.pi * imaginary_frequency / 2 / xff
        else:
            upper_bound = math.pi * ((imaginary_frequency - math.sqrt(imaginary_frequency ** 2 + 4 * xff * bar_height)) / (-2 * xff))
        theta = np.linspace(-0, upper_bound, num=500)

        E = [bar_height + (-imaginary_frequency * t / math.pi) - xff * (t / math.pi) ** 2 for t in theta]

    # plt.plot(E, theta, label=label)

    plt.plot(E, [1 / (1 + math.exp(2 * i)) for i in theta], label=label)
    # plt.show()


#################################################################
######## Full dimension Dimesnison new perspective functions
###################################################################

def plot_q_star_vs_v(filename):
    df = pd.read_csv(filename, sep=',', skiprows=1)

    temps = list(df)[5:]
    n_temps = len(temps)

    df.plot(x='Energy', y=[temps[0], temps[1], temps[2]])
    plt.xlabel("V")
    plt.ylabel("Q*(T, V)")
    plt.tight_layout()
    plt.savefig("plots/V_vs_Q.pdf")
    plt.show()


def plot_Q_star_vs_T(filename):
    df = pd.read_csv(filename, sep=',', skiprows=1)

    dead_columns = 4

    t_list = []
    q_list = []
    log_q_list = []
    for column in df.columns[dead_columns:]:

        t_list.append(float(column))
        sum = df[column].sum()
        q_list.append(sum)
        if sum > 0:
            log_q_list.append(math.log(sum))
        else:
            log_q_list.append(0)

    plt.plot(t_list, log_q_list)
    plt.xlabel("Temperature / K")
    plt.ylabel("Q*(T)")
    plt.show()


def calc_q_star(filename):
    df = pd.read_csv(filename, sep=',', skiprows=1)

    dead_columns = 4

    q_list = []
    for column in df.columns[dead_columns:]:
        sum = df[column].sum()
        q_list.append(sum)
    return q_list


if __name__ == '__main__':

    print(integrand_deep_tunneling(5, 0.008884, -6 * 10 ** (-5), 500, 0.0282))
    print(integrand_vpt2(5, 0.008884, -6 * 10 ** (-5), 500))

    i = []
    i_deep = []
    for t in np.linspace(-5, 30, num=200):
        i.append(integrand_vpt2(t, 0.008884, -6 * 10 ** (-5), 500))
        i_deep.append(integrand_deep_tunneling(t, 0.008884, -6 * 10 ** (-5), 500, 0.0282))

    plt.plot(np.linspace(-5, 30, num=200), i)
    plt.plot(np.linspace(-5, 30, num=200), i_deep)
    plt.show()
    pass
