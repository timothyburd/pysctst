# Very janky and slow code to fit symmetric and asymmetric eckart potentials to values along an IRC

import matplotlib.pyplot as plt
import numpy as np
import scipy.optimize as opt


def aym_eckart_function(x, params):
    v1, v2, a, c = params
    return ((v1 + v2 / 2.0) / np.power(np.cosh(x / a), 2)) + (v2 / (1.0 + np.exp(c * x)))


def eckart_function(x, params):
    v1, a, shift = params
    return v1 / np.power(np.cosh(x / a), 2) + shift


def sqdiff_obs_model(params):
    sq_diff = 0.0
    for i in range(len(x_vals)):
        sq_diff += (y_vals[i] - aym_eckart_function(x_vals[i], params)) ** 2

    return sq_diff


def weighted_obs_model(params):
    cost = 0.0
    max_y = max(y_vals)

    for i in range(len(x_vals)):
        obs_y = y_vals[i]
        model_y = eckart_function(x_vals[i], params)

        if obs_y == max_y or x_vals[i] == np.min(x_vals):
            # Weight the cost function to reproduce the forward barrier height
            cost += float(1E3) * (obs_y - model_y) ** 2
        else:
            cost += (obs_y - model_y) ** 2

    return cost


def opt_eckart(params):
    result = opt.basinhopping(weighted_obs_model, x0=params)
    predicted_y = [eckart_function(x, result.x) for x in x_vals]

    plt.scatter(x_vals, y_vals)
    plt.scatter(x_vals, predicted_y)
    plt.show()

    print(weighted_obs_model(params))
    print(weighted_obs_model(result.x))
    print(result.x)


def opt_asym_eckart(params):
    result = opt.basinhopping(sqdiff_obs_model, x0=params)
    predicted_y = [aym_eckart_function(x, result.x) for x in x_vals]

    plt.scatter(x_vals, y_vals)
    plt.scatter(x_vals, predicted_y)
    plt.show()

    print(sqdiff_obs_model(params))
    print(sqdiff_obs_model(result.x))
    print(result.x)


if __name__ == '__main__':
    x_vals = [
        0,
        0.10628,
        0.21269,
        0.31929,
        0.42603,
        0.53271,
        0.63949,
        0.74625,
        0.85288,
        0.95956,
        1.06622,
        1.1729,
        1.27961,
        1.38631,
        1.49303,
        1.59978,
        1.70653,
        1.81332,
        1.92001,
        2.02679,
        2.13353,
        2.24029,
        2.34695,
        2.45344,
        2.55984,
        2.66629,
        2.77299,
        2.87977,
        2.98656,
        3.09332,
        3.19996,
        3.30626,
        3.41286,
        3.51934,
        - 0.10654,
        - 0.21322,
        - 0.31996,
        - 0.42669,
        - 0.53314,
        - 0.63869,
        - 0.74497,
        - 0.85174,
        - 0.95855,
        - 1.06536,
        - 1.17218,
        - 1.279,
        - 1.38583,
        - 1.49266,
        - 1.59949,
        - 1.70632,
        - 1.81314,
        - 1.91994,
        - 2.02665,
        - 2.13342,
        - 2.24012,
        - 2.34684,
        - 2.45359,
        - 2.56033,
        - 2.66711,
        - 2.77388,
        - 2.8806
    ]

    y_vals = [
        0.020918762,
        0.020450632,
        0.019398265,
        0.018082627,
        0.016635459,
        0.015167516,
        0.013720478,
        0.012327155,
        0.011019625,
        0.009795631,
        0.008667474,
        0.007616968,
        0.006675044,
        0.005825725,
        0.005051632,
        0.004369768,
        0.003760117,
        0.003215257,
        0.002747224,
        0.002330188,
        0.001968363,
        0.001646347,
        0.001377829,
        0.001135739,
        0.000930467,
        0.000752029,
        0.000598846,
        0.000467708,
        0.000350434,
        0.000254278,
        0.000172231,
        0.000104621,
        4.44977E-05,
        0.0,
        0.020239859,
        0.017914278,
        0.014158882,
        0.009938329,
        0.006275783,
        0.003640776,
        0.001567385,
        - 0.000194915,
        - 0.001730325,
        - 0.003068155,
        - 0.004231789,
        - 0.005241614,
        - 0.006115542,
        - 0.006869223,
        - 0.007516884,
        - 0.008071224,
        - 0.008543899,
        - 0.008945118,
        - 0.009283384,
        - 0.009569642,
        - 0.00980946,
        - 0.010013734,
        - 0.010181152,
        - 0.010322389,
        - 0.010440201,
        - 0.010536188,
        - 0.01061504
    ]

    asym_eckart_guess = np.array([0.031, -0.018, 0.85, 2.5])
    eckart_guess = np.array([0.032, 0.95, -0.018])

    opt_eckart(eckart_guess)
    opt_asym_eckart(asym_eckart_guess)
