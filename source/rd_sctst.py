"""

NOW DEPRECATED! USE MD_SCTST.PY
This is a module to do SCTST in reduced dimensions other than one.

It does this in a NOT VERY EFFICIENT WAY, which is to sum over 1D sctst rates, with an effective barrier and frequency
calcualted from the X matrix. 

"""

import itertools
import operator
from operator import add

import matplotlib.pyplot as plt
import mpl_toolkits.axisartist as AA
import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot
from numpy import linalg as LA

import partiton_function as pf
import physical_constants as pc
import read_gaussian as rg
import sctst


def md_sctst(chemical_list, transition_modes, temperatures, plot_barriers=True, spectator_coupling=False, max_quanta=10, plot_contributions=False):
    color_list = ['b', 'g', 'r', 'c', 'm', 'y', 'k'] * 9  # 9 is just some big number to make a long list of colors for plotting

    # ------------------ Determine if its a bimolecular or unimolecular reaction, and assigen reactant/ts/product labels ------------------------
    if len(chemical_list) == 3:
        unimolecular = True
        ts = chemical_list[1]
        r = chemical_list[0]
        p = chemical_list[2]
    elif len(chemical_list) == 5:
        unimolecular = False
        ts = chemical_list[2]
        r1 = chemical_list[0]
        r2 = chemical_list[1]
        p1 = chemical_list[3]
        p2 = chemical_list[4]
    else:
        raise ValueError("Chemical list shoud have three items for a unimolecular reaction, or 5 items for a \
         bimolecular reaction")

    active_modes = [0] + transition_modes  # include the reaction mode
    transition_mode_freqs = [ts.atomicfrequencies[-i] for i in transition_modes]
    print("The transition modes have frequiencies: ", [i * 1 for i in transition_mode_freqs])
    n_modes = len(active_modes)

    # produce the x sub matrix
    print(ts.xmat)
    x_sub_matrix = np.zeros((n_modes, n_modes))
    for counter, value in enumerate(active_modes):
        for counter2, value2 in enumerate(active_modes):
            x_sub_matrix[counter, counter2] = ts.xmat[value, value2]

    one_d_xff = ts.xff
    ts.xff = x_sub_matrix[0, 0]

    # -----------------------------------------------------------------------------------------------------------------#
    if not spectator_coupling:
        # if 1==1:
        # For consistancy, anything you treat as anharmonic in the TS, should also be treated as anharmonic in the reactant
        # This is kinda annoying as it would require you to project out DOFs etc eugh
        # An alternative is to ONLY include anharmonic terms along the reactin mode and thos that couple to the reaction mode
        # As these anharmonicicities can accuratly be approimated as zero in the reacant (usually)
        # To do this, set all elements of the anharmonic matrix that are not in row or column 0 to zero.
        # THIS BIT IS STRICTLY OPTIONAL AND IM NOT SURE IF THIS IS THE BEST WAY TO DO IT!
        # This also has the nice effect that all vibrational states are bound, so there is no need to check!
        for i in range(1, n_modes):
            for j in range(1, n_modes):
                x_sub_matrix[i, j] = 0
    # -----------------------------------------------------------------------------------------------------------------#

    print(x_sub_matrix)

    # remove the trantision modes from the freq list, so these arn't put in the partition funciton calculation
    original_frequencies = ts.frequencies
    spectator_list = ts.frequencies
    transition_modes_flipped = [len(spectator_list) - t for t in transition_modes]
    spectator_list = [i for j, i in enumerate(spectator_list) if j not in transition_modes_flipped]
    print("The original frequencies are ", [i * 1 for i in original_frequencies])
    print("The transition frequencies are ", [i * pc.hartree_to_cm for i in transition_mode_freqs])
    print("THe spectator frequencies are ", [i * 1 for i in spectator_list])

    # for mode in transition_modes:
    #     spectator_list.pop(- mode)      # ISSUE HERE
    ts.manual_set_frequencies(spectator_list)

    n_rate_contribution = [0] * max_quanta ** (len(transition_modes))
    ts_energy_0 = ts.energy
    print(ts_energy_0 * pc.hartree_to_cm)
    ts_freq_0 = ts.atomic_imaginary_frequency

    total_rate = [0] * len(temperatures)

    quanta = range(max_quanta)
    quanta_order = [0] * max_quanta ** (len(transition_modes))

    # --------- >2 d calculation -----------------------------------------------------------------------------------
    if len(transition_modes) > 1:
        counter = 0
        for quanta_list in itertools.product(quanta, repeat=len(transition_modes)):  # itterate over all combinations of numbers
            print(quanta_list)
            quanta_order.append(quanta_list)

            # ---------- Set barrier ---------------------------------------
            barrier = ts_energy_0
            vibrational_energy = 0
            for i in range(len(transition_modes)):
                for j in range(i, len(transition_modes)):
                    vibrational_energy += x_sub_matrix[1 + i, 1 + j] * (quanta_list[i] + 0.5) * (
                                quanta_list[j] + 0.5)  # eqtn 4, Burd- 'Catalysis and Tunneling in ... 'The '1 +' here is because the zero'th row is the reactin mode
                vibrational_energy += transition_mode_freqs[i] * (quanta_list[i] + 0.5)
            barrier += vibrational_energy
            ts.manual_set_energy(barrier)

            # ------------------ Set Frequency -----------------------------
            freq = ts_freq_0
            freq += sum([(quanta_list[k] + 0.5) * x_sub_matrix[0, k + 1] for k in range(len(quanta_list))])  # eqtn 6, Burd- 'Catalysis and Tunneling in ...The '1 +' here is because the zero'th row is the reactin mode
            ts.manual_set_im_freq(freq)
            print(freq)
            print("FREQ: ", freq * pc.hartree_to_cm)

            # ------ SHould really check if the state is bound or not here! ---------------------
            if unimolecular:
                reaction = sctst.UnimolecularReaction(r, ts, p, temperatures, label=str(quanta_list), deep_tunneling=False,
                                                      theta_integration=False)
            else:
                reaction = sctst.Reaction(r1, r2, ts, p1, p2, temperatures, label=str(quanta_list), deep_tunneling=False,
                                          theta_integration=False)

            new_total_rate = list(map(add, total_rate, reaction.sctstRate))
            n_rate_contribution[counter] = reaction.sctstRate
            quanta_order[counter] = quanta_list

            # ------------------------- Test for Convergence ------------------------------------------- #
            change = list(map(operator.sub, new_total_rate, total_rate))  # Change in rates
            fraction_change = np.asarray(list(map(operator.truediv, change, new_total_rate)))  # fractional change in rates
            converged = fraction_change < -0.00000000000000001  # returns array with True or Falses
            if False not in converged:  # i.e. if all rates have converged
                break

            total_rate = new_total_rate

            if plot_barriers:
                plt.plot(reaction.reaction_coordinates, reaction.wagner_potential, label=str(quanta_list), c=color_list[quanta_list[0]])

            counter += 1
    # --------- 2d calculation -----------------------------------------------------------------------------------
    if len(transition_modes) == 1:
        for n1 in range(max_quanta):  # itterate over all combinations of numbers
            quanta_list = [n1]
            print("(", n1, ")")

            barrier = ts_energy_0

            barrier += (n1 + 0.5) * transition_mode_freqs[0] + (n1 + 0.5) ** 2 * x_sub_matrix[1, 1]
            ts.manual_set_energy(barrier)

            freq = ts_freq_0
            freq += (n1 + 0.5) * x_sub_matrix[0, 1]
            ts.manual_set_im_freq(freq)

            # ------ SHould really check if the state is bound or not here! ---------------------
            if unimolecular:
                reaction = sctst.UnimolecularReaction(r, ts, p, temperatures, label=str(quanta_list), deep_tunneling=True,
                                                      theta_integration=False)
            else:
                reaction = sctst.Reaction(r1, r2, ts, p1, p2, temperatures, label=str(quanta_list), deep_tunneling=True,
                                          theta_integration=False)
            print(freq)
            n_rate_contribution[n1] = reaction.sctstRate
            quanta_order[n1] = quanta_list

            new_total_rate = list(map(add, total_rate, reaction.sctstRate))

            # ------------------------- Test for Convergence ------------------------------------------- #
            change = list(map(operator.sub, new_total_rate, total_rate))  # Change in rates
            fraction_change = np.asarray(list(map(operator.truediv, change, new_total_rate)))  # fractional change in rates
            converged = fraction_change < -0.0001  # returns array with True or Falses
            if False not in converged:  # i.e. if all rates have converged
                break

            total_rate = new_total_rate
            if plot_barriers:
                plt.plot(reaction.reaction_coordinates, reaction.wagner_potential, label=str(n1), c=color_list[n1])

    plt.rcParams.update({'font.size': 15})
    plt.rcParams.update({'lines.linewidth': 2})

    if plot_barriers:
        plt.legend()
        plt.savefig("plots/" + ts.name + "_" + str(n_modes) + "d_reaction_barriers.pdf")
        print("plots/" + ts.name + "_" + str(n_modes) + "d_reaction_barriers.pdf")
        plt.show()
    if plot_contributions:
        rec_temps = [1000 / i for i in temperatures]
        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        dashed = True
        for counter, i in enumerate(n_rate_contribution):
            relative_contribution = [j / k for j, k in zip(i, total_rate)]
            line, = ax.plot(rec_temps, relative_contribution, label="n = " + str(quanta_order[counter]))
        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
            ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
        elif max(rec_temps) < 1000 / 250:
            ax2.set_xticks([1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["300 K", "500 K", "1000 K"])

        else:
            pass

        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)

        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("Relative contribution to the rate")
        plt.legend()

        if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
            plt.xticks([1, 2, 3, 4, 5])
        elif max(rec_temps) < 1000 / 250:
            plt.xticks([1, 2, 3, 4])
        else:
            pass
        # plt.yticks([-28, -24, -20, -16, -12])
        plt.draw()
        plt.tight_layout()
        plt.legend()
        fig.savefig("plots/" + ts.name + "_" + str(n_modes) + "contributions.pdf")
        print("plots/" + ts.name + "_" + str(n_modes) + "contributions.pdf")
        plt.show()

    # return ts back to its original state
    ts.manual_set_energy(ts_energy_0)
    ts.manual_set_frequencies(original_frequencies)
    ts.manual_set_im_freq(ts_freq_0)
    ts.xff = one_d_xff

    return total_rate


def calc_derrivatives(name, n_atoms, rich_step_size, masses, active_modes):
    """
    
    :param name: Gives info on where the hessian files are
    :param n_atoms: Number of atoms
    :param rich_step_size: The step size used to displace the geometry from the stationary point
    :param masses: list of atom masses
    :param active_modes: list of the active modes, as normal modes where 0 is the highest freq mode
    :return: 
    """

    n_active = len(active_modes)
    dof = 3 * n_atoms
    thirds = np.zeros((dof, dof, dof))
    fourths = np.zeros((dof, dof))

    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)

    # mass weight
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)

    # diagonalise
    w, v = LA.eig(hess_zero_weighted)
    normal_vectors = v
    # Diagonalise
    hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal

    # check hess_zero_weighted_normal is diagnoal
    approx_zero = hess_zero_weighted_normal / hess_zero_weighted_normal[int(n_active / 2), int(n_active / 2)] > 0.001  # which elements are more than 1/1000 of the final diagonal element?
    # print(hess_zero_weighted_normal)
    for i in range(3 * n_atoms):
        for j in range(i + 1, 3 * n_atoms):
            if approx_zero[i, j]:
                plt.imshow(hess_zero_weighted_normal, cmap='hot', interpolation='nearest')
                plt.show()
                raise ValueError("The hessian matrix is not diagonal!", i, j, hess_zero_weighted_normal[i, j])

    imaginary_index = np.argmin(w)

    w = w[:-6]  # remove translational/rotational frequencies from list, assuming they are at the end
    freqs = [i ** 0.5 if i > 0 else (-i) ** 0.5 for i in w]

    freqs = [value / (pc.amu ** 0.5) for value in freqs]  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )
    print([f * pc.hartree_to_cm for f in freqs])
    # generate list of hessians
    plus_hessians = [0] * dof
    minus_hessians = [0] * dof

    for k in active_modes:
        hess_plus = rg.find_hessian("RD_files/" + name + "/"  "p1_" + str(k) + "_hess.fchk", n_atoms)
        hess_minus = rg.find_hessian("RD_files/" + name + "/"  "m1_" + str(k) + "_hess.fchk", n_atoms)

        # perform mass weighting
        hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, masses))
        hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

        #### perform diagonalisation
        hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
        hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

        plus_hessians[k] = hess_plus_weighted_normal
        minus_hessians[k] = hess_minus_weighted_normal

    kk = 0  # active_mode_index
    for k in active_modes:
        for i in range(dof):
            for j in range(dof):

                # if i or j are active modes, we can get better estimates for the mixed derrivatives by using info from both sets of displaced hessians.

                if i in active_modes and j in active_modes:
                    thirds[i, j, k] = ((minus_hessians[k][i, j] - plus_hessians[k][i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5) +
                                       (minus_hessians[i][k, j] - plus_hessians[i][k, j]) / (2 * rich_step_size) / (pc.amu ** 1.5) +
                                       (minus_hessians[j][k, i] - plus_hessians[j][k, i]) / (2 * rich_step_size) / (pc.amu ** 1.5)) * 0.33333333333
                elif i in active_modes:
                    thirds[i, j, k] = ((minus_hessians[k][i, j] - plus_hessians[k][i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5) +
                                       (minus_hessians[i][k, j] - plus_hessians[i][k, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)) * 0.5
                elif j in active_modes:
                    thirds[i, j, k] = ((minus_hessians[k][i, j] - plus_hessians[k][i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5) +
                                       (minus_hessians[j][k, i] - plus_hessians[j][k, i]) / (2 * rich_step_size) / (pc.amu ** 1.5)) * 0.5
                else:
                    thirds[i, j, k] = (minus_hessians[k][i, j] - plus_hessians[k][i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
                # Symmetry of thirds matrix
                thirds[j, k, i] = thirds[i, j, k]
                thirds[k, i, j] = thirds[i, j, k]
                thirds[i, k, j] = thirds[i, j, k]
                thirds[k, j, i] = thirds[i, j, k]
                thirds[j, i, k] = thirds[i, j, k]

            if i in active_modes:
                fourths[i, k] = 0.5 * (((plus_hessians[k][i, i] + minus_hessians[k][i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2)) +
                                       ((plus_hessians[i][k, k] + minus_hessians[i][k, k] - 2 * hess_zero_weighted_normal[k, k]) / (rich_step_size ** 2) / (pc.amu ** 2)))
            else:
                fourths[i, k] = ((plus_hessians[k][i, i] + minus_hessians[k][i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2))

            # symmetry of fourths matrix
            fourths[k, i] = fourths[i, k]
        kk += 1

    return [freqs, thirds, fourths, imaginary_index]


def calc_xnn(freqs, thirds, fourths, F, n):
    # F corresponds to the imaginary frequency
    # n is the diagonal matrix element we are calculating
    # NOTE incoming freq list MUST have the Fth frequency as pure real!

    freqs = list(freqs)
    freqs2 = [i ** 2 for i in freqs]
    freqs2[F] = freqs2[F] * (-1)

    ##################
    ### For comparison with log file
    #######################
    thirds_lot = thirds * pc.amu ** 1.5
    fourths_log = fourths * pc.amu ** 2

    first = fourths[n, n]
    second = sum([thirds[n, j, n] ** 2 * (8 * freqs2[n] - 3 * freqs2[j]) / ((freqs2[j]) * (4 * freqs2[n] - freqs2[j]))
                  for j in range(len(freqs))])

    fourth_contr = (1 / (16 * freqs2[n])) * first
    thirds_contr = (1 / (16 * freqs2[n])) * second
    xnn = (1 / (16 * freqs2[n])) * (first - second)

    return xnn


def calc_xnm(freqs, thirds, fourths, F, n, m):
    # F corresponds to the imaginary frequency
    # (n,m) is the diagonal matrix element we are calculating
    # NOTE incoming freq list MUST have the Fth frequency as pure real!

    freqs = list(freqs)
    freqs2 = [i ** 2 for i in freqs]
    freqs2[F] = freqs2[F] * (-1)

    first = fourths[n, m]
    second = sum([thirds[k, n, n] * thirds[k, m, m] / freqs2[k] for k in range(0, len(freqs))])
    third = sum([2 * thirds[n, k, m] ** 2 * (freqs2[n] + freqs2[m] - freqs2[k]) / (((freqs[n] + freqs[m]) ** 2 - freqs2[k])
                                                                                   * ((freqs[n] - freqs[m]) ** 2 - freqs2[k]))
                 for k in range(0, len(freqs))])

    xnm = (1 / (4 * freqs[n] * freqs[m])) * (first - second + third)

    if n == F or m == F:
        xnm = xnm * (-1)  # This is because the freqs[n] or freqs[m] in the formula three lines above should have an i attached to the imaginary frequencuy.
        # Multipluying topo and botom by -i gives a pure imaginary value of xnm, and you have multiplied its value by (-1).

    return xnm
