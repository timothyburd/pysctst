"""
This module contains code for reading Gaussian fchk files.
"""

import os.path
import unittest
from itertools import islice

import numpy as np


# TODO replace most of this module with a proper library! http://onlinelibrary.wiley.com/doi/10.1002/jcc.20823/abstract;jsessionid=0BC6836DE9A1507542E8D55901287A2A.f04t03

def find_basis_set(file_name):
    searchfile = open(file_name, "r")
    for i, line in enumerate(searchfile):
        if i == 1:
            basis = line.split()[2]
            searchfile.close()
            return basis


    raise ValueError('File ', file_name, ' is of length zero!')


def find_calulation_details(file_name):
    # Prints the second line in the fchk file, which has the basis set, and method
    searchfile = open(file_name, 'r')

    for i, line in enumerate(searchfile):
        if i == 1:
            details = line
    searchfile.close()
    return details


def std_to_numeric(inputstring):
    """
    Turns string formatted number from fchk to float
    :param inputstring: number of the form "1.039302927293E+07" to be converted to a float
    :return: Float e.g. 10393029.27293
    """
    prefactor = float(inputstring[0:-4])
    exponent = float(inputstring[-3:])
    result = prefactor * (10 ** exponent)
    return result


def find_energy(file_name):
    """
    Returns single point energy from a fchk file
    :param file_name: name of fchk file
    :return: energy float
    """
    if os.path.exists(file_name):
        searchfile = open(file_name, "r")
    else:
        raise ValueError("No such file '{}'. For 1D SCTST, 8 single point energy calculations are required along the reactive mode, "
              "in order to find xff by richardson extrapolation. These files must be in a folder labeled 'name', and with names"
              " m4.fchk, m3.fchk m2, m1, p1, p2, p3, p4.fchk and energy_mp2.fchk. Input files to calculate these are generated automatically.".format(file_name))

    for line in searchfile:
        if "Total Energy" in line:
            energy = line.split()[-1]
            total_energy = std_to_numeric(energy)
            searchfile.close()
            return total_energy

    raise ValueError('No energy found in file ', file_name)


def find_mass(file_name):
    """
    :param file_name: fchk file
    :return: list of masses of atoms in this file
    """
    numerical_masses = []
    searchfile = open(file_name, "r")
    for line in searchfile:
        if "Real atomic weights" in line:
            mass_list = "".join(list(islice(searchfile, 6)))
            mass_list = mass_list.split()
            mass_list = [item for item in mass_list if ("E+" in item) or ("E-" in item)]

            for mass in mass_list:
                numerical_masses.append(std_to_numeric(mass))
    searchfile.close()
    return numerical_masses


def find_coordinates(file_name, n_atoms):
    """
    :param file_name: fchk file of optimised structure
    :return: matrix of coordinates
    """
    file_is_empty = True  # will switch to false when it finds content
    searchfile = open(file_name, "r")
    for line in searchfile:
        if "Current cartesian coordinates" in line:
            file_is_empty = False
            coord_string = "".join(list(islice(searchfile, n_atoms)))
            coord_string = coord_string.split()
            coord_string = [item for item in coord_string if ("E" in item)]
            coord_list = coord_string
            coord_list = [std_to_numeric(x) for x in coord_list]
            coord_list = [coord_list[i] for i in range(n_atoms * 3)]
            coord_array = np.asarray(coord_list)
            # n_atoms = len(coord_array) / 3
            # assert n_atoms.is_integer()
            n_atoms = int(n_atoms)
            coord_array = coord_array.reshape((n_atoms, 3))

    searchfile.close()

    if file_is_empty:
        raise ValueError('The coordinates of ' + file_name + ' have not been found, so there is probably something wrong with the fchk file. Is it empty?')

    return coord_array


def find_hessian(file_name, n_atoms):
    """
    :param file_name: fchk file for a frequency calculation
    :param n_atoms: number of atoms in structure
    :return: hessian matrix
    """
    degrees_of_freedom = n_atoms * 3
    items_per_line = 5
    lines = degrees_of_freedom * (degrees_of_freedom + 1) / (
            2 * items_per_line)  # Number of elements in the upper diagonal of the matrix / 5 (the number of numbers per line in the output file)
    lines = int(lines)
    searchfile = open(file_name, "r")
    for line in searchfile:
        if "Cartesian Force Constants " in line:
            hessian_list = ("".join(list(islice(searchfile, lines + 1)))).split()
            hessian_list = [item for item in hessian_list if
                            len(item) > 10]  # Check we are only picking up numbers, not text etc
            hessian_list = [item for item in hessian_list if item[-4] == "E"]
            hessian_list = [std_to_numeric(x) for x in hessian_list]

    hessian = np.zeros((degrees_of_freedom, degrees_of_freedom))

    counter = 0
    for i in range(degrees_of_freedom):
        for j in range(i + 1):
            hessian[i][j] = hessian_list[counter]
            hessian[j][i] = hessian_list[counter]
            counter = counter + 1
    searchfile.close()

    return hessian


def find_polarisability(file_name, linear=False):
    """
    Returns 3 x 3 matrix of alpha values
    gaussian uses units of ea0^2
    """
    if linear:
        return np.zeros((3, 3))
    searchfile = open(file_name, "r")
    read = False
    alphas = []
    polarisability_matrix = np.zeros((3, 3))
    for line in searchfile:
        if read == True:
            alphas += line.split()
            if len(line.split(" ")) < 5:
                read = False
        if "Polarizability" in line:
            if not "derivs" in line:  # stops picking up of dipole derives bit
                read = True
    searchfile.close()
    if len(alphas) != 6:
        if not linear:
            # print("WarningL: Have not foudn6 pol")
            # return 0
            raise ValueError("Have not found 6 polarizabilities!")

    bohr_to_A = 5.2917721067 * 10 ** (-1)
    ea0_to_debye = 1 / 0.393430307

    alphas = [std_to_numeric(i) * ea0_to_debye / (bohr_to_A ** 3) for i in alphas]  # converts to Debye / A^3

    polarisability_matrix[0][0] = alphas[0]
    polarisability_matrix[1][0] = alphas[1]
    polarisability_matrix[1][1] = alphas[2]
    polarisability_matrix[2][0] = alphas[3]
    polarisability_matrix[2][1] = alphas[4]
    polarisability_matrix[2][2] = alphas[5]

    polarisability_matrix[0][1] = alphas[1]
    polarisability_matrix[0][2] = alphas[3]
    polarisability_matrix[1][2] = alphas[4]

    return (polarisability_matrix)


def find_dipole(file_name, linear=False):
    """
    Returns 1x3 list of dipole moments
    gaussian file lists dipoles in units of ea0,
    so we will convert to debyes
    """

    if linear:
        return [0, 0, 0]
    ea0_to_debye = 1 / 0.393430307

    searchfile = open(file_name, "r")
    read = False
    mu = []
    for line in searchfile:
        if read == True:
            mu += line.split()
            if len(line.split(" ")) != 3:
                read = False
        if "Dipole Moment" in line:
            if not "derivs" in line:  # stops picking up of dipole derives bit
                read = True
    searchfile.close()
    if len(mu) != 3:
        if not linear:
            raise ValueError("Have not found 3 dipole moments!")

    mu = [std_to_numeric(i) * ea0_to_debye for i in mu]

    return (mu)


def freq_corect_ordering_in_log(filename):
    """
    When you do a FD VPT2 calcilation using the gaussian keyword freq=anharmonic, you get a log file out.
    SOMETIMES (I think this is likely to do with having degenerate frequencies) it jumbles around the order of the frequencies
    for the x matrix. This messes up the whole calculation!
    This function checks to see if this has happened!
    """
    bands = get_freq_order(filename)

    if len(bands) == 0:  # this happens if there is like 2 atoms in the molecule i.e. there is only like one frequency
        return True
    if bands[0] < 0:
        bands = bands[1:]

    # check if a sorted list
    sorted = all(bands[i] >= bands[i + 1] for i in range(len(bands) - 1))

    return sorted


def get_freq_order(filename):
    searchfile = open(filename, "r")

    read = False
    bands = []
    for line in searchfile:
        if " Overtones (DE w.r.t. Ground State)" in line:
            read = False
        if read:
            bands.append(float(line.split()[2]))
        if " Fundamental Bands (DE w.r.t. Ground State)" in line:
            read = True
    searchfile.close()

    return bands


def repair_x_matrix(x, filename):
    """
    If the function freq_corect_ordering_in_log return False, that means gaussian has jumbled up the frequecneis in the log file, so the VPT2 x matrix is jumbled up too.
    This function takes the 'jumbled' x matrix and sorts it
    :param x:
    :param filename:
    :return:
    """
    dim = x.shape[1]
    x_fixed = np.zeros((dim, dim))
    bands_fixed = np.zeros(dim)  # use this to check the reshuffling has worked

    if freq_corect_ordering_in_log(filename):  # the x matrix is not jumbled - the log file is fine.
        return x

    # get the jumbled freq list
    bands = get_freq_order(filename)

    # If there is an imaginary frequency, that will be in the correct place
    k = 0

    bands = np.asarray(bands)
    if bands[0] < 0:
        bands[0] = 999999999999999

    temp = bands.argsort()
    ranks = np.empty_like(temp)
    ranks[temp] = np.arange(len(bands))

    # reverse the ordering
    ranks = [max(ranks) - r for r in ranks]

    for i in range(dim):
        for j in range(dim):
            x_fixed[ranks[i], ranks[j]] = x[i, j]
        bands_fixed[ranks[i]] = bands[i]
    # check the bands are sorted now; this will imply the x matrix is ordered properly too as they have passed through a similar algorithm
    sorted = all(bands_fixed[i] >= bands_fixed[i + 1] for i in range(len(bands_fixed) - 1))
    if not sorted:
        raise ValueError("The sorting thing hasn't worked!")

    return x_fixed


def extract_gradient_vector(file_name, n_atoms):
    """
    Returns 1x3 list of dipole moments

    gaussian file lists dipoles in units of ea0,
    so we will convert to debyes
    """

    searchfile = open(file_name, "r")
    read = False
    grad = []
    for line in searchfile:
        if "Dipole Moment" in line:
            read = False
        if read == True:
            grad += line.split()
        if "Cartesian Gradient" in line:
            read = True
    searchfile.close()

    dof = 3 * n_atoms
    if len(grad) != dof:
        raise ValueError("Have not found the full grad vector!")

    grad = [std_to_numeric(i) for i in grad]

    return np.asarray(grad)


def find_forces_vector(file_name, n_atoms, skip=0):
    """
    Find the force constant vector from a log file
    skip = the number of grads to IGNORE before returning the found gradient (e.g. in a file with 10 gradients, skip the first five)
    """

    searchfile = open(file_name, "r")
    read = False
    grad = []
    for line in searchfile:
        if "Cartesian Forces:  " in line:
            read = False
        if read == True:
            grad += line.split()
        if "Forces (Hartrees/Bohr)" in line:
            if skip == 0:
                read = True
                skip -= 1
            else:
                skip -= 1
    searchfile.close()

    grad_new = []
    for item in grad:
        if '.' in item:
            grad_new.append(float(item))
    if len(grad_new) != n_atoms * 3:
        print(len(grad_new))
        raise ValueError("Wrong length force contsnts")

    return np.asarray(grad_new)


def find_irc_distances(file_name, n_atoms, skip=0):
    """
    Find the the IRC distance of a geometry from the TS
    skip = the number of grads to IGNORE before returning the found gradient (e.g. in a file with 10 gradients, skip the first five)
    """

    searchfile = open(file_name, "r")
    distances = []
    for line in searchfile:
        if "NET REACTION COORDINATE UP TO THIS POINT" in line:
            distances.append(float(line.split()[-1]))
    return distances


def fetch_gradient_vector(file_name, n_atoms, n_points, direction, index):
    """
    Return the gradient vector from a long file form an IRC calcualtion
    :param file_name:
    :param n_atoms:
    :param n_points:    The number of points used in the IRC calculation (probably ten)
    :param direction:   Either 'Plus' or 'Minus'
    :param index:       Which point along the MEP you want to use. Larger index is further away from the TS, index = 0 is at the TS.
    :return:            Numpy vector
    """
    if index == 0:
        return find_forces_vector(file_name, n_atoms, skip=0)
    if direction == 'Plus':
        skips = index
    elif direction == 'Minus':
        skips = index + n_points
    else:
        raise ValueError("Direction must be Plus or Minus")

    return find_forces_vector(file_name, n_atoms, skip=skips)



