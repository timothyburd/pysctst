"""
Functions to do VPT4 calculations, as used in the JPCA paper
"""

import math

import matplotlib.pyplot as plt
import numpy as np

import physical_constants as pc
import read_gaussian as rg


def find_energy_list(name):
    """
    Return the lsit of energies from the 21 single point energy fchk files
    """
    name_list = ['m10', 'm9', 'm8', 'm7', 'm6', 'm5', 'm4', 'm3', 'm2', 'm1', "energy_mp2", 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8', 'p9', 'p10']

    name_list = ["checkpoint_files/" + name + "/" + n + ".fchk" for n in name_list]

    energy_list = [rg.find_energy(n) for n in name_list]

    return energy_list


def plot_energies(list, step_size):
    """
    Plot the energies of the 21 single point energy files
    """
    n_points = len(list)
    step_size = step_size * pc.dalton_to_electronmass ** 0.5

    side_points = int((n_points - 1) / 2)

    displacements = [-step_size * 2 ** p for p in range(0, side_points)][::-1] + [0] + [step_size * 2 ** p for p in range(0, side_points)]

    z = np.polyfit(displacements, list, 6)
    fit_displacements = np.linspace(displacements[0], displacements[-1], num=300)
    fit = np.poly1d(z)
    fit_energies = [fit(x) for x in fit_displacements]

    plt.plot(fit_displacements, fit_energies)

    plt.scatter(displacements, list)
    plt.ylabel("Energy / hartrees")
    plt.xlabel("Displacement / $a_0 (amu)^{0.5}$")
    plt.savefig("plots/vpt4_energies.pdf")
    print("plots/vpt4_energies.pdf")
    plt.show()


def find_derivatives(list, step_size):
    """
    Find the numerical derivatives of the reaction mode
    return: Derivatives list
    """

    n_points = len(list)

    step_size = step_size * pc.dalton_to_electronmass ** 0.5

    side_points = int((n_points - 1) / 2)

    displacements = [-step_size * 2 ** p for p in range(0, side_points)][::-1] + [0] + [step_size * 2 ** p for p in range(0, side_points)]
    z = np.polyfit(displacements, list, 6)

    fit_displacements = np.linspace(displacements[0], displacements[-1], num=100)
    fit = np.poly1d(z)
    fit_energies = [fit(x) for x in fit_displacements]
    # plt.scatter(displacements, list)
    # plt.plot(fit_displacements, fit_energies)
    # plt.show()

    derivatives = []
    f_n = z[::-1]  # now lowest order coeffieicnt is firsts
    for i in range(len(f_n)):
        derivatives.append(f_n[i] * math.factorial(i))  # this is how the polynomial coefieicnt relates to the derrivative

    return derivatives


def calc_phi_list(derivative_list):
    """
    Stanton uses Phi's rather than derivatives in his VPT4 paper. This returns the phiss
    :param derivative_list: list of derivatives
    :return: phi_list
    """

    phi_2 = math.sqrt(-derivative_list[2])

    phi_list = []
    for i in range(len(derivative_list)):
        phi_list.append(-derivative_list[i] / (phi_2 ** (i / 2)))  # equation 8 in stantons vpt4 paper

    return phi_list


def calc_X(phi_list):
    """
    DOI: 10.1021/acs.jpclett.6b01239 
    Equation 6
    """

    X = phi_list[4] / 16 - (5 * phi_list[3] ** 2 / (48 * phi_list[2]))

    return X


def calc_Y(phi):
    """
    DOI: 10.1021/acs.jpclett.6b01239 
    Equation 5
    """
    return phi[6] / 288 - 7 * phi[3] * phi[5] / (288 * phi[2]) - 17 * phi[4] ** 2 / (2304 * phi[2]) + \
           25 * phi[3] ** 2 * phi[4] / (384 * phi[2] ** 2) - 235 * phi[3] ** 4 / (6912 * phi[2] ** 3)


def calc_freq(phi_list):
    return phi_list[2]


def calc_W(phi):
    """
    DOI: 10.1021/acs.jpclett.6b01239 
    Equation 4
    """

    return phi[2] - 67 * phi[4] ** 2 / (9216 * phi[2]) + 5 * phi[6] / 1152


def calc_G(phi):
    """
    DOI: 10.1021/acs.jpclett.6b01239 
    Equation 7
    """

    return phi[4] / 64 - 7 * phi[3] ** 2 / (576 * phi[2])
