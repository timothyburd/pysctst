"""
Find a matrix R that rotates unit vector a onto unit vector b in N dimensions
"""
import numpy as np


def find_rot_matrix(x, y):
    """
    Find rotation matrix in N dimensions that rotates x onto y such that R @ x = y
    :return:    NxN numpy array
    """

    N = len(x)

    if np.array_equal(x / np.linalg.norm(x), y / np.linalg.norm(y)):
        return np.identity(N)

    u = x / np.linalg.norm(x)

    v = y - np.dot(u, y) * u
    v /= np.linalg.norm(v)

    cos_theta = np.dot(x, y) / np.linalg.norm(x) / np.linalg.norm(y)
    sin_theta = np.sqrt(1 - cos_theta ** 2)

    R_theta = np.asarray([[cos_theta, -sin_theta], [sin_theta, cos_theta]])

    stack = np.stack([u, v])

    R = np.identity(N) - np.outer(u, u) - np.outer(v, v) + stack.transpose() @ R_theta @ stack

    return R



