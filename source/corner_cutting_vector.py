"""
Calculate the vector from the (classical) transition state, to the peak of the instanton
"""

import itertools

import numpy as np


def find_corner_cutting_vector(tsname, instname, mass_list):
    """
    Reutnrs vector from TS to top of instanton
    :param tsname: xyz file TSopt.xys
    :param instname: xyz file of the instanton e.g. inst_512_200.xyz
    :return: vector in cartesian coordinates
    """
    print(tsname)
    print(instname)
    vectorname = instname[:-4] + "vector.xyz"
    inst = open(instname)

    n_atoms = int(inst.readline())
    lines_per_struc = n_atoms + 2
    inst.close()

    # get number of lines in file
    with open(instname) as f:
        for i, l in enumerate(f):
            pass
    n_structures = (i + 1) / (lines_per_struc)
    if not n_structures.is_integer():
        raise ValueError("Have calcualted a non-integer number of stgructures!")
    else:
        n_structures = int(n_structures)

    # lines with the energy on
    energies = [lines_per_struc * i + 1 for i in range(n_structures)]

    # find maximum energy structure
    max_energy = -9999999
    with open(instname) as f:
        for i, line in enumerate(f):
            if i in energies:
                en = float(line)
                if en > max_energy:
                    max_energy = en
                    max_structure = (i - 1) / lines_per_struc
                    max_energy_line = i

    # generate the max struc
    max_st = False
    coords = []
    with open(instname) as f:
        for i, line in enumerate(f):
            if i == max_energy_line + 1:
                max_st = True
            if i == max_energy_line + lines_per_struc - 1:
                max_st = False
            if max_st:
                coords.append([float(k) for k in line.split()[1:]])
    coords = np.asarray(coords)  # coordinates of the maximum energy structure

    # fetch classical TS coordinates
    ts_coords = []
    with open(tsname) as f:
        for i, line in enumerate(f):
            if i > 1:
                ts_coords.append([float(k) for k in line.split()[1:]])
    ts_coords = np.asarray(ts_coords)

    corner_cutting_vector = coords - ts_coords

    corner_cutting_vector = np.reshape(corner_cutting_vector, (len(coords) * 3))

    mass_vector = list(itertools.chain.from_iterable((itertools.repeat(i ** -0.5, 3) for i in mass_list)))
    corner_cutting_vector = [i * j for i, j in zip(mass_vector, corner_cutting_vector)]

    return corner_cutting_vector


if __name__ == '__main__':
    vector = find_corner_cutting_vector('TSopt.xyz', 'inst_512_200.xyz')
    print(vector)
