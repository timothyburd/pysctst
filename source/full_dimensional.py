"""
Module contains tools required for a full dimensional SCTST Caluclation
Timothy Burd 2018

NOTE that doing the FD tunneling calcilationm in pyhthon is very slow. As such, by default, a C program is run, rather than this module.

Partially based on c++ code Created by Samuel Greene on 27/03/2016.
"""
import ast
import cmath
import math
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

import generate_gaussian as gg
import physical_constants as pc


def find_variable(key_word, inputfile):
    f = open(inputfile)

    for i, line in enumerate(f):
        no_comment = line.split("#")[0]

        title = str(no_comment.split("=")[0]).rstrip()

        if title == key_word:
            value = no_comment.split("=")[1].rstrip().lstrip()
            value = ast.literal_eval(value)

    return value


def main(rxnFreq, barHeight, revBarHeight, xFF, reacZPE, ts_states_filename, tunneling_type, label=""):
    """
    NOTE THIS FUNCTION IS VERY SLOW. C CODE IS MUCH FASTER AND IS CALLED BY DEFAULT RATHER THAN THIS.
    AS SUCH< THIS FUNCTION IS OUTDATED

    NOTE rxn barrier height INCLUDES / reac ZPE, reacG0, TS G0(a.u.) and may be negative
        reverse barrier height INCLUDES / prod ZPE, TSG0(a.u.)
    Functioncalculates and prints to file the cumulative reaction probability over thev range of energies

    Output file is "CRP_%c.txt"
    Input
    required is [SCTST input file] which contains the imaginary frequency, the barrier height(inc.ZPE, G0 etc)
    xff and ZPE.[States file] contains the results of the Monte carlo(?) simulation giving the density of states
    of the system produced by the programme "main.c".[M or W] specifies whether Wagner deep tunnening corrections are included.
    """
    print("In fd.main!")
    CRPLen = 5000
    # statesF = open(ts_states_filename, 'r')  # e.g.States_Output(oc).txt

    r_states = pd.read_csv(ts_states_filename, skiprows=[0])

    energies = r_states['Energy']
    nStates = r_states[' # of states']
    Ev = r_states[' average Ev']
    coupling = r_states[' average coupling']
    nBins = len(energies)

    Estep = energies[1] - energies[0]

    CRP = np.zeros(CRPLen)

    ergicity = barHeight - revBarHeight
    ergjMin = ergicity / Estep
    # Wagner requires rxn to be exoergic
    if (ergjMin < 0):
        ergjMin = 0
        wagForward = barHeight
        wagReverse = revBarHeight
    else:
        wagForward = revBarHeight
        wagReverse = barHeight

    print("itterating over ", nBins, " bins")
    for i in range(nBins):

        omegaF = rxnFreq + coupling[i]
        jMin = (energies[i] - reacZPE) / Estep
        if (jMin < ergjMin):
            jMin = ergjMin
        print("Itterating between ", jMin, " and ", CRPLen)
        for j in range(int(jMin), CRPLen):

            if (tunneling_type == 'M'):  # I think M means no deep tunneling corrections
                deltaE = barHeight + energies[i] - j * Estep
                tmp = omegaF * omegaF + 4 * xFF * deltaE  # THis is the 'determinant' from eqtn 2.91 from Sam's thesis
                if (tmp >= 0):
                    prob = 1 / (1 + math.exp(2 * math.pi * (-omegaF + math.sqrt(tmp)) / (2 * xFF)))  # eqtn 2.91 / 2.92 in Sam's thesis
                else:
                    prob = 0
            elif (tunneling_type == 'W'):  # I think W means including Wagner's deep tunneling corrections
                prob = wagProb(wagForward + energies[i], wagReverse + energies[i], xFF, omegaF, j * Estep)
            else:
                print("Please specify W or M for the method.\n")
                return 0

            CRP[j] += nStates[i] * prob  # eqtn 2.93 in Sam 's thesis

    # Produce output file listing the CRP for all energies up to CPRLen * Estep
    outname = "CRP_" + tunneling_type + "_" + label + ".txt"
    outfile = open(outname, "w")

    for i in range(CRPLen):
        outfile.write(str(i * Estep) + "," + str(CRP[i]))

    return 0


def thetaReg(iseg, zlo, zhi, eta, rho):
    """
    Called in FD Tunneling integral. Note: python is slow at doing this calcualtion, so the C code is used instead
    :param iseg:
    :param zlo:
    :param zhi:
    :param eta:
    :param rho:
    :return:
    """
    c0 = rho * rho + eta - 1
    if (eta < 0) or (c0 < 0):
        return 0

    c3 = 1 + rho
    c2 = math.sqrt(c0)
    c1 = math.sqrt(eta)

    if (eta > 1):
        return math.pi * (c3 - c2 - c1)

    else:
        c4 = math.sqrt(1 - eta)
        c5 = c3 * c4
        c6 = c3 - eta
        if (iseg >= 2):
            arg1l = (c6 * zlo - eta) / zlo / c5
            arg1l = max(arg1l, -1)
            arg1l = min(arg1l, 1)

            arg2l = (c6 - c0 * zlo) / c5
            arg2l = max(arg2l, -1)
            arg2l = min(arg2l, 1)

            arg3l = (rho * zlo - 1) / (1 + zlo) / c4
            arg3l = max(arg3l, -1)
            arg3l = min(arg3l, 1)

            thtal = c3 * math.asin(arg3l) + c2 * math.asin(arg2l) - c1 * math.asin(arg1l)

        else:
            thtal = -math.pi / 2 * (c3 - c2 - c1)

        if (iseg <= 2):
            arg1h = (c6 * zhi - eta) / zhi / c5
            arg1h = max(arg1h, -1)
            arg1h = min(arg1h, 1)

            arg2h = (c6 - c0 * zhi) / c5
            arg2h = max(arg2h, -1)
            arg2h = min(arg2h, 1)

            arg3h = (rho * zhi - 1) / (1 + zhi) / c4
            arg3h = max(arg3h, -1)
            arg3h = min(arg3h, 1)
            thtah = c3 * math.asin(arg3h) + c2 * math.asin(arg2h) - c1 * math.asin(arg1h)

        else:
            thtah = math.pi / 2 * (c3 - c2 - c1)

        return thtah - thtal


def thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc):
    """
    Called in FD Tunneling integral. Note: python is slow at doing this calcualtion, so the C code is used instead
    :param iseg:
    :param zlo:
    :param zhi:
    :param eta:
    :param rho:
    :return:
    """

    c1 = math.sqrt(1 - eta)
    c2 = 1 - rho * rho - eta
    c3 = 1 + rho - eta

    arg3h = (rho * zhi - 1) / (1 + zhi) / c1
    arg3h = max(arg3h, -1)
    arg3h = min(arg3h, 1)

    arg3l = (rho * zlo - 1) / (1 + zlo) / c1
    arg3l = max(arg3l, -1)
    arg3l = min(arg3l, 1)
    thta = (1 + rho) * (math.asin(arg3h) - math.asin(arg3l))

    rhi = cc + bb * zhi + aa * zhi * zhi
    rhi = max(rhi, 0)

    rlo = cc + bb * zlo + aa * zlo * zlo
    rlo = max(rlo, 0)
    arg1 = (2 * cc + bb * zhi + 2 * math.sqrt(cc * rhi)) / zhi
    arg2 = (2 * cc + bb * zlo + 2 * math.sqrt(cc * rlo)) / zlo
    thta -= math.sqrt(-eta) * math.log(arg1 / arg2)

    if (-c2 >= 0):
        arg2h = (c2 * zhi + c3) / (1 + rho) / c1

        arg2h = max(arg2h, -1)
        arg2h = min(arg2h, 1)

        arg2l = (c2 * zlo + c3) / (1 + rho) / c1

        arg2l = max(arg2l, -1)
        arg2l = min(arg2l, 1)

        return thta + math.sqrt(-c2) * (math.asin(arg2h) - math.asin(arg2l))
    else:
        arg1 = 2 * math.sqrt(aa * rhi) + 2 * aa * zhi + bb
        arg2 = 2 * math.sqrt(aa * rlo) + 2 * aa * zlo + bb
        return thta + math.sqrt(c2) * math.log(arg1 / arg2)


def wagProb(barHeight, revBarHeight, xFF, omegaF, energy):
    """
    This function turned out to be super slow in python, so better to use the c++ version (fdsctst.c) that this is based on!
    :param barHeight:
    :param revBarHeight:
    :param xFF:
    :param omegaF:
    :param energy:
    :return:
    """
    rho = math.sqrt(revBarHeight / barHeight)
    Db = -omegaF * omegaF / 4.0 / xFF * (1.0 - 1.0 / rho + 1.0 / rho / rho)
    alphab = abs(omegaF) * (1.0 + rho) / rho * math.sqrt(0.5 / Db)
    f = math.sqrt(Db / barHeight)

    tmp = 3 * f if f > 1 else 3
    ur = (math.sqrt(rho * rho + rho + 1) - (rho - 1)) / tmp
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (
            f - rho * (1 + f) * (1 + f) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (
                  1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                 210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (
                 1 - rho) * f * f * f * ur * ur * ur + 105 * pow(f * ur, 4))
    alphar = alphab * f * Rr
    umr1 = (math.sqrt((1 - rho) * (1 - rho) * pow(1 - Rr * f, 2) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) + (1 - rho) * (
            1 - Rr * f)) / (2 * (1 - Rr * f * f))
    umr2 = -(math.sqrt(pow(1 - rho, 2) * pow(1 - Rr * f, 2) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) + (1 - rho) * (1 - Rr * f)) / (
            2 * (1 - Rr * f * f))

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab
    log_value = (1 - f * umr) / (rho + f * umr)

    if log_value > 0:
        dr = -ybr + math.log((1 - f * umr) / (rho + f * umr)) / alphar
    else:
        dr = 0  # TODO work out the effect of this fix

    tmp = 3 * f if f > 1 else 3
    up = (math.sqrt(rho * rho + rho + 1) + rho - 1) / tmp

    Rp = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * up + 140 * (
            f - rho * (1 + f) * (1 + f) + f * rho * rho) * up * up - 120 * f * (1 + f) * (
                  1 - rho) * up * up * up + 105 * f * f * pow(up, 4)) / (
                 210 * rho * rho + 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho * rho) * f * f * up * up - 240 * (
                 1 - rho) * f * f * f * up * up * up + 105 * pow(f * up, 4))
    alphap = alphab * f * Rp
    ump1 = (math.sqrt((1 - rho) * (1 - rho) * pow(1 - Rp * f, 2) - 4 * rho * (1 - Rp * f * f) * (Rp - 1)) + (1 - rho) * (
            1 - Rp * f)) / (2 * (1 - Rp * f * f))
    ump2 = -(
            math.sqrt(pow(1 - rho, 2) * pow(1 - Rp * f, 2) - 4 * rho * (1 - Rp * f * f) * (Rp - 1)) + (1 - rho) * (1 - Rp * f)) / (
                   2 * (1 - Rp * f * f))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1
    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab
    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp
    zrb = math.exp(alphar * (ybr + dr))
    zbr = math.exp(alphab * ybr)
    zpb = math.exp(alphap * (ybp + dp))
    zbp = math.exp(alphab * ybp)
    headb = 2 * Db / omegaF * rho / (1 + rho)
    headr = headb * barHeight / Db / Rr
    headp = headr * Rr / Rp

    thtaEn = energy if energy < barHeight else (2 * barHeight - energy)
    ep = thtaEn - barHeight + Db
    eta = ep / Db

    if (thtaEn > (barHeight - Db)):
        sqrtdelld2a = (1 + rho) * math.sqrt(1 - eta) / (1 - rho * rho - eta)
        zzlo = -(1 + rho - eta) / (1 - rho * rho - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a

        zlo = zbr if zbr > zzlo else zzlo

        zhi = zbp if zbp < zzhi else zzhi
        thta = headb * thetaReg(2, zlo, zhi, eta, rho)
    else:
        zlo = zbr
        zhi = zbp
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho * rho - eta)
        cc = -ep
        thta = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)
    if (zlo == zbr):
        eta = thtaEn / barHeight
        sqrtdelld2a = (1 + rho) * math.sqrt(1 - eta) / (1 - rho * rho - eta)
        zlo = -(1 + rho - eta) / (1 - rho * rho - eta) + sqrtdelld2a
        zzhi = zlo - 2 * sqrtdelld2a
        thta += headr * thetaReg(1, zlo, zrb, eta, rho)
        if (zhi == zbp):
            thta += headp * thetaReg(3, zpb, zzhi, eta, rho)

    if (energy < barHeight):
        return 1 / (1 + math.exp(2 * thta))
    elif ((energy - barHeight) < barHeight):
        return 1 - 1 / (1 + math.exp(2 * thta))
    else:
        return 1


def find_reactant_pf(filename, temp):
    """
    Find partition fucntion at temp=temp from the states output file (i.e. the anharmonic vibrational energy levels)
    :param filename: Output of the Wang Landau code
    :param temp:
    :return:
    """
    r_states = pd.read_csv(filename, skiprows=[0])
    r_states['relative_energy'] = r_states['Energy'] - r_states['Energy'][0]
    r_states['Partition_contribution'] = r_states[' # of states'] * np.exp(-1 * r_states['relative_energy'] * pc.hartree_to_joule / (temp * pc.kb))
    return r_states['Partition_contribution'].sum()


def find_reactant_ZPE(filename):
    """
    Find ZPE from the states output file (i.e. the anharmonic vibrational energy levels)
    :param filename:Output of the Wang Landau code
    :return:
    """

    r_states = pd.read_csv(filename, skiprows=[0])
    return r_states['Energy'][0]


def find_ts_higher_vib_state(filename, state_number=0):
    """
    :param filename:Output of the Wang Landau code
    :return:
    """

    df = pd.read_csv(filename, skiprows=[0])
    df = df.drop(df[df[' # of states'] == 0].index)
    df = df.reset_index(drop=True)
    return df['Energy'][state_number], df[' average coupling'][state_number]


def integrate_CRP(filename, temp):
    CRP = pd.read_csv(filename)

    CRP['boltzman_exponant'] = (-1) * CRP['Energy'] * pc.hartree_to_joule / (temp * pc.kb)
    CRP['boltzman_factor'] = np.exp(CRP['boltzman_exponant'])
    # CRP['CRP'] = float(CRP['CRP'])
    # CRP = CRP.replace({'-1.#IND00000000000000000000000000': 0}, regex=True)
    # CRP['CRP'][0] = float(CRP['CRP'][0])
    # for index, row in CRP.iterrows():
    #     print(index, row['CRP'], row['boltzman_factor'])
    #     row['CRP'] = float(row['CRP'])

    CRP['weighted_CRP'] = CRP['CRP'] * CRP['boltzman_factor']
    # CRP['energy_ev'] = CRP['Energy'] * pc.hartree_to_ev
    step = CRP['Energy'][1] - CRP['Energy'][0]

    # integral = sum([(CRP['weighted_CRP'][i] + CRP['weighted_CRP'][i+1]) * step * pc.hartree_to_joule/2 for i in range(len(CRP)-1)])
    integral = sum([(CRP['weighted_CRP'][i] + CRP['weighted_CRP'][i + 1]) * step * pc.hartree_to_joule * 0.5 for i in range(len(CRP) - 1)])
    return integral


def plot_weighted_CRP(filename, temp_list):
    for temp in temp_list:
        CRP = pd.read_csv(filename)
        CRP['boltzman_exponant'] = (-1) * CRP['Energy'] * pc.hartree_to_joule / (temp * pc.kb)
        CRP['boltzman_factor'] = np.exp(CRP['boltzman_exponant'])
        CRP['weighted_CRP'] = CRP['CRP'] * CRP['boltzman_factor']

        plt.plot(CRP['Energy'], CRP['weighted_CRP'], label=str(temp))
    plt.xlabel("Energy (Hartrees)")
    plt.ylabel("Weighted CRP")
    plt.legend()
    plt.savefig("plots/weighted_CRP.pdf")
    plt.show()


def fetch_CRP(filename):
    CRP = pd.read_csv(filename)
    return CRP['CRP']


def fetch_energylist(filename):
    CRP = pd.read_csv(filename)
    return CRP['Energy']


def calc_g0(freqs, thirds, fourths, nD):
    """
    Using third and fourth order derivatives, calculate the full dimensional value of G0
    """
    cutOff = 300 / (2.1947463 * 10 ** 5)  # Fermi resonance cutoff

    freqs = [cmath.sqrt(-1) * (-1) * f if f < 0 else f for f in freqs]

    # fermi resonance matrix. Will be 1 if ok, 0 if a resonance and thus should ignore those terms
    fermitwo = np.zeros((nD, nD))
    fermithree = np.zeros((nD, nD, nD))
    for k in range(nD):
        for l in range(nD):
            if freqs[k].imag == 0 and freqs[l].imag == 0 and abs((thirds[k, k, l] / freqs[k] / math.sqrt(freqs[l])) ** 4 / (2 * freqs[k] - freqs[l]) ** 3 / 256) > cutOff:
                fermitwo[k][l] = 0
            else:
                fermitwo[k][l] = 1

    for k in range(nD):
        for l in range(nD):
            for m in range(nD):
                if freqs[k].imag == 0 and freqs[l].imag == 0 and freqs[m].imag == 0 and abs(thirds[k, l, m] ** 4 / freqs[k] ** 2 / freqs[l] ** 2 / freqs[m] ** 2 / (freqs[k] - freqs[l] + freqs[m]) ** 3 / 64) > cutOff:
                    fermithree[k][l][m] = 0
                else:
                    fermithree[k][l][m] = 1

    termone = (1 / 64) * sum([fourths[i, i] / freqs[i] ** 2 for i in range(nD)])

    termtwo = (7 / 576) * sum([thirds[i, i, i] ** 2 / freqs[i] ** 2 for i in range(nD)])

    termthree = (3 / 64) * sum([thirds[k, l, l] ** 2 / freqs[l] ** 2 * fermitwo[l, k] * (1 / (2 * freqs[l] - freqs[k])) * (1 / (2 * freqs[l] + freqs[k])) if k != l else 0 for k in range(nD) for l in range(nD)])

    termfour = (1 / 4) * sum(
        [thirds[k, l, m] ** 2 / ((freqs[k] + freqs[l]) ** 2 - freqs[m] ** 2) / ((freqs[k] - freqs[l]) ** 2 - freqs[m] ** 2) * fermithree[k, m, l] * fermithree[k, l, m] for k in range(nD) for l in range(k + 1, nD) for m
         in range(l + 1, nD)])

    g0 = termone - termtwo + termthree - termfour

    return g0.real


def find_freqs(filename):
    """ Return a list of frequencies (atomic units) found from a vpt2 log file in descending order"""
    f = open(filename, 'r')

    freq_list = []
    for i, line in enumerate(f):
        if 'Frequencies --' in line:
            linelist = line.split("  ")
            linelist = [i for i in linelist if len(i) > 0]
            linelist = linelist[1:]
            linelist = [float(i) for i in linelist]
            freq_list += linelist

    f.close()

    # These frequencies are NOT in the descending order for the anharmonic output
    # They are extracted from the log file BEFORE the Gaussian reordering happened.
    # Thus, here we have to reorder them ourselves.

    freq_list = freq_list[:int(len(freq_list) / 2)]

    freq_list = [i / pc.hartree_to_cm for i in freq_list]

    # if there is a negative freq, we want to move it to the back, but it will currently be at the front
    negative_freq = False
    for f in freq_list:
        if f < 0:
            negative_freq = True

    if negative_freq:
        freq_list.append(freq_list.pop(0))

    # put freq list in descending order
    freq_list = freq_list[::-1]

    return freq_list


def find_fourths(filename, label=""):
    """ Write the fourth derivatives from a log file to fourths_label.txt"""
    f = open(filename, 'r')
    outf = open('fourths' + label + '.txt', 'w')
    reading = False
    for i, line in enumerate(f):
        if 'I  J  K  L   FI(I,J,K,L)  k(I,J,K,L)  K(I,J,K,L)' in line:
            reading = True
        if '4th derivatives larger than' in line:
            reading = False
        if reading:
            outf.write(line)

    f.close()
    outf.close()


def build_thirds(filename, size, label=""):
    """First read third derivatives from log file and dump them in thirds_label.txt.
    Then read them into a matrix thirds, and return it"""
    os.makedirs(os.path.dirname('derivatives/thirds' + label + '.txt'), exist_ok=True)

    f = open(filename, 'r')
    outf = open('derivatives/thirds' + label + '.txt', 'w')
    reading = False
    for i, line in enumerate(f):
        if 'derivatives larger than' in line:
            reading = False
        if reading:
            outf.write(line)
        # Michael: change finding starting line for extracting data
        if 'FI(I,J,K)' in line and 'k(I,J,K)' in line and 'K(I,J,K)' in line:
            reading = True

    f.close()
    outf.close()

    f = open('derivatives/thirds' + label + '.txt', 'r')

    if "a" in f.read():
        # print("Degenerate nh3 case")
        nh3_deg = True
    else:
        # print("normal case")
        nh3_deg = False
    f.close()

    # Comment to activate reordering for high symmetric NH3
    nh3_deg = False

    f = open('derivatives/thirds' + label + '.txt', 'r')

    thirds = np.zeros((size, size, size))
    # Michael addition: Remove specific mode number contributions - ToDO
    # exclude_mode = 240

    for line in f:
        if len(line.split()) > 4:
            if not nh3_deg:
                index0 = int(line.split()[0]) - 1
                index1 = int(line.split()[1]) - 1
                index2 = int(line.split()[2]) - 1
            else:
                # For NH3 GS degeneracy
                index0 = int(ammonia_relabel(line.split()[0])) - 1
                index1 = int(ammonia_relabel(line.split()[1])) - 1
                index2 = int(ammonia_relabel(line.split()[2])) - 1

            # Michael addition: Remove specific mode number contributions - ToDO
            # if index0 == exclude_mode or index1 == exclude_mode or index2 == exclude_mode:
            #    value = float(0.0)
            # else:
            #    value = float(line.split()[5])
            value = float(line.split()[5])

            thirds[index0, index1, index2] = value / (pc.amu ** 1.5)
            thirds[index2, index0, index1] = value / (pc.amu ** 1.5)
            thirds[index1, index2, index0] = value / (pc.amu ** 1.5)
            thirds[index1, index0, index2] = value / (pc.amu ** 1.5)
            thirds[index2, index1, index0] = value / (pc.amu ** 1.5)
            thirds[index0, index2, index1] = value / (pc.amu ** 1.5)
    f.close()
    return thirds


# def ammonia_relabel(index):
#     #version 1
#     if index == "1a":
#         return 1
#     elif index == "1b":
#         return 2
#     elif index == "2":
#         return 3
#     elif index == "3a":
#         return 4
#     elif index == "3b":
#         return 5
#     elif index == "4":
#         return 6

def ammonia_relabel(index):
    # version 2
    if index == "1":
        return 1
    elif index == "2":
        return 2
    elif index == "3":
        return 3
    elif index == "4a":
        return 4
    elif index == "4b":
        return 5
    elif index == "5":
        return 6


def build_fourths(filename, size, label=""):
    """ Write the fourth derrivatives from a log file to fourths_label.txt

    I have moved these to aderivatives directory. I do not think they are actuall called anyhere. But if there's an error that it cant find these files, theyve moved to the derivatives directory

    """

    os.makedirs(os.path.dirname('derivatives/fourths' + label + '.txt'), exist_ok=True)

    f = open(filename, 'r')
    outf = open('derivatives/fourths' + label + '.txt', 'w')
    reading = False
    for i, line in enumerate(f):

        if '4th derivatives larger than' in line:
            reading = False
        if reading:
            outf.write(line)
        # Michael: change finding starting line for extracting data
        if 'FI(I,J,K,L)' in line and 'k(I,J,K,L)' in line and 'K(I,J,K,L)' in line:
            reading = True

    f.close()
    outf.close()

    f = open('derivatives/fourths' + label + '.txt', 'r')

    # print("Fourths")
    if "a" in f.read():
        # print("nh3 degeneracy case")
        nh3_deg = True
    else:
        # print("normal case")
        nh3_deg = False
    f.close()

    # Comment to activate reordering for high symmetric NH3
    nh3_deg = False

    f = open('derivatives/fourths' + label + '.txt', 'r')

    fourths = np.zeros((size, size))

    # Michael addition: Remove specific mode number contributions - ToDO
    # exclude_mode = 240
    # exclude_mode = int(exclude_mode - 1)

    for line in f:
        if len(line.split()) > 4:

            if not nh3_deg:
                index0 = int(line.split()[0]) - 1
                index1 = int(line.split()[1]) - 1
                index2 = int(line.split()[2]) - 1
                index3 = int(line.split()[3]) - 1
            else:
                # For NH3 degeneracy
                index0 = int(ammonia_relabel(line.split()[0])) - 1
                index1 = int(ammonia_relabel(line.split()[1])) - 1
                index2 = int(ammonia_relabel(line.split()[2])) - 1
                index3 = int(ammonia_relabel(line.split()[3])) - 1

            if (index0 == index1) and (index2 == index3):
                # Michael addition: Remove specific mode number contributions - ToDo
                # if index0 == exclude_mode or index1 == exclude_mode or index2 == exclude_mode or index3 == exclude_mode:
                #    value = float(0.0)
                # else:
                #    value = float(line.split()[6])
                value = float(line.split()[6])

                fourths[index0, index2] = value / pc.amu ** 2
                fourths[index2, index0] = value / pc.amu ** 2
    f.close()
    return fourths


def calc_microcannonical(crpfilename, rstatesfilename, barrier):
    crp = pd.read_csv(crpfilename)

    r_states = pd.read_csv(rstatesfilename, skiprows=[0])
    r_states['E_ref'] = r_states['Energy']
    r_states['E(cm)'] = r_states['E_ref'] * pc.hartree_to_cm / pc.hartree_to_ev

    crp['CRP'] = crp['CRP']  # / crp['CRP'][len(crp)-1]
    crp['E(cm)'] = crp['Energy'] * pc.hartree_to_cm / pc.hartree_to_ev

    energy_list = list(r_states['E(cm)'])

    e_step = r_states['E(cm)'][1] - r_states['E(cm)'][0]
    zpe = r_states['E(cm)'][0]
    stagger = int(zpe / e_step)

    k_df = pd.DataFrame()
    k_df['Energy(cm)'] = crp['E(cm)']
    k_df['CRP'] = crp['CRP']
    k_df['r_states'] = r_states[' # of states']
    k_df['rate'] = k_df['CRP'] / (pc.h * k_df['r_states'])
    k_df['logk'] = np.log10(k_df['rate'])

    plt.plot(r_states['E(cm)'], (k_df['r_states']))
    plt.show()

    plt.plot(k_df['Energy(cm)'], (k_df['CRP']))
    plt.show()

    plt.plot(k_df['Energy(cm)'], k_df['logk'])
    plt.show()
    pass


def calc_microcannonical_2(crpfilename, rstatesfilename):
    """ Calcualte the microcannonical rate from the TS CRP and Reactant states output file
     NOTE: This is untested and possible contains bugs"""

    crp = pd.read_csv(crpfilename)

    r_states = pd.read_csv(rstatesfilename, skiprows=[0])

    k_df = pd.DataFrame()
    k_df['Energy'] = crp['Energy']
    k_df['r_states'] = r_states[' # of states']
    k_df['crp'] = crp['CRP']
    k_df['rate'] = k_df['crp'] / ((2 * 3.33582657 * 10 ** (-11)) * k_df['r_states'])

    e_list = list(k_df['Energy'] * pc.hartree_to_cm)
    k = list(k_df['rate'])
    logk = list(np.log10(k_df['rate']))

    return [e_list, k, logk]


def calc_rrkm(r_statesfile, ts_statesfile, barrier):
    """ Calcualte the microcannonical classical rate from the TS CRP and Reactant states output file
     NOTE: This is untested and possible contains bugs"""

    print(barrier * pc.hartree_to_cm)
    ts = pd.read_csv(ts_statesfile, skiprows=[0])
    r = pd.read_csv(r_statesfile, skiprows=[0])

    ts['rel'] = ts['Energy'] - ts['Energy'][0]
    r['rel'] = r['Energy'] - r['Energy'][0]

    e_step = ts['Energy'][1] - ts['Energy'][0]

    barrier_steps = int(barrier / e_step)
    k = []
    e_list = []
    print((pc.h / pc.hartree_to_joule))
    for i in range(barrier_steps, 5998):
        rate = ts[' # of states'][i - barrier_steps] / ((2 * 3.33582657 * 10 ** (-11)) * r[' # of states'][i])
        e_list.append(ts['rel'][i] * pc.hartree_to_cm)
        k.append(rate)
    k = np.asarray(k)
    logk = np.log10(k)

    plt.plot(r['rel'] * pc.hartree_to_cm, np.log10(r[' # of states']))
    plt.show()

    plt.plot(ts['rel'] * pc.hartree_to_cm, np.log10(ts[' # of states']))
    plt.show()

    # plt.plot(e_list, k)
    # plt.show()
    #
    # plt.plot(e_list, logk)
    # plt.show()

    print(barrier_steps)

    return [e_list, k, logk]


def generate_full_set_of_hessians(ts_name, n_atoms, dq, dir_path, unpaired_electron, basis_set, linear=False):
    dof = (n_atoms * 3) - 6

    for i in range(dof):
        gg.generate_anharm_gaussian_files(ts_name, n_atoms, dq, dir_path, active_mode=i, hessian=True, rd_file=True, linear=linear, unpaired_electron=unpaired_electron, method=basis_set, n_steps=2)
    pass


# if __name__ == '__main__':
#
#     print(barrier_steps)
#
#     old_barrier = 0.0258947383608
#
#     [e_list_1, sctst, logsctst] = calc_microcannonical_2("criegee/FD_files/CRP_W_criegee_FD.txt", "criegee/FD_files/States_Output_r.txt")
#     [e_list_2, oned_sctst, logonedsctst] = calc_microcannonical_2("criegee/FD_files/CRP_W_ts_harmonic.txt", "criegee/FD_files/States_Output_r_harmonic.txt")
#
#
#     [e_list, RRKM, logrrkm] = calc_rrkm("criegee/FD_files/States_Output_ts_harmonic.txt", "criegee/FD_files/States_Output_r_harmonic.txt", 0.0258947383608)
#
#     plt.plot(e_list, logrrkm, label="RRKM")
#     plt.plot(e_list_1, logsctst, label="FD SCTST")
#     plt.plot(e_list_2, logonedsctst,label="1D-SCTST")
#     plt.legend()
#     plt.show()
#
#     plt.plot(e_list, RRKM, label="RRKM")
#     plt.plot(e_list_1, sctst, label="FD SCTST")
#     plt.plot(e_list_2, oned_sctst,label="1D-SCTST")
#     plt.legend()
#     plt.show()
#


if __name__ == '__main__':
    find_ts_higher_vib_state("test_files/States_Output_ts.txt")
    # old_barrier = 0.0258947383608
    #
    # [e_list_1, sctst, logsctst] = calc_microcannonical_2("criegee/FD_files/CRP_W_criegee_FD.txt", "criegee/FD_files/States_Output_r.txt")
    # [e_list_2, oned_sctst, logonedsctst] = calc_microcannonical_2("criegee/FD_files/CRP_W_ts_harmonic.txt", "criegee/FD_files/States_Output_r_harmonic.txt")
    #
    #
    # [e_list, RRKM, logrrkm] = calc_rrkm("criegee/FD_files/States_Output_ts_harmonic.txt", "criegee/FD_files/States_Output_r_harmonic.txt", 0.0258947383608)
    #
    # plt.plot(e_list, logrrkm, label="RRKM")
    # plt.plot(e_list_1, logsctst, label="FD SCTST")
    # plt.plot(e_list_2, logonedsctst,label="1D-SCTST")
    # plt.legend()
    # plt.show()
    #
    # plt.plot(e_list, RRKM, label="RRKM")
    # plt.plot(e_list_1, sctst, label="FD SCTST")
    # plt.plot(e_list_2, oned_sctst,label="1D-SCTST")
    # plt.legend()
    # plt.show()
