import matplotlib.pyplot as plt
import numpy as np


def std_to_numeric(inputstring):
    """
    Turns string formatted number from fchk to float
    :param inputstring: number of the form "1.039302927293E+07" to be converted to a float
    :return: Float e.g. 10393029.27293
    """
    prefactor = float(inputstring[0:-4])
    exponent = float(inputstring[-3:])
    result = prefactor * (10 ** exponent)
    return result


def get_ir_coords(file_name, atomnumber, atommass):
    """
    Get the coordinates from a VPT2 log file
    """
    coords = []

    key_word = "    {:d}          {:d}           ".format(atomnumber, atommass)

    searchfile = open(file_name, "r")
    for line in searchfile:
        if key_word in line:
            coords.append(np.asarray([float(line.split()[2:][1]), float(line.split()[2:][2]), float(line.split()[2:][3])]))
    searchfile.close()
    if len(coords) == 0:
        return -1
    else:
        return coords[-1]


def get_bond_length(filename, atom1, atom2, atom1mass, atom2mass):
    """
    Get the bond length between atom1 and atom 2 from a vpt2 log file
    """
    coords1 = np.asarray(get_ir_coords(filename, atom1, atom1mass))
    coords2 = np.asarray(get_ir_coords(filename, atom2, atom2mass))
    bond = coords1 - coords2

    bond_length = np.linalg.norm(bond)

    return bond_length


def plot_projected_path(filename, atom1, atom2, atom1mass, atom2mass, atom3, atom4, atom3mass, atom4mass):
    bond1 = get_bond_length(filename, atom1, atom2, atom1mass, atom2mass)

    bond2 = get_bond_length(filename, atom3, atom4, atom3mass, atom4mass)

    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(111)
    ax.scatter(bond1, bond2)
    plt.xlabel("Bond Length: " + str(atom1) + "-" + str(atom2))
    plt.ylabel("Bond Length: " + str(atom3) + "-" + str(atom4))
    plt.show()

    pass


def get_irc_plot(file_name):
    """
    Returns a list of energies and coordinates from an IRC gaussian log file
    :param filename: 
    :return: [coords, energies]
    """

    coords = []

    searchfile = open(file_name, "r")
    for line in searchfile:
        if "NET REACTION COORDINATE UP TO THIS POINT" in line:
            coord = line.split()[-1]
            coords.append(float(coord))
    searchfile.close()

    energies = []
    searchfile = open(file_name, "r")
    for line in searchfile:
        if "Corrected End Point Energy" in line:
            energy = line.split()[-1]
            energies.append(float(energy))
    searchfile.close()

    # need to minus sign the first half of coords
    n = len(coords)
    for i in range(int(n / 2)):
        coords[i] = coords[i] * (-1)

    for i in range(n):
        energies[i] = energies[i] - energies[-1]
    return [coords, energies]


def main():
    plot_projected_path("criegee_small_irc.log", 5, 6, 6, 1, 4, 6, 8, 1)
    exit()

    [coords, energies] = get_irc_plot('irc.log')

    print(coords)
    plt.scatter(coords, energies)
    plt.show()

    # plt.plot(energies)
    # plt.show()


if __name__ == '__main__':
    main()
