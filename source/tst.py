"""
Functions to do TST calculations
"""

import math

import numpy as np

import physical_constants as pc


def calculate_rate(r1, r2, ts, t_list, sigma):
    """
    Calculates the Transition state throey rate
    :param r1: reactant 1
    :param r2: reactat 2
    :param ts: transition state
    :param t_list: temperatures
    :param sigma: symmetry number change
    :return: TST rate list
    """
    barrier = ((ts.energy - r1.energy - r2.energy) * pc.hartree_to_joule) + ts.zpe - r1.zpe - r2.zpe
    rate_list = []
    for i in range(len(t_list)):
        rate = (ts.q_tot[i] / (r1.q_tot[i] * r2.q_tot[i])) * ((pc.kb * t_list[i]) / (2 * math.pi * pc.hbar)) * math.e ** (((-1) * barrier) / (pc.kb * t_list[i]))
        rate_list.append(sigma * rate * 100 ** 3)  # convert to molecules / cm3 from molecules /m3
    return np.asarray(rate_list)


def calculate_deformed_rate(r1, r2, ts, t_list, sigma):
    """
    Deformed TST is a correction to TST. See https://link.springer.com/content/pdf/10.1007%2Fs11224-019-01437-3.pdf
    :param r1: reactant 1
    :param r2: reactat 2
    :param ts: transition state
    :param t_list: temperatures
    :param sigma: symmetry number change
    :return: TST rate list
    """
    barrier = ((ts.energy - r1.energy - r2.energy) * pc.hartree_to_joule) + ts.zpe - r1.zpe - r2.zpe

    atomic_barrier = barrier / pc.hartree_to_joule
    d = -(1 / 3) * (ts.atomic_imaginary_frequency / (atomic_barrier * 2)) ** 2

    rate_list = []
    for i in range(len(t_list)):
        rate = (ts.q_tot[i] / (r1.q_tot[i] * r2.q_tot[i])) * ((pc.kb * t_list[i]) / (2 * math.pi * pc.hbar)) * (1 - ((d * barrier) / (pc.kb * t_list[i]))) ** (1 / d)
        rate_list.append(sigma * rate * 100 ** 3)  # convert to molecules / cm3 from molecules /m3
    return np.asarray(rate_list)


def calculate_unimolecular_rate(r1, ts, t_list, sigma):
    """
    Calculates the Transition state throey rate
    :param r1: reactant 1
    :param ts: transition state
    :param t_list: temperatures
    :param sigma: symmetry number change
    :return: TST rate list
    """
    barrier = ((ts.energy - r1.energy) * pc.hartree_to_joule) + ts.zpe - r1.zpe
    rate_list = []
    for i in range(len(t_list)):
        rate = (ts.q_tot[i] / (r1.q_tot[i])) * ((pc.kb * t_list[i]) / (2 * math.pi * pc.hbar)) * math.e ** (((-1) * barrier) / (pc.kb * t_list[i]))
        rate_list.append(sigma * rate)  # Units s-1
    return np.asarray(rate_list)


def calculate_anharmonic_unimolecular_rate(r1, ts, t_list, sigma):
    """
    Calculates the Transition state throey rate using anhrmonic vibrational partition functions
    :param r1: reactant 1
    :param ts: transition state
    :param t_list: temperatures
    :param sigma: symmetry number change
    :return: TST rate list
    """
    barrier = ((ts.energy - r1.energy) + ts.anharmonic_zpe - r1.anharmonic_zpe + ts.fd_g0 - r1.fd_g0) * pc.hartree_to_joule
    rate_list = [sigma * (ts.q_rot[i] * ts.anharmonic_vib_pf[i] / (r1.q_rot[i] * r1.anharmonic_vib_pf[i])) * ((pc.kb * t_list[i]) / (2 * math.pi * pc.hbar)) * math.e ** (((-1) * barrier) / (pc.kb * t_list[i])) for i in
                 range(len(t_list))]
    return np.asarray(rate_list)


def calculate_anharmonic_bimolecular_rate(r1, r2, ts, t_list, sigma):
    """
    Calculates the Transition state throey rate using anhrmonic vibrational partition functions
    :param r1: reactant 1
    :param ts: transition state
    :param t_list: temperatures
    :param sigma: symmetry number change
    :return: TST rate list
    """
    barrier = ((ts.energy - r1.energy - r2.energy) + ts.anharmonic_zpe - r1.anharmonic_zpe - r2.anharmonic_zpe + ts.fd_g0 - r1.fd_g0 - r2.fd_g0) * pc.hartree_to_joule
    rate_list = [sigma * 100 ** 3 * (ts.q_rot[i] * ts.anharmonic_vib_pf[i] * ts.q_trans[i] / (r1.q_rot[i] * r1.q_trans[i] * r2.q_trans[i] * r1.anharmonic_vib_pf[i] * r2.q_rot[i] * r2.anharmonic_vib_pf[i])) * (
                (pc.kb * t_list[i]) / (2 * math.pi * pc.hbar)) * math.e ** (((-1) * barrier) / (pc.kb * t_list[i])) for i in range(len(t_list))]
    return np.asarray(rate_list)
