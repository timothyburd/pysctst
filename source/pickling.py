import os
import pickle


def pickle_list(list, filename):
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    pickle_out = open(filename, "wb")
    pickle.dump(list, pickle_out)
    pickle_out.close()


def unpickle_list(filename):
    pickle_in = open(filename, "rb")
    list = pickle.load(pickle_in)

    return list
