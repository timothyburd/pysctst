"""
# Timothy Burd 2017
# SCTST calculator

# This module contains the key classes required to perform an ab initio SCTST calculation in full or one dimension
# An example calculation is in the folder ch3_ch4
# In general the idea is you:
# 1) Create an object 'Chemical' for the reactants and  products which requires at least one fchk file in a folder
# 'Checkpoint_files'
# NOTE if one of the reactants is a single atom, use the Atom class (its difference  because a single atom has no
# vibrational or rotational contribution)
# 2) Create an object 'Transition State' for the TS
# 3) Create an object 'Reaction' or 'UnimolecilarReaction' which accepts the reactant and TS and product objectsa
# as arguments
# 4) You can plot rates etc by doing reactionname.Plot_rates()
# 5) If you wish to compare reaction rates, create an object 'Series' which accepts reaction objects as arguments
# 6) You can then do something like series.compare_rates() to plot all the rates together
"""

import cmath
import math
import os
from operator import add

import matplotlib.cm as cm
import matplotlib.pyplot as plt
import mpl_toolkits.axisartist as AA
import numpy as np
import pandas as pd
import seaborn as sns
from mpl_toolkits.axes_grid1 import host_subplot
from numpy import linalg as LA

import approximate_FD as af
import extract_anharm_matrix as ex
import full_dimensional as fd
import generate_gaussian as gg
import new_perspective as ti  # ("T"heta "I"ntegral)
import oneD_sctst_VPTn as od  # Now in tom_youngs_project directory
import partiton_function as pf
import physical_constants as pc
import read_gaussian as rg
import rotation_matrix as rotation
import sct_MEP_method as sct
import semi_classical as sc
import tst
import vpt4

# Location of formatted checkpoint files from gaussian
checkpoint_root = "checkpoint_files/"


class Atom:
    """
    Represents a species that is a single atom, and so has no vibrational or rotational partition function
    """

    def __init__(self, mass, energy, temps, basis, name="", high_level_energy=True):
        """
        mass: Mass of the atom
        energy: The energy e.g. from a gaussian file of the atom (Hartrees)
        temps: The list of temperatures of interest (K)
        basis: string: the basis set used e.g. "cc-pVTZ"
        name: string: assign a unique name. If name is given, I will find the atoms energy from name_freq.fchk
        high_level_energy: bool: True if there is a name_energy.fchk file from which the energy can be read
        """
        if name == "":
            self.energy = energy
        else:
            self.energy = rg.find_energy(
                "checkpoint_files/" + name + "_energy.fchk") if high_level_energy else rg.find_energy(
                "checkpoint_files/" + name + "_freq.fchk")

        self.mass = [mass]
        self.coordinates = [[0.0, 0.0, 0.0]]
        self.reduced_moment_of_inertia = 0
        self.hessian = [[0.0, 0.0, 0.0], [0.0, 0.0, 0.0], [0.0, 0.0, 0.0]]
        self.symmetry_number = 1
        self.frequencies = []
        self.imaginary_frequency = 0
        self.q_trans = pf.q_translational(self.mass, temps)
        self.q_vib = [1 for t in temps]
        self.moment_of_inertia = 0.0
        self.q_rot = [1 for t in temps]
        self.q_tot = self.q_trans
        self.zpe = 0.0
        self.zpe_kj_per_mol = self.zpe * pc.avogadro / 1000
        self.zpe_hartrees = self.zpe / pc.hartree_to_joule
        self.basis_set_freq = basis
        self.anharmonic_zpe = 0
        self.fd_g0 = 0
        self.name = "atom"


class Chemical:
    def __init__(self, name, n_atoms, temps, sigma=1, mass_list=None, hr_option="Harmonic", hr_groups=[[0]],
                 eigen_list=[], linear=False, hr_index=0, high_level_energy=True, stationary_point=True,
                 vpt2_log=False, vpt2_log_name="", run_dos=True,
                 vscf=False, intermolecular_correction=False, intermolecular_modes=0,
                 dissociation_energy=9 * 10 ** (30),
                 harmonic_bound_modes=False):
        """
        Class representing a STABLE chemical species. Contains all the partition function info, energies, DOFs etc
        :param name: NOTE formatted checkpoint file must called (name+"_energy.fchk" and name+"_freq.fchk") 
        for the frequency and single point energy calculations respectivly
        :param n_atoms: int: Number of atoms
        :param temps: list: List of temperatures to run the calculation at (in kelvin)
        :param sigma: int: rotational symmetry number
        :param mass_list: list: List of masses of atoms in atomic units (this can be read from the gaussian files, but may be useful for e.g. for isotopic substitution)
        :param hr_option: string: Treatment of the hindered rotor degree of freedom ("harmonic, tanh, eigen"). Note that tanh or eigen require further inputs.
        :param hr_groups: list of three lists: if using hr_option = "tanh", list of [rotating group 1, reacting group 2, axis]
        :param eigen_list: list: if hr_option == "eigen", must provide the hr partiton function (e.g. calculated using mathematica)
        :param high_level_energy: bool: specifies if the species has a single-point energy fchk file (name_energy.fchk) seperate from the _freq file
        :param stationary_point: bool: True if this corresponds to a minimum/saddle, false if not
        :param vpt2_log: bool: if there is a name_vpt2.log file with VPT2 data in it available e.g. for a FD calcualtion
        :param vpt2_log_name: string: if this is not name_vpt2.log please specipy it here!
        :param run_dos: bool: If you want to find the anharmonic density of states, you need to do a DOS calcualtion. The results are saved though, so only need to run once ever!
        :param other: Unlikely to be required
        """
        # Make sure there is a plots directory here
        os.makedirs(os.path.dirname("plots/"), exist_ok=True)

        # ---------- Basic Properties -------------------------------------------------------------------------------- #
        self.temps = temps
        self.name = name
        self.symmetry_number = sigma
        self.nAtoms = n_atoms
        self.linear = linear
        self.harmonic_bound_modes = harmonic_bound_modes
        if linear:
            self.DOF = 3 * self.nAtoms - 5
        else:
            self.DOF = 3 * self.nAtoms - 6
        self.dipole = rg.find_dipole(checkpoint_root + name + "_freq.fchk", linear=linear)
        self.polarisability = rg.find_polarisability(checkpoint_root + name + "_freq.fchk", linear=linear)
        self.basis_set_freq = rg.find_basis_set(checkpoint_root + name + "_freq.fchk")
        if high_level_energy:
            gg.generate_energy_gaussian_files(name, n_atoms, linear=linear, unpaired_electron=False,
                                              method=self.basis_set_freq)  # Generates Gaussian input file that will find the single point energy
        self.energy = rg.find_energy(checkpoint_root + name + "_energy.fchk") if high_level_energy \
            else rg.find_energy(checkpoint_root + name + "_freq.fchk")  # Read from the fchk file
        self.mass = rg.find_mass(checkpoint_root + name + "_freq.fchk") if mass_list is None else mass_list
        self.coordinates = rg.find_coordinates(checkpoint_root + name + "_freq.fchk", n_atoms)
        self.reduced_moment_of_inertia = pf.reduced_inertia(self.coordinates, self.mass, hr_groups) \
            if hr_option == "tanh" else 0
        self.hessian = rg.find_hessian(checkpoint_root + name + "_freq.fchk", n_atoms)
        self.calc_details = [rg.find_calulation_details(checkpoint_root + name + "_freq.fchk")]
        # ------------------------------------------------------------------------------------------------------------ #

        # ---------- More specialised parameters, only used in rare situations --------------------------------------- #
        self.vscf = vscf
        self.intermolecular_modes = intermolecular_modes  # NUMBER of low frequency intermolecular modes (default zero)
        self.dissociation_energy = dissociation_energy
        self.eigen_list = eigen_list
        self.intermolecular_correction = intermolecular_correction
        self.hr_option = hr_option
        # ------------------------------------------------------------------------------------------------------------ #

        if stationary_point:  # default True
            if high_level_energy:  # default True
                self.calc_details.append(rg.find_calulation_details(checkpoint_root + name + "_energy.fchk"))
                self.calc_details.append(rg.find_calulation_details(checkpoint_root + name + "_energy.fchk"))
                self.basis_set_energy = rg.find_basis_set(checkpoint_root + name + "_energy.fchk")
            self.frequencies = pf.find_frequencies(self.hessian, self.mass, linear)  # cm-1
            self.atomicfrequencies = [f / pc.hartree_to_cm for f in self.frequencies]  # converts cm-1 to atomic units
            self.imaginary_frequency = pf.find_imaginary_frequency(self.hessian, self.mass)
            self.imaginary_vector = pf.find_imaginary_vector(self.hessian, self.mass)
            self.atomic_imaginary_frequency = self.imaginary_frequency / pc.hartree_to_cm
            self.q_trans = pf.q_translational(self.mass, temps)
            self.q_vib = pf.q_vib(self.frequencies, temps, hr_option, self.reduced_moment_of_inertia,
                                  sigma=self.symmetry_number, eigen_list=self.eigen_list, hr_index=hr_index)
            self.moment_of_inertia = pf.moment_of_intertia(self.coordinates, self.mass)
            self.q_rot = pf.q_rotational(self.moment_of_inertia, temps)
            self.q_tot = [self.q_trans[i] * self.q_vib[i] * self.q_rot[i] for i in range(len(temps))]
            self.zpe = pf.zpe(self.frequencies, vscf=self.vscf, name=self.name)
            self.zpe_kj_per_mol = self.zpe * pc.avogadro / 1000.0
            self.zpe_hartrees = self.zpe / pc.hartree_to_joule
            [self.mode_eigenvalues, self.normal_modes] = pf.find_normal_vectors(hessian=self.hessian, masses=self.mass,
                                                                                linear=self.linear)

            if high_level_energy:  # default True, requires a _energy.fchk file
                # Check the _energy and the _freq structures are the same by comparing inertia
                energy_mass = rg.find_mass(checkpoint_root + name + "_energy.fchk")
                energy_coords = rg.find_coordinates(checkpoint_root + name + "_energy.fchk", n_atoms)
                energy_inertia = pf.moment_of_intertia(energy_coords,
                                                       energy_mass)  # will compare this to the freq structure inertia lower down
                if not (math.isclose(self.moment_of_inertia[0], energy_inertia[0], rel_tol=0.00001)
                        or math.isclose(self.moment_of_inertia[0], energy_inertia[1], rel_tol=0.00001) or math.isclose(
                    self.moment_of_inertia[0], energy_inertia[2], rel_tol=0.00001)):
                    if mass_list is None:  # if you artificially change the masses e.g. isotopic substitution, this will flag up! So this line avoids unnecessary warnings.
                        print("WARNING")
                        print("The energy and frequency structures for species", name, "are not the same!",
                              "They are ", self.moment_of_inertia, " and ", energy_inertia)

        # ---------- FD Calculation  --------------------------------------------------------------------------------- #
        if len(vpt2_log_name) > 0:
            self.vpt2_log = "FD_files/" + vpt2_log_name + "_vpt2.log"
        else:
            self.vpt2_log = "FD_files/" + name + "_vpt2.log"
        if vpt2_log:  # default False, True implies we are doing a FD calculation
            self.vpt2_freqs = fd.find_freqs(self.vpt2_log)
            self.thirds = fd.build_thirds(self.vpt2_log, self.DOF, self.name)  # Return array of f_{ijk} derivatives
            self.fourths = fd.build_fourths(self.vpt2_log, self.DOF, self.name)  # Return array of f_{iijj} derivatives
            self.fd_g0 = fd.calc_g0(self.vpt2_freqs, self.thirds, self.fourths, self.DOF)
            self.write_states_input()  # Writes the input file for the density of states calculation
            if run_dos:  # default True.
                print("Running DOS calculation for ", self.name)
                os.system(
                    'gcc -O2 -o dos.exe ../../density_of_states.c')  # Compile the c program that performs the density of states calcualtion

                if not os.path.exists('FD_files/States_Output_' + self.name + '.txt'):
                    with open('FD_files/States_Output_' + self.name + '.txt', 'w'): pass

                import subprocess

                args = 'dos.exe FD_files/' + self.name + '_states_input.txt FD_files/States_Output_' + self.name + '.txt > dos.log'  # String to execute in shell

                output_win = subprocess.run(args, capture_output=True, check=False, shell=True,
                                            text=True)  # First try windows version
                # Check if error occurs and if error is related to dos.exe not found
                if "dosDegenerate nh3 case.exe: command not found" in output_win.stderr:
                    print("Error found that might relate to treating OS as windows. Try MAC version")
                    output_mac = subprocess.run("./" + args, capture_output=True, check=True, shell=True,
                                                text=True)  # MAC version
                else:
                    if not output_win.stderr is None and not output_win.stderr == "":  # Error not related to operating system version. Raise error.
                        raise Exception(output_win.stderr)

                print("Finished DOS calculation for ", self.name)

            # Can now find the anharmonic partiton functions from the results of the DOS calculation
            self.anharmonic_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + self.name + ".txt", i) for i in
                                      self.temps]
            self.anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.name + ".txt")

    def print_coordinates_gaussian_format(self):
        """
        Writes a gaussian input file for a high level energy single point calculation called name_energy.fchk. 
        Could be improved for ease of use.
        """
        gg.generate_energy_gaussian_files(self.name, self.nAtoms, linear=False, unpaired_electron=False,
                                          method=self.basis_set_freq)

    def write_approximate_states_input(self):
        raise ValueError("You only should be running an afd states calculation for a Transition State")

    def plot_anharmonic_dos(self):
        """
        Plots the anharmonic DOS as a function of energy at various temperatures
        """
        if not self.vpt2_log:
            print("This is only available for molecules with a VPT2 log file")
        else:
            df = pd.read_csv("FD_files/States_Output_" + self.name + ".txt", skiprows=1)
            df.plot(x='Energy', y=' # of states')
            plt.show()

            T = [200, 500, 1000, 2000]  # temperatures at which to plot the dos
            for t in T:
                df['Boltzman'] = np.exp(-1 * df['Energy'] * pc.hartree_to_joule / (pc.boltzman * t))
                df['pf_contribution' + str(t)] = df['Boltzman'] * df[' # of states']
                df['log_contribution' + str(t)] = np.log10(df['pf_contribution' + str(t)])
            df.plot(x='Energy', y='pf_contribution500', label=str(t))
            plt.legend()
            plt.show()

            df.plot(x='Energy', y='log_contribution200', label=str(t))
            plt.legend()
            plt.show()

    def plot_anharmonic_matrix(self):
        """
        Plot a heat map of the anharmonic matrix based on the VPT2 calculation
        """
        if not self.vpt2_log:
            print("This is only available for molecules with a VPT2 log file")
        else:
            ax = sns.heatmap(self.xmat * pc.hartree_to_cm, linewidth=0.5, cmap='bwr', square=True, center=0.0)
            plt.savefig("plots/" + self.name + "_x_matrix_heatmap.pdf")
            print("plots/" + self.name + "_x_matrix_heatmap.pdf")
            plt.show()

    def plot_fourths(self):
        """
        Plot a heat map of the fourth order derivatives based on the VPT2 calculation
        """
        abs_fourths = np.absolute(self.fourths)
        plt.imshow(abs_fourths, cmap='binary', interpolation='nearest')
        plt.savefig("plots/fourths_" + self.name + ".pdf")
        plt.show()

    def plot_thirds(self):
        """
        Plot a heat map of the third order derivatives based on the VPT2 calculation
        """
        coupled_thirds = self.thirds[0, :, :]
        abs_thirds = np.absolute(coupled_thirds)
        plt.imshow(abs_thirds, cmap='binary', interpolation='nearest')
        plt.savefig("plots/thirds_" + self.name + ".pdf")
        plt.show()

    def manual_set_energy(self, new_energy):
        """
        May be useful e.g. if using a CBS energy value or you want to artifically change the energy for some reason
        """
        self.energy = new_energy

    def manual_set_frequencies(self, freq_list):
        """
        Can manually set the frequencies, useful e.g. for removing a mode or adding some in. It also recalculates 
        all the needed ZPEs and partition function
        param: Freq_list: List of new frequencies in inverse centimetres
        """
        self.frequencies = freq_list
        self.atomicfrequencies = [f / 219474.6306 for f in self.frequencies]  # converts cm-1 to atomic units
        self.zpe = pf.zpe(self.frequencies, vscf=self.vscf, name=self.name)
        self.zpe_kj_per_mol = self.zpe * pc.avogadro / 1000
        self.zpe_hartrees = self.zpe / pc.hartree_to_joule
        self.q_vib = pf.q_vib(self.frequencies, self.temps, self.hr_option, self.reduced_moment_of_inertia,
                              sigma=self.symmetry_number, eigen_list=self.eigen_list, vscf=self.vscf, name=self.name,
                              intermolecular_correction=self.intermolecular_correction,
                              intermolecular_modes=self.intermolecular_modes,
                              dissocation_energy=self.dissociation_energy)
        self.q_tot = [self.q_trans[i] * self.q_vib[i] * self.q_rot[i] for i in range(len(self.temps))]

    def write_states_input(self):
        """
        required for a FD calculation, writes the input file for the density of states code (densityofstates.c)
        """
        filename = "FD_files/" + self.name + "_states_input.txt"
        f = open(filename, "w")
        f.write("Number of bound modes\n")
        f.write(str(len(self.frequencies)) + "\n")
        f.write("\n")
        f.write("Frequencies (a.u.)\n")
        freq_string = ""
        for t in self.atomicfrequencies[::-1]:
            freq_string += str(t) + ", "
        freq_string = freq_string[:len(freq_string) - 2]  # gets rid of the last comma
        f.write(str(freq_string) + "\n")
        f.write("\n")
        f.write("Coupling anharmonic constants, x_nF (a.u.)\n")
        zero_string = "0," * len(self.frequencies)
        zero_string = zero_string[:len(zero_string) - 1]  # gets rid of final comma
        f.write(zero_string + "\n")
        f.write("\n")
        f.write("Anharmonic Matrix\n")

        x = ex.extract_matrix("FD_files/" + self.name + "_vpt2.log", len(self.atomicfrequencies))
        if not rg.freq_corect_ordering_in_log(self.vpt2_log):  # check if the log file has jumbled the freqs up
            print(
                "Warning: The frequencies are jumbled in the VPT2 log file. I will try and fix this, but maybe double check!",
                "FD_files/" + self.name + "_vpt2.log")
            print("WARNING: THe fix is only done for the x matrix NOT the derivatives. Errors are expected!")
            x = rg.repair_x_matrix(x, "FD_files/" + self.name + "_vpt2.log")
        self.xmat = x
        if not self.harmonic_bound_modes:
            for i in range(len(self.atomicfrequencies)):
                for j in range(len(self.atomicfrequencies)):
                    f.write("%5.9f" % x[i, j])
                    if not j == len(self.atomicfrequencies) - 1:
                        f.write(",")
                f.write("\n")

        else:
            for i in range(len(self.atomicfrequencies)):
                for j in range(len(self.atomicfrequencies)):
                    f.write("%5.9f" % 0)
                    if not j == len(self.atomicfrequencies) - 1:
                        f.write(",")

                f.write("\n")

        f.close()

    def recalculate_partition_functions(self):
        """
        Recalculates all the partition functions
        """
        temps = self.temps
        self.zpe = pf.zpe(self.frequencies, vscf=self.vscf, name=self.name)
        self.zpe_kj_per_mol = self.zpe * pc.avogadro / 1000
        self.zpe_hartrees = self.zpe / pc.hartree_to_joule
        self.q_vib = pf.q_vib(self.frequencies, temps, self.hr_option, self.reduced_moment_of_inertia,
                              sigma=self.symmetry_number, eigen_list=self.eigen_list, vscf=self.vscf,
                              name=self.name, intermolecular_correction=self.intermolecular_correction,
                              intermolecular_modes=self.intermolecular_modes,
                              dissocation_energy=self.dissociation_energy)
        self.q_tot = [self.q_trans[i] * self.q_vib[i] * self.q_rot[i] for i in range(len(temps))]

    def solvate(self, bath_freqs, couplings):
        """
        'solvate' the chemical in a bath of harmonic oscillators (freqs = bath_freqs) which are
        bilinearly coupled to the chemical via the coupling matrix, which is shaped
        n_bath x n_vib
        Not tested, and may not be useful anyway
        """

        bath_hess = np.diag([i * i for i in bath_freqs])
        system_hess = np.diag([i * i for i in self.frequencies])
        hessian = np.bmat([[system_hess, couplings], [np.transpose(couplings), bath_hess]])
        w, v = LA.eig(hessian)  # w is the eigenvectors
        new_freqs = [i ** 0.5 for i in w]

        self.manual_set_frequencies(new_freqs)
        self.recalculate_partition_functions()

    def plot_locality(self):
        """
        Plot the locality of the imaginary mode of the molecule
        return: The overall locality of the mode
        """

        atom = []
        j = 0
        for i in range(self.nAtoms):
            atom.append(
                self.imaginary_vector[j] ** 2 + self.imaginary_vector[j + 1] ** 2 + self.imaginary_vector[j + 2] ** 2)
            j = j + 3
        locality = sum([i * i for i in atom])
        plt.bar(range(1, self.nAtoms + 1), atom)
        plt.text(2, 0.5, "$\epsilon = $ " + "{:.3f}".format(locality))
        plt.xlabel("Atom Number, i")
        plt.ylabel("$C_i$")
        plt.savefig("plots/localisation_im_mode.pdf")
        plt.title("Contribution of each atom to the reaction mode")
        plt.show()

        return locality

    def plot_locality_modes(self):
        """
            Plots the localities of all the normal modes
        """

        [freqs, vectors] = pf.find_normal_vectors(hessian=self.hessian, masses=self.mass, linear=self.linear)
        orderlist = range(len(freqs))

        orderlist = [x for _, x in sorted(zip(freqs, orderlist),
                                          reverse=True)]  # gives the order to pick the freqs so going in descending order

        locality_list = []
        for i in orderlist:
            atom = []
            j = 0
            for k in range(self.nAtoms):
                atom.append(vectors[j, i] ** 2 + vectors[j + 1, i] ** 2 + vectors[j + 2, i] ** 2)
                j = j + 3
            locality_list.append(sum([l * l for l in atom]))

        locality_list.insert(0, locality_list.pop())

        bar_lsit = plt.bar(range(len(locality_list)), locality_list)

        # ----- Get reaction mode locality
        atom_list = []
        j = 0
        for i in range(self.nAtoms):
            atom_list.append(
                self.imaginary_vector[j] ** 2 + self.imaginary_vector[j + 1] ** 2 + self.imaginary_vector[j + 2] ** 2)
            j = j + 3

        reaction_mode_locality = sum([i * i for i in atom_list])

        for i in range(len(bar_lsit)):
            if (abs(locality_list[i] - reaction_mode_locality) / reaction_mode_locality) < 0.000001:
                bar_lsit[i].set_color('r')  # This is the reaction mode
            else:
                bar_lsit[i].set_color('b')

        for k in range(1, 7):
            bar_lsit[-k].set_color('k')  # These are translations/rotations
        plt.ylabel("Locality ($\zeta$) ")
        plt.xlabel("Mode")
        plt.show()

    def plot_locality_overlap(self):
        """
        Plot the overlap with the reaction mode in an atom basis?
        """

        [freqs, vectors] = pf.find_normal_vectors(hessian=self.hessian, masses=self.mass, linear=self.linear)

        # sort vectors by frequency
        orderlist = range(len(freqs))

        orderlist = [x for _, x in sorted(zip(freqs, orderlist), reverse=True)]

        # reaction mode
        atom_reactive = []
        j = 0
        for k in range(self.nAtoms):
            atom_reactive.append(
                self.imaginary_vector[j] ** 2 + self.imaginary_vector[j + 1] ** 2 + self.imaginary_vector[j + 2] ** 2)
            j = j + 3
        net_overlap = []

        # Normalisation constant is the overlap of the reaction mode with itself
        N = sum([k * k for k in atom_reactive])

        for i in orderlist:
            overlap = 0
            atom = []
            locality_list = []
            j = 0
            overlap_list = []
            for k in range(self.nAtoms):
                atom_movement = vectors[j, i] ** 2 + vectors[j + 1, i] ** 2 + vectors[j + 2, i] ** 2
                j = j + 3
                overlap_list.append(atom_movement * atom_reactive[k])
                locality_list.append(atom_movement ** 2)
            net_overlap.append(sum(overlap_list) / (N * sum(locality_list)) ** 0.5)

        # move the reaction mode to the first element of the list
        net_overlap.insert(0, net_overlap.pop())

        bar_lsit = plt.bar(range(len(net_overlap)), net_overlap)

        for i in range(len(bar_lsit)):
            if (abs(net_overlap[i] - 1) < 0.00001):
                bar_lsit[i].set_color('r')  # This is the reaction mode
            else:
                bar_lsit[i].set_color('b')  # This is the reaction mode

        for k in range(1, 7):
            bar_lsit[-k].set_color('c')  # These are translations/rotations
        plt.ylabel("Atomic Overlap ($\zeta$) ")
        plt.xlabel("Mode")
        plt.savefig("plots/localisation_overlap_" + self.name + ".pdf")
        print("plots/localisation_overlap_" + self.name + ".pdf")
        plt.show()

        return net_overlap


class TransitionState(Chemical):
    """
    Transition states have one imaginary frequency.
    For 1D SCTST, 8 single point energy calculations are required along the reactive mode, in order to find xff by richardson extrapolation
    These files must be in a folder labeled 'name', and with names "m4.fchk, m3.fchk m2, m1, p1, p2, p3, p4.fchk and energy_mp2.fchk".
    Input files to calculate these are generated automatically.
    """

    def __init__(self, name, n_atoms, temps, dir_path="", sigma=1, mass_list=None, hr_option="Harmonic", hr_groups=[[]],
                 hr_index=0, eigen_list=[], vscf=False, intermolecular_correction=False,
                 intermolecular_modes=0, dissociation_energy=9 * 10 ** (30), rich_step_size=0.02, vpt2_log=False,
                 vpt2_log_name="", run_dos=True, approximate_fd=False, one_d_plus=False, linear=False,
                 high_level_energy=True, vpt4_data=False, harmonic_bound_modes=False,
                 approx_xif_cutoff=0, unpaired_electron=True, exact_afd=False,
                 use_apprxoximate_xifs=False, sct_X_mode=False):
        """
        :param name: Identifies the relevent checkpoit files e.g. formatted checkpoint file must called (name+"_energy.fchk" and name+"_freq.fchk")
        for the frequenrespectivelyle point energy calculations respectivly
        :param n_atoms: int: Number of atoms
        :param temps: list: List of temperatures to run the calculation at (in kelvin)
        :param dir_path: string: Can specify a different path to files
        :param sigma: int: rotational symmetry number
        :param mass_list: list: List of masses of atoms in atomic units (this can be read from the gaussian files, but may be useful for e.g. for isotopic substitution)
        :param hr_option: string: Treatment of the hindered rotor degree of freedom ("harmonic, tanh, eigen"). Note that tanh or eigen require further inputs.
        :param hr_groups: list of three lists: if using hr_option = "tanh", list of [rotating group 1, reacting group 2, axis]
        :param eigen_list: list: if hr_option == "eigen", must provide the hr partiton function (e.g. calculated using mathematica)
        :param high_level_energy: bool: specifies if the species has a single-point energy fchk file (name_energy.fchk) seperate from the _freq file
        :param rich_step_size float: Size of the step used in richardson extrapolation
        :param stationary_point: bool: True if this corresponds to a minimum/saddle, false if not
        :param vpt2_log: bool: if there is a name_vpt2.log file with VPT2 data in it available e.g. for a FD calcualtion
        :param vpt2_log_name: string: if this is not name_vpt2.log please specipy it here!
        :param run_dos: bool: If you want to find the anharmonic density of states, you need to do a DOS calcualtion. The results are saved though, so only need to run once ever!
        :param other: Unlikely to be required
        """

        self.use_apprximate_xifs = use_apprxoximate_xifs
        self.rich_step_size = rich_step_size
        self.approx_xif_cutoff = approx_xif_cutoff
        self.n_atoms = n_atoms
        self.hr_index = hr_index
        self.hr_option = hr_option
        self.linear = linear
        self.unpaired_electron = unpaired_electron
        self.sct_X_mode = sct_X_mode
        self.vpt4_data = vpt4_data
        self.one_d_plus = one_d_plus

        # All transition states have many of the properties of chemicals e.g. partition functions, frequencies, masses etc.
        # Therefore the TS class inherits from the Chemcial class
        super().__init__(name, n_atoms, temps, sigma, mass_list, hr_option, hr_groups, eigen_list, vscf=vscf,
                         hr_index=hr_index, intermolecular_correction=intermolecular_correction,
                         intermolecular_modes=intermolecular_modes, dissociation_energy=dissociation_energy,
                         vpt2_log=vpt2_log, vpt2_log_name=vpt2_log_name, run_dos=run_dos,
                         linear=linear, high_level_energy=high_level_energy, harmonic_bound_modes=harmonic_bound_modes)

        # generate gaussian input files for richardson extrapolation. These will be found in checkpoint_files/ts_name/*.com
        gg.generate_anharm_gaussian_files(name, n_atoms, rich_step_size, dir_path, linear=linear,
                                          unpaired_electron=unpaired_electron, method=self.basis_set_freq,
                                          mass_list=self.mass)

        # List of energies for use in richardson extrapolation
        self.richard_list = [rg.find_energy(str(checkpoint_root + name + "/" + x + ".fchk")) for x in
                             ['m4', 'm3', 'm2', 'm1', "energy_mp2", 'p1', 'p2', 'p3', 'p4']]
        [self.xff, self.g0, self.third, self.fourth] = sc.anhCalc1D(self.richard_list, rich_step_size,
                                                                    self.imaginary_frequency)
        self.test_parabola()  # Check that the richardson list is an upside down parabola, as expected
        self.Tc = self.atomic_imaginary_frequency * pc.hartree_to_joule / (
            2 * math.pi * pc.boltzman)  # Cross over temperature at which an instanton collapses

        # ------------------------- approximate Full-dimensional -----------------------------------------------------------------------------------------------------------------------------
        if vpt2_log:
            self.xmat = np.asarray(ex.extract_matrix("FD_files/" + self.name + "_vpt2.log",
                                                     len(self.frequencies) + 1))  # has this been cleaned?
            # sometimes the log file jumbles up the x matrix (happens i think when there are degenerate frequencies)
            # This means we need to check the ordering is correct, and fix it if required
            if not rg.freq_corect_ordering_in_log(self.vpt2_log):
                print("Warning: The frequencies are jumbled in the VPT2 log file. I will fix this!",
                      "FD_files/" + self.name + "_vpt2.log")
                print("WARNING: THe fix is only done for the x matrix NOT the derivatives. Errors are expected!")
                self.xmat = rg.repair_x_matrix(self.xmat, "FD_files/" + self.name + "_vpt2.log")
            self.fd_xff = self.xmat[0][0]

        # ------------------------- the Two Hessian Method ----------------------------------------------------------------------------------------------------------------------------
        if one_d_plus:  # requires two hessians, displaced along the reaction coordinate. This will adjust the x_{FF} to be the full d value, calculated  from the derivatives of the hessians
            if vpt2_log and exact_afd:
                xif_exact = self.xmat[0, 1:]  # these are the exact xifs that xif_approx is trying to approximate
                self.imaginary_frequency = self.imaginary_frequency + sum(
                    [0.5 * i for i in xif_exact]) * pc.hartree_to_cm
            else:
                freqs_afd, thirds_afd, fourths_afd, F = af.calc_derrivatives(name, n_atoms, rich_step_size, self.mass)
                self.xff = af.calc_x(freqs_afd, thirds_afd, fourths_afd, F)[
                    F, F].real  # This should already be pure real, but the .real just makes sure there is no +0j bit
                self.fourths_afd = fourths_afd
                self.thirds_afd = thirds_afd
                self.freqs_afd = freqs_afd
                self.F_afd = F
                self.xif_approx = [i if abs(i * pc.hartree_to_cm) > approx_xif_cutoff else 0 for i in
                                   self.return_xif_approx()]  # returns x_[if} with i \neq F

        if sct_X_mode:  # Truhlar inspired method. Untested.
            if one_d_plus:
                raise ValueError(
                    "I dont think you can do one-d-plus AND sct-x-mode as they both modulate the frequency")
            if vpt2_log:
                raise ValueError(" YOu cant do VPTR2 and st_x_mdoe, sorry!")

            # First we need the FD version ox xff, using m1_hess and p1_hess
            freqs_afd, thirds_afd, fourths_afd, F = af.calc_derrivatives(name, n_atoms, rich_step_size, self.mass)
            self.xff = af.calc_x(freqs_afd, thirds_afd, fourths_afd, F)[
                F, F].real  # This should already be pure real, but the .real just makes sure there is no +0j bit

            self.find_path_curvature_vector()
            [x_XF, X_xx] = self.find_sct_coupling()
            self.manual_set_energy(self.energy + 0.25 * X_xx)
            self.manual_set_im_freq(self.atomic_imaginary_frequency + 0.5 * x_XF)

        # --------- VPT4 method --------------------------------#
        if vpt4_data:
            energies = vpt4.find_energy_list('vpt4_files/' + self.name)
            derivatives = vpt4.find_derivatives(energies, 0.0005)
            phi_list = vpt4.calc_phi_list(derivatives)
            self.vpt4_G = vpt4.calc_G(phi_list)
            self.vpt4_W = vpt4.calc_W(phi_list)
            self.vpt4_X = vpt4.calc_X(phi_list)
            self.vpt4_Y = vpt4.calc_Y(phi_list)
            vpt4.plot_energies(energies, 0.0005)

    def plot_X_mode(self):
        """
        Plot the X mode in the truhlar inspired method. Untested.
        """

        if not self.sct_X_mode:
            raise ValueError("Needs to be sct_X_mode for this functino to work!")

        bs = np.asarray(
            [x for _, x in sorted(zip(self.mode_freqs, self.B))])  # get the b matrix in the right order by freuqency
        bs = bs[::-1]  # flip order
        bs = bs[:-6]  # remove rotranslational mdoes
        bs = np.insert(bs, 0, 0)  # add a zero corresponding tot he reaction mode here
        bs = np.delete(bs, np.argmin(self.mode_eigenvalues))
        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)
        ax.bar(range(len(self.mode_freqs) - 6), -bs, zorder=1, color='w', linewidth=2, edgecolor='k')
        plt.xlabel("Mode")
        plt.ylabel("X mode Vector Componant")
        plt.tight_layout()
        plt.savefig("plots/sct_mode_x_" + self.name + ".pdf")
        print("plots/sct_mode_x_" + self.name + ".pdf")
        plt.show()

    def generate_set_of_hessians_FD(self):
        fd.generate_full_set_of_hessians(self.name, self.n_atoms, self.rich_step_size, "", linear=self.linear,
                                         unpaired_electron=self.unpaired_electron, basis_set=self.basis_set_freq)

    def plot_anharmonic_matrix_fourths(self):
        if not self.vpt2_log:
            print("This is only available for molecules with a VPT2 log file")
        else:
            x_fourths = ex.extract_matrix("FD_files/" + self.name + "_vpt2.log", len(self.atomicfrequencies) + 1,
                                          fourths_only=True, thirds_only=False)
            x_thirds = ex.extract_matrix("FD_files/" + self.name + "_vpt2.log", len(self.atomicfrequencies) + 1,
                                         fourths_only=False, thirds_only=True)
            x_total = ex.extract_matrix("FD_files/" + self.name + "_vpt2.log", len(self.atomicfrequencies) + 1,
                                        fourths_only=False, thirds_only=False)
            min_vals = [np.amin(x_fourths), np.amin(x_thirds), np.amin(x_total)]
            min_val = min(min_vals) * pc.hartree_to_cm
            max_vals = [np.amax(x_fourths), np.amax(x_thirds), np.amax(x_total)]
            max_val = max(max_vals) * pc.hartree_to_cm

            fig, ax = plt.subplots(ncols=3, figsize=(15, 5))
            plt.subplot(1, 3, 2)
            ax = sns.heatmap(x_fourths * pc.hartree_to_cm, linewidth=0.5, cmap='bwr', square=True, center=0.0,
                             vmin=min_val, vmax=max_val, cbar=False)
            plt.title("Fourths")

            plt.subplot(1, 3, 1)
            ax = sns.heatmap(x_thirds * pc.hartree_to_cm, linewidth=0.5, cmap='bwr', square=True, center=0.0,
                             vmin=min_val, vmax=max_val, cbar=False)
            plt.title("Thirds")

            plt.subplot(1, 3, 3)
            ax = sns.heatmap(x_total * pc.hartree_to_cm, linewidth=0.5, cmap='bwr', square=True, center=0.0,
                             vmin=min_val, vmax=max_val)
            plt.title("Total")

            name = 'quartic_variational_coupling.pdf'
            plt.savefig("plots/" + self.name + "_x_matrix_fourths_heatmap.pdf")
            print("plots/" + self.name + "_x_matrix_fourths_heatmap.pdf")
            plt.show()

    def find_path_curvature_vector(self):
        """
        Use gradient calculations from displaced geometries to find a 'curvature vector' that points along the direction of path curvature
        This is inspired by Truhlars SCT approximation.
        Also generate two displaced gaussian .com files along this vector called X_minus.com and X_plus.com
        untested method
        :return: none
        """

        # ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # ---------------------- Old Method -------------------------------------------
        # In the old method, I used gradients calculated at geometries displaced along the reaction coordinate.
        # This didn't work very well because these geometries were not really on the MEP, and so their gradient vectors pointed up the sides of the alley, rather than towards the TS
        # This means the result is something super weird
        # NOTE it may actually be that this is not that innacurate at all, and is much cheaper than the MEP method.
        # The new method acrually has to do an IRC clauclation to find points along the MEP, ad use gradients from that.

        # gradient_index = 1  # You can use the gradient vectors at m1, m2, m3 or m4. In my experience they give very similar answers. The lowest index will lie closest to the TS (and so closest to the true MEP).
        # # check the gradient calculations have the same basis set
        # if rg.find_basis_set(checkpoint_root + self.name + "/p" + str(gradient_index) + "_grad.fchk") != self.basis_set_freq:
        #     raise ValueError("Your gradient calculations are in the wrong basis set", self.basis_set_freq, rg.find_basis_set(str(checkpoint_root + self.name + "/p" + str(gradient_index) + "_grad.fchk")))
        #
        # # Now extract the gradient data from the grad files.
        # # You can do this either from the fchk files int he "Cartesian Gradient" section, or fromt he log files int he "Forces" section. They both give the same thing
        # # Both methods are below, the log file one is commented out because its easier to always be using fchk files
        # # NOTE GRADIENTS ARE IN CARTESIAN, NOT MASS WEIGHTED, COORDINATES
        # grad_plus = rg.extract_gradient_vector(str(checkpoint_root + self.name + "/p" + str(gradient_index) + "_grad.fchk"), self.n_atoms)
        # grad_minus = rg.extract_gradient_vector(str(checkpoint_root + self.name + "/m" + str(gradient_index) + "_grad.fchk"), self.n_atoms)

        # # or can get the force constants form the log file:
        # grad_plus = rg.find_forces_vector(str(checkpoint_root + self.name + "/p" + str(gradient_index) + "_grad.log"), self.n_atoms)
        # grad_minus = rg.find_forces_vector(str(checkpoint_root + self.name + "/m" + str(gradient_index) + "_grad.log"), self.n_atoms)
        # ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        # ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
        # ---------------------- New Method -------------------------------------------
        # The new method required you do an IRC clcualtion, giving an irc.log file. The default atm is doing ten points.

        n_points = 10  # this is the default, from the generate_gaussian function
        gradient_index = n_points  # I think the most acurate one will come at the largest displacement along the MEP
        gradient_index = 10  # I think the most acurate one will come at the largest displacement along the MEP

        grad_plus = rg.fetch_gradient_vector(str(checkpoint_root + self.name + "/irc.log"), self.n_atoms,
                                             n_points=n_points, direction='Plus', index=gradient_index)
        grad_minus = rg.fetch_gradient_vector(str(checkpoint_root + self.name + "/irc.log"), self.n_atoms,
                                              n_points=n_points, direction='Minus', index=gradient_index)

        # normalise the grads - we are not interested in the size of the curvature here, but just its direction
        grad_plus /= np.linalg.norm(grad_plus)
        grad_minus /= np.linalg.norm(grad_minus)

        # Find curvature vector in cartesian space
        derivative = (grad_plus + grad_minus)  # It is PLUS because the gradient 'changes sign' either side of the TS
        self.curvature_vector = derivative / np.linalg.norm(
            derivative)  # This is dv/ds in equation 2.36 in Yee's Thesis

        # It actually doesnt matter about dividing by the step size as we normalise anyway (i.e. we are interested in the direction, not the magnitude)

        # We now need to project out any contribution from the reaction mode (or ro-translational modes) to this curvature vector, to get a vector orthogonal to the reaction mode
        # These contributions should be small anyway

        # lets start with mass weighting
        sqrt_masses = [mass ** (-0.5) for mass in self.mass]
        sqrt_mass_list = []
        for i in range(len(sqrt_masses)):
            sqrt_mass_list.append(sqrt_masses[i])
            sqrt_mass_list.append(sqrt_masses[i])
            sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list = np.asarray(sqrt_mass_list)
        self.mass_weighted_curvature_vector = self.curvature_vector / sqrt_mass_list
        self.mass_weighted_curvature_vector /= np.linalg.norm(self.mass_weighted_curvature_vector)

        # Get imaginary index and check it
        F = np.argmin(self.mode_eigenvalues)  # The index of the imaginary mode
        # Check this agrees with what I think the imaginary mode is:
        if not math.isclose((-self.mode_eigenvalues[F] / pc.amu) ** 0.5, self.atomic_imaginary_frequency,
                            rel_tol=10 ** -5):
            raise ValueError("There is an inconsistancy here", self.atomic_imaginary_frequency,
                             (-self.mode_eigenvalues[F] / pc.amu) ** 0.5)

        # We can just check that the last six modes are the ro/translational ones:
        low_freqs = [self.mode_eigenvalues[-i] / self.mode_eigenvalues[-7] for i in [1, 2, 3, 4, 5, 6]]
        for f in low_freqs:
            if f > 0.001:
                raise ValueError("ARe you sure the -1 -> -6 modes are rotranslational?", f)

        # now we subtract out the reaction mode contribution (and ro-translational mode contributions)
        # Note these contributions SHOULD be small, anyway
        self.mass_weighted_curvature_vector -= (
            np.dot(self.mass_weighted_curvature_vector, self.normal_modes[:, F]) * self.normal_modes[:,
                                                                                   F])  # Reaction Mode
        self.mass_weighted_curvature_vector -= np.dot(self.mass_weighted_curvature_vector,
                                                      self.normal_modes[:, -1]) * self.normal_modes[:,
                                                                                  -1]  # Translational Mode
        self.mass_weighted_curvature_vector -= np.dot(self.mass_weighted_curvature_vector,
                                                      self.normal_modes[:, -2]) * self.normal_modes[:,
                                                                                  -2]  # Translational Mode
        self.mass_weighted_curvature_vector -= np.dot(self.mass_weighted_curvature_vector,
                                                      self.normal_modes[:, -3]) * self.normal_modes[:,
                                                                                  -3]  # Translational Mode
        self.mass_weighted_curvature_vector -= np.dot(self.mass_weighted_curvature_vector,
                                                      self.normal_modes[:, -4]) * self.normal_modes[:,
                                                                                  -4]  # Rotational Mode
        self.mass_weighted_curvature_vector -= np.dot(self.mass_weighted_curvature_vector,
                                                      self.normal_modes[:, -5]) * self.normal_modes[:,
                                                                                  -5]  # Rotational Mode
        self.mass_weighted_curvature_vector -= np.dot(self.mass_weighted_curvature_vector,
                                                      self.normal_modes[:, -6]) * self.normal_modes[:,
                                                                                  -6]  # Rotational Mode

        # renormalise the mass_weighted curvature vector
        self.mass_weighted_curvature_vector /= np.linalg.norm(self.mass_weighted_curvature_vector)

        # put back into non-mass weighted cartesian coordinates, so we can put this vector into Gaussian
        self.curvature_vector_orthononal = self.mass_weighted_curvature_vector * sqrt_mass_list

        gg.generate_curvature_hessians(self.name, self.n_atoms, self.rich_step_size, "",
                                       self.curvature_vector_orthononal, unpaired_electron=self.unpaired_electron,
                                       method=self.basis_set_freq)

    def find_sct_coupling(self):
        """
        Using the hessians displaced along the reaction mode and the X mode, find the effective coupling
        Untested.
        :return: x_{XF}
        """

        # first check the basis set is good
        if rg.find_basis_set(str("checkpoint_files/" + self.name + "/X_plus.fchk")) != self.basis_set_freq:
            raise ValueError("Your X_hess calculations are in the wrong basis set", self.basis_set_freq,
                             rg.find_basis_set(str("checkpoint_files/" + self.name + "/X_plus.fchk")))

        # first we need to find the B vector, which is the projection of the curvature vector dG/ds onto each normal mode
        B = np.asarray([np.dot(mode, self.mass_weighted_curvature_vector) for mode in
                        self.normal_modes.T])  # iterating over column self.normal_modes by iterating over through rows of self.normal_modes.T
        self.B = B

        positive_eigenvalues = np.abs(self.mode_eigenvalues)
        mode_freqs = (positive_eigenvalues ** 0.5) / pc.amu ** 0.5
        self.mode_freqs = mode_freqs

        # The componant of B along the reaction mode should be basically zero
        F = np.argmin(self.mode_eigenvalues)
        if B[F] > (1 / 10000):
            raise ValueError("B is not orthogonal to F!")

        # Find effective frequency of the X mode
        omega_eff = np.linalg.norm(np.multiply(B, mode_freqs))  # Equation 2.37 in Yee's Thesis
        omega_X = omega_eff  # probably better nomenclature since 'eff' sounds like F...
        print("OMEGA X is ", omega_X)
        # We can also estimate omega_X from the energies of the geometries displaced along the X mode. This will help check they are in the expected place.
        e_minus = rg.find_energy(str("checkpoint_files/" + self.name + "/X_minus.fchk"))
        e_zero = rg.find_energy(str("checkpoint_files/" + self.name + "/energy_mp2.fchk"))
        e_plus = rg.find_energy(str("checkpoint_files/" + self.name + "/X_plus.fchk"))
        space = np.linspace(-self.rich_step_size * pc.amu ** 0.5, self.rich_step_size * pc.amu ** 0.5)
        potential = 0.5 * omega_X ** 2 * space * space
        # now i want to calculate x_{XF}

        F_xxxx = sct.find_F_xxxx(self.name, self.n_atoms, self.mass, self.rich_step_size,
                                 self.mass_weighted_curvature_vector)

        F_ffff = sct.find_F_ffff(self.name, self.n_atoms, self.mass, self.rich_step_size,
                                 self.mass_weighted_curvature_vector)
        F_ffxx = sct.find_F_ffxx(self.name, self.n_atoms, self.mass, self.rich_step_size,
                                 self.mass_weighted_curvature_vector)
        F_xxx = sct.find_F_xxx(self.name, self.n_atoms, self.mass, self.rich_step_size,
                               self.mass_weighted_curvature_vector)
        F_xxf = sct.find_F_xxf(self.name, self.n_atoms, self.mass, self.rich_step_size,
                               self.mass_weighted_curvature_vector)
        F_xff = sct.find_F_xff(self.name, self.n_atoms, self.mass, self.rich_step_size,
                               self.mass_weighted_curvature_vector)
        F_fff = sct.find_F_fff(self.name, self.n_atoms, self.mass, self.rich_step_size,
                               self.mass_weighted_curvature_vector)

        # We also need to find the rotation matrix, that transforms one of the normal modes onto the X mode
        hess_zero = rg.find_hessian("checkpoint_files/" + self.name + "_freq.fchk", self.n_atoms)
        hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, self.mass)
        w, v = LA.eig(hess_zero_weighted)
        if F == 0:
            first_mode = v[:, 1]
        else:
            first_mode = v[:, 0]
        rot_mat = rotation.find_rot_matrix(first_mode, self.mass_weighted_curvature_vector)
        rot_space = [rot_mat @ v[:, i] for i in range(len(w)) if i != F]

        # now need to find the rotated frequencies
        rot_freqs = []
        for rot_mode in rot_space:
            B_mode = np.asarray([np.dot(mode, rot_mode) for mode in self.normal_modes.T])
            rot_freq = np.linalg.norm(np.multiply(B_mode, mode_freqs))  # Equation 2.37 in Yee's Thesis
            # plt.plot(B_mode, label=str(rot_freq*pc.hartree_to_cm))
            rot_freqs.append(rot_freq)
        # rot_freqs[0] should equal omega_X and rot_space[0] should equal mass_weighted_curvature_mdoe

        # # # # miller 1990 equation 13e.
        term_1 = F_ffxx
        term_2 = (F_xxf * F_fff / self.atomic_imaginary_frequency ** 2)
        term_3 = (2 * F_xff ** 2 / (omega_X ** 2 + 4 * self.atomic_imaginary_frequency ** 2))
        term_4 = (sum(
            [sct.find_F_xxm(self.name, self.n_atoms, self.mass, self.rich_step_size,
                            self.mass_weighted_curvature_vector, rot_space[m]) * sct.find_F_mff(self.name, self.n_atoms,
                                                                                                self.mass,
                                                                                                self.rich_step_size,
                                                                                                self.mass_weighted_curvature_vector,
                                                                                                rot_space[m]) /
             rot_freqs[
                 m] ** 2
             for m in range(len(rot_freqs) - 6)]
        ))
        term_5 = (2 * sum([sct.find_F_xfm(self.name, self.n_atoms, self.mass, self.rich_step_size,
                                          self.mass_weighted_curvature_vector, rot_space[m]) ** 2 * (
                               omega_X ** 2 - self.atomic_imaginary_frequency ** 2 - rot_freqs[m] ** 2) / (
                               (omega_X + rot_freqs[m]) ** 2 + self.atomic_imaginary_frequency ** 2)
                           / ((omega_X - rot_freqs[m]) ** 2 + self.atomic_imaginary_frequency ** 2)
                           for m in range(len(rot_freqs) - 6)]))

        X_xf = (1 / omega_X / self.atomic_imaginary_frequency / 4) * (term_1 + term_2 + term_3 - term_4 + term_5)

        print("TERMS", pc.hartree_to_cm * (1 / omega_X / self.atomic_imaginary_frequency / 4) * term_1,
              pc.hartree_to_cm * (1 / omega_X / self.atomic_imaginary_frequency / 4) * term_2,
              pc.hartree_to_cm * (1 / omega_X / self.atomic_imaginary_frequency / 4) * term_3,
              -pc.hartree_to_cm * (1 / omega_X / self.atomic_imaginary_frequency / 4) * term_4,
              pc.hartree_to_cm * (1 / omega_X / self.atomic_imaginary_frequency / 4) * term_5)

        print("X_XF is...", X_xf * pc.hartree_to_cm)

        # What about X_XX? For a bimiolecualr reaciton, this is probably projecteded onto a zero-mode in the reactants, and so is fair game
        new_term1 = F_xxxx
        newterm2 = F_xxf ** 2 * (
            8 * omega_X ** 2 + 3 * self.atomic_imaginary_frequency ** 2) / self.atomic_imaginary_frequency ** 2 / (
                       4 * omega_X ** 2 + self.atomic_imaginary_frequency ** 2)
        newterm3 = F_xxx * 82 * (5 * omega_X ** 2) / omega_X ** 2 / (3 * omega_X ** 2)
        X_xx = (1 / 16 * omega_X * self.atomic_imaginary_frequency) * (new_term1 + newterm2 - newterm3)
        print("MY X_XX is ", X_xx * pc.hartree_to_cm)
        return [X_xf, X_xx]

    def compare_approximate_xif(self):
        """
        Plot the approximate xifs with the exact xifs
        """

        if not self.one_d_plus or not self.vpt2_log:
            raise ValueError("Needs to be one_d_plus and have vpt2_log for this functino to work!")

        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)
        ax.bar(range(len(self.frequencies)), self.xmat[0, 1:] * pc.hartree_to_cm, label="$x_{if}$", zorder=1, color='w',
               linewidth=2, edgecolor='k')
        ax.scatter(range(len(self.frequencies)), [i * pc.hartree_to_cm for i in self.xif_approx],
                   label="$x_{if}^{approximate}$", zorder=3, marker='x', c='r')
        plt.xlabel("Mode")
        plt.ylabel("$x_{iF} / \mathrm{cm^{-1}}$")
        plt.legend()
        plt.savefig("plots/afd_performance_" + self.name + ".pdf")
        print("plots/afd_performance_" + self.name + ".pdf")
        plt.show()

    def plot_xif(self):

        if not self.vpt2_log:
            raise ValueError("Needs to have a vpt2_log for this functino to work!")

        color_values = self.xmat[0, 1:] - float(min(self.xmat[0, 1:]))
        color_values = color_values / float(max(color_values))
        colors = cm.coolwarm(color_values)

        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)
        ax.bar(range(len(self.frequencies)), self.xmat[0, 1:] * pc.hartree_to_cm, label="$x_{if}$", zorder=1,
               color=colors, linewidth=2, edgecolor='k')
        plt.xlabel("Mode")
        plt.ylabel("$x_{iF} / \mathbf{i} \mathrm{cm^{-1}}$")
        plt.tight_layout()
        plt.legend()
        plt.savefig("plots/xif_plot_" + self.name + ".pdf")
        print("plots/xif_plot_" + self.name + ".pdf")
        plt.show()

    def return_xif_approx(self):
        """
        Return a a vector of approximate values of x_{iF} based on derivatives from two displaced hessians along the reaciton mode
        MUST HAVE one_d_plus=True (and the corresponding m1_hess.fchk, p1_hess.fchk files in the checkpoint files folder
        """

        # ----------------------------------------- Set up derivatives      _afd means its been calculated from the two displaced hessians
        freqs = self.freqs_afd  # I think these should be the same as the normal frequencies, but with the imaginary mode within them

        raw_freqs = self.freqs_afd
        freqs = np.delete(freqs, self.F_afd)  # freqs without the im mode
        fourths = self.fourths_afd[
                  :-6]  # fourths is a list of derivatives of the form f_{iiFF}. [:-6] removes fourths along translational/rotatonal modes
        fourths = np.delete(fourths, self.F_afd)  # removes f_{FFFF}
        fFFF = self.thirds_afd[self.F_afd, self.F_afd]  # f_{FFF}

        # thirds and fourths are not in the same order as thr freqs are
        # -------------- which is what the freqs are in, we thus need to rehuffle the fourths and thirds to match
        orderlist = range(len(freqs))
        orderlist = [x for _, x in sorted(zip(freqs, orderlist), reverse=True)]
        fourths = [fourths[i] for i in orderlist]

        raw_orderlist = range(len(raw_freqs))
        raw_orderlist = [x for _, x in sorted(zip(raw_freqs, raw_orderlist), reverse=True)]

        thirds = np.zeros((len(raw_orderlist), len(raw_orderlist)))
        for counter, index in enumerate(raw_orderlist):
            for counter2, index2 in enumerate(raw_orderlist):
                thirds[counter, counter2] = self.thirds_afd[index, index2]

        freqs = [freqs[i] for i in orderlist]

        # Now we can calculate the approximate x_{iF} terms. There are three terms in its formula, but we only have the derivatives to calcuialte the first and third of these terms
        first_term = [fourths[i] / (4 * freqs[i] * self.atomic_imaginary_frequency) for i in range(len(freqs))]

        # actually the last bit of the second term can be evaluated (when j = f):
        second_term = [(-1) * thirds[i, i] * fFFF / (self.atomic_imaginary_frequency ** 2) for i in
                       range(len(raw_freqs))]
        second_term = np.delete(second_term, self.F_afd)

        third_term = [sum(
            [2 * thirds[i, j] ** 2 * (raw_freqs[i] ** 2 + self.atomic_imaginary_frequency ** 2 - raw_freqs[j] ** 2) /
             (((raw_freqs[i] + self.atomic_imaginary_frequency) ** 2 - raw_freqs[j]) * (
                 (raw_freqs[i] - self.atomic_imaginary_frequency) ** 2 - raw_freqs[j])) for j in range(len(raw_freqs))])
                      / (4 * raw_freqs[i] * self.atomic_imaginary_frequency)
                      for i in range(len(raw_freqs))]
        third_term = np.delete(third_term, self.F_afd)

        # add the two lists together
        first_and_third = list(map(add, first_term, third_term))
        first_second_and_third = list(map(add, first_and_third, second_term))

        return first_second_and_third
        # return first_and_third

    def manual_set_im_freq(self, new_freq):
        """ input the ATOMIC unit of freq"""

        self.atomic_imaginary_frequency = new_freq
        self.imaginary_frequency = new_freq * 219474.6306

    def test_parabola(self):
        # Check that the richardson list is an upside down parabola, as expected
        parabola = True
        diference_list = [self.richard_list[i + 1] - self.richard_list[i] for i in range(len(self.richard_list) - 1)]
        for i in range(int(len(diference_list) / 2)):
            if diference_list[i] < 0:
                parabola = False
                break
        for i in range(int(len(diference_list) / 2) + 1, len(diference_list)):
            if diference_list[i] > 0:
                parabola = False
                break
        if parabola == False:
            # if True:
            print('Warning; The energies in the Richarson extrapolation list of ', self.name,
                  'look like they are not an inverted parabola, as would be expected! Check it out!')
            ax1 = plt.plot(self.richard_list)
            plt.show()

    def plot_displaced_energies(self):
        """ Plot the energies of the single point calcualtions """
        plt.scatter(range(len(self.richard_list)), self.richard_list)
        plt.show()

    def write_states_input(self):
        """
        required for a FD calculation, writes the input file for the desnity of states code (densityofstates.c)
        Output is States_Input_name.in
        """
        x = ex.extract_matrix("FD_files/" + self.name + "_vpt2.log", len(self.atomicfrequencies) + 1)
        ordered_freqs = self.atomicfrequencies[::-1]

        # check if the log file has jumbled the freqs up (possible with degenerate frequencies)
        if not rg.freq_corect_ordering_in_log(self.vpt2_log):
            # print("Warning: The frequencies are jumbled in the VPT2 log file. I will try and fix this, but maybe double check!", "FD_files/" + self.name + "_vpt2.log")
            x = rg.repair_x_matrix(x, "FD_files/" + self.name + "_vpt2.log")

        if self.hr_option == 'tanh':
            # remove hindered rotor from x matrix
            x[:, self.hr_index] = 0
            x[self.hr_index, :] = 0

        filename = "FD_files/" + self.name + "_states_input.txt"
        f = open(filename, "w")
        f.write("Number of bound modes\n")
        f.write(str(len(ordered_freqs)) + "\n")
        f.write("\n")
        f.write("Frequencies (a.u.)\n")
        freq_string = ""
        for t in ordered_freqs:
            freq_string += str(t) + ", "
        freq_string = freq_string[:len(freq_string) - 2]  # gets rid of the last comma
        f.write(str(freq_string) + "\n")
        f.write("\n")
        f.write("Coupling anharmonic constants, x_nF (a.u.)\n")

        if not self.use_apprximate_xifs:  # use the xifs from the VPT2 matrix, as you would in a FD calcualtion
            f.write("%5.9f" % x[1, 0])
            for i in range(2, len(ordered_freqs) + 1):
                f.write(", %5.9f" % x[i, 0])
            f.write("\n")
            f.write("\n")
        else:  # use the apprxomate xifs from a two hessian calculation

            freqs_afd, thirds_afd, fourths_afd, F = af.calc_derrivatives(self.name, self.n_atoms, self.rich_step_size,
                                                                         self.mass)
            self.xff = af.calc_x(freqs_afd, thirds_afd, fourths_afd, F)[
                F, F].real  # This should already be pure real, but the .real just makes sure there is no +0j bit
            self.fourths_afd = fourths_afd
            self.thirds_afd = thirds_afd
            self.freqs_afd = freqs_afd
            self.F_afd = F
            self.xif_approx = [i if abs(i * pc.hartree_to_cm) > self.approx_xif_cutoff else 0 for i in
                               self.return_xif_approx()]  # returns x_[if} with i \neq F

            f.write("%5.9f" % self.xif_approx[0])
            for i in range(1, len(ordered_freqs)):
                f.write(", %5.9f" % self.xif_approx[i])
            f.write("\n")
            f.write("\n")

        f.write("Anharmonic Matrix\n")

        if self.harmonic_bound_modes:
            for i in range(1, len(ordered_freqs) + 1):
                for j in range(1, len(ordered_freqs) + 1):
                    f.write("%5.9f" % 0)
                    if not j == len(ordered_freqs):
                        f.write(",")
                f.write("\n")
        else:
            for i in range(1, len(ordered_freqs) + 1):
                for j in range(1, len(ordered_freqs) + 1):
                    f.write("%5.9f" % x[i, j])
                    if not j == len(ordered_freqs):
                        f.write(",")

                f.write("\n")
        f.close()

    def write_approximate_states_input(self):
        """
        required for a AFD calculation, writes the input file for the desnity of states code (densityofstates.c)
        """
        filename = "FD_files/" + self.name + "_afd_states_input.txt"
        f = open(filename, "w")
        f.write("Number of bound modes\n")
        f.write(str(len(self.freqs_afd) - 1) + "\n")
        f.write("\n")

        f.write("Frequencies (a.u.)\n")
        freq_string = ""
        for t in range(len(self.freqs_afd)):
            if t != self.F:  # Don't want to be writing the imaginary frequency here!
                freq_string += str(self.freqs_afd[t]) + ", "
        freq_string = freq_string[:len(freq_string) - 2]  # gets rid of the last comma
        f.write(str(freq_string) + "\n")
        f.write("\n")

        f.write("Coupling anharmonic constants, x_nF (a.u.)\n")
        f.write("%5.9f" % self.x_approx[self.F, 0].real)
        for i in range(1, len(self.x_approx[self.F])):
            if i != self.F:
                f.write(", %5.9f" % self.x_approx[self.F, i].real)
        f.write("\n")
        f.write("\n")

        f.write("Anharmonic Matrix\n")
        for i in range(1, len(self.atomicfrequencies) + 1):
            for j in range(1, len(self.atomicfrequencies) + 1):
                f.write("%3.1f" % 0.0)
                if not j == len(self.atomicfrequencies):
                    f.write(",")

            f.write("\n")


class GeneralReaction:
    """
    Class contains functions that all reactions of any type can be asked to do. Base class for all reactions.
    """

    def plot_e_of_theta(self):
        """
        Plot E(theta) e.g. Fig. 6 in J. Phys. Chem. A 2019, 123, 4639−4657
        """
        ti.plot_e_of_theta(self.ts.atomic_imaginary_frequency, self.ts.xff, self.barrier, self.reverse_barrier,
                           self.deep_tunneling, label=self.name)
        plt.show()

    def compare_experiment(self):
        """
        Compare the SCTST rate to experimental data
        """
        fig = plt.figure(figsize=(6, 6))
        rec_temps = [1000 / t for t in self.temperature_list]
        log_rates_sctst = self.log_rate_sctst
        log_rates_tst = self.log_rate_tst

        plt.plot(rec_temps, log_rates_tst, label="TST", dashes=[6, 2])
        plt.plot(rec_temps, log_rates_sctst, label="SCTST")
        colors = cm.rainbow(np.linspace(0, 1, len(self.experimental_rates)))
        for exp, color in zip(self.experimental_rates, colors):
            plt.scatter([1000 / i for i in exp[0]], [math.log10(i) for i in exp[1]], label=exp[2], c=color)
        plt.legend()
        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")
        plt.xticks([1, 2, 3, 4, 5])
        plt.tight_layout()
        fig.savefig("plots/compare_experiment" + self.name + ".pdf")
        plt.show()
        print("plots/compare_experiment" + self.name + ".pdf")

    def write_new_perspective_input_file(self, t_low=250, t_high=2000, step=50):
        """
        Required for a FD calculation in the sctst-theta formalism, writes the input file for the desnity of states code (densityofstates.c)
        generates file name_new_perspective.in
        """

        x = ex.extract_matrix("FD_files/" + self.ts.name + "_vpt2.log", len(self.ts.atomicfrequencies) + 1)
        # check if the log file has jumbled the freqs up (possible with degenerate frequencies)
        if not rg.freq_corect_ordering_in_log(self.ts.vpt2_log):
            print(
                "Warning: The frequencies are jumbled in the VPT2 log file. I will try and fix this, but maybe double check!",
                "FD_files/" + self.ts.name + "_vpt2.log")
            print("WARNING: THe fix is only done for the x matrix NOT the derivatives. Errors are expected!")
            x = rg.repair_x_matrix(x, "FD_files/" + self.ts.name + "_vpt2.log")

        filename = "FD_files/" + self.name + "_new_perspective.in"
        f = open(filename, "w")
        f.write("Number of bound modes\n")
        f.write(str(len(self.ts.frequencies)) + "\n")
        f.write("\n")
        f.write("Frequencies (a.u.)\n")
        freq_string = ""
        for t in self.ts.atomicfrequencies[::-1]:
            freq_string += str(t) + ", "
        freq_string = freq_string[:len(freq_string) - 2]  # gets rid of the last comma
        f.write(str(freq_string) + "\n")
        f.write("\n")
        f.write("Coupling anharmonic constants, x_nF (a.u.)\n")
        f.write("%5.9f" % x[1, 0])
        for i in range(2, len(self.ts.atomicfrequencies) + 1):
            f.write(", %5.9f" % x[i, 0])
        f.write("\n")
        f.write("\n")
        f.write("Anharmonic Matrix\n")

        for i in range(1, len(self.ts.atomicfrequencies) + 1):
            for j in range(1, len(self.ts.atomicfrequencies) + 1):
                f.write("%5.9f" % x[i, j])
                if not j == len(self.ts.atomicfrequencies):
                    f.write(",")

            f.write("\n")
        f.write("\n")

        f.write("Anharmonic Barrier\n")
        f.write(str(self.sctst_barrier) + "\n")
        f.write("\n")

        f.write("Reactant ZPE\n")
        f.write(str(self.total_reactant_zpe) + "\n")
        f.write("\n")

        f.write("xff\n")
        f.write(str(self.xFF) + "\n")
        f.write("\n")

        f.write("im freq\n")
        f.write(str(self.ts.atomic_imaginary_frequency) + "\n")
        f.write("\n")

        f.write("t_low\n")
        f.write(str(t_low) + "\n")
        f.write("\n")

        f.write("t_high\n")
        f.write(str(t_high) + "\n")
        f.write("\n")

        f.write("step\n")
        f.write(str(step) + "\n")
        f.write("\n")

        f.write("tunneling\n")
        if self.deep_tunneling:
            f.write("1\n")
        else:
            f.write("0\n")
        f.write("\n")

        f.write("rev_zpe_barrier\n")
        f.write(str(self.sctst_rev_barrier) + "\n")
        f.write("\n")

    def plot_theta_integral(self, T_list):
        """
        Plot the integrand as a function of theta, if using the 'new perspective' method of SCTST
        :param T_list: list of temperatures at which to plot the integrand
        """
        to_be_squareooted = self.ts.atomic_imaginary_frequency ** 2 + 4 * self.ts.xff * self.barrier
        if to_be_squareooted < 0:
            upper_bound = -math.pi * self.ts.atomic_imaginary_frequency / 2 / self.ts.xff
        else:
            upper_bound = math.pi * ((self.ts.atomic_imaginary_frequency - math.sqrt(
                self.ts.atomic_imaginary_frequency ** 2 + 4 * self.ts.xff * self.barrier)) / (-2 * self.ts.xff))

        ds = 0.05
        t_list = np.arange(-5, 30, step=ds)
        t_list_range = np.arange(-5, upper_bound, step=ds)

        for T in T_list[::-1]:

            if self.deep_tunneling:
                theta_max = ti.thetamax(self.ts.atomic_imaginary_frequency, self.ts.xff, self.barrier,
                                        self.reverse_barrier)
                t_list_range = np.arange(-5, theta_max, step=ds)
                upper_bound = theta_max
                value = [ti.integrand_deep_tunneling(t, self.ts.atomic_imaginary_frequency, self.ts.xff, T, theta_max,
                                                     self.barrier) for t in t_list]
                value_fill = [
                    ti.integrand_deep_tunneling(t, self.ts.atomic_imaginary_frequency, self.ts.xff, T, theta_max,
                                                self.barrier) for t in t_list_range]
            elif self.vpt4:
                theta_max = ti.vpt4_max_theta(self.ts.vpt4_W, self.ts.xff, self.ts.vpt4_Y, self.barrier)
                t_list_range = np.arange(-5, theta_max, step=ds)
                value = [ti.integrand_vpt4(t, self.ts.vpt4_W, self.ts.xff, self.ts.vpt4_Y, T) for t in t_list]
                value_fill = [ti.integrand_vpt4(t, self.ts.vpt4_W, self.ts.xff, self.ts.vpt4_Y, T) for t in
                              t_list_range]
                upper_bound = theta_max

            else:
                value = [ti.integrand_vpt2(t, self.ts.atomic_imaginary_frequency, self.ts.xff, T) for t in t_list]
                value_fill = [ti.integrand_vpt2(t, self.ts.atomic_imaginary_frequency, self.ts.xff, T) for t in
                              t_list_range]

            plt.fill_between(t_list_range, value_fill, alpha=.3, y2=[0] * len(value_fill))
            legend_label = str(T) + " K"
            plt.plot(t_list, value, label=legend_label)

        plt.axvline(x=upper_bound, linestyle=':', c='k')
        plt.legend()
        plt.ylabel("Integrand")
        plt.xlabel("Theta")
        plt.savefig("plots/" + self.name + "_theta_integrand.pdf")
        print("plots/" + self.name + "_theta_integrand.pdf")
        plt.show()

    def plot_arrhenius_tst(self):
        rec_temps = [1000 / t for t in self.temperature_list]
        log_rates = self.log_rate_tst
        plt.plot(rec_temps, log_rates)
        plt.title('Transition State Theory Results')
        plt.savefig("plots/TST_" + self.name + ".pdf")
        plt.show()
        print("plots/TST_" + self.name + ".pdf")

    def plot_total_integral(self):
        """
        Plots the total value of the theta integral over all the termpareures. Only meaninful is using the 'new
        perspective' sctst method
        :return: None
        """
        value = [math.log(ti.integral(self.ts.atomic_imaginary_frequency, self.ts.xff, T, self.anharmonic_barrier,
                                      deep_tunneling=self.deep_tunneling)) for T in self.temperature_list]
        plt.plot(self.temperature_list, value)
        plt.show()

    def plot_barrier(self, n_states=10):
        """
        Plots the 1D barrier as a function of reaction coordinate.
        n_states is the number of vibrational state barriers to plot too (FD reactions only)
        """

        if "FD" in self.__class__.__name__:  # We will plot the 1D, 1D+ and FD barriers together to compare
            coordinate_range_min, coordinate_range_max = -200, 50

            # Plot the 1d barrier ----------------------

            # Get everything in the same units
            ts_anharmonic_zpe = self.ts.zpe / pc.hartree_to_joule
            im_freq = self.ts.atomic_imaginary_frequency
            bar_height = self.anharmonic_barrier / pc.hartree_to_joule
            rev_bar_height = self.anharmonic_reverse_barrier / pc.hartree_to_joule

            # Set up reaction coordinates
            self.reaction_coordinates = np.linspace(coordinate_range_min, coordinate_range_max, 501)
            r = [i * pc.bohr_to_m / pc.angstrom_to_m / pc.amu ** 0.5 for i in self.reaction_coordinates]

            if self.deep_tunneling:  # Use WagBar function for barrier potential
                potential = [sc.WagBar(im_freq * pc.hartree_to_cm, self.ts.xff, bar_height * pc.hartree_to_joule,
                                       rev_bar_height * pc.hartree_to_joule, x) for x in self.reaction_coordinates]
            else:  # Use Morse barrier for barrier potential
                D = 0.25 * im_freq * im_freq / self.ts.xff
                a = im_freq / math.sqrt(-2 * D)
                potential = [D * (1 - math.exp(1 * a * r)) ** 2 + bar_height for r in self.reaction_coordinates]

            kj_potential = [w * pc.hartree_to_kj_per_mol for w in potential]
            plt.plot(r, kj_potential, linewidth=3, label="1D Barrier")

            # plot the 1d+ barrier, i.e. now use the FD value of xff
            if self.deep_tunneling:
                oneD_plus_potential = [
                    sc.WagBar(im_freq * pc.hartree_to_cm, self.ts.fd_xff, bar_height * pc.hartree_to_joule,
                              rev_bar_height * pc.hartree_to_joule, x) for x in self.reaction_coordinates]
            else:
                D = 0.25 * im_freq * im_freq / self.ts.fd_xff
                a = im_freq / math.sqrt(-2 * D)
                oneD_plus_potential = [D * (1 - math.exp(1 * a * r)) ** 2 + bar_height for r in
                                       self.reaction_coordinates]

            oneD_plus_potential_kj = [w * pc.hartree_to_kj_per_mol for w in oneD_plus_potential]
            plt.plot(r, oneD_plus_potential_kj, linewidth=3, label="1D+")

            for ts_vib_state in range(n_states):
                ts_anharmonic_zpe, freq_adjustment = fd.find_ts_higher_vib_state(
                    "FD_files/States_Output_" + self.ts.name + ".txt", ts_vib_state)
                im_freq = self.ts.atomic_imaginary_frequency + freq_adjustment
                bar_height = self.sctst_barrier + ts_anharmonic_zpe
                rev_bar_height = self.sctst_rev_barrier + ts_anharmonic_zpe
                self.reaction_coordinates = np.linspace(coordinate_range_min, coordinate_range_max, 501)
                r = [i * pc.bohr_to_m / pc.angstrom_to_m / pc.amu ** 0.5 for i in self.reaction_coordinates]
                if self.deep_tunneling:
                    wagner_potential = [
                        sc.WagBar(im_freq * pc.hartree_to_cm, self.ts.fd_xff, bar_height * pc.hartree_to_joule,
                                  rev_bar_height * pc.hartree_to_joule, x) for x in self.reaction_coordinates]
                else:
                    D = 0.25 * im_freq * im_freq / self.ts.fd_xff
                    a = im_freq / math.sqrt(-2 * D)
                    wagner_potential = [D * (1 - math.exp(1 * a * r)) ** 2 + bar_height for r in
                                        self.reaction_coordinates]

                kj_wagner = [w * pc.hartree_to_kj_per_mol for w in wagner_potential]
                plt.plot(r, kj_wagner, linewidth=3)

            plt.ylim([-0.05 * max(kj_wagner), 1.05 * max(kj_wagner)])
            plt.xlim([-1.5, 0.5])
            plt.xlabel("Reaction Coordinate")
            plt.ylabel("Energy / kJ/mol")
            plt.savefig("plots/FD_barrier_" + self.name + ".pdf")
            plt.show()

        else:
            r = [i / pc.amu ** 0.5 for i in self.reaction_coordinates]
            plt.plot(r, self.wagner_potential)
            plt.title('Barrier Shape')
            plt.xlabel("Reaction Coordinate")
            plt.ylabel("Energy / kJ/mol")
            plt.savefig("plots/barrier_" + self.name + ".pdf")
            plt.show()

    def plot_no_zpe_barrier(self):
        """
        Plot the Wagner barrier shape without the ZPE correction
        """
        r = [i * pc.bohr_to_m / pc.angstrom_to_m / pc.amu ** 0.5 for i in self.reaction_coordinates]
        p = [sc.WagBar(self.ts.imaginary_frequency, self.ts.xff, self.barrier * pc.hartree_to_joule,
                       self.reverse_barrier * pc.hartree_to_joule, x) for x in self.reaction_coordinates]
        plt.plot(r, p)
        plt.title('Barrier Shape')
        plt.savefig("plots/no_zpe_barrier_" + self.name + ".pdf")
        plt.show()

    def generate_barrier_xyz(self):
        r = [i * pc.bohr_to_m / pc.angstrom_to_m / pc.amu ** 0.5 for i in self.reaction_coordinates]
        p = [sc.WagBar(self.ts.imaginary_frequency, self.ts.xff, self.barrier * pc.hartree_to_joule,
                       self.reverse_barrier * pc.hartree_to_joule, x) for x in self.reaction_coordinates]
        plt.plot(r, p)
        plt.title('Barrier Shape')
        plt.savefig("plots/no_zpe_barrier_" + self.name + ".pdf")
        plt.show()

    def plot_composite_barrier(self):
        """
        Plot the 1D barrier as a function of reaction coordinate including all three composite barriers
        """
        a = []
        b = []
        c = []

        for x in self.reaction_coordinates:
            result = sc.WagBar_composite(self.ts.imaginary_frequency, self.ts.xff, self.anharmonic_barrier,
                                         self.anharmonic_reverse_barrier, x)
            a.append(result[0])
            b.append(result[1])
            c.append(result[2])

        ybp = result[3]
        ybr = result[4]
        plt.plot(self.reaction_coordinates, a, c='r', label='Reactant')
        plt.plot(self.reaction_coordinates, b, c='b', label='TS')
        plt.plot(self.reaction_coordinates, c, c='k', label='Prpduct')
        plt.plot(self.reaction_coordinates, self.wagner_potential, label='composite')
        plt.axvline(x=ybp)
        plt.axvline(x=ybr)
        plt.legend()
        plt.title('Barrier Shape')
        plt.savefig("plots/composite_barrier_" + self.name + ".pdf")
        plt.show()

    def plot_CRP(self):
        """
        Plot the CRP
        """
        plt.plot(self.energy_list, self.CRPWag)
        plt.title('Cumulative reaciton probability')
        plt.savefig("plots/CRP_" + self.name + ".pdf")
        plt.show()

    def plot_weighted_CRP(self, temps=[250]):
        """
        Plot the CRP weighted by a Boltzman function. The integral of this is proportional to the rate.
        """
        for temp in temps:
            weight_list = []
            for i in range(len(self.energy_list)):
                weight_list.append(self.CRPWag[i] * math.exp((-self.energy_list[i] / (pc.kb * temp))))
            plt.plot([i / pc.hartree_to_joule for i in self.energy_list], weight_list, label=str(temp) + " K")
        plt.title('Weighted CRP')
        plt.legend()
        plt.savefig("plots/Weighted_CRP_" + self.name + ".pdf")
        plt.show()

    def plot_arrhenius_sctst(self):
        """
        Plot the SCTST rate on an arrhenius plot
        """
        rec_temps = [1000 / t for t in self.temperature_list]
        plt.plot(rec_temps, self.log_rate_sctst)
        plt.title('Semiclassical Transition State Theory Results')
        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")
        plt.savefig("plots/SCTST_" + self.name + ".pdf")
        plt.show()

    def compare_tst_sctst(self):
        """
        Plot the TST and SCTST rate on an arrhenius plot
        """
        rec_temps = [1000 / t for t in self.temperature_list]
        plt.plot(rec_temps, self.log_rate_tst, label="TST")
        plt.plot(rec_temps, self.log_rate_sctst, label="SCTST")
        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / s^{-1}]}$")
        # plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")
        plt.tight_layout()
        plt.legend()
        plt.savefig("plots/SCTST_vs_TST_" + self.name + ".pdf")
        plt.show()


class Unimolecular(GeneralReaction):
    """
    Generic class for ANY unimolecular reaction. Does a simple TST calcuilation and calcualtes basic reacitno properties
    """

    def __init__(self, reactant1, ts, product1, temps, label):

        # check all species have the same basis set!
        basis_list = [i.basis_set_freq.lower() for i in [reactant1, ts, product1]]
        same_basis = all(x == basis_list[0] for x in basis_list)
        if not same_basis:
            print(reactant1.name, ts.name, product1.name)
            print(basis_list)
            raise ValueError("I'M NOT SURE ALL YOUR CHEMICALS IN THE REACTION ", label, "HAVE THE SAME BASIS SET!!!!")

        self.reactant1 = reactant1
        self.product1 = product1
        self.ts = ts
        self.name = label
        self.n_temps = len(temps)
        self.temperature_list = temps
        # self.n_temps = len(temps)

        # Basic Reaction Properties
        self.barrier = ts.energy - reactant1.energy
        self.reverse_barrier = ts.energy - product1.energy
        self.adiabatic_barrier = self.barrier + ts.zpe_hartrees - reactant1.zpe_hartrees
        self.adiabatic_rev_barrier = self.reverse_barrier + ts.zpe_hartrees - product1.zpe_hartrees

        self.adiabatic_barrier_kj_per_mol = self.adiabatic_barrier * pc.hartree_to_joule * pc.avogadro * 0.001
        self.reverse_barrier = ts.energy - product1.energy
        self.symmetry_change = reactant1.symmetry_number / ts.symmetry_number
        self.rate_tst = tst.calculate_unimolecular_rate(reactant1, ts, temps, self.symmetry_change)

        self.rate_tst = self.rate_tst / (
            100 ** 3)  # This is here because the unites returned by the function on the above line are correct for bimolecular in cm, rhater than unimolecular

        self.log_rate_tst = [math.log10(i) if i != 0 else 0 for i in self.rate_tst]
        self.anharmonic_barrier = sc.unimolecular_anharmonic_barrier(reactant1, ts)
        self.anharmonic_reverse_barrier = sc.unimolecular_anharmonic_barrier(product1, ts)
        self.anharmonic_barrier_kj_per_mol = self.anharmonic_barrier * pc.avogadro * 0.001
        self.anharmonic_reverse_barrier_kj_per_mol = self.anharmonic_reverse_barrier * pc.avogadro * 0.001

        # TST Rate Calcualtions
        self.rate_tst = tst.calculate_unimolecular_rate(reactant1, ts, temps, self.symmetry_change)
        self.log_rate_tst = [math.log10(i) if i != 0 else 0 for i in self.rate_tst]

    def plot_arrhenius_tst(self):
        rec_temps = [1000 / t for t in self.temperature_list]
        log_rates = self.log_rate_tst

        plt.plot(rec_temps, log_rates)
        plt.title('Transition State Theory Results')
        plt.savefig("plots/TST_" + self.name + ".pdf")
        plt.show()

        print("plots/TST_" + self.name + ".pdf")

    def print_report(self):
        print("Reaction: ", self.reactant1.name, " ->", self.ts.name)
        print("Basis set: ", self.reactant1.basis_set_freq.lower())

    def check_basis(self):
        basis_list = [i.basis_set_freq.lower() for i in [self.reactant1, self.ts, self.product1]]
        same_basis = all(x == basis_list[0] for x in basis_list)
        if not same_basis:
            print(self.reactant1.name, basis_list[0])
            print(self.ts.name, basis_list[2])
            print(self.product1.name, basis_list[3])
            raise ValueError("I'M NOT SURE ALL YOUR CHEMICALS IN THE REACTION ", self.name.upper(),
                             "HAVE THE SAME BASIS SET!!!!")


class Bimolecular(GeneralReaction):
    """
    Generic class for ANY bimolecular reaction. Does a simple TST calcuilation and calcualtes basic reacitno properties
    """

    def __init__(self, reactant1, reactant2, ts, product1, product2, temps, label):
        # check all species have the same basis set!

        self.reactant1 = reactant1
        self.reactant2 = reactant2
        self.product1 = product1
        self.product2 = product2
        self.ts = ts
        self.name = label
        self.n_temps = len(temps)
        self.temperature_list = temps
        # self.n_temps = len(temps)

        self.check_basis()

        # Basic Reaction Properties specific for Bimolecular reactions
        self.barrier = ts.energy - reactant1.energy - reactant2.energy
        self.adiabatic_barrier = self.barrier + ts.zpe_hartrees - reactant2.zpe_hartrees - reactant1.zpe_hartrees
        self.adiabatic_barrier_kj_per_mol = self.adiabatic_barrier * pc.hartree_to_joule * pc.avogadro / 1000
        self.reverse_barrier = ts.energy - product1.energy - product2.energy
        self.adiabatic_reverse_barrier = self.reverse_barrier + ts.zpe_hartrees - product1.zpe_hartrees - product2.zpe_hartrees
        self.symmetry_change = reactant2.symmetry_number * reactant1.symmetry_number / ts.symmetry_number
        self.rate_tst = tst.calculate_rate(reactant1, reactant2, ts, temps, self.symmetry_change)
        self.rate_deformed_tst = tst.calculate_deformed_rate(reactant1, reactant2, ts, temps,
                                                             self.symmetry_change)  # The deforemd TST approximation to tunnelling

        self.log_rate_tst = [math.log10(i) if i != 0 else 0 for i in self.rate_tst]
        self.anharmonic_barrier = sc.anharmonic_barrier(reactant1, reactant2, ts)
        self.anharmonic_reverse_barrier = sc.anharmonic_barrier(product1, product2, ts)
        self.anharmonic_barrier_kj_per_mol = self.anharmonic_barrier * pc.avogadro / 1000
        self.anharmonic_reverse_barrier_kj_per_mol = self.anharmonic_reverse_barrier * pc.avogadro / 1000
        self.temperature_list = temps
        self.n_temps = len(temps)

    def print_report(self):
        """
        Prints information about this reaction inc basis set and reactant names
        :return:
        """
        print("Reaction: ", self.reactant1.name, " + ", self.reactant2.name)
        print("1D - SCTST")
        print("Basis set: ", self.reactant1.basis_set_freq.lower())

    def check_basis(self):
        """
        Checks all the reactants, ts and products have used the same basis set for the electronic structure calculations!
        :return: None, ValueError if not all bases the same
        """
        basis_list = [i.basis_set_freq.lower() for i in
                      [self.reactant1, self.reactant2, self.ts, self.product1, self.product2]]
        same_basis = all(x == basis_list[0] for x in basis_list)
        if not same_basis:
            print(self.reactant1.name, basis_list[0])
            print(self.reactant2.name, basis_list[1])
            print(self.ts.name, basis_list[2])
            print(self.product1.name, basis_list[3])
            print(self.product2.name, basis_list[4])
            raise ValueError("I'M NOT SURE ALL YOUR CHEMICALS IN THE REACTION ", self.name.upper(),
                             "HAVE THE SAME BASIS SET!!!!")


class Reaction(Bimolecular):  # Bimolecular
    """
    Class for 1D Bimolecular SCTST
    """

    def __init__(self, reactant1, reactant2, ts, product1, product2, temps, label="", deep_tunneling=True,
                 vpt4=False, vpt4_corr=False, fitted_potential=None, wkb=False, theta_integration=False,
                 minimum_tunneling_energy=0,
                 pre_reactive_complex=False, post_reative_complex_energy=False):
        super().__init__(reactant1, reactant2, ts, product1, product2, temps, label)

        self.experimental_rates = []
        self.deep_tunneling = deep_tunneling
        self.vpt4 = vpt4

        # pre/post reactive com,plexes refer to the chemical objects correspionding to a pre/post reactive compelx. Their energies can be sued to fit the Wagner barrier

        if theta_integration:  # From Bill Miller's "New Perspective", Default False

            theta_integral = []
            for t in temps:
                if not self.vpt4:
                    i = ti.integral(ts.atomic_imaginary_frequency, ts.xff, t, self.anharmonic_barrier,
                                    deep_tunneling=deep_tunneling, rev_barrier=self.anharmonic_reverse_barrier,
                                    vpt4=self.vpt4)
                else:
                    # SHOULD THIS BE THE ZPE CORRECTED BARRIER???
                    i = ti.integral(ts.atomic_imaginary_frequency, ts.xff, t, self.barrier,
                                    deep_tunneling=deep_tunneling, rev_barrier=self.reverse_barrier, vpt4=self.vpt4,
                                    Wff=self.ts.vpt4_W, Yfff=self.ts.vpt4_Y)
                theta_integral.append(i)

            q_star = [ts.q_tot[i] * math.exp(-1 * self.anharmonic_barrier / (pc.boltzman * self.temperature_list[i]))
                      * theta_integral[i] for i in range(self.n_temps)]
            self.q_star = q_star
            self.sctstRate = [(pc.boltzman * temps[i] / pc.h) * q_star[i] * (100 ** 3) * self.symmetry_change / (
                reactant1.q_tot[i] * reactant2.q_tot[i]) for i in range(self.n_temps)]

        if not theta_integration:  # default stream. This is the 'old method' we have been using

            self.reaction_coordinates = np.linspace(-100, 100, 901)

            if deep_tunneling:

                if pre_reactive_complex:  # Default False. Includes tunnelling from energies below zero corresponding to a PRC
                    prc_energy = pre_reactive_complex.energy + pre_reactive_complex.zpe_hartrees - (
                        reactant1.energy + reactant1.zpe_hartrees + reactant2.zpe_hartrees + reactant2.energy)
                    wagner_barrier_height = self.anharmonic_barrier - prc_energy * pc.hartree_to_joule
                    wagner_shift = prc_energy
                    if wagner_shift > 0:
                        # This means the PRC is less stable than the reactants! Weird!
                        raise ValueError(
                            "Your PRC energy (inc ZPE) is higher than the free reactants! I don't think there is a PRC along the reaction pathway!")
                else:
                    wagner_barrier_height = self.anharmonic_barrier
                    wagner_shift = 0

                if post_reative_complex_energy:
                    prc_energy = post_reative_complex_energy.energy + post_reative_complex_energy.zpe_hartrees - (
                        product1.energy + product1.zpe_hartrees + product2.zpe_hartrees + product2.energy)
                    wagner_reverse_barrier_height = self.anharmonic_reverse_barrier - prc_energy * pc.hartree_to_joule
                    if prc_energy > 0:
                        raise ValueError(
                            "Your Post reactive complex energy (inc ZPE) is higher than your free products!")

                else:
                    wagner_reverse_barrier_height = self.anharmonic_reverse_barrier

                self.wagner_potential = [
                    sc.WagBar(ts.imaginary_frequency, ts.xff, wagner_barrier_height, wagner_reverse_barrier_height,
                              x) + wagner_shift for x in self.reaction_coordinates]
                # NOTE wagner_potential is just the function used for PLOTTING the barrier, but is not explicitly used when calcualting tunneling! The tunneling calc is done with CRPWag

            if not deep_tunneling:  # Default False. This is pure VPT2-SCTST
                # With no deep tunneling, the effective potential is a morse barrier. For "historical" reasons, we are still calling this the wagner_potential

                D = 0.25 * ts.atomic_imaginary_frequency * ts.atomic_imaginary_frequency / (ts.xff)
                a = ts.atomic_imaginary_frequency / math.sqrt(-2 * D)
                self.reaction_coordinates = np.linspace(-100, 100, 201)
                self.wagner_potential = [
                    D * (1 - math.exp(1 * a * r)) ** 2 + self.anharmonic_barrier / pc.hartree_to_joule for r in
                    self.reaction_coordinates]

            # Calculate CRP
            nSteps = 5000  # this might need to be higher for v low temp reacitons
            eMin = minimum_tunneling_energy
            eMax = 4.0025 * self.anharmonic_barrier
            Estep = eMax / nSteps
            self.energy_list = np.linspace(eMin, eMax, nSteps - 1)

            if vpt4_corr:  # added by Tom Young. Default False
                imaginary_frequency = ts.stanton_imaginary_frequency
            else:
                imaginary_frequency = ts.imaginary_frequency

            if deep_tunneling:  # Use Wagner's DT corrections
                self.CRPWag = [
                    sc.WagProb(imaginary_frequency, ts.xff, wagner_barrier_height, wagner_reverse_barrier_height,
                               energy - wagner_shift * pc.hartree_to_joule) for energy in self.energy_list]
            else:  # IF YOU DON'T WANT DEEP TUNNELING CORRECTIONS
                self.CRPWag = [
                    sc.TunnelProb(imaginary_frequency, ts.xff, self.anharmonic_barrier, self.anharmonic_reverse_barrier,
                                  energy, vpt4_correction=vpt4) for energy in self.energy_list]

            if fitted_potential is not None:  # Added by Tom Young, for fitted potentials. Default None
                e_thresh = 0.0

                if hasattr(fitted_potential, 'v2'):

                    if fitted_potential.v2 < 0.0:
                        e_thresh = -fitted_potential.v2 * pc.hartree_to_joule

                        if vpt4:
                            self.CRPWag = [od.sctst_prob((energy - e_thresh) / pc.hartree_to_joule,
                                                         fitted_potential.theta_vpt4) if energy > e_thresh else 0 for
                                           energy in self.energy_list]
                        else:
                            self.CRPWag = [od.sctst_prob((energy - e_thresh) / pc.hartree_to_joule,
                                                         fitted_potential.theta_vpt2) if energy > e_thresh else 0 for
                                           energy in self.energy_list]

                    if fitted_potential.v2 > 0.0:
                        e_reac = fitted_potential.v2
                        if vpt4:
                            self.CRPWag = [
                                od.sctst_prob((energy + e_reac) / pc.hartree_to_joule, fitted_potential.theta_vpt4) for
                                energy in self.energy_list]
                        else:
                            self.CRPWag = [
                                od.sctst_prob((energy + e_reac) / pc.hartree_to_joule, fitted_potential.theta_vpt2) for
                                energy in self.energy_list]

                if vpt4:  # Default False
                    self.CRPWag = [od.sctst_prob(energy / pc.hartree_to_joule - e_thresh,
                                                 fitted_potential.theta_vpt4) if energy > e_thresh else 0 for energy in
                                   self.energy_list]
                if wkb:
                    self.CRPWag = [
                        od.wkb_prob(potential=fitted_potential, energy=(energy / pc.hartree_to_joule - e_thresh),
                                    kemble=False) if energy > e_thresh else 0 for energy in self.energy_list]
                else:
                    self.CRPWag = [od.sctst_prob(energy / pc.hartree_to_joule - e_thresh,
                                                 fitted_potential.theta_vpt2) if energy > e_thresh else 0 for energy in
                                   self.energy_list]

            self.weightWag = []
            self.Wag = []
            for k in range(len(temps)):
                # perform numerical boltzman integration
                weight_list = [self.CRPWag[i] * math.exp((-self.energy_list[i] / (pc.kb * temps[k]))) for i in
                               range(len(self.energy_list))]
                averagedWag = [0.5 * (weight_list[i] + weight_list[i + 1]) for i in range(len(self.energy_list) - 1)]
                self.Wag.append(sum(averagedWag))

            self.sctstRate = [((ts.q_tot[i] / (reactant1.q_tot[i] * reactant2.q_tot[i])) * (100 ** 3) * self.Wag[
                i] * Estep * self.symmetry_change / pc.h).real for i in range(len(temps))]

        self.log_rate_sctst = [math.log10(rate) if rate != 0 else 0 for rate in
                               self.sctstRate]  # If a rate comes out as zero, it cannot be logged!


class UnimolecularReaction(Unimolecular):
    """
    Class for 1D SCTST unimoelcualr reactions
    """

    def __init__(self, reactant1, ts, product1, temps, label="", theta_integration=False, deep_tunneling=True,
                 vpt4=False, minimum_tunneling_energy=0, unimolecular_dt_corrections=False):
        """
        :param unimolecular_dt_corrections:     Include Tim's Unimolecular speicif DT corrections, Instead of Wagner's corrections
        """
        super().__init__(reactant1, ts, product1, temps, label)
        self.deep_tunneling = deep_tunneling

        self.vpt4 = vpt4
        if theta_integration:  # From Bill Miller's "New Perspective", Default False

            theta_integral = []
            for t in temps:

                if not self.vpt4:
                    i = ti.integral(ts.atomic_imaginary_frequency, ts.xff, t, self.barrier,
                                    deep_tunneling=deep_tunneling, rev_barrier=self.reverse_barrier, vpt4=self.vpt4)
                else:
                    i = ti.integral(ts.atomic_imaginary_frequency, ts.xff, t, self.barrier,
                                    deep_tunneling=deep_tunneling, rev_barrier=self.reverse_barrier, vpt4=self.vpt4,
                                    Wff=self.ts.vpt4_W, Yfff=self.ts.vpt4_Y)
                theta_integral.append(i)

            q_star = [ts.q_tot[i] * math.exp(-1 * self.anharmonic_barrier / (pc.boltzman * self.temperature_list[i]))
                      * theta_integral[i] for i in range(self.n_temps)]

            self.sctstRate = [(pc.boltzman * temps[i] / pc.h) * q_star[i] * self.symmetry_change / (reactant1.q_tot[i])
                              for i in range(self.n_temps)]
        else:
            # SCTST Calculation
            reaction_coordinate_range = 200
            self.reaction_coordinates = np.linspace(-reaction_coordinate_range, reaction_coordinate_range, 201)

            if unimolecular_dt_corrections:  # Default False. This is Tim's alternative DT corrections, designed for unimoelcualr reactions
                self.reactant_freq = np.prod(reactant1.frequencies) / np.prod(ts.frequencies)
                self.wagner_potential = [
                    sc.BoundBar(ts.imaginary_frequency, self.reactant_freq, ts.xff, self.anharmonic_barrier,
                                self.anharmonic_reverse_barrier, x) for x in self.reaction_coordinates]
            elif deep_tunneling:  # use normal Wagner potential
                self.wagner_potential = [
                    sc.WagBar(ts.imaginary_frequency, ts.xff, self.anharmonic_barrier, self.anharmonic_reverse_barrier,
                              x) for x in self.reaction_coordinates]
            else:
                # With no deep tunneling, the effective potential is a morse barrier. For "historical" reasons, we are still calling this the wagner_potential
                # morse parameters  # THERE MAY BE AN ISSUE HERE # TODO CHECK THIS

                D = 0.25 * ts.atomic_imaginary_frequency * ts.atomic_imaginary_frequency / (ts.xff)
                a = ts.atomic_imaginary_frequency / math.sqrt(-2 * D)
                self.reaction_coordinates = np.linspace(-100, 100, 201)
                self.wagner_potential = [
                    D * (1 - math.exp(1 * a * r)) ** 2 + self.anharmonic_barrier / pc.hartree_to_joule for r in
                    self.reaction_coordinates]
            nSteps = 2000  # Sets number of poionts to calculate the CRP
            eMax = 4.0025 * self.anharmonic_barrier  # Sets the maximum energy at which the CRP is calcualted (4*barrier should be high enough!)
            Estep = eMax / nSteps
            eMin = minimum_tunneling_energy + Estep
            self.energy_list = np.linspace(eMin, eMax, nSteps - 1)
            if deep_tunneling:
                # IF YOU WANT DEEP TUNNELING CORRECTIONS:

                if unimolecular_dt_corrections:  # Default False, Tim's Unimoelcuar specific corrections
                    self.CRPWag = [
                        sc.BoundProb(ts.imaginary_frequency, self.reactant_freq, ts.xff, self.anharmonic_barrier,
                                     self.anharmonic_reverse_barrier, energy) for energy in self.energy_list]
                    # plt.plot(self.bound_thetas)
                    # plt.show()
                else:  # Use Wagner's DT corrections
                    self.CRPWag = [sc.WagProb(ts.imaginary_frequency, ts.xff, self.anharmonic_barrier,
                                              self.anharmonic_reverse_barrier, energy) for energy in self.energy_list]

            else:  # Pure VPT2-SCTST. Wrong in the deep tunnelling regeime
                # IF YOU DON'T WANT DEEP TUNNELING CORRECTIONS
                self.CRPWag = [sc.TunnelProb(ts.imaginary_frequency, ts.xff, self.anharmonic_barrier,
                                             self.anharmonic_reverse_barrier, energy) for energy in self.energy_list]

            self.weightWag = []
            self.Wag = []
            for k in range(len(temps)):
                weight_list = []
                for i in range(len(self.energy_list)):
                    weight_list.append(self.CRPWag[i] * math.exp((-self.energy_list[i] / (pc.kb * temps[k]))))
                averagedWag = []
                for i in range(len(self.energy_list) - 1):
                    averagedWag.append(0.5 * (weight_list[i] + weight_list[i + 1]))

                self.Wag.append(sum(averagedWag))

            self.sctstRate = []

            for i in range(len(temps)):
                rate = (ts.q_tot[i] / (reactant1.q_tot[i])) * self.Wag[i] * Estep * self.symmetry_change / pc.h
                self.sctstRate.append(rate.real)

        # If a rate comes out as zero, it cannot be logged!
        self.log_rate_sctst = [math.log10(rate) if rate != 0 else 0 for rate in self.sctstRate]

        self.experimental_rates = []

    def plot_theta_of_E(self):

        ti.plot_theta_of_E(self.ts.atomic_imaginary_frequency, self.ts.xff, self.barrier, self.reverse_barrier)

    def plot_thetas(self):
        """
        Plot Theta(E) for the bound "unimolecular" potenital, for the Wagner potential and for the pure VPT2 potential. Only the bound potential gives the correct tunnell splitting
        """

        nSteps = 2000  # Sets number of poionts to calculate the CRP
        eMax = 1.0025 * self.anharmonic_barrier  # Sets the maximum energy at which the CRP is calcualted (4*barrier should be high enough!)
        Estep = eMax / nSteps
        short_energy_list = np.linspace(Estep, eMax, nSteps - 1)

        reactant_freq = np.prod(self.reactant1.frequencies) / np.prod(self.ts.frequencies)
        bound_thetas = [sc.BoundTheta(self.ts.imaginary_frequency, reactant_freq, self.ts.xff, self.anharmonic_barrier,
                                      self.anharmonic_reverse_barrier, energy) for energy in short_energy_list]
        unbound_thetas = [sc.WagTheta(self.ts.imaginary_frequency, self.ts.xff, self.anharmonic_barrier,
                                      self.anharmonic_reverse_barrier, energy) for energy in short_energy_list]
        vpt2_thetas = [sc.TunnelTheta(self.ts.imaginary_frequency, self.ts.xff, self.anharmonic_barrier,
                                      self.anharmonic_reverse_barrier, energy) for energy in short_energy_list]

        plt.plot(self.energy_list / self.anharmonic_barrier, bound_thetas, label="Unimolecular")
        plt.plot(self.energy_list / self.anharmonic_barrier, vpt2_thetas, label="VPT2")
        plt.plot(self.energy_list / self.anharmonic_barrier, unbound_thetas, linestyle=":",
                 label="Wagner DT correction")
        plt.xlabel("Energy / V0")
        plt.ylabel("$\\theta$")
        plt.tight_layout()
        plt.legend()
        print("plots/" + self.name + "_compare_thetas_unimolecular.pdf")
        plt.savefig("plots/" + self.name + "_compare_thetas_unimolecular.pdf")
        plt.show()

    def plot_wag(self):
        plt.plot(self.temperature_list, self.Wag)
        plt.title('Wag')
        plt.savefig("plots/Wag_" + self.name + ".pdf")
        plt.show()
        print("plots/CRP_" + self.name + ".pdf")


class FD_Unimolecular_Reaction(Unimolecular):
    def __init__(self, r, ts, p, temps, label="", integration=True, deep_tunneling=True, theta_integration=False,
                 harmonic_transition_modes=False):
        super().__init__(r, ts, p, temps, label)
        """
        harmonic_transiton_modes = True would treat all the partition functions as harmonic (essecitally, the anharmonicicty is only accounted for in the tunneling path).
        """

        # Basic Reaction Properties
        self.experimental_rates = []
        self.deep_tunneling = deep_tunneling

        # Anharmonic partition functions (if needed)
        if not harmonic_transition_modes:
            self.r_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r.name + ".txt", i) for i in
                             self.temperature_list]
            self.r_anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.reactant1.name + ".txt")
            self.p_anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.product1.name + ".txt")
            r.anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.reactant1.name + ".txt")
            ts.anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.ts.name + ".txt")
        else:
            r.anharmonic_zpe = r.zpe_hartrees
            ts.anharmonic_zpe = ts.zpe_hartrees

        # Calcualte g0
        if harmonic_transition_modes:  # default false
            self.ts_g0 = ts.g0
            self.r_g0 = 0
            self.p_g0 = 0
        else:
            self.ts_g0 = ts.fd_g0
            self.r_g0 = r.fd_g0
            self.p_g0 = p.fd_g0
        xFF = ex.extract_matrix("FD_files/" + ts.name + "_vpt2.log", len(ts.frequencies) + 1)[0][0]

        # Genreate CRP
        if harmonic_transition_modes:  # default false
            sctst_barrier = ts.energy - r.energy - r.zpe_hartrees + self.ts_g0 - self.r_g0
            sctst_rev_barrier = ts.energy - p.energy - p.zpe_hartrees + self.ts_g0 - self.p_g0
            total_reactant_zpe = r.zpe_hartrees
            total_product_zpe = p.zpe_hartrees
        else:
            sctst_barrier = ts.energy - r.energy - self.r_anharmonic_zpe + self.ts_g0 - self.r_g0
            sctst_rev_barrier = ts.energy - p.energy - self.p_anharmonic_zpe + self.ts_g0 - self.p_g0
            total_reactant_zpe = self.r_anharmonic_zpe
            total_product_zpe = self.p_anharmonic_zpe

        # assign self labels because tim forgot these earlier
        self.sctst_barrier = sctst_barrier
        self.sctst_rev_barrier = sctst_rev_barrier
        self.total_reactant_zpe = total_reactant_zpe
        self.total_product_zpe = total_product_zpe
        self.xFF = xFF

        if not theta_integration:
            self.write_sctst_input_file(ts.atomic_imaginary_frequency, sctst_barrier, sctst_rev_barrier, xFF,
                                        total_reactant_zpe, total_product_zpe)

            if deep_tunneling:
                out_name = 'FD_files/CRP_W_' + label + '.txt'
            else:
                out_name = 'FD_files/CRP_M_' + label + '.txt'
            self.out_name = out_name

            if integration:
                print("Performing tunnelling calculation for ", self.name)
                os.system('gcc -O2 -o sctst.exe ../../fdsctst.c')
                if deep_tunneling:
                    os.system(
                        'sctst.exe FD_files\\' + self.name + '_crp.in FD_files\\States_Output_' + self.ts.name + '.txt W FD_files\\CRP_W_' + label + '.txt > sctst.log')
                else:
                    os.system(
                        'sctst.exe FD_files\\' + self.name + '_crp.in FD_files\\States_Output_' + self.ts.name + '.txt M FD_files\\CRP_M_' + label + '.txt > sctst.log')

                print("Finished tunnelling calculation for ", self.name)  # change to trigger git
            # Do this using the C++ program instead! Use the input file generated above!

            # FD Calculations
            self.CRP = fd.fetch_CRP(out_name)
            self.CRPWag = fd.fetch_CRP(out_name)
            self.energy_list = fd.fetch_energylist(out_name)
            self.Estep = self.energy_list[1] - self.energy_list[0]
            self.tunneling = [fd.integrate_CRP(out_name, i) for i in self.temperature_list]

            if harmonic_transition_modes:  # default false
                self.ts_vib_pf = ts.q_vib  # only called is doing anharmonic TST, in which case having harmonic transiton modes means you will actually get just tst...
                self.r_vib_pf = r.q_vib
                r.anharmonic_vib_pf = r.q_vib
                ts.anharmonic_vib_pf = ts.q_vib

            else:
                r.anharmonic_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r.name + ".txt", i) for i in
                                       self.temperature_list]

                self.r_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r.name + ".txt", i) for i in
                                 self.temperature_list]
                self.ts_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + ts.name + ".txt", i) for i in
                                  self.temperature_list]  # only needed for anharmonic TST
                ts.anharmonic_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + ts.name + ".txt", i) for i in
                                        self.temperature_list]  # only needed for anharmonic TST
            # check temperatures line up
            if len(self.tunneling) != len(ts.q_rot):
                raise ValueError("Your reaction temps are different to your transition state temps!")

            self.rate_sctst = [
                self.symmetry_change * ts.q_rot[i] * self.tunneling[i] / (pc.h * r.q_rot[i] * self.r_vib_pf[i]) for i in
                range(len(self.temperature_list))]

        else:

            self.write_new_perspective_input_file(t_low=temps[0], t_high=temps[-1], step=temps[1] - temps[0])
            if integration:  # If you have already done the integration, and produced the new_perspective_name.out file, there is no need to do it again, so set integraiton=False
                print("Performing Unimolecular new perspective calculation for ", self.name)
                os.system('gcc -O2 -o np.exe ../new_perspective.c')
                os.system(
                    'np.exe FD_files\\' + self.name + '_new_perspective.in FD_files\\new_perspective_' + self.name + '.out > FD_files\\new_perspective_' + self.name + '.log')
                print("Finished tunneling calculation for ", self.name)

            q_list = ti.calc_q_star('FD_files\\new_perspective_' + self.name + '.out')

            # The factor of 10000000000000 below is perhaps not the best way to solve a problem I had
            # The values of the integrals from new_perspective.c are very very small. Even with 18 decimal places, they mostly come out to zero.
            # To try and fix this, i multiplied the integrals in the C code by 100000000000000 and am compensating for that here

            self.rate_sctst = [
                (pc.kb * temps[ii] / pc.h) * self.symmetry_change * ts.q_rot[ii] * ts.q_trans[ii] * q_list[ii] / (
                    r.q_rot[ii] * r.q_trans[ii] * self.r_vib_pf[ii] * 100000000000000000) for ii in
                range(len(self.temperature_list))]

        self.rate_anharmonic_tst = tst.calculate_anharmonic_unimolecular_rate(r, ts, temps, sigma=self.symmetry_change)
        self.log_anharmonic_tst = [math.log10(i) for i in self.rate_anharmonic_tst]

        self.log_rate_sctst = [math.log10(rate) if rate != 0 else 0 for rate in self.rate_sctst]

    def calc_microcannonical_rate(self, exp_data=[]):
        anharmonic_barrier = self.sctst_barrier + self.ts.anharmonic_zpe
        df_dos = pd.read_csv("FD_files/States_Output_" + self.reactant1.name + ".txt", skiprows=1)
        energy_gap_DOS = df_dos['Energy'][1] - df_dos['Energy'][0]

        df_dos['DOS'] = df_dos[' # of states']

        df_dos_ts = pd.read_csv("FD_files/States_Output_" + self.ts.name + ".txt", skiprows=1)
        df_dos_ts['DOS'] = df_dos_ts[' # of states'] / energy_gap_DOS
        df_dos_ts['Energy'] = df_dos_ts['Energy'] - df_dos_ts['Energy'][0]
        energy_gap_ts = df_dos_ts['Energy'][1] - df_dos_ts['Energy'][0]

        df_CRP = pd.read_csv(self.out_name)
        energy_gap_CRP = df_CRP['Energy'][1] - df_CRP['Energy'][0]

        # check the energy gaps are the same in the two dataframes:
        if abs(np.log((energy_gap_DOS / energy_gap_CRP))) > 0.01:
            raise ValueError("Energy steps are different here")

        if len(df_dos) != len(df_CRP):
            raise ValueError("Dfs not the same length", len(df_dos), len(df_CRP))

        df_CRP['Rel_energy'] = df_CRP['Energy'] / anharmonic_barrier
        anharmonic_barrier_cm = anharmonic_barrier * pc.hartree_to_cm
        df_CRP['DOS'] = df_dos['DOS']
        df_CRP['TST_CRP'] = (df_CRP['Rel_energy'] > 1).astype(int)

        df_CRP['rate'] = df_CRP['CRP'] * self.symmetry_change / (2 * math.pi * df_CRP['DOS'])
        df_CRP['TST_rate'] = df_CRP['TST_CRP'] / (2 * math.pi * df_CRP['DOS'])
        df_CRP['Energy_cm'] = df_CRP['Energy'] * pc.hartree_to_cm

        df_CRP['log_rate'] = np.log10(df_CRP['rate'])
        df_CRP['rate_millionths'] = df_CRP['rate'] * 10 ** 6 * 10 ** 6

        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)
        plt.plot(np.asarray(df_CRP['Energy_cm']), np.asarray(df_CRP['rate']) * 10 ** 6, color='k', lw=2.0, zorder=0,
                 label="SCTST")

        for i in exp_data:
            plt.errorbar(i[0], i[1] * 10 ** 6, yerr=i[2] * 10 ** 6, elinewidth=2, capthick=0.1, ecolor='r',
                         markeredgewidth=1, capsize=5, zorder=10, label="Experiment - Fang (2016)")
            plt.scatter(i[0], i[1] * 10 ** 6, c='r', zorder=10)

        plt.xlim([3750, 4600])
        plt.ylim([0.5, 7])

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        ax2.set_xticks(
            [anharmonic_barrier_cm * (2 / 3), anharmonic_barrier_cm * (3 / 4), anharmonic_barrier_cm * (4 / 5)])
        ax2.set_xticklabels(["$\\frac{2}{3} V_0$", "$\\frac{3}{4} V_0$", "$\\frac{4}{5} V_0$"])

        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["right"].minor_ticklabels.set_visible(False)

        ax2.axis["top"].major_ticklabels.set_visible(True)

        # ax.axes.set_xlim(left = 3900, right = 4600)
        plt.xlabel("Excitation Energy / $\\mathrm{cm}^{-1}$")
        plt.ylabel("$k(E) / 10^{-6} \\mathrm{s}^{-1}$")
        plt.legend(['SCTST', "Experiment - Fang (2016)"])
        # plt.yticks([-28, -24, -20, -16, -12])
        plt.draw()
        plt.tight_layout()
        fig.savefig("plots/microcannonical_" + self.name + ".pdf")
        plt.show()

        print("plots/microcannonical_" + self.name + ".pdf")

        pass

    def plot_CRP(self):
        plt.plot(self.energy_list, self.CRP)
        plt.title('Cumulative reaciton probability')
        plt.savefig("plots/FD_CRP_" + self.name + ".pdf")
        plt.show()

    def plot_tunneling(self):
        plt.plot(self.temperature_list, self.tunneling)
        plt.title('tunneling')
        plt.savefig("plots/FD_tunneling_" + self.name + ".pdf")
        plt.show()

    def write_sctst_input_file(self, rxmode, fbarrier, revbarrier, xff, rzpe, pzpe):
        filename = "FD_files/" + self.name + "_crp.in"
        f = open(filename, "w")
        f.write("Reaction mode frequency (a.u.)\n")
        f.write(str(rxmode) + "\n")
        f.write("\n")
        f.write("Forward barrier height w/ reac ZPE, reac G0, TS G0 (a.u.)\n")
        f.write(str(fbarrier) + "\n")
        f.write("\n")
        f.write("Reverse barrier height w/ prod ZPE, TS G0 (a.u.)\n")
        f.write(str(revbarrier) + "\n")
        f.write("\n")
        f.write("Reaction mode diagonal anharmonic constant (a.u.)\n")
        f.write(str(xff) + "\n")
        f.write("\n")
        f.write("Reactants ZPE (a.u.)\n")
        f.write(str(rzpe) + "\n")
        f.write("\n")
        f.write("Products ZPE (a.u.)\n")
        f.write(str(pzpe) + "\n")
        f.write("\n")
        f.close()

    def plot_arrhenius_sctst(self):
        rec_temps = [1000 / t for t in self.temperature_list]
        log_rates = self.log_rate_sctst
        plt.plot(rec_temps, log_rates)
        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")
        plt.title('FD Semiclassical Transition State Theory Results')
        plt.savefig("plots/FDSCTST_" + self.name + ".pdf")
        plt.show()

    def compare_tst_sctst(self):
        rec_temps = [1000 / t for t in self.temperature_list]
        log_rates_sctst = [i for i in self.log_rate_sctst]
        log_rates_tst = self.log_rate_tst
        plt.plot(rec_temps, log_rates_tst, label="TST")
        plt.plot(rec_temps, log_rates_sctst, label="FDSCTST")
        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")
        plt.legend()
        plt.savefig("plots/FDSCTST_vs_TST_" + self.name + ".pdf")
        plt.show()

    def check_basis(self):
        basis_list = [i.basis_set_freq.lower() for i in [self.reactant1, self.ts, self.product1]]
        same_basis = all(x == basis_list[0] for x in basis_list)
        if not same_basis:
            print(self.reactant1.name, basis_list[0])
            print(self.ts.name, basis_list[2])
            print(self.product1.name, basis_list[3])
            raise ValueError("I'M NOT SURE ALL YOUR CHEMICALS IN THE REACTION ", self.label.upper(),
                             "HAVE THE SAME BASIS SET!!!!")


class FD_Bimolecular_Reaction(Bimolecular):
    """
    Class for Full dimenisonal SCTST with a bimiolecular reaciton.
    """

    def __init__(self, r1, r2, ts, p1, p2, temps, label="", use_new_states_output="", integration=True,
                 deep_tunneling=True, theta_integration=False, vpt4=False, harmonic_transition_modes=False):

        super().__init__(r1, r2, ts, p1, p2, temps, label)

        # Basic Reaction Properties
        self.experimental_rates = []
        self.deep_tunneling = deep_tunneling

        # Anharmonic partition functions
        if harmonic_transition_modes:
            self.r1_vib_pf = r1.q_vib
            self.r2_vib_pf = r2.q_vib
            self.ts_g0 = ts.g0
            self.r1_g0 = 0
            self.r2_g0 = 0
            self.p1_g0 = 0
            self.p2_g0 = 0

            # Genreate CRP
            sctst_barrier = ts.energy - r1.energy - r2.energy - r1.zpe_hartrees - r2.zpe_hartrees + self.ts_g0
            sctst_rev_barrier = ts.energy - p1.energy - p2.energy - p1.zpe_hartrees - p2.zpe_hartrees + self.ts_g0
            total_reactant_zpe = r1.zpe_hartrees + r2.zpe_hartrees
            total_product_zpe = p1.zpe_hartrees + p2.zpe_hartrees
            r1.anharmonic_vib_pf = r1.q_vib
            r2.anharmonic_vib_pf = r2.q_vib
            ts.anharmonic_vib_pf = ts.q_vib
            r1.anharmonic_zpe = r1.zpe_hartrees
            r2.anharmonic_zpe = r2.zpe_hartrees
            ts.anharmonic_zpe = ts.zpe_hartrees


        else:
            if r1.name == 'atom':
                self.r1_vib_pf = [1 for i in self.temperature_list]
                r1.anharmonic_vib_pf = [1 for i in self.temperature_list]
                self.r1_anharmonic_zpe = 0
                r1.anharmonic_zpe = 0

            else:
                self.r1_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r1.name + ".txt", i) for i in
                                  self.temperature_list]
                r1.anharmonic_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r1.name + ".txt", i) for i in
                                        self.temperature_list]
                self.r1_anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.reactant1.name + ".txt")
                r1.anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.reactant1.name + ".txt")

            if r2.name == 'atom':
                self.r2_vib_pf = [1 for i in self.temperature_list]
                r2.anharmonic_vib_pf = [1 for i in self.temperature_list]
                self.r2_anharmonic_zpe = 0
                r2.anharmonic_zpe = 0

            else:
                self.r2_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r2.name + ".txt", i) for i in
                                  self.temperature_list]
                r2.anharmonic_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + r2.name + ".txt", i) for i in
                                        self.temperature_list]
                self.r2_anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.reactant2.name + ".txt")
                r2.anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.reactant2.name + ".txt")

            ts.anharmonic_vib_pf = [fd.find_reactant_pf("FD_files/States_Output_" + ts.name + ".txt", i) for i in
                                    self.temperature_list]

            # Anharmonic ZPEs
            ts.anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.ts.name + ".txt")

            self.p1_anharmonic_zpe = fd.find_reactant_ZPE(
                "FD_files/States_Output_" + self.product1.name + ".txt") if not p1.name == 'atom' else 0
            self.p2_anharmonic_zpe = fd.find_reactant_ZPE(
                "FD_files/States_Output_" + self.product2.name + ".txt") if not p2.name == 'atom' else 0

            # Calcualte g0
            self.ts_g0 = ts.fd_g0
            self.r1_g0 = r1.fd_g0 if not r1.name == 'atom' else 0
            self.r2_g0 = r2.fd_g0 if not r2.name == 'atom' else 0
            self.p1_g0 = p1.fd_g0 if not p1.name == 'atom' else 0
            self.p2_g0 = p2.fd_g0 if not p2.name == 'atom' else 0

            # Genreate CRP
            sctst_barrier = ts.energy - r1.energy - r2.energy - self.r1_anharmonic_zpe - self.r2_anharmonic_zpe + self.ts_g0 - self.r1_g0 - self.r2_g0
            sctst_rev_barrier = ts.energy - p1.energy - p2.energy - self.p1_anharmonic_zpe - self.p2_anharmonic_zpe + self.ts_g0 - self.p1_g0 - self.p2_g0
            total_reactant_zpe = self.r1_anharmonic_zpe + self.r2_anharmonic_zpe
            total_product_zpe = self.p1_anharmonic_zpe + self.p2_anharmonic_zpe

        xFF = ex.extract_matrix(ts.vpt2_log, len(ts.frequencies) + 1)[0][0]

        # assign self labels because tim forgot these earlier
        self.sctst_barrier = sctst_barrier
        self.sctst_rev_barrier = sctst_rev_barrier
        self.total_reactant_zpe = total_reactant_zpe
        self.total_product_zpe = total_product_zpe
        self.xFF = xFF

        if not theta_integration:
            ts_states_outful_filename = "FD_files/States_Output_" + self.ts.name + ".txt"
            tunneling_type = "W"

            self.write_sctst_input_file(ts.atomic_imaginary_frequency, sctst_barrier, sctst_rev_barrier, xFF,
                                        total_reactant_zpe, total_product_zpe)

            if deep_tunneling:
                out_name = 'FD_files/CRP_W_' + label + '.txt'
            else:
                out_name = 'FD_files/CRP_M_' + label + '.txt'

            if integration:
                print("Performing tunnelling calculation for ", self.name)
                os.system('gcc -O2 -o sctst.exe ../../fdsctst.c')

                if len(use_new_states_output) > 0:
                    if deep_tunneling:
                        os.system(
                            'sctst.exe FD_files\\' + self.name + '_crp.in ' + use_new_states_output + ' W FD_files\\CRP_W_' + label + '.txt')
                    else:
                        os.system(
                            'sctst.exe FD_files\\' + self.name + '_crp.in ' + use_new_states_output + ' M FD_files\\CRP_M_' + label + '.txt')

                else:
                    if deep_tunneling:
                        os.system(
                            'sctst.exe FD_files\\' + self.name + '_crp.in FD_files\\States_Output_' + self.ts.name + '.txt W FD_files\\CRP_W_' + label + '.txt')
                    else:
                        os.system(
                            'sctst.exe FD_files\\' + self.name + '_crp.in FD_files\\States_Output_' + self.ts.name + '.txt M FD_files\\CRP_M_' + label + '.txt')

                print("Finished tunnelling calculation for ", self.name)

            # FD Calculations
            self.CRP = fd.fetch_CRP(out_name)
            self.energy_list = fd.fetch_energylist(out_name)
            self.Estep = self.energy_list[1] - self.energy_list[0]
            self.tunneling = [fd.integrate_CRP(out_name, i) for i in self.temperature_list]

            # check temperatures line up
            if len(self.tunneling) != len(ts.q_rot):
                raise ValueError("Your reaction temps are different to your transition state temps!")

            # The factor of 100^3 converts from molecules/m3 to molecules/cm3
            self.rate_sctst = [
                self.symmetry_change * 100 ** 3 * ts.q_rot[i] * ts.q_trans[i] * self.tunneling[i] / (
                    pc.h * r1.q_rot[i] * r1.q_trans[i] * r2.q_rot[i] * r2.q_trans[i] * self.r1_vib_pf[i] *
                    self.r2_vib_pf[i])
                for i in range(len(self.temperature_list))]

        else:
            self.write_new_perspective_input_file(t_low=temps[0], t_high=temps[-1], step=temps[1] - temps[0])
            if integration:  # If you have already done the integration, and produced the new_perspective_name.out file, there is no need to do it again, so set integraiton=False
                print("Performing bimolecular new perspective calculation for ", self.name)
                os.system('gcc -O2 -o np.exe ../new_perspective.c')
                os.system(
                    'np.exe FD_files\\' + self.name + '_new_perspective.in FD_files\\new_perspective_' + self.name + '.out > FD_files\\new_perspective_' + self.name + '.log')
                print("Finished tunneling calculation for ", self.name)

            q_list = ti.calc_q_star('FD_files\\new_perspective_' + self.name + '.out')

            # The factor of 10000000000000 below is perhaps not the best way to solve a problem I had
            # The values of the integrals from new_perspective.c are very very small. Even with 18 decimal places, they mostly come out to zero.
            # To try and fix this, i multiplied the integrals in the C code by 100000000000000 and am compensating for that here
            # The factor of 100^3 converts from molecules/m3 to molecules/cm3

            self.rate_sctst = [
                (pc.kb * temps[ii] / pc.h) * 100 ** 3 * self.symmetry_change * ts.q_rot[ii] * ts.q_trans[ii] * q_list[
                    ii] / (
                    r1.q_rot[ii] * r2.q_rot[ii] * r1.q_trans[ii] * r2.q_trans[ii] * self.r1_vib_pf[ii] * self.r2_vib_pf[
                        ii] * 100000000000000000) for ii in range(len(self.temperature_list))]

        if ts.hr_option == 'tanh':
            sorted_freq = sorted(ts.frequencies)
            hr_freq = sorted_freq.pop(ts.hr_index)
            tanh_pf = [pf.tanh_function(hr_freq, ts.reduced_moment_of_inertia, t, ts.symmetry_number) for t in
                       self.temperature_list]
            self.rate_sctst = [i * j for i, j in zip(self.rate_sctst, tanh_pf)]
            pass

        self.rate_anharmonic_tst = tst.calculate_anharmonic_bimolecular_rate(r1, r2, ts, temps, self.symmetry_change)
        self.log_anharmonic_tst = [math.log10(i) for i in self.rate_anharmonic_tst]

        self.log_rate_sctst = [math.log10(rate) if rate != 0 else 0 for rate in self.rate_sctst]

    def plot_CRP(self):
        plt.plot(self.energy_list, self.CRP)
        plt.title('Cumulative reaciton probability')
        plt.savefig("plots/FD_CRP_" + self.name + ".pdf")
        plt.show()

    def plot_tunneling(self):
        plt.plot(self.temperature_list, self.tunneling)
        plt.title('tunneling')
        plt.savefig("plots/FD_tunneling_" + self.name + ".pdf")
        plt.show()

    def write_sctst_input_file(self, rxmode, fbarrier, revbarrier, xff, rzpe, pzpe):
        filename = "FD_files/" + self.name + "_crp.in"
        f = open(filename, "w")

        f.write("Reaction mode frequency (a.u.)\n")
        f.write(str(rxmode) + "\n")
        f.write("\n")
        f.write("Forward barrier height w/ reac ZPE, reac G0, TS G0 (a.u.)\n")
        f.write(str(fbarrier) + "\n")
        f.write("\n")
        f.write("Reverse barrier height w/ prod ZPE, TS G0 (a.u.)\n")
        f.write(str(revbarrier) + "\n")
        f.write("\n")
        f.write("Reaction mode diagonal anharmonic constant (a.u.)\n")
        f.write(str(xff) + "\n")
        f.write("\n")
        f.write("Reactants ZPE (a.u.)\n")
        f.write(str(rzpe) + "\n")
        f.write("\n")
        f.write("Products ZPE (a.u.)\n")
        f.write(str(pzpe) + "\n")
        f.write("\n")
        f.close()


class Series:
    def __init__(self, reaction_list, title, rec_temps):

        self.reaction_list = reaction_list
        self.title = title
        self.rec_temps = rec_temps

    def plot_e_of_theta(self, xrange=None):
        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        plot_title = self.title
        for i in self.reaction_list:

            if i.vpt4:
                ti.plot_e_of_theta(i.ts.atomic_imaginary_frequency, i.ts.xff, i.barrier, i.reverse_barrier,
                                   i.deep_tunneling,
                                   "Deep Tunneling: " + str(i.deep_tunneling) + "VPT4: " + str(i.vpt4),
                                   vpt4=i.vpt4, Wff=i.ts.vpt4_W, Yfff=i.ts.vpt4_Y)
            else:
                ti.plot_e_of_theta(i.ts.atomic_imaginary_frequency, i.ts.xff, i.barrier, i.reverse_barrier,
                                   i.deep_tunneling,
                                   "Deep Tunneling: " + str(i.deep_tunneling) + "VPT4: " + str(i.vpt4), vpt4=i.vpt4)
        ti.plot_e_of_theta(self.reaction_list[0].ts.atomic_imaginary_frequency, self.reaction_list[0].ts.xff,
                           self.reaction_list[0].barrier, self.reaction_list[0].reverse_barrier, False, "Parabolic",
                           parabolic=True)

        if xrange == None:
            plt.ylim([-0.3, 0.5])
        else:
            plot_title += "_deep"
            plt.ylim([-0.01, 0.04])
            plt.xlim(xrange)
        plt.xlabel("Theta")
        plt.ylabel("$E(\theta)$")
        plt.legend()
        plt.ylim([0, 0.03])
        plt.savefig("plots/E_of_theta_" + plot_title + ".pdf")
        plt.show()
        print("plots/E_of_theta_" + self.title + ".pdf")

    def plot_crp(self):

        fig = plt.figure()
        ax = fig.add_subplot(1, 1, 1)

        plot_title = self.title
        for i in self.reaction_list:
            plt.plot([k / i.anharmonic_barrier for k in i.energy_list],
                     [k / l if l != 0 else 0 for k, l in zip(i.CRPWag, self.reaction_list[0].CRPWag)])
        plt.title('Cumulative reaciton probability')
        plt.xlim(0.2, 1)
        plt.savefig("plots/compare_CRP_" + self.title + ".pdf")
        plt.show()

    def plot_p_of_e(self, xrange=None):

        plot_title = self.title
        for i in self.reaction_list:
            ti.plot_p_of_e(i.ts.atomic_imaginary_frequency, i.ts.xff, i.barrier, i.reverse_barrier, i.deep_tunneling,
                           "Deep Tunneling: " + str(i.deep_tunneling) + "VPT4: " + str(i.vpt4), vpt4=i.vpt4)

        plot_title += "p_of_e"
        plt.xlabel("E")
        plt.ylabel("$P(E)$")
        plt.legend()
        plt.savefig("plots/E_of_theta_" + plot_title + ".pdf")
        plt.show()
        print("plots/E_of_theta_" + self.title + ".pdf")

    def plot_tst(self):
        fig = plt.figure(figsize=(6.3, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        dashed = True
        s = 0
        for i in self.reaction_list:
            line, = ax.plot(self.rec_temps, i.log_rate_tst, label=i.name, lw=1.0, linestyle='dashed')

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        if max(self.rec_temps) >= 1000 / 250 and max(self.rec_temps) < 1000 / 199:
            ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
        elif max(self.rec_temps) < 1000 / 250:
            ax2.set_xticks([1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["300 K", "500 K", "1000 K"])
        else:
            pass

        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)

        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")
        plt.legend()

        if max(self.rec_temps) > 1000 / 250 and max(self.rec_temps) < 1000 / 199:
            plt.xticks([1, 2, 3, 4, 5])
        elif max(self.rec_temps) < 1000 / 250:
            plt.xticks([1, 2, 3, 4])
        else:
            pass
        # plt.yticks([-28, -24, -20, -16, -12])
        plt.draw()
        plt.tight_layout()
        fig.savefig("plots/compare_TST_" + self.title + ".pdf")
        plt.show()

        print("plots/compare_TST_" + self.title + ".pdf")

    def plot_halflife(self, multiplier=[], with_tst=False, style=None, with_anharmonic_tst=False):

        if style == None:
            style = ['plot'] * len(self.reaction_list)

        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        dashed = True
        s = 0
        unimolecular = False

        # plot sctst and experimental results
        for i in self.reaction_list:

            if style[s] == 'plot':
                line, = ax.plot(self.rec_temps, np.log(2) / i.sctstRate, label=str(i.name))
            elif style[s] == 'scatter':
                plt.scatter(self.rec_temps, np.log(2) / i.sctstRate, label=str(i.name))
            else:
                raise ValueError("Please enter a valid style, not", style[s])

            s += 1

        # get the ylim values having pltting the experimental and sctst results. We will then plot the TST results bu RESET the ylim values to best fit the SCTST results (since the TST results go way way down off the page, often
        ymin, ymax = ax.get_ylim()
        # reset the color cycle so TST and ScTST hae the same colors
        plt.gca().set_prop_cycle(None)
        for i in self.reaction_list:
            if with_tst == i.name:
                line, = ax.plot(self.rec_temps, np.log(2) / i.log_rate_tst, lw=1.0, linestyle='dashed')
            if with_anharmonic_tst == i.name:
                line, = ax.plot(self.rec_temps, np.log(2) / i.log_anharmonic_tst, label='Anharmonic TST', lw=1.0,
                                linestyle='dashed')

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        if max(self.rec_temps) >= 4 and max(self.rec_temps) < 5.02:
            ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
        elif max(self.rec_temps) < 4:
            ax2.set_xticks([1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["300 K", "500 K", "1000 K"])
        else:
            ax2.set_xticks([1000 / 100, 1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["100 K", "200 K", "300 K", "500 K", "1000 K"])
            pass
        ax.set_ylim([ymin, ymax])
        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)

        plt.xlabel("$1000 \ \mathrm{K} / T$")
        if 'Unimolecular' in self.reaction_list[0].__class__.__name__:  # unimoelcuar reacitons have different units
            plt.ylabel("$\mathrm{log[k /  s^{-1}]}$")
        else:
            plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")

        plt.legend()

        if max(self.rec_temps) > 1000 / 250 and max(self.rec_temps) < 1000 / 199:
            plt.xticks([1, 2, 3, 4, 5])
        elif max(self.rec_temps) < 1000 / 250:
            plt.xticks([1, 2, 3, 4])
        else:
            pass
        plt.draw()
        plt.tight_layout()
        fig.savefig("plots/compare_" + self.title + ".pdf")
        plt.title(self.title, y=1.1)
        plt.show()

        print("plots/compare_" + self.title + ".pdf")

    def plot_series(self, multiplier=[], with_tst=[], style=None, with_anharmonic_tst=False,
                    with_experimental_data=True, with_deformed_tst=False):

        if style == None:
            style = ['plot'] * len(self.reaction_list)

        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        dashed = True
        s = 0
        unimolecular = False

        # plot sctst and experimental results
        for i in self.reaction_list:
            if style[s] == 'plot':
                line, = ax.plot(self.rec_temps, i.log_rate_sctst, label=str(i.name))
            elif style[s] == 'scatter':
                plt.scatter(self.rec_temps, i.log_rate_sctst, label=str(i.name))
            else:
                raise ValueError("Please enter a valid style, not", style[s])
            if with_experimental_data:
                for exp in i.experimental_rates:
                    # ax.scatter([1000 / temp for temp in exp[0]], [math.log10(rate) for rate in exp[1]], label=exp[2])
                    ax.plot([1000 / temp for temp in exp[0]], [math.log10(rate) for rate in exp[1]], label=exp[2],
                            linestyle='dashed')

            s += 1

        # get the ylim values having pltting the experimental and sctst results. We will then plot the TST results bu RESET the ylim values to best fit the SCTST results (since the TST results go way way down off the page, often
        ymin, ymax = ax.get_ylim()
        # reset the color cycle so TST and ScTST hae the same colors
        plt.gca().set_prop_cycle(None)
        for i in self.reaction_list:
            if with_tst == i.name or i.name in with_tst:
                line, = ax.plot(self.rec_temps, i.log_rate_tst, lw=1.0, linestyle='dashed', label='TST')
            if with_anharmonic_tst == i.name:
                line, = ax.plot(self.rec_temps, i.log_anharmonic_tst, label='Anharmonic TST', lw=1.0,
                                linestyle='dashed')
            if with_deformed_tst == i.name:
                line, = ax.plot(self.rec_temps, [np.log10(x) for x in i.rate_deformed_tst], label='Deformed TST')
                print([np.log10(x) for x in i.rate_deformed_tst])
        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        if max(self.rec_temps) >= 4 and max(self.rec_temps) < 5.02:
            ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
        elif max(self.rec_temps) < 4:
            ax2.set_xticks([1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["300 K", "500 K", "1000 K"])
        elif max(self.rec_temps) < 13:
            ax2.set_xticks([1000 / 100, 1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["100 K", "200 K", "300 K", "500 K", "1000 K"])
        else:
            ax2.set_xticks([1000 / 100, 1000 / 200, 1])
            ax2.set_xticklabels(["100 K", "200 K", "1000 K"])

            pass
        ax.set_ylim([ymin, ymax])
        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)

        plt.xlabel("$1000 \ \mathrm{K} / T$")
        if 'Unimolecular' in self.reaction_list[0].__class__.__name__:  # unimoelcuar reacitons have different units
            plt.ylabel("$\mathrm{log[k /  s^{-1}]}$")
        else:
            plt.ylabel("$\mathrm{log[k / cm^3 \ molecule^{-1} \ s^{-1}]}$")

        plt.legend()

        if max(self.rec_temps) > 1000 / 250 and max(self.rec_temps) < 1000 / 199:
            plt.xticks([1, 2, 3, 4, 5])
        elif max(self.rec_temps) < 1000 / 250:
            plt.xticks([1, 2, 3, 4])
        else:
            pass
        plt.draw()
        plt.tight_layout()
        fig.savefig("plots/compare_" + self.title + ".pdf")
        plt.show()

        print("plots/compare_" + self.title + ".pdf")

    def plot_series_ratio(self, multiplier=[], with_tst=False, with_experimental_data=False):
        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        dashed = True

        for i in self.reaction_list:
            line, = ax.plot(self.rec_temps, [i.log_rate_sctst[j] - self.reaction_list[0].log_rate_sctst[j] for j in
                                             range(len(i.log_rate_sctst))], label=str(i.name))

            if with_tst == i.name:
                line, = ax.plot(self.rec_temps, [i.log_rate_tst[j] - self.reaction_list[0].log_rate_sctst[j] for j in
                                                 range(len(i.log_rate_sctst))], label='TST', lw=1.0, linestyle='dashed')

            if with_experimental_data:
                for exp in i.experimental_rates:
                    from scipy.interpolate import spline

                    xold = np.asarray([1000 / temp for temp in exp[0]])
                    yold = np.asarray([math.log10(exp[1][j]) - self.reaction_list[0].log_rate_sctst[j] for j in
                                       range(len(i.log_rate_sctst))])
                    xnew = np.flip(np.linspace(min(xold), max(xold), num=100))

                    # power_smooth = spline(np.flip(xold),  np.flip(yold), xnew)
                    power_smooth = spline(list(xold)[::-1], list(yold)[::-1], xnew, order=3)

                    # print(power_smooth)
                    ax.scatter(xold, yold, s=4.0)
                    # ax.plot(xold, yold, label=exp[2])
                    ax.plot(xnew, power_smooth)

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        if max(self.rec_temps) >= 4 and max(self.rec_temps) < 5.02:
            ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
        elif max(self.rec_temps) < 4:
            ax2.set_xticks([1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["300 K", "500 K", "1000 K"])
        else:
            ax2.set_xticks([1000 / 100, 1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["100 K", "200 K", "300 K", "500 K", "1000 K"])
            pass

        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)

        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / k_{FD}]}$")  # for comparing rates to the FD rate
        # plt.ylabel("$\mathrm{log[k(H) / k(D)]}$")       # for comparins isotope effects

        plt.legend()

        if max(self.rec_temps) > 1000 / 250 and max(self.rec_temps) < 1000 / 199:
            plt.xticks([1, 2, 3, 4, 5])
        elif max(self.rec_temps) < 1000 / 250:
            plt.xticks([1, 2, 3, 4])
        else:
            pass
        plt.draw()
        plt.tight_layout()
        fig.savefig("plots/compare_rate_ratio_" + self.title + ".pdf")
        plt.title(self.title, y=1.1)
        plt.show()

        print("plots/compare_rate_ratio_" + self.title + ".pdf")

    def plot_series_tunneling_factor(self, multiplier=[], with_tst=False, ymax=None):
        """

        :param multiplier:
        :param with_tst:
        :param ymax:  can specify max y in axes
        :return:
        """
        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        dashed = True

        for i in self.reaction_list:

            if 'FD' in i.__class__.__name__:  # divide the FD rates by anharmonic TST and the 1D rates my TST
                line, = ax.plot(self.rec_temps,
                                [i.log_rate_sctst[j] - i.log_anharmonic_tst[j] for j in range(len(i.log_rate_sctst))],
                                label=str(i.name))
            else:
                line, = ax.plot(self.rec_temps,
                                [i.log_rate_sctst[j] - i.log_rate_tst[j] for j in range(len(i.log_rate_sctst))],
                                label=str(i.name))

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        if max(self.rec_temps) >= 4 and max(self.rec_temps) < 5.02:
            ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
        elif max(self.rec_temps) < 4:
            ax2.set_xticks([1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["300 K", "500 K", "1000 K"])
        else:
            ax2.set_xticks([1000 / 100, 1000 / 200, 1000 / 300, 1000 / 500, 1])
            ax2.set_xticklabels(["100 K", "200 K", "300 K", "500 K", "1000 K"])
            pass

        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)
        if ymax is not None:
            ax.set_ylim(top=ymax)
        plt.xlabel("$1000 \ \mathrm{K} / T$")
        plt.ylabel("$\mathrm{log[k / k_{TST}]}$")
        plt.legend()
        plt.draw()
        fig.savefig("plots/compare_tunneling_factor_" + self.title + ".pdf")
        plt.show()

        print("plots/compare_tunneling_factor_" + self.title + ".pdf")

    def compare_barriers(self, bohr=False, ev=False, reflect_path=False):
        """
        :param bohr:
        :param ev:
        :param reflect_path: E.g. if comparing two different paths, can plot the barriers in opposite directions
        :return:
        """

        reflected = False
        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)

        coordinate_range_min = -200
        coordinate_range_max = 200
        for i in self.reaction_list:

            if "FD" in i.__class__.__name__:  # i.e. if this reaction is a FD one

                barrier_index = 1
                ts_states = pd.read_csv("FD_files/States_Output_" + i.ts.name + ".txt", skiprows=[0])
                coupling_ground_state = ts_states[' average coupling'][barrier_index]
                gound_state_energy = ts_states['Energy'][barrier_index]
                print(gound_state_energy)

                im_freq = i.ts.atomic_imaginary_frequency + coupling_ground_state
                bar_height = i.sctst_barrier + gound_state_energy
                rev_bar_height = i.sctst_rev_barrier + gound_state_energy

                i.reaction_coordinates = np.linspace(coordinate_range_min, coordinate_range_max, 201)
                if i.deep_tunneling:
                    i.wagner_potential = [
                        sc.WagBar(im_freq * pc.hartree_to_cm, i.ts.fd_xff, bar_height * pc.hartree_to_joule,
                                  rev_bar_height * pc.hartree_to_joule, x) for x in i.reaction_coordinates]
                else:
                    forward_barrier = (i.sctst_barrier + i.ts.anharmonic_zpe) * pc.hartree_to_joule
                    D = 0.25 * im_freq * im_freq / i.ts.fd_xff
                    a = im_freq / math.sqrt(-2 * D)
                    i.wagner_potential = [D * (1 - math.exp(1 * a * r)) ** 2 + bar_height for r in
                                          i.reaction_coordinates]

            potential = np.asarray([x * pc.hartree_to_kj_per_mol for x in i.wagner_potential])
            barrier_top = i.reaction_coordinates[np.argmax(potential)]

            shifted_coords = [k - barrier_top for k in
                              i.reaction_coordinates]  # this just shifts everythin g so the zero coordinate is at the maximunm

            if bohr:
                shifted_coords = [i / pc.amu ** 0.5 for i in shifted_coords]
            if ev:
                potential = np.asarray([i * pc.hartree_to_ev / pc.hartree_to_kj_per_mol for i in potential])

            if reflect_path and not reflected:
                shifted_coords = [300 - i for i in shifted_coords]
                reflected = True

            above_zero = potential > 0
            avobe_zero_potential = [i for i, j in zip(potential, above_zero) if j]
            above_zero_coords = [i for i, j in zip(shifted_coords, above_zero) if j]

            ax.plot(shifted_coords, potential, label=str(i.name), linestyle="-")

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        ax2.axis["right"].major_ticklabels.set_visible(False)

        plt.legend()
        plt.ylabel("Energy / kJ / mol")
        plt.xlabel("Reaction Coordinate / atomic units")
        plt.gca().set_ylim(bottom=0)
        plt.gca().set_ylim(top=80)
        plt.draw()
        plt.legend()
        plt.tight_layout()
        plt.savefig("plots/compare_barriers_" + self.title + ".pdf")
        plt.show()

        print("plots/compare_barriers_" + self.title + ".pdf")

    def compare_vibrational_prefactor(self):
        ax = host_subplot(111, axes_class=AA.Axes)

        for i in self.reaction_list:
            ax.plot([1000 / t for t in i.reactant2.temps],
                    [math.log10((i.ts.q_vib[j]) / (i.reactant2.q_vib[j] * i.reactant1.q_vib[j])) for j in
                     range(len(i.reactant2.q_tot))], label=str(i.name))

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        ax2.axis["right"].major_ticklabels.set_visible(False)

        plt.legend()
        plt.ylabel("$\mathrm{Log ( Q_{TS}^{vib} / Q_{R}^{vib}} )$")
        plt.xlabel("$1000/T$")
        plt.draw()
        plt.savefig("plots/compare_qvib_" + self.title + ".pdf")
        plt.show()

        print("plots/compare_qvib_" + self.title + ".pdf")

    def compare_prefactor(self):
        ax = host_subplot(111, axes_class=AA.Axes)

        for i in self.reaction_list:
            ax.plot([1000 / t for t in i.reactant2.temps],
                    [math.log10((i.ts.q_tot[j]) / (i.reactant2.q_tot[j])) for j in range(len(i.reactant2.q_tot))],
                    label=str(i.name))

        ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

        ax2.axis["right"].major_ticklabels.set_visible(False)
        ax2.axis["top"].major_ticklabels.set_visible(True)

        plt.legend()
        plt.ylabel("log($Q^{TS} / Q^{R}$)")
        plt.draw()
        plt.savefig("plots/compare_qtot_" + self.title + ".pdf")
        plt.title(self.title, y=1.1)
        plt.show()
        print("plots/compare_qtot_" + self.title + ".pdf")


class SplittingWell(Unimolecular):
    """
    Class for 1D and FD SCTST tunnelling splittings.
    """

    def __init__(self, reactant1, ts, product1, reactant_freq, label="", full_dimensional=False, ts_vib_state=None,
                 reactant_mode=None, excited_state=False):
        """
        :param full_dimensional: This allows for treating the curvature and variational effects in the reaciton path. If True, need vpt2 log files for all reactants/products
        """

        temps = []  # Tunnelling splittings is a temperature independent thing, unlike rates. Therefore the temeprature we look at is arbitrary.

        super().__init__(reactant1, ts, product1, temps, label)
        self.reactant_freq = reactant_freq

        if full_dimensional:
            self.r_anharmonic_zpe = fd.find_reactant_ZPE(
                "FD_files/States_Output_" + self.reactant1.name + ".txt")  # Hartrees
            ts_xff = ts.fd_xff

            if ts_vib_state is not None:  # tunnelling through excited states UNTESTED
                ts_anharmonic_zpe, freq_adjustment = fd.find_ts_higher_vib_state(
                    "FD_files/States_Output_" + self.ts.name + ".txt", ts_vib_state)
                imaginary_frequency = ts.imaginary_frequency + freq_adjustment * pc.hartree_to_cm
            else:
                ts_anharmonic_zpe = fd.find_reactant_ZPE("FD_files/States_Output_" + self.ts.name + ".txt")
                imaginary_frequency = ts.imaginary_frequency + pc.hartree_to_cm * sum(ts.xmat[0, 1:]) * 0.5

            self.anharmonic_barrier = ((ts.energy - reactant1.energy + ts.fd_g0) + (
                ts_anharmonic_zpe - self.r_anharmonic_zpe)) * pc.hartree_to_joule
            self.reactant_zpe = 0.5 * (self.reactant_freq / pc.hartree_to_cm * pc.hartree_to_joule) + \
                                self.reactant1.xmat[
                                    reactant_mode, reactant_mode] * 0.25 * pc.hartree_to_joule  # in Joules NOTE THIS IS THE REACTION MODE ZPE NOT THE OVERALL ZPE
            self.prefactor = (self.reactant_freq / math.pi)

            # Anharmonic potential well
            effective_reactant_mode_zpe = pc.hartree_to_cm * (
                0.5 * (self.reactant_freq / pc.hartree_to_cm) + self.reactant1.xmat[reactant_mode, reactant_mode] * (
                    1 / 4) + (1 / 8) * sum(
                    self.reactant1.xmat[reactant_mode,
                    :]))  # in Joules NOTE THIS IS THE REACTION MODE ZPE NOT THE OVERALL ZPE
            effective_reactant_freq = self.reactant_freq - sum(reactant1.xmat[reactant_mode, :]) * pc.hartree_to_cm
            effective_dissociation = - effective_reactant_freq ** 2 / (
                4 * reactant1.xmat[reactant_mode, reactant_mode] * pc.hartree_to_cm)
            self.anharmonic_prefactor = (effective_reactant_freq * np.sqrt(
                (self.anharmonic_barrier / pc.hartree_to_joule) / self.barrier) / math.sqrt(math.pi * math.e))

        else:  # 1D treatment
            self.anharmonic_barrier = ((ts.energy - reactant1.energy + ts.g0) * pc.hartree_to_joule) + (
                ts.zpe - reactant1.zpe)
            imaginary_frequency = ts.imaginary_frequency  # In 1D-SCTST, the barrier frequency is equal to the imaginary frequency of the TS.
            self.reactant_zpe = 0.5 * (self.reactant_freq / pc.hartree_to_cm * pc.hartree_to_joule)  # in Joules
            ts_xff = ts.xff
            self.prefactor = (self.reactant_freq / math.pi)
            self.anharmonic_prefactor = self.prefactor  # There is no anharmonicity in the reactant when its a 1D treatment

        self.anharmonic_reverse_barrier = self.anharmonic_barrier

        # Calculate Thetas
        # We only technically need theta at E=0 for calculating the tunnelling splitting, but its probably helpful to calculate it at a range of energies in order to
        # 1) Verify Theta(E) is smooth, continuous and sensible looking
        # 2) It will be needed at E \neq 0, if we want to calculate the tunnelling splittings at excited vibrational levels

        print(
            "-----------------------------------------------------------------------------------------------------------------------------------------------")
        print(self.name)
        theta_0 = sc.TunnelTheta(imaginary_frequency, ts_xff, self.anharmonic_barrier, self.anharmonic_reverse_barrier,
                                 energy=0)
        print("Theta = ", theta_0)

        # Calculate the splitting
        self.delta_LH = self.prefactor * np.exp(-theta_0)  # This should be the correct tunnelling splitting in cm^{-1}.
        self.delta_wkb = self.prefactor * np.arctan(
            np.exp(-theta_0))  # This should be the correct tunnelling splitting in cm^{-1}.

        print("Lifshitz Herring Delta -> ", str(self.delta_LH)[:6], "cm-1 ")
        print("WKB Delta -> ", str(self.delta_wkb)[:6], "cm-1 ")

        # # ANHARMONIC DELTA
        #
        # self.delta_0 = self.anharmonic_prefactor * np.exp(-theta_0)  # This should be the correct tunnelling splitting in cm^{-1}.
        # print("Anharmonic VPT2 Delta -> ", str(self.delta_0)[:5], "cm-1 ->", "(", self.barrier * pc.hartree_to_kj_per_mol, self.anharmonic_barrier * pc.hartree_to_kj_per_mol / pc.hartree_to_joule, imaginary_frequency,
        #       ts_xff * pc.hartree_to_cm, ")")

        theta_wagner = sc.WagTheta(imaginary_frequency, ts_xff, self.anharmonic_barrier + self.reactant_zpe,
                                   self.anharmonic_reverse_barrier + self.reactant_zpe, energy=self.reactant_zpe)
        self.delta_wagner = self.prefactor * np.exp(
            -theta_wagner)  # This should be the correct tunnelling splitting in cm^{-1}.
        print(
            "-----------------------------------------------------------------------------------------------------------------------------------------------\n")

        # Stuff for plotting the barrier
        imaginary_frequency /= pc.hartree_to_cm
        D = 0.25 * imaginary_frequency * imaginary_frequency / (ts_xff)

        a = imaginary_frequency / math.sqrt(-2 * D)
        self.reaction_coordinates = np.linspace(-100, 100, 201)
        self.vpt2_potential = [D * (1 - math.exp(1 * a * r)) ** 2 + self.anharmonic_barrier / pc.hartree_to_joule for r
                               in self.reaction_coordinates]
        self.wagner_potential = [sc.WagBar(ts.imaginary_frequency, ts.xff, self.anharmonic_barrier + self.reactant_zpe,
                                           self.anharmonic_reverse_barrier + self.reactant_zpe, x) for x in
                                 self.reaction_coordinates]

    def compare_barrierss(self):
        vpt2_adjusted = [
            self.reactant_zpe * pc.hartree_to_kcal_per_mol / pc.hartree_to_joule + i * pc.hartree_to_kcal_per_mol for i
            in self.vpt2_potential]
        r = [0.3 + i / pc.amu ** 0.5 for i in self.reaction_coordinates]
        plt.axhline(y=self.reactant_zpe * pc.hartree_to_kcal_per_mol / pc.hartree_to_joule, linestyle=':')
        plt.plot([i + 0.25 for i in r], vpt2_adjusted)
        plt.plot(r, [i * pc.hartree_to_kcal_per_mol for i in self.wagner_potential])
        plt.gca().set_ylim(bottom=min(0, vpt2_adjusted[0]), top=1.1 * max(vpt2_adjusted))
        plt.show()

    def plot_thetas(self):
        """
        Plot Theta(E) for the bound "unimolecular" potenital, for the Wagner potential and for the pure VPT2 potential. Only the bound potential gives the correct tunnell splitting
        """
        plt.plot(self.energy_list / self.anharmonic_barrier, self.bound_thetas_f, label="Unimolecular")
        plt.plot(self.energy_list / self.anharmonic_barrier, self.vpt2_thetas, label="VPT2")
        plt.plot(self.energy_list / self.anharmonic_barrier, self.unbound_thetas, linestyle=":",
                 label="Wagner DT correction")
        plt.xlabel("Energy / V0")
        plt.ylabel("$\\theta$")
        plt.tight_layout()
        plt.legend()
        print("plots/" + self.name + "_compare_thetas_well.pdf")
        plt.savefig("plots/" + self.name + "_compare_thetas_well.pdf")
        plt.show()

    def plot_theta_segment_contributions(self):
        """
        Plot Theta(E) for the bound "unimolecular" potenital, breaking down contributions fromt he central and edge barriers.
        """
        centre = []
        edges = []
        totals = []
        for e in self.energy_list:
            # print()
            [_, central, edge, total] = sc.BoundTheta_individual_contributions(self.ts.imaginary_frequency,
                                                                               self.reactant_freq, self.ts.xff,
                                                                               self.anharmonic_barrier,
                                                                               self.anharmonic_reverse_barrier, e)
            centre.append(central)
            edges.append(edge)
            totals.append(total)

        plt.plot(self.energy_list / self.anharmonic_barrier, centre, label="Central segment")
        plt.plot(self.energy_list / self.anharmonic_barrier, edges, label='Edge Segment')
        plt.plot(self.energy_list / self.anharmonic_barrier, totals, label="Total", c='k', linewidth=2)

        plt.xlabel("Energy / V0")
        plt.ylabel("$\\theta$")
        plt.tight_layout()
        plt.legend()
        print("plots/" + self.name + "_compare_thetas_well_segments.pdf")
        plt.savefig("plots/" + self.name + "_compare_thetas_well_segments.pdf")
        plt.show()

    def plot_well(self):
        """
        Plot the potential well
        """

        plt.plot(self.reaction_coordinates, self.vpt2_potential / self.anharmonic_barrier * pc.hartree_to_joule,
                 label="VPT2")
        plt.plot(self.reaction_coordinates, self.well_potential / self.anharmonic_barrier * pc.hartree_to_joule,
                 label="Unimolecular")
        plt.legend()
        plt.ylabel("$E / V_1$")
        plt.tight_layout()
        print("plots/well_" + self.name + ".pdf")
        plt.savefig("plots/well_" + self.name + ".pdf")
        plt.title("Barrier = " + str(self.anharmonic_barrier / pc.hartree_to_joule))
        plt.show()

    def plot_well_composite(self):
        """
        Plot the potential well, with the three barrier that compose it
        """

        a = []
        b = []
        c = []

        for x in self.reaction_coordinates:
            [r, ba, v, b_prime, b_prime_prime] = sc.BoundBar_composite(self.ts.imaginary_frequency, self.reactant_freq,
                                                                       self.ts.xff, self.anharmonic_barrier,
                                                                       self.anharmonic_barrier, x)

            a.append(r)
            b.append(ba)
            c.append(v)

        r = [i * pc.bohr_to_m / pc.angstrom_to_m / pc.amu ** 0.5 for i in self.reaction_coordinates]
        self.well_potential = [
            sc.BoundBar(self.ts.imaginary_frequency, self.reactant_freq, self.ts.xff, self.anharmonic_barrier,
                        self.anharmonic_barrier, x) for x in self.reaction_coordinates]

        plt.plot(self.reaction_coordinates, self.wag_potential / self.anharmonic_barrier * pc.hartree_to_joule,
                 label="Wag Potential")
        plt.plot(self.reaction_coordinates, a / self.anharmonic_barrier * pc.hartree_to_joule, label="a")
        plt.plot(self.reaction_coordinates, b / self.anharmonic_barrier * pc.hartree_to_joule, label="b")
        plt.plot(self.reaction_coordinates, c / self.anharmonic_barrier * pc.hartree_to_joule, label="c")
        plt.plot(self.reaction_coordinates, self.well_potential / self.anharmonic_barrier * pc.hartree_to_joule,
                 label="Well Potential")
        plt.axvline(x=b_prime, linestyle=':')
        plt.axvline(x=b_prime_prime, linestyle=':')

        plt.ylabel("$E / V_1$")
        plt.legend()
        plt.tight_layout()
        print("plots/well_composite_" + self.name + ".pdf")
        plt.savefig("plots/well_composite_" + self.name + ".pdf")
        plt.title("Barrier = " + str(self.anharmonic_barrier / pc.hartree_to_joule))
        plt.show()

    def plot_well_composite_limits(self):
        """
        Plot the potential well, with the three barrier that compose it
        """

        a = []
        b = []
        c = []

        for x in self.reaction_coordinates:
            [r, ba, v, b_prime, b_prime_prime] = sc.BoundBar_composite(self.ts.imaginary_frequency, self.reactant_freq,
                                                                       self.ts.xff, self.anharmonic_barrier,
                                                                       self.anharmonic_barrier, x)

            a.append(r)
            b.append(ba)
            c.append(v)

        r = [i * pc.bohr_to_m / pc.angstrom_to_m / pc.amu ** 0.5 for i in self.reaction_coordinates]
        self.well_potential = [
            sc.BoundBar(self.ts.imaginary_frequency, self.reactant_freq, self.ts.xff, self.anharmonic_barrier,
                        self.anharmonic_barrier, x) for x in self.reaction_coordinates]

        central_low, central_high, edge_low, edge_high = sc.BoundTheta_individual_contributions_limits(
            self.ts.imaginary_frequency, self.reactant_freq, self.ts.xff, self.anharmonic_barrier,
            self.anharmonic_barrier, 0)

        plt.plot(self.reaction_coordinates, a / self.anharmonic_barrier * pc.hartree_to_joule, label="a")
        plt.plot(self.reaction_coordinates, b / self.anharmonic_barrier * pc.hartree_to_joule, label="b")
        plt.plot(self.reaction_coordinates, c / self.anharmonic_barrier * pc.hartree_to_joule, label="c")
        plt.plot(self.reaction_coordinates, self.well_potential / self.anharmonic_barrier * pc.hartree_to_joule,
                 label="Well Potential")
        plt.axvline(x=b_prime, linestyle=':')
        plt.axvline(x=b_prime_prime, linestyle=':')

        plt.axvline(x=central_low, linestyle='-')
        plt.axvline(x=central_high, linestyle='-')
        plt.axvline(x=edge_low, linestyle='--', color="red")
        plt.axvline(x=edge_high, linestyle='--', color="red")

        print("central_low, central_high, edge_low, edge_high", central_low, central_high, edge_low, edge_high)

        plt.ylabel("$E / V_1$")
        plt.legend()
        plt.tight_layout()
        print("plots/well_composite_" + self.name + ".pdf")
        plt.savefig("plots/well_composite_" + self.name + ".pdf")
        plt.title("Barrier = " + str(self.anharmonic_barrier / pc.hartree_to_joule))
        plt.show()


class ParabolicReaction(Bimolecular):  # Bimolecular
    """
    Class for 1D bimolecular parabolic wigner tunnelling (this is the same as setting xff = 0 and not including deep tunneling corrections. HOWEVER the inverstion of theta assumes xff != 0, so the equations are different
    """

    def __init__(self, reactant1, reactant2, ts, product1, product2, temps, label=""):
        super().__init__(reactant1, reactant2, ts, product1, product2, temps, label)

        self.experimental_rates = []

        self.reaction_coordinates = np.linspace(-100, 100, 201)
        # Calculate CRP
        nSteps = 5000  # this might need to be higher for v low temp reacitons
        eMax = 4.0025 * self.anharmonic_barrier
        eMin = 0

        Estep = eMax / nSteps
        self.energy_list = np.linspace(eMin, eMax, nSteps - 1)

        theta = (np.pi / self.ts.atomic_imaginary_frequency) * (
            self.anharmonic_barrier - self.energy_list) / pc.hartree_to_joule
        print(theta)
        P_E = 1 / (1 + np.exp(2 * theta))

        self.CRPWag = P_E

        self.weightWag = []
        self.Wag = []
        for k in range(len(temps)):
            # perform numerical boltzman integration
            weight_list = [self.CRPWag[i] * math.exp((-self.energy_list[i] / (pc.kb * temps[k]))) for i in
                           range(len(self.energy_list))]
            averagedWag = [0.5 * (weight_list[i] + weight_list[i + 1]) for i in range(len(self.energy_list) - 1)]
            self.Wag.append(sum(averagedWag))

        self.sctstRate = [((ts.q_tot[i] / (reactant1.q_tot[i] * reactant2.q_tot[i])) * (100 ** 3) * self.Wag[
            i] * Estep * self.symmetry_change / pc.h).real for i in range(len(temps))]

        self.log_rate_sctst = [math.log10(rate) if rate != 0 else 0 for rate in
                               self.sctstRate]  # If a rate comes out as zero, it cannot be logged!

        self.wagner_potential = [
            self.anharmonic_barrier / pc.hartree_to_joule - 0.5 * ts.atomic_imaginary_frequency ** 2 * x * x for x in
            self.reaction_coordinates]


class AssymetricEckartReaction(Bimolecular):  # Bimolecular
    """
    Class for 1D bimolecular tunnelling fit to an assymetric eckart barrier
    based on:
    https://pubs.acs.org/doi/pdf/10.1021/j100809a040
    """

    def __init__(self, reactant1, reactant2, ts, product1, product2, temps, label=""):
        super().__init__(reactant1, reactant2, ts, product1, product2, temps, label)

        self.experimental_rates = []

        self.reaction_coordinates = np.linspace(-100, 100, 201)

        v1 = self.anharmonic_barrier / pc.hartree_to_joule
        v2 = self.anharmonic_reverse_barrier / pc.hartree_to_joule

        # eckart parameters
        A = v1 - v2

        B = (v1 ** 0.5 + v2 ** 0.5) ** 2

        F_star = ts.atomic_imaginary_frequency ** 2
        L = 2 * math.pi * math.sqrt(2 / F_star) * (((1 / math.sqrt(v1)) + (1 / math.sqrt(v2))) ** (-1))

        # Calculate CRP
        nSteps = 5000  # this might need to be higher for v low temp reacitons, shouwld be 5000
        eMax = 4.0025 * v1
        eMin = 0

        Estep = eMax / nSteps
        self.energy_list = np.linspace(eMin, eMax, nSteps - 1)

        # wagenrs try
        rho = math.sqrt(v2 / v1)

        a = (2 / ts.atomic_imaginary_frequency) * (rho / (1 + rho)) * np.sqrt(v1 * self.energy_list)
        b = (2 / ts.atomic_imaginary_frequency) * (rho / (1 + rho)) * np.sqrt(
            v1 * (self.energy_list - v1 * (1 - rho * rho)))
        d = (2 / ts.atomic_imaginary_frequency) * rho * v1 * cmath.sqrt(
            1 - (ts.atomic_imaginary_frequency / (4 * rho * v1)) ** 2)

        # print(d)

        if d.real == 0:
            d = d.imag
            P_E = 1 - (((np.cosh(2 * np.pi * a - 2 * np.pi * b)) + np.cos(2 * np.pi * d)) / (
                (np.cosh(2 * np.pi * a + 2 * np.pi * b)) + np.cos(2 * np.pi * d)))

        else:
            d = d.real
            P_E = 1 - (((np.cosh(2 * np.pi * a - 2 * np.pi * b)) + np.cosh(2 * np.pi * d)) / (
                (np.cosh(2 * np.pi * a + 2 * np.pi * b)) + np.cosh(2 * np.pi * d)))
            P_E = np.nan_to_num(P_E)

        self.CRPWag = P_E

        self.weightWag = []
        self.Wag = []

        for k in range(len(temps)):
            # perform numerical boltzman integration
            weight_list = [self.CRPWag[i] * math.exp((-self.energy_list[i] * pc.hartree_to_joule / (pc.kb * temps[k])))
                           for i in range(len(self.energy_list))]
            averagedWag = [0.5 * (weight_list[i] + weight_list[i + 1]) for i in range(len(self.energy_list) - 1)]
            self.Wag.append(sum(averagedWag))

        self.sctstRate = [((ts.q_tot[i] / (reactant1.q_tot[i] * reactant2.q_tot[i])) * (100 ** 3) * self.Wag[
            i] * Estep * pc.hartree_to_joule * self.symmetry_change / pc.h).real for i in range(len(temps))]

        self.log_rate_sctst = [math.log10(rate) if rate != 0 else 0 for rate in
                               self.sctstRate]  # If a rate comes out as zero, it cannot be logged!

        # calculate the actual potential
        y = -1 * np.exp(2 * np.pi * self.reaction_coordinates / L)
        V = (-1 * A * y / (1 - y)) - (B * y / ((1 - y) ** 2))
        self.wagner_potential = V
