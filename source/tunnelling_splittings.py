"""
Module to do stuff to calculate tunnelling splittings in symmetric well potentials
"""
import math

import semi_classical as sc


def oned_theta(freq, xff, bar_height, rev_bar_height, energy, reversed_flag=False):
    """
    Returns the 1D value of theta. This function is nearly identical to WagProb in semiclassical.py, but instead returns theta instead of P(E)
    :param freq: in atomic units
    :param xff: in atomic units
    :param bar_height: in atomic units
    :param rev_bar_height: in atomic units
    :param energy: in atomic units
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # These vairables jsut store the input values, which are changed units just after
    in_freq = freq
    in_bar_height = bar_height
    in_rev_bar_height = rev_bar_height
    in_energy = energy
    in_xff = xff

    # Originally, the code made sure it did its calcualtion in the exothermic direction.
    # This is fine, but for some reactions (high frequncy + small barrier) the matcing conditions in the compositie potential don't work!
    # In this case, you have to try doing it in the endothermic direciton.
    # In order for that to work, i have disabled the auto-exothermic snippet, but added a warning that you are looking at an endothermic reaciton

    # if bar_height > rev_bar_height:
    #     print("Warning: WagProb has been called with an endothermic potential. This is either because your reaction is endothermic, or your barrier is sufficiently weird that the matching "
    #           "conditions don't work! I am trying to use the reverse potential with a shifted energy to fix it!")
    #     en = energy - (bar_height - rev_bar_height)
    #     delVf = rev_bar_height
    #     delVr = bar_height
    # else:
    #     en = energy
    #     delVf = bar_height
    #     delVr = rev_bar_height
    en = energy
    delVf = bar_height
    delVr = rev_bar_height

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:
        # print("one")
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    if (Db / delVf) < 0:
        # print("two")
        return 0
    f = (Db / delVf) ** 0.5
    ur = (((rho * rho + rho + 1) ** 0.5) - (rho - 1)) / max(3 * f, 3)
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (f - rho * ((1 + f) ** 2) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (1 - rho) * f * f * f * ur * ur * ur + 105 * f * f * f * f * ur * ur * ur * ur)

    alphar = alphab * f * Rr
    umr1 = (((1 - rho) * (1 - rho) * (1 - Rr * f) * (1 - Rr * f) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) ** 0.5 + (1 - rho) * (1 - Rr * f)) / (
                2 * (1 - Rr * f * f))  # (S2.11) supp. info. umr is the corssover coordinate, and this eqtn requires the derivatives mathch here
    umr2 = (-1) * umr1

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab  # (S2.13)

    # --------------------- DEEP TUNELING ISSUES ------------------------------------------------------------------------------------------------------------------------------------------
    # The deep tunneling corrections have an issue, which is that for low barriers with high frequencies, the matching crieteria cannot be met, and you get a math error because it tries to log a
    # negative number!
    # It turns out that, in this situation, the reverse barrier can be constructed normally (e.g. by swapping the forward and reverse barriers)
    # The solution is thus simple. If this is an issue, we reverse the barrier, and calcualte the tunneling integrals as before
    # we then have to shift the energy by the difference in barrier heights.
    # NOTE this works because the tunneling integral is independent of whicih direction you are going!
    # this is done in the following line, where the test for an error is followed by calling the same function, with reversed barriers and a shifted energy

    # if ((1-f*umr) / (rho+f*umr) <= 0):  # important, since (1-f*umr) is about to be logged in the next line!
    # if reversed_flag == False:
    #     return WagProb(in_freq, in_xff, in_rev_bar_height, in_bar_height, in_energy + in_rev_bar_height - in_bar_height, reversed_flag=True)

    yrb = math.log(((1 - f * umr) / (rho + f * umr))) / alphar  # (S2.12) This is the value of y at which Vr matches Vb in value and slope

    # dr = -ybr + math.log(((1 - f * umr) / (rho + f * umr))) / alphar
    dr = -ybr + yrb  # same as above, but clearer        (S2.14)
    ###################
    up = (((rho ** 2 + rho + 1) ** 0.5) + rho - 1) / max(3 * f, 3)
    Rp = (210 * rho * rho - 168 * (1 + f) * rho * (1 - rho) * up + 140 * (f - (1 + f) ** 2 * rho + f * rho ** 2) * up * up + 120 * f * (1 + f) * (1 - rho) * up ** 3 + 105 * f ** 2 * up ** 4) / (
                210 * rho ** 2 - 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho ** 2) * f ** 2 * up ** 2 + 240 * (1 - rho) * f ** 3 * up ** 3 + 105 * f ** 4 * up ** 4)

    alphap = alphab * f * Rp

    ump1 = (((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    ump2 = (-((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1

    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab

    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp

    zrb = math.exp(alphar * (ybr + dr))
    zbr = math.exp(alphab * ybr)
    zpb = math.exp(alphap * (ybp + dp))
    zbp = math.exp(alphab * ybp)
    headb = 2 * Db / freq * rho / (1 + rho)
    headr = headb * delVf / Db / Rr
    headp = headr * Rr / Rp

    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    ep = thtaEn - delVf + Db

    eta = ep / Db

    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 assymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        thta = headb * sc.thetaReg(2, zlo, zhi, eta, rho)
    else:  # The energy is below the VPT2 assymptote
        zlo = zbr
        zhi = zbp
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        thta = headb * sc.thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)

    if zlo == zbr:
        eta = thtaEn / delVf
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)

        zlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zlo - 2 * sqrtdelld2a
        thta += headr * sc.thetaReg(1, zlo, zrb, eta, rho)

        if zhi == zbp:
            thta += headp * sc.thetaReg(3, zpb, zzhi, eta, rho)
    else:
        pass

    return thta.real


def find_delta(reactant_freq, im_freq, xff, bar_height, rev_bar_height):
    """
    :return:    Delta (float)
    """

    zpe_energy = im_freq / 2
    theta = oned_theta(im_freq, xff, bar_height, rev_bar_height, zpe_energy)

    return reactant_freq / (math.sqrt(math.pi * math.e)) * math.exp(-theta)
