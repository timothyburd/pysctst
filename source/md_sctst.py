"""
This file contains code to do MD-SCTST with (in theory) as many active modes as you like

The results of which are in the JCPA review paper.

NOTE THIS IS NOT THE MOST EFFECIENT WAY OF DOING THIS! The method used here is that of the physical interpretation, as outlines in the JCPA paper. It basically (literally) does
a 1D calculation for every effective barrier height/effective frequency and adds them up.

"""

import itertools
from operator import add
import copy
import matplotlib.pyplot as plt
import mpl_toolkits.axisartist as AA
import numpy as np
from mpl_toolkits.axes_grid1 import host_subplot

import physical_constants as pc
import sctst


def md_sctst(chemical_list, transition_modes, temperatures, plot_barriers=False, spectator_coupling=False,
             max_quanta=10, deep_tunneling=True, verbose = False):

    """
    Returns a multidimensional SCTST rate
    :param chemical_list: e.g. [r,ts,p] or [r,r2,ts,p1,p2]
    :param transition_modes: List containing the indicies of the transition modes to be treated anharmonically, indexed from high to low
    :param temperatures: Temperature list
    :param plot_barriers: Produce a plot showing the barriers for each vibrational state of te transition modes
    :param spectator_coupling: Include transition mode - transition mode coupling
    :param max_quanta: How many vibrational states of the transition modes to count (ensure result is converged). Typically 5 for a high freq mode is fine
    :param deep_tunneling: Include Wagner's deep tunneling corrections
    :param verbose: Print output as to whats going on 
    :return: Rate list of same length as temperature list
    """

    if verbose: print(len(transition_modes)+1, "D calculation")

    if plot_barriers:  # This will produce a plot of all the effective barriers. This just sets up the plotting env
        fig = plt.figure(figsize=(6, 6))
        ax = host_subplot(111, axes_class=AA.Axes)


    # --------- Determine if reaction is unimolecular or bimolecular ------------------------------------------------ #
    if len(chemical_list) == 3:
        unimolecular = True
        [r, ts_tmp, p] = chemical_list
    elif len(chemical_list) == 5:
        unimolecular = False
        [r1, r2, ts_tmp, p1, p2] = chemical_list
    else:
        raise ValueError("Chemical list should have three items for a unimolecular reaction, or 5 items for a \
        bimolecular reaction")
    ts = copy.deepcopy(ts_tmp)

    # --------- Identify active/transition/spectator modes ---------------------------------------------------------- #
    active_modes = [0] + transition_modes  # List of active mode indicies. 0 is the imaginary mode index.
    transition_mode_freqs = [ts.atomicfrequencies[-i] for i in transition_modes]  # List of active mode frequencies.
    n_modes = len(active_modes)             # Number of active modes
    if verbose: print("Transition mode frequencies are: \n", [i * pc.hartree_to_cm for i in transition_mode_freqs])

    # --------- Produce the x sub matrix from the n_modes active modes ---------------------------------------------- #
    x_sub_matrix = np.zeros((n_modes, n_modes))
    for counter, value in enumerate(active_modes):
        for counter2, value2 in enumerate(active_modes):
            x_sub_matrix[counter, counter2] = ts.xmat[value, value2]
    ts.xff = x_sub_matrix[0, 0]  # we want to be using the FD value of xff when we do our '1D' rate calculations

    # -----------------------------------------------------------------------------------------------------------------#
    if not spectator_coupling:
        # For consistancy, anything you treat as anharmonic in the TS, should also be treated as anharmonic in the
        # reactant
        # This is kinda annoying as it would require you to project out DOFs etc
        # An alternative is to ONLY include anharmonic terms along the reaction mode and those that couple to the
        # reaction mode
        # As these anharmonicicities can accuratly be approimated as zero in the reacant (usually)
        # To do this, set all elements of the anharmonic matrix that are not in row or column 0 to zero.
        # THIS BIT IS STRICTLY OPTIONAL AND IM NOT SURE IF THIS IS THE BEST WAY TO DO IT!
        # This also has the nice effect that all vibrational states are bound, so there is no need to check!
        for i in range(1, n_modes):
            for j in range(1, n_modes):
                x_sub_matrix[i, j] = 0
        if verbose: print("The active sub matrix is:\n", x_sub_matrix * pc.hartree_to_cm)

    # -----------------------------------------------------------------------------------------------------------------#

    # Remove the transition modes from the freq list, so these are not put in the partition function calculation
    # Must be careful about the ordering of the modes. In ts.frequencies they are ordered from low to high freq
    # BUT in the input, we are going high to low. Therefore, the corresponding mode indicies in ts.frequencies are:
    transition_modes_flipped = [len(ts.frequencies) - t for t in transition_modes]
    # thus the spectator mode FREQUENCIES are:
    spectator_list = [i for j, i in enumerate(ts.frequencies) if j not in transition_modes_flipped]
    ts.manual_set_frequencies(spectator_list)

    # --------- Set up things to keep track of rates at each temperature -------------------------------------------- #
    total_rate = [0] * len(temperatures)            # This stores the rates at each temperatures
    total_classical_rate = [0] * len(temperatures)  # This stores the TST rates at each temperature

    # --------- Store the TS energies and imaginary freqs ----------------------------------------------------------- #
    ts_energy_0 = ts.energy
    ts_freq_0 = ts.atomic_imaginary_frequency

    # --------- Generate a list of the possible vibrational states of each mode ------------------------------------- #
    quanta = range(max_quanta)

    # --------- >2D calculation ------------------------------------------------------------------------------------- #
    if len(transition_modes) > 1:
        #  Itterate over all combinations of quantum numbers, with max value 'quanta'
        for quanta_list in itertools.product(quanta, repeat=len(transition_modes)):

            if verbose: print(quanta_list)

            # ---------- Calculate energy of state using VPT2 expression  ------------------------------------------- #
            vibrational_energy = ts_energy_0
            for i in range(len(transition_modes)):
                for j in range(i, len(transition_modes)):
                    vibrational_energy += x_sub_matrix[i, j] * (quanta_list[i] + 0.5) * (quanta_list[j] + 0.5)
                vibrational_energy += transition_mode_freqs[i] * (quanta_list[i] + 0.5)
            ts.manual_set_energy(vibrational_energy)

            # ---------- Set Frequency ------------------------------------------------------------------------------ #
            freq = ts_freq_0 + sum([(quanta_list[k] + 0.5) * x_sub_matrix[0, k + 1] for k in range(len(quanta_list))])
            ts.manual_set_im_freq(freq)

            # ---------- Perform 1D SCTST calculations -------------------------------------------------------------- #
            if unimolecular:
                reaction = sctst.UnimolecularReaction(r, ts, p, temperatures, label=str(quanta_list),
                                                      deep_tunneling=deep_tunneling)
            else:
                reaction = sctst.Reaction(r1, r2, ts, p1, p2, temperatures, label=str(quanta_list),
                                          deep_tunneling=deep_tunneling)

            new_total_rate = list(map(add, total_rate, reaction.sctstRate))
            new_total_classical_rate = list(map(add, total_classical_rate, reaction.rate_tst))

            # # # ------------------------- Test for Convergence ------------------------------------------- #
            # # # ---- I have turned this off
            # change = list(map(operator.sub, new_total_rate, total_rate))  # Change in rates
            # fraction_change = np.asarray(list(map(operator.truediv, change, new_total_rate)))  # fractional change in rates
            # converged = fraction_change < 0.05  # returns array with True or Falses
            # print(converged)
            # if False not in converged:  # i.e. if all rates have converged
            #     break

            total_rate = new_total_rate
            total_classical_rate = new_total_classical_rate

            if plot_barriers:
                ax.plot(reaction.reaction_coordinates, reaction.wagner_potential, label="n = " + str(quanta_list))

    # --------- 2d calculation -----------------------------------------------------------------------------------
    else:
        for n1 in range(max_quanta):  # iterate over all combinations of numbers
            quanta_list = [n1]
            if verbose: print(n1)

            # ---------- Calculate energy of state using VPT2 expression  ------------------------------------------- #
            barrier = ts_energy_0 + (n1 + 0.5) * transition_mode_freqs[0] + (n1 + 0.5) ** 2 * x_sub_matrix[1, 1]
            ts.manual_set_energy(barrier)

            # ------------------ Set Frequency ---------------------------------------------------------------------- #
            freq = ts_freq_0 + (n1 + 0.5) * x_sub_matrix[0, 1]
            ts.manual_set_im_freq(freq)

            # ---------- Perform 1D SCTST calculations -------------------------------------------------------------- #
            if unimolecular:
                reaction = sctst.UnimolecularReaction(r, ts, p, temperatures, label=str(quanta_list),
                                                      deep_tunneling=deep_tunneling, theta_integration=False)
            else:
                reaction = sctst.Reaction(r1, r2, ts, p1, p2, temperatures, label=str(quanta_list),
                                          deep_tunneling=deep_tunneling, theta_integration=False)

            new_total_rate = list(map(add, total_rate, reaction.sctstRate))
            new_total_classical_rate = list(map(add, total_classical_rate, reaction.rate_tst))

            # # ------------------------- Test for Convergence ------------------------------------------- #
            # # I have disabled this byt having a negative crieteria
            # change = list(map(operator.sub, new_total_rate, total_rate))  # Change in rates
            # fraction_change = np.asarray(list(map(operator.truediv, change, new_total_rate)))  # fractional change in rates
            # converged = fraction_change < -0.0001  # returns array with True or Falses
            # if False not in converged:  # i.e. if all rates have converged
            #     break

            total_rate = new_total_rate
            total_classical_rate = new_total_classical_rate

            if plot_barriers:
                ax.plot(reaction.reaction_coordinates, reaction.wagner_potential, label="n = " + str(n1))

    if plot_barriers:
        plt.xlabel("Reaction Coordinate")
        plt.ylabel("Energy (Hartrees)")
        plt.legend(loc='best')
        plt.savefig("plots/" + ts.name + "_" + str(n_modes) + "d_reaction_barriers.pdf")
        print("plots/" + ts.name + "_" + str(n_modes) + "d_reaction_barriers.pdf")
        plt.show()

    return total_rate
