// The is an attempt to alter the Wang-landau method (from the file density_of_states.c) to fit the "SCTST-theta" method described in the JPCA paper.
// I dont think it really works

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>


#define Emax 0.30             // chosen to give 6000 steps
#define Estep 0.00005        // =11 cm-1
#define Niter 21
#define Ntrial 10000000  // original 10000000


int nvmax(unsigned int nv[], unsigned int k, double Eu, double anh[], double freq[], unsigned int n);
double nNRG(double *freq, double *anh, unsigned int *nv, unsigned int n);
int ckderiv(double *freq, double *anh, unsigned int *nv, unsigned int n); // returns 0 if ok, 1 if fail
double nCoup(double *coup, unsigned int *nv, unsigned int n);

int main(int argc, const char * argv[]) {


    // Specify the input file. This is generated automaically by the python code, by the transition_state method "write_states_input"
    // Will generate a file called "FD_files/name_states_input.txt"
    FILE *inFile = fopen(argv[1], "r");  // Contains N bonds, frrquencies, X_nF and X_ij
    double xff = 1.0;
    double xff_rec = 1.0 / xff;
    double im_freq = 1.0; // TO DO READ THESE IN PROPERLY


    char *buf=NULL;
    size_t bufLen;
    getline(&buf, &bufLen, inFile);

    unsigned int nModes = 1;
    fscanf(inFile, "%d", &nModes);  // number of modes


    fscanf(inFile, "%s", buf);
    getline(&buf, &bufLen, inFile);
    double freq[nModes];
    unsigned int i,j,k,l;
    for (i = 0; i < nModes; i++) {
        fscanf(inFile, "%lf,", &freq[i]);   // frequency of modes
    }
    double coupling[nModes];
    fscanf(inFile, "%s", buf);
    getline(&buf, &bufLen, inFile);
    for (i=0; i < nModes; i++) {
        fscanf(inFile, "%lf,", &coupling[i]);   // X_nF values (coupling to reaction coordinate)
    }


    double anh[nModes][nModes];
    fscanf(inFile, "%s", buf);
    getline(&buf, &bufLen, inFile);
    for (i=0; i < nModes; i++) {
        for (j=0; j < nModes; j++) {
            fscanf(inFile, "%lf,", &anh[i][j]);     // X_ij values
        }
    }

    printf("Read in data:\n");
    printf("Number of Modes: %i\n", nModes);
    printf("Frequencies: ");
    for (int i=0; i<nModes; i++){printf("%f, ", freq[i]);}
    printf("\n Reaction coordinate coupling: ");
    for (int i=0; i<nModes; i++){printf("%f, ", coupling[i]);}
    printf("\n");
    printf("Anharmonic Constants: \n");
    for (int i=0; i<nModes; i++){
        for (j=0; j<nModes; j++){
            printf("%f, ", anh[i][j]);
        }
        printf("\n");
    }


    unsigned int nSteps = Emax / Estep;
    double g[nSteps];       // This is the (log of the) Un-normalsied density of states
    double TTF[nSteps];
    double Ev[nSteps];
    for (i = 0; i < nSteps; i++) {
        g[i] = 0;
        TTF[i] = 0;
        Ev[i] = 0;
    }
    double lnf = 1;

    // Calculate anharmonicly corrected ZPE
    double ZPE = 0;
    for (i = 0; i < nModes; i++) {
        ZPE += freq[i] / 2;
        for (j = i; j < nModes; j++) {
            ZPE += anh[i][j] / 4;
        }
    }

    for (i=0; i<nModes; i++){printf("%f\n ", freq[i]);}

    time_t t;
    srand((unsigned) time(&t));
    rand(); // This randomly generated number will always be the same, but subsequent calls to rand() will generate random numbers

    // Initialize quantum numbers i.e. vibrational states of each of the modes
    unsigned int nvold[nModes];
    int ntest = 1;
    while (ntest == 1) {
        double Estart = (double)rand() / (double)RAND_MAX * Emax;    // Random energy between 0 and Emax

        // initialise states as 0
        for (i = 0; i < nModes; i++) {
            nvold[i] = 0;
        }

        //
        for (l = 0; l < nModes; l++) {
            double Eu = Estart - nNRG(freq, (double *)anh, nvold, nModes);   // Calculates energy change
            if (Eu != 0) {
                int nstart = nvmax(nvold, l, Eu, (double *)anh, freq, nModes);  // Calculates the maximum allowed quantum number
                if (nstart == -1) {    // Happens if Eu <0
                    break;
                }
                nvold[l] = nstart * (double)rand() / (double)RAND_MAX;
            }
        }
        ntest = ckderiv(freq, (double *)anh, nvold, nModes); //  If derrivative test passed, ntest =0 then escape this while loop
        if (ntest == 1) {

            for (t=0; t<nModes; t++){printf("%d ", nvold[t]);}

            int sum = 0;
            for (t=0; t<nModes; t++){sum = sum + nvold[t];}
            if (sum ==0){printf("Derivative test failed even in lowest energy state! Are your bonds too anharmonic, Tim?\n");}
            if (sum>0){printf("Failed derivative test\n");}



        }
    }
    double Eold = nNRG(freq, (double *)anh, nvold, nModes) - ZPE;
    unsigned int nold = Eold / Estep;    // I think this is the energy bin the state sits in

    double p = 1.0 / nModes;
    // Begin algorithm
    // I think the first Niter itterations are just to equilibrate the system? Then only the last itteration is used
    // to compute the couplings  and energies?
    for (i = 0; i < Niter; i++) {    // Niter is like 12
        printf("Iteration %u of %u\n", i + 1, Niter + 1);

        for (j = 0; j < Ntrial; j++) {    // Ntrial is like 100000000
            unsigned int nvnew[nModes];    // new state list
            int allZero = 1;
            for (k = 0; k < nModes; k++) {
                double r1 = (double)rand() / (double)RAND_MAX;
                if (r1 <= p) {
                    int new = nvold[k] - 1;
                    if (new < 0) {
                        new = 0;
                    }
                    nvnew[k] = new;
                }
                else if (r1 <= 2 * p) {
                    nvnew[k] = nvold[k] + 1;
                }
                else {
                    nvnew[k] = nvold[k];
                }
                if (allZero && nvnew[k] != 0) {
                    allZero = 0;
                }
            }
            double newE = nNRG(freq, (double *)anh, nvnew, nModes) - ZPE;
            if (newE < Emax && ckderiv(freq, (double *)anh, nvnew, nModes) == 0) {
                unsigned int nnew = newE / Estep;    // New energy bin
                double r2 = (double)rand() / (double)RAND_MAX;
                if (exp(g[nold] - g[nnew]) > r2) { // Metropolis criteria
                    nold = nnew;
                    for (k = 0; k < nModes; k++) {
                        nvold[k] = nvnew[k];
                    }
                    Eold = newE;
                }
            }
            g[nold] += lnf;     //  eqtn 4.20 Sam's Thesis
        }

        lnf *= 0.5;
    }

    // Last iteration to compute energies (Ev) & couplings (TTF)
    printf("Starting final iteration\n");
    unsigned int H[nSteps];
    for (l = 0; l < nSteps; l++) {
        H[l] = 0;
    }

    for (j = 0; j < Ntrial; j++) {      // Ntrial is like 100000000
        unsigned int nvnew[nModes];
        for (k = 0; k < nModes; k++) {
            double r1 = (double)rand() / (double)RAND_MAX;
            if (r1 <= p) {
                int new = nvold[k] - 1;
                if (new < 0) {
                    new = 0;
                }
                nvnew[k] = new;
            }
            else if (r1 <= 2 * p) {
                nvnew[k] = nvold[k] + 1;
            }
            else {
                nvnew[k] = nvold[k];
            }
        }
        double newE = nNRG(freq, (double *)anh, nvnew, nModes) - ZPE;
        if (newE < Emax && ckderiv(freq, (double *)anh, nvnew, nModes) == 0) {
            unsigned int nnew = newE / Estep;
            double r2 = (double)rand() / (double)RAND_MAX;
            if (exp(g[nold] - g[nnew]) > r2) {
                nold = nnew;
                for (k = 0; k < nModes; k++) {
                    nvold[k] = nvnew[k];
                }
                Eold = newE;
            }
        }
        H[nold]++;
        g[nold] += lnf;
        Ev[nold] += Eold;
        TTF[nold] += nCoup(coupling, nvold, nModes);  // Calculates coupling terms from x_{nF} and quantum numbers
    }






    // Write output file for use in sctst.c
    FILE *outFile = fopen(argv[2], "w");
    fprintf(outFile, "Estep = %lf; Niter = %u; Ntrial = %u\n", Estep, Niter, Ntrial);
    fprintf(outFile, "Energy, # of states, average Ev, average coupling, average integral\n");
    for (i = 0; i < nSteps; i++) {
        if (H[i] > 0) {
            Ev[i] = Ev[i] / H[i];
            TTF[i] = TTF[i] / H[i];
        }
        else {
            Ev[i] = -ZPE;
            TTF[i] = -ZPE;
        }


        // Do the integrals
        double omega_star = TIF[i] + im_freq;
        double upper_bound = (omega_star - sqrt( omega_star*omega_star + 4.0 * xff * (Ev[i] + ZPE)) ) * 0.5 * xff_rec * (-1) * 3.14159265;
        double beta = 2.0;

        double integral = int_simpson(-20.0, upper_bound, n_points, integrand,beta, Ev[i] + ZPE, omega_star, xff);





        fprintf(outFile, "%.7lf,%lf,%.7lf,%.8lf,%.8lf\n", ZPE + i * Estep, exp(g[i] - g[0]), Ev[i] + ZPE, TTF[i], integral);
    }

}

// Returns 1 if derivative of energy with respect to any quantum number is <0
int ckderiv(double *freq, double *anh, unsigned int *nv, unsigned int n) {
    int ntest = 0;
    unsigned int i,k;
    for (k = 0; k < n && ntest == 0; k++) {         // Itterate over all quantum numbers
        double sum = 0;
        for (i = 0; i < n; i++) {
            double inTerm = nv[i] + 0.5;
            sum += anh[k * n + i] * inTerm;
        }
        double knTerm = nv[k] + 0.5;
        double deriv = freq[k] + knTerm * anh[k * n + k] + sum; // eqtn 2.97 Sam's Thesis
        if (deriv < 0) {
//            printf("Issue with mode %i\n", k);
            ntest = 1;
        }
    }
    return ntest;
}


double integrand(double theta, double beta, double V, double coupling, double xff){

    double sech2 = pow(cosh(theta), 2);
    double pi_minus_1 = 0.31830988618;

    double n_term = (V - theta * coupling * pi_minus_1);

    return 0.5 * sech2 * exp(beta * (xff * theta * theta * pi_minus_1 * pi_minus_1 - n_term));

}

// Calculated a 1D integral usign Simpson's rule
double int_simpson(double from, double to, double n, double (*func)(), double beta, double V, double coupling, double xff)
{
    double h = (to - from) / n;
    double sum1 = 0.0;
    double sum2 = 0.0;
    int i;

    double x;

    for(i = 0;i < n;i++)
        sum1 += func(from + h * i + h / 2.0);

    for(i = 1;i < n;i++)
        sum2 += func(from + h * i);

    return h / 6.0 * (func(from, beta, V, coupling, xff) + func(to, beta, V, coupling, xff) + 4.0 * sum1 + 2.0 * sum2);
}




// Calculates energy from frequencies, anharmonic constants, and quantum numbers (VPT2 expression eqtn 2.88 Sam's Thesis)
double nNRG(double *freq, double *anh, unsigned int *nv, unsigned int n) {
    double result = 0;
    unsigned int i,j;
    for (i = 0; i < n; i++) {
        result += freq[i] * (nv[i] + 0.5);
        for (j = 0; j <= i; j++) {
            result += anh[j * n + i] * (nv[i] + 0.5) * (nv[j] + 0.5);
        }
    }
    return result;
}

// Calculates coupling terms from x_{nF} and quantum numbers
double nCoup(double *coup, unsigned int *nv, unsigned int n) {
    double result = 0;
    unsigned int i;
    for (i = 0; i < n; i++) {
        result += coup[i] * (nv[i] + 0.5);

    }
    return result;
}

// Calculates maximum allowed quantum number for a mode k given an energy Eu and a set of quantum numbers nv
int nvmax(unsigned int nv[], unsigned int k, double Eu, double anh[], double freq[], unsigned int n) {
    if (Eu < 0) {
        return -1;
    }
    if (anh[k * n + k] != 0) {
        double sum = 0;
        unsigned int i;
        for (i = 0; i < n; i++) {
            if (i != k) {
                sum += (nv[i] + 0.5) * anh[i * n + k];
            }
        }
        double anhkk = anh[k * n + k];
        double zpe = anhkk * 0.25 + freq[k] * 0.5 + sum * 0.5;
        double vd = (-freq[k] - sum) / (2 * anhkk) - 0.5;

        double anhksum = 0;
        for (i=0; i < n; i++) {
            if (i != k) {
                anhksum = anh[k * n + i];
            }
        }

        double Dk = -(freq[k] + sum) * (freq[k] + sum) / 4 / anhkk - zpe;

        unsigned int vmax = vd * (1 - sqrt(1 - Eu / Dk));
        if ((freq[k] + sum) > 0 && anhkk < 0) {
            if (Eu > Dk) {
                return vd;
            }
            else {
                return vmax;
            }
        }
        else {
            if ((freq[k] + sum) > 0 && anhkk > 0) {
                return vmax;
            }
            else {
                return -1;
            }
        }
    }

    return -1;
}
