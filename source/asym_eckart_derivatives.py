import numpy as np

"""
Analytic derivatives of the function: V = V1sech^2(x/a) + V2 / (1 + exp(kappa x))

here V1 = v, V2 = b, kappa = c, sech = sech(x/a), tanh = tanh(x/a), y = exp(c*x)

all evaluated using Mathmatica.

NOT used as an active function in the SCTST code

"""


def calc_f1(v, a, b, c, x):
    y = np.exp(c * x)
    tanh = np.tanh(x / a)
    sech = 1.0 / np.cosh(x / a)

    f1 = -(b * c * y) / (1.0 + y) ** 2 - (2.0 * v * sech ** 2 * tanh) / a

    return f1


def calc_f2(v, a, b, c, x):
    y = np.exp(c * x)
    tanh = np.tanh(x / a)
    sech = 1.0 / np.cosh(x / a)

    f2 = ((2.0 * b * c ** 2 * y ** 2) / (1.0 + y) ** 3) - \
         ((b * c ** 2 * y) / (1.0 + y) ** 2) - \
         ((2.0 * v * sech ** 4) / a ** 2) + \
         (4.0 * v * sech ** 2 * tanh ** 2) / a ** 2

    return f2


def calc_f3(v, a, b, c, x):
    tanh = np.tanh(x / a)
    sech = 1.0 / np.cosh(x / a)
    sech_cx_2 = 1.0 / np.cosh(c * x / 2.0)

    f3 = ((-1.0 / 8.0) * b * c ** 3 * (np.cosh(c * x) - 2.0) * sech_cx_2 ** 4) - \
         ((4.0 * v * (np.cosh(2.0 * x / a) - 5.0) * sech ** 4 * tanh) / a ** 3)

    return f3


def calc_f4(v, a, b, c, x):
    sech = 1.0 / np.cosh(x / a)
    sech_cx_2 = 1.0 / np.cosh(c * x / 2.0)

    f4 = (2.0 * v * (33.0 - 26.0 * np.cosh(2.0 * x / a) + np.cosh(4.0 * x / a)) * sech ** 6 / a ** 4) + \
         ((1.0 / 8.0) * b * c ** 4 * (np.cosh(c * x) - 5.0) * sech_cx_2 ** 4 * np.tanh(c * x / 2.0))

    return f4


def calc_f5(v, a, b, c, x):
    sech = 1.0 / np.cosh(x / a)
    sech_cx_2 = 1.0 / np.cosh(c * x / 2.0)

    f5 = (- 1.0 / 32.0) * b * c ** 5 * (33.0 - 26.0 * np.cosh(c * x) + np.cosh(2.0 * c * x)) * sech_cx_2 ** 6 - \
         ((4.0 * v * (123.0 - 56.0 * np.cosh(2.0 * x / a) + np.cosh(4.0 * x / a)) * sech ** 6 * np.tanh(x / a)) / a ** 5)

    return f5


def calc_f6(v, a, b, c, x):
    sech = 1.0 / np.cosh(x / a)
    sech_cx_2 = 1.0 / np.cosh(c * x / 2.0)

    f6 = (2.0 * v * (-1208.0 + 1191.0 * np.cosh(2.0 * x / a) - 120.0 * np.cosh(4.0 * x / a) + np.cosh(6.0 * x / a)) * sech ** 8) / a ** 6 + \
         (1.0 / 32.0) * b * c ** 6 * (123.0 - 56.0 * np.cosh(c * x) + np.cosh(2.0 * c * x)) * sech_cx_2 ** 6 * np.tanh(c * x / 2.0)

    return f6
