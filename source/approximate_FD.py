"""
Module to calculate matrix elements of the form x_iF where F is the reaction mode, CHEAPLY, using the 'Two HESSIAN' 
method as described in : https://doi.org/10.1016/j.cplett.2019.136783 (email timothyahburd@googlemail.com for access, 
if needed). 

NOTE this has been superseded by the Gaussian function freq=(anharmonic,selectnormalmodes), and so is not needed for 
routine calculations. 
"""

import statistics
import numpy as np
from numpy import linalg as LA

import partiton_function as pf
import physical_constants as pc
import read_gaussian as rg


def calc_derrivatives_full(name, n_atoms, rich_step_size, masses, F=None, two_hessian_approxmation=False):
    """
    Calculate the full set of third and fourth order derivatives required for a FD SCTST calcualtion, based on a full set of hessian calcualtions
    :param name: name of the directory in which the files are held i.e. the name of the species
    """

    dof = 3 * n_atoms - 6
    thirds = np.zeros((dof, dof, dof))
    fourths = np.zeros((dof, dof))
    thirds_n_contributions = np.zeros((dof, dof, dof))  # keeps track of how many approximations have been used for each
    fourths_n_contributions = np.zeros((dof, dof))

    if two_hessian_approxmation:
        if F == 9999999:
            raise ValueError("Need to give me an F here if you are doing the two hessian approximation")
        hessians_we_can_use = [F]
    else:
        hessians_we_can_use = range(dof)

    for mode in hessians_we_can_use:
        root = 'RD_files/'
        hess_plus = rg.find_hessian(root + name + "/m1_" + str(mode) + "_hess.fchk", n_atoms)
        hess_zero = rg.find_hessian(root + name + "/energy_mp2_0_hess.fchk", n_atoms)
        hess_minus = rg.find_hessian(root + name + "/p1_" + str(mode) + "_hess.fchk", n_atoms)

        # -------- Perform mass weighting -------------------------------------------------------------------------- #

        hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, masses))
        hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)
        hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

        w, v = LA.eig(hess_zero_weighted)
        normal_vectors = v

        # -------- Perform diagonalisation -------------------------------------------------------------------------- #

        hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
        hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal
        hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

        # -------- Check hess_zero_weighted_normal is diagonal ------------------------------------------------------ #
        approx_zero = hess_zero_weighted_normal / hess_zero_weighted_normal[-1, -1] > 0.001  # which elements are more than 1/1000 of the final diagonal element?
        for i in range(3 * n_atoms):
            for j in range(i + 1, 3 * n_atoms):
                if approx_zero[i, j]:
                    pass
                    # plt.imshow(hess_zero_weighted_normal, cmap='hot', interpolation='nearest')
                    # plt.show()
                    raise ValueError("The hessian matrix is not diagonal!")

        imaginary_index = np.argmin(w)
        F = imaginary_index

        w = w[:-6]  # remove translational/rotational frequencies from list, assuming they are at the end
        freqs = [i ** 0.5 if i > 0 else (-i) ** 0.5 for i in w]

        freqs = [value / (pc.amu ** 0.5) for value in freqs]  # converts to purely atomic units

        for i in range(dof):
            for j in range(dof):
                thirds_n_contributions[i, j, mode] += 1
                thirds_n_contributions[mode, i, j] += 1
                thirds_n_contributions[j, mode, i] += 1
                thirds_n_contributions[i, mode, j] += 1
                thirds_n_contributions[mode, j, i] += 1
                thirds_n_contributions[j, i, mode] += 1

                third_derr = abs((hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5))
                thirds[i, j, mode] += third_derr
                thirds[mode, i, j] += third_derr
                thirds[j, mode, i] += third_derr
                thirds[i, mode, j] += third_derr
                thirds[mode, j, i] += third_derr
                thirds[j, i, mode] += third_derr

            fourths[i, mode] += (hess_plus_weighted_normal[i, i] + hess_minus_weighted_normal[i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2)
            fourths[mode, i] += (hess_plus_weighted_normal[i, i] + hess_minus_weighted_normal[i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2)
            fourths_n_contributions[i, mode] += 1
            fourths_n_contributions[mode, i] += 1

    # Many of the derivatives have not been 'visited' at all. That will mean we have to divide zero by zero in the
    # next bit. Here, ill just set the zeros to ones so we do 0/1 = 0 still
    fourths_n_contributions[fourths_n_contributions == 0] = 1
    thirds_n_contributions[thirds_n_contributions == 0] = 1

    thirds = np.divide(thirds, thirds_n_contributions)
    fourths = np.divide(fourths, fourths_n_contributions)

    return [freqs, thirds, fourths, F]


def calcualte_third_derivative_explicitly(i, j, k, name, n_atoms, rich_step_size, masses):
    """
    Find the value of f_{ijk} directly, and its error
    """

    estimate_list = []

    for mode in [i, j, k]:  # this is the mode that we are stepping along and calcualting the hessain at
        root = 'RD_files/'
        hess_plus = rg.find_hessian(root + name + "/m1_" + str(mode) + "_hess.fchk", n_atoms)
        hess_zero = rg.find_hessian(root + name + "/energy_mp2_0_hess.fchk", n_atoms)
        hess_minus = rg.find_hessian(root + name + "/p1_" + str(mode) + "_hess.fchk", n_atoms)

        # -------- Perform mass weighting -------------------------------------------------------------------------- #
        hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, masses))
        hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)
        hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

        w, v = LA.eig(hess_zero_weighted)
        normal_vectors = v

        # -------- Perform diagonalisation -------------------------------------------------------------------------- #
        hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
        hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal
        hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

        imaginary_index = np.argmin(w)
        F = imaginary_index

        w = w[:-6]  # remove translational/rotational frequencies from list, assuming they are at the end
        freqs = [i ** 0.5 if i > 0 else (-i) ** 0.5 for i in w]

        if mode == i:
            hessian_point_1 = j
            hessian_point_2 = k
        elif mode == j:
            hessian_point_1 = i
            hessian_point_2 = k
        elif mode == k:
            hessian_point_1 = j
            hessian_point_2 = i
        else:
            print(mode, i, j, k)
            raise ValueError("WEIRD")

        estimate = (hess_minus_weighted_normal[hessian_point_1, hessian_point_2] - hess_plus_weighted_normal[hessian_point_1, hessian_point_2]) / (2 * rich_step_size) / (pc.amu ** 1.5)
        identical_estimate = (hess_minus_weighted_normal[hessian_point_2, hessian_point_1] - hess_plus_weighted_normal[hessian_point_2, hessian_point_1]) / (2 * rich_step_size) / (pc.amu ** 1.5)

        estimate_list.append((estimate))

    mean = statistics.mean(estimate_list)
    std = statistics.stdev(estimate_list)
    estimate_list.append(mean)
    estimate_list.append(std)
    return estimate_list


def calcualte_fourth_derivative_explicitly(i, j, name, n_atoms, rich_step_size, masses):
    """
    Find the value of f_{iijj} directly, and its error
    """
    dof = 3 * n_atoms - 6

    estimate_list = []

    for mode in [i, j]:  # this is the mode that we are stepping along and calcualting the hessain at
        root = 'RD_files/'
        hess_plus = rg.find_hessian(root + name + "/m1_" + str(mode) + "_hess.fchk", n_atoms)
        hess_zero = rg.find_hessian(root + name + "/energy_mp2_0_hess.fchk", n_atoms)
        hess_minus = rg.find_hessian(root + name + "/p1_" + str(mode) + "_hess.fchk", n_atoms)

        # perform mass weighting
        hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, masses))
        hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)
        hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

        w, v = LA.eig(hess_zero_weighted)
        normal_vectors = v


        #### perform diagonalisation

        hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
        hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal
        hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

        imaginary_index = np.argmin(w)
        F = imaginary_index

        w = w[:-6]  # remove translational/rotational frequencies from list, assuming they are at the end
        freqs = [i ** 0.5 if i > 0 else (-i) ** 0.5 for i in w]

        if mode == i:
            hessian_point_1 = j
        elif mode == j:
            hessian_point_1 = i
        else:
            print(mode, i, j)
            raise ValueError("WEIRD")

        estimate = (hess_plus_weighted_normal[hessian_point_1, hessian_point_1] + hess_minus_weighted_normal[hessian_point_1, hessian_point_1]
                    - 2 * hess_zero_weighted_normal[hessian_point_1, hessian_point_1]) / (rich_step_size ** 2) / (pc.amu ** 2)

        estimate_list.append(estimate)

    mean = statistics.mean(estimate_list)
    std = statistics.stdev(estimate_list)
    estimate_list.append(mean)
    estimate_list.append(std)
    return estimate_list


def calc_derrivatives_two_hessians(name, n_atoms, rich_step_size, masses, F):
    """
    Calculate the full set of third and fourth order derivatives required for a FD SCTST calculation, based on a full 
    set of hessian calculations
    :param name: name of the directory in which the fiels are helf i.e. the name of the species
    """
    dof = 3 * n_atoms - 6
    thirds = np.zeros((dof, dof, dof))
    fourths = np.zeros((dof, dof))
    thirds_n_contributions = np.zeros((dof, dof, dof))  # keeps track of how many approximations have been used for each element
    fourths_n_contributions = np.zeros((dof, dof))

    for mode in [F]:
        root = 'RD_files/'
        hess_plus = rg.find_hessian(root + name + "/m1_" + str(mode) + "_hess.fchk", n_atoms)
        hess_zero = rg.find_hessian(root + name + "/energy_mp2_0_hess.fchk", n_atoms)
        hess_minus = rg.find_hessian(root + name + "/p1_" + str(mode) + "_hess.fchk", n_atoms)

        # perform mass weighting
        hess_plus_weighted = pf.find_mass_weighted_hessian(hess_plus, masses)
        hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)
        hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

        w, v = LA.eig(hess_zero_weighted)
        normal_vectors = v
        #### perform diagonalisation

        hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
        hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal
        hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

        # check hess_zero_weighted_normal is diagnoal
        approx_zero = hess_zero_weighted_normal / hess_zero_weighted_normal[-1, -1] > 0.001  # which elements are more than 1/1000 of the final diagonal element?
        for i in range(3 * n_atoms):
            for j in range(i + 1, 3 * n_atoms):
                if approx_zero[i, j]:
                    pass
                    # plt.imshow(hess_zero_weighted_normal, cmap='hot', interpolation='nearest')
                    # plt.show()
                    raise ValueError("The hessian matrix is not diagonal!")

        imaginary_index = np.argmin(w)
        F = imaginary_index

        # w = np.delete(w, imaginary_index)   # remove imaginary frequency from list

        w = w[:-6]  # remove translational/rotational frequencies from list, assuming they are at the end
        freqs = [i ** 0.5 if i > 0 else (-i) ** 0.5 for i in w]

        freqs = [value / (pc.amu ** 0.5) for value in freqs]  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )

        for i in range(dof):
            for j in range(dof):
                thirds_n_contributions[i, j, mode] += 1
                thirds_n_contributions[mode, i, j] += 1
                thirds_n_contributions[j, mode, i] += 1
                thirds_n_contributions[i, mode, j] += 1
                thirds_n_contributions[mode, j, i] += 1
                thirds_n_contributions[j, i, mode] += 1

                thirds[i, j, mode] += (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
                thirds[mode, i, j] += (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
                thirds[j, mode, i] += (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
                thirds[i, mode, j] += (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
                thirds[mode, j, i] += (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
                thirds[j, i, mode] += (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)

            fourths[i, mode] += (hess_plus_weighted_normal[i, i] + hess_minus_weighted_normal[i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2)
            fourths[mode, i] += (hess_plus_weighted_normal[i, i] + hess_minus_weighted_normal[i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2)
            fourths_n_contributions[i, mode] += 1
            fourths_n_contributions[mode, i] += 1

    # many of the derivatives have not been 'visited' at all. That will mean we have to divide zero by sero in the next
    #  bit. Here, ill just set the zers to ones so we do 0/1 = 0 still
    fourths_n_contributions[fourths_n_contributions == 0] = 1
    thirds_n_contributions[thirds_n_contributions == 0] = 1

    thirds = np.divide(thirds, thirds_n_contributions)
    fourths = np.divide(fourths, fourths_n_contributions)
    return [freqs, thirds, fourths, F]


def calc_derrivatives(name, n_atoms, rich_step_size, masses, mode=1):
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/m" + str(mode) + "_hess.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)

    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/p" + str(mode) + "_hess.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, masses))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

    w, v = LA.eig(hess_zero_weighted)
    normal_vectors = v
    #### perform diagonalisation

    hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
    hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal
    hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

    # check hess_zero_weighted_normal is diagnoal
    approx_zero = hess_zero_weighted_normal / hess_zero_weighted_normal[-1, -1] > 0.001  # which elements are more than 1/1000 of the final diagonal element?
    for i in range(3 * n_atoms):
        for j in range(i + 1, 3 * n_atoms):
            if approx_zero[i, j]:
                pass
                # plt.imshow(hess_zero_weighted_normal, cmap='hot', interpolation='nearest')
                # plt.show()
                # raise ValueError ("The hessian matrix is not diagonal!")

    imaginary_index = np.argmin(w)
    F = imaginary_index

    # w = np.delete(w, imaginary_index)   # remove imaginary frequency from list
    w = w[:-6]  # remove translational/rotational frequencies from list, assuming they are at the end
    freqs = [i ** 0.5 if i > 0 else (-i) ** 0.5 for i in w]

    freqs = [value / (pc.amu ** 0.5) for value in freqs]  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )

    dof = 3 * n_atoms
    thirds = np.zeros((dof, dof))
    fourths = np.zeros(dof)
    for i in range(dof):
        for j in range(dof):
            thirds[i, j] = (hess_minus_weighted_normal[i, j] - hess_plus_weighted_normal[i, j]) / (2 * rich_step_size) / (pc.amu ** 1.5)
        fourths[i] = (hess_plus_weighted_normal[i, i] + hess_minus_weighted_normal[i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2) / (pc.amu ** 2)

    # faster using numpy:
    # thirds = hess_plus_weighted_normal - hess_minus_weighted_normal / (2 * rich_step_size)

    return [freqs, thirds, fourths, F]


def calc_xff(freqs, thirds, fourths, F):
    """
    Calculate the full dimensional value of xff 
    NOTE incoming freq list MUST have the Fth frequency as pure imaginary!
    """

    freqs = list(freqs)
    freqs2 = [i ** 2 for i in freqs]

    first = fourths[F]
    second = sum([thirds[F, j] ** 2 * (8 * freqs2[F] - 3 * freqs2[j]) / ((freqs2[j]) * (4 * freqs2[F] - freqs2[j])) for j in range(0, len(freqs))])

    xff = (1 / (16 * freqs2[F])) * (first - second)
    third_cont = (1 / (16 * freqs2[F])) * (-1 * second)
    fourth_cont = (1 / (16 * freqs2[F])) * first
    return xff


def calc_x(freqs, thirds, fourths, F):
    """
    Calculate the full approximate x matrix.
    The entire set of x_{iF} should come out as pure imaginary!
    Here we calculated them as imaginary, then take the imagninary part to give a real number
    """

    freqs = list(freqs)
    freqs[F] = complex(0, freqs[F])

    freqs2 = [i ** 2 for i in freqs]

    x = np.zeros((len(freqs), len(freqs)), dtype=complex)

    for k in range(len(freqs)):
        first = fourths[k]
        second = sum([2 * thirds[j, k] ** 2 * (freqs2[F] + freqs2[k] - freqs2[j]) / (((freqs[F] + freqs[k]) ** 2 - freqs[j] ** 2) * ((freqs[F] - freqs[k]) ** 2 - freqs[j] ** 2)) for j in range(len(freqs))])
        third = thirds[F, F] * thirds[k, k] / freqs2[F]
        prefactor = -1 / (4 * freqs[F] * freqs[k])

        # Check pure imaginary:

        if (prefactor * (first - second - third)).real != 0 and k != F:
            raise ValueError("These matrix elements should be pure imaginary! look at ", k, (prefactor * (first - second)))
        x[F, k] = (prefactor * (first - second - third)).imag
        x[k, F] = (prefactor * (first - second - third)).imag

    x[F, F] = calc_xff(freqs, thirds, fourths, F)

    return x


def calc_x_chainrule(freqs, thirds, fourths, F):
    """ Calculates the x matrix with only x_{iF} values non-zero
    uses the chain rule relation to get the mixed third derrivatives not involving F
    NOTE this chain rule thing is mathematically incorrect!"""

    # The entire set of x_{iF} should come out as pure imaginary!
    # Here we calculated them as imaginary, then take the imagninary part to give a real numbers

    freqs = list(freqs)
    freqs[F] = complex(0, freqs[F])

    freqs2 = [i ** 2 for i in freqs]

    x = np.zeros((len(freqs), len(freqs)), dtype=complex)

    for k in range(len(freqs)):
        first = fourths[k]
        second = sum([2 * thirds[j, k] ** 2 * (freqs2[F] + freqs2[k] - freqs2[j]) / (((freqs[F] + freqs[k]) ** 2 - freqs[j] ** 2) * ((freqs[F] - freqs[k]) ** 2 - freqs[j] ** 2)) for j in range(len(freqs))])
        third = sum([thirds[F, j] * chain_rule_thirds(thirds, F, j, k) / freqs2[j] for j in range(len(freqs))])
        prefactor = -1 / (4 * freqs[F] * freqs[k])

        # Check pure imaginary:

        if (prefactor * (first - second - third)).real != 0 and k != F:
            raise ValueError("These matrix elements should be pure imaginary! look at ", k, (prefactor * (first - second)))
        x[F, k] = (prefactor * (first - second - third)).imag
        x[k, F] = (prefactor * (first - second - third)).imag

    x[F, F] = calc_xff(freqs, thirds, fourths, F)

    return x


def chain_rule_thirds(F_thirds, F, j, k):
    """ calculates f_{jkk} using third derivatives along the F normal mode"""

    est_1 = F_thirds[F, k] * F_thirds[j, k] / F_thirds[F, F]
    est_2 = F_thirds[k, k] * F_thirds[j, F] / F_thirds[F, F]
    est_3 = F_thirds[k, k] * F_thirds[j, j] / F_thirds[j, F]
    est_4 = F_thirds[j, k] * F_thirds[j, k] / F_thirds[j, F]

    return 0.5 * (est_1 + est_2)


def calc_x_matrix(name, n_atoms, rich_step_size, masses):
    """OLD AND UNUSED"""

    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/m1_hess.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)

    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/p1_hess.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, masses))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, masses)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, masses)

    w, v = LA.eig(hess_zero_weighted)
    normal_vectors = v
    #### perform diagonalisation

    hess_plus_weighted_normal = np.transpose(normal_vectors) @ (hess_plus_weighted) @ (normal_vectors)
    hess_zero_weighted_normal = np.transpose(normal_vectors) @ (hess_zero_weighted) @ (normal_vectors)  # Should be diagonal
    hess_minus_weighted_normal = np.transpose(normal_vectors) @ (hess_minus_weighted) @ (normal_vectors)

    # check hess_zero_weighted_normal is diagnoal
    approx_zero = hess_zero_weighted_normal / hess_zero_weighted_normal[-1, -1] > 0.001  # which elements are more than 1/1000 of the final diagonal element?
    for i in range(3 * n_atoms):
        for j in range(i + 1, 3 * n_atoms):
            if approx_zero[i, j]:
                raise ValueError("The hessian matrix is not diagonal!")

    imaginary_index = np.argmin(w)
    F = imaginary_index
    imaginary_freq = (-1 * w[imaginary_index]) ** 0.5
    imaginary_freq = imaginary_freq / (1822.888486192 ** 0.5)  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )

    w = np.delete(w, imaginary_index)  # remove imaginary frequency from list
    w = w[:-6]  # remove translational/rotational frequencies from list
    freqs = w ** 0.5

    freqs = [value / (1822.888486192 ** 0.5) for value in freqs]  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )

    dof = 3 * n_atoms
    thirds = np.zeros((dof, dof))
    fourths = np.zeros(dof)
    for i in range(dof):
        # for j in range(dof):
        #     thirds[i, j] = (hess_plus_weighted_normal[i, j] - hess_minus_weighted_normal[i, j]) / (2 * rich_step_size)
        fourths[i] = (hess_plus_weighted_normal[i, i] + hess_minus_weighted_normal[i, i] - 2 * hess_zero_weighted_normal[i, i]) / (rich_step_size ** 2)

    # faster using numpy:
    thirds = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (2 * rich_step_size)

    x = []
    for k in range(len(freqs)):
        first = fourths[k]
        second = sum([thirds[j, k] ** 2 * (imaginary_freq ** 2 + freqs[k] ** 2 - freqs[j] ** 2) / (((imaginary_freq + freqs[k]) ** 2 - freqs[j] ** 2) * ((imaginary_freq - freqs[k]) ** 2 - freqs[j] ** 2)) for j in
                      range(len(freqs))])
        prefactor = 1 / (4 * imaginary_freq * freqs[k])
        x.append(prefactor * (first + second))

    xff = (1 / (16 * imaginary_freq ** 2)) * (fourths[F] - sum([thirds[F, j] ** 2 * (8 * imaginary_freq ** 2 - 3 * freqs[j] ** 2) / freqs[j] ** 2 / (4 * imaginary_freq ** 2 - freqs[j] ** 2) for j in range(len(freqs))]))

    return [x, xff]


def calc_full_x_from_derivatives(freqs, thirds, fourths, F):
    dimensions = len(freqs)
    freqs2 = [j * j for j in freqs]
    freqs2[F] *= -1
    xmat = np.zeros((dimensions, dimensions))

    for i in range(dimensions):
        for k in range(dimensions):
            if i == k:
                xmat[i, k] = (1 / 16) * (1 / freqs2[i]) * (fourths[i, i] - sum([(thirds[i, i, j] ** 2 / freqs2[j]) * (8 * freqs2[i] - 3 * freqs2[j]) / (4 * freqs2[i] - freqs2[j]) for j in range(dimensions)]))

            else:
                xmat[i, k] = (1 / 4) * (1 / (freqs[i] * freqs[k])) * (fourths[i, k] - sum([thirds[i, i, j] * thirds[j, k, k] / freqs2[j] for j in range(dimensions)])
                                                                      + sum(
                            [2 * thirds[i, j, k] ** 2 * (freqs2[i] + freqs2[k] - freqs2[j]) / ((freqs[i] + freqs[k]) ** 2 - freqs2[j]) / ((freqs[i] - freqs[k]) ** 2 - freqs2[j]) for j in range(dimensions)])
                                                                      )

    return xmat


def calc_approx_x_from_derivatives(freqs, thirds, fourths, F):
    dimensions = len(freqs)
    freqs2 = [j * j for j in freqs]
    freqs2[F] *= -1

    xmat = np.zeros((dimensions, dimensions))

    for i in range(dimensions):
        for k in range(dimensions):
            if i == k:
                xmat[i, k] = (1 / 16) * (1 / freqs2[i]) * (fourths[i, i] - sum([(thirds[i, i, j] ** 2 / freqs2[j]) * (8 * freqs2[i] - 3 * freqs2[j]) / (4 * freqs2[i] - freqs2[j]) for j in range(dimensions)]))

            else:
                xmat[i, k] = (1 / 4) * (1 / (freqs[i] * freqs[k])) * (fourths[i, k]
                                                                      + sum(
                            [2 * thirds[i, j, k] ** 2 * (freqs2[i] + freqs2[k] - freqs2[j]) / ((freqs[i] + freqs[k]) ** 2 - freqs2[j]) / ((freqs[i] - freqs[k]) ** 2 - freqs2[j]) for j in range(dimensions)])
                                                                      )

    return xmat


def main():
    import matplotlib.pyplot as plt
    import math
    thirds_mean_list = []
    thirds_std_lsit = []

    fourths_mean_list = []
    fourths_std_list = []
    n_dim = 18
    for i in range(n_dim):
        for j in range(n_dim):
            thirds_data_list = calcualte_third_derivative_explicitly(i, j, j, "ts", 8, 0.05, [12, 1, 16, 16, 12, 1, 1, 1])
            print(thirds_data_list)
            thirds_mean_list.append(thirds_data_list[3])
            thirds_std_lsit.append(thirds_data_list[4] / math.sqrt(3))
            fourths_data_list = calcualte_fourth_derivative_explicitly(i, j, "ts", 8, 0.05, [12, 1, 16, 16, 12, 1, 1, 1])
            fourths_mean_list.append(fourths_data_list[2])
            fourths_std_list.append(fourths_data_list[3] / math.sqrt(2))

    plt.errorbar(range(len(thirds_mean_list)), thirds_mean_list, yerr=thirds_std_lsit, capsize=1, ecolor='r')
    plt.show()

    plt.errorbar(range(len(fourths_mean_list)), fourths_mean_list, yerr=fourths_std_list, capsize=1, ecolor='r')
    plt.show()

    import extract_anharm_matrix as ex
    [freqs, thirds, fourths, F] = calc_derrivatives_full("ts", 8, 0.05, [12, 1, 16, 16, 12, 1, 1, 1])

    [freqs_app, thirds_app, fourths_app, F] = calc_derrivatives_full("ts", 8, 0.05, [12, 1, 16, 16, 12, 1, 1, 1], F=4, two_hessian_approxmation=True)

    thirds_app_exact = thirds
    fourths_app_exact = fourths
    modes_not_f = [x for x in range(len(freqs)) if x != F]
    for i in modes_not_f:
        for j in modes_not_f:
            for k in modes_not_f:
                thirds_app_exact[i, j, k] = 0
            fourths_app_exact[i, j] = 0

    # [freqs_plus, thirds_plus, fourths_plus, F_plus] = calc_derrivatives("ts", 8, 0.05, [12,1,16,16,12,1,1,1])
    # xff_plus = calc_xff(freqs_plus, thirds_plus, fourths_plus, F_plus)
    # print(xff_plus*pc.hartree_to_cm)

    xmat = calc_full_x_from_derivatives(freqs, thirds, fourths, F)
    xmat_approx = calc_full_x_from_derivatives(freqs_app, thirds_app, fourths_app, F)
    xmat_approx_exact = calc_full_x_from_derivatives(freqs_app, thirds_app_exact, fourths_app_exact, F)
    xmat_gaussian = ex.extract_matrix("test_files/ts_vpt2.log", len(freqs))

    gaussian_xifs = list(xmat_gaussian[0, :])
    gaussian_xifs.insert(F, gaussian_xifs.pop(0))

    plt.plot(xmat[F, :] * pc.hartree_to_cm, label="Many Hessians")
    plt.plot(xmat_approx[F, :] * pc.hartree_to_cm, label="Two Hessians")
    # plt.plot(xmat_approx_exact[F,:]*pc.hartree_to_cm, label="Many Hessians but approximate formula")
    plt.plot(np.asarray(gaussian_xifs) * pc.hartree_to_cm, label="Gaussian (Many Hessians)")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
