"""
Functions do do small curvature inspuired SCTST approximation
"""
import numpy as np
from numpy import linalg as LA

import partiton_function as pf
import physical_constants as pc
import read_gaussian as rg


def find_F_xxxx(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are one ways to estimate this. I will do both now.
    """

    # ESTIMATE ONE - Use the X displaced hessians, and the XX'th element
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/X_plus.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/X_minus.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find the imaginary normal vector
    w, v = LA.eig(hess_zero_weighted)
    F = np.argmin(w)
    imaginary_vector = v[:, F]

    # Find the FFth element
    hess_plus_weighted_normal = np.transpose(curvature_vector) @ (hess_plus_weighted) @ (curvature_vector)
    hess_zero_weighted_normal = np.transpose(curvature_vector) @ (hess_zero_weighted) @ (curvature_vector)  # Should be diagonal
    hess_minus_weighted_normal = np.transpose(curvature_vector) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_1 = (hess_plus_weighted_normal + hess_minus_weighted_normal - 2 * hess_zero_weighted_normal) / (rich_step_size ** 2) / (pc.amu ** 2)

    return estimate_1


def find_F_ffxx(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are two ways to estimate this. I will do both now.
    """

    # ESTIMATE ONE - Use the X displaced hessians, and the FF'th element
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/X_plus.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/X_minus.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find the imaginary normal vector
    w, v = LA.eig(hess_zero_weighted)
    F = np.argmin(w)
    imaginary_vector = v[:, F]

    # Find the FFth element
    hess_plus_weighted_normal = np.transpose(imaginary_vector) @ (hess_plus_weighted) @ (imaginary_vector)
    hess_zero_weighted_normal = np.transpose(imaginary_vector) @ (hess_zero_weighted) @ (imaginary_vector)  # Should be diagonal
    hess_minus_weighted_normal = np.transpose(imaginary_vector) @ (hess_minus_weighted) @ (imaginary_vector)

    estimate_1 = (hess_plus_weighted_normal + hess_minus_weighted_normal - 2 * hess_zero_weighted_normal) / (rich_step_size ** 2) / (pc.amu ** 2)

    # ESTIMATE Two - Use the F displaced hessians, and the XX'th element
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/p1_hess.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/m1_hess.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = pf.find_mass_weighted_hessian(hess_plus, mass)
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find the XXth element
    hess_plus_weighted_normal = np.transpose(curvature_vector) @ (hess_plus_weighted) @ (curvature_vector)
    hess_zero_weighted_normal = np.transpose(curvature_vector) @ (hess_zero_weighted) @ (curvature_vector)
    hess_minus_weighted_normal = np.transpose(curvature_vector) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_2 = (hess_plus_weighted_normal + hess_minus_weighted_normal - 2 * hess_zero_weighted_normal) / (rich_step_size ** 2) / (pc.amu ** 2)

    if abs(np.log(estimate_2 / estimate_1)) > 1 or ((estimate_1 > 0) != (estimate_2 > 0)):
        print("WARNING, ESTIMATES FOR f_{FFXX} are quite different")

    return 0.5 * (estimate_2 + estimate_1)  # uncat criegee, methane


def find_F_ffff(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are one ways to estimate this.
    """
    # ESTIMATE Two - Use the F displaced hessians, and the XX'th element
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/p1_hess.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/m1_hess.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find the imaginary normal vector
    w, v = LA.eig(hess_zero_weighted)
    F = np.argmin(w)
    imaginary_vector = v[:, F]

    # Find the XXth element
    hess_plus_weighted_normal = np.transpose(imaginary_vector) @ (hess_plus_weighted) @ (imaginary_vector)
    hess_zero_weighted_normal = np.transpose(imaginary_vector) @ (hess_zero_weighted) @ (imaginary_vector)  # Should be diagonal
    hess_minus_weighted_normal = np.transpose(imaginary_vector) @ (hess_minus_weighted) @ (imaginary_vector)

    estimate_2 = (hess_plus_weighted_normal + hess_minus_weighted_normal - 2 * hess_zero_weighted_normal) / (rich_step_size ** 2) / (pc.amu ** 2)

    return estimate_2


def find_F_xxx(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are only one ways to estimate this.
    """

    # ESTIMATE ONE - Use the X displaced hessians, and the FF'th element
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/X_plus.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/X_minus.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    #### perform diagonalisation
    hess_plus_weighted_normal = np.transpose(curvature_vector) @ (hess_plus_weighted) @ (curvature_vector)
    hess_minus_weighted_normal = np.transpose(curvature_vector) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_1 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    return estimate_1  # uncat criegee


def find_F_xxf(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are two ways to estimate this.
    """
    # ESTIMATE One  step along X, take element XF
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/X_plus.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/X_minus.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index
    imaginary_vecotr = v[:, F]

    hess_plus_weighted_normal = np.transpose(imaginary_vecotr) @ (hess_plus_weighted) @ (curvature_vector)
    hess_minus_weighted_normal = np.transpose(imaginary_vecotr) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_1 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    # ESTIMATE Two  step along F, take element XX
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/p1_hess.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/m1_hess.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Get element XX
    hess_plus_weighted_normal = np.transpose(curvature_vector) @ (hess_plus_weighted) @ (curvature_vector)
    hess_minus_weighted_normal = np.transpose(curvature_vector) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_2 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    if abs(np.log(estimate_2 / estimate_1)) > 1 or ((estimate_1 > 0) != (estimate_2 > 0)):
        print("WARNING, ESTIMATES FOR f_{XXF} are quite different")

    return 0.5 * (estimate_2 + estimate_1)  # uncat criegee
    # return -0.5*(estimate_2+estimate_1)# methane


def find_F_xxm(name, n_atoms, mass, rich_step_size, curvature_vector, m_vector):
    """
    There are one ways to estimate this.
    """
    # ESTIMATE One  step along X, take element XF
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/X_plus.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/X_minus.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index
    # m_vector = v[:, m] #- np.dot(curvature_vector, v[:, m])*curvature_vector

    hess_plus_weighted_normal = np.transpose(m_vector) @ (hess_plus_weighted) @ (curvature_vector)
    hess_minus_weighted_normal = np.transpose(m_vector) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_1 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    return estimate_1


def find_F_xff(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are two ways to estimate this.
    :return:
    """

    # ESTIMATE One  step along X, take element FF
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/X_plus.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/X_minus.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find imaginary Vector
    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index
    imaginary_vecotr = v[:, F]

    # Find element FF
    hess_plus_weighted_normal = np.transpose(imaginary_vecotr) @ hess_plus_weighted @ imaginary_vecotr
    hess_minus_weighted_normal = np.transpose(imaginary_vecotr) @ hess_minus_weighted @ imaginary_vecotr

    estimate_1 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    # ESTIMATE Two  step along F, take element XF
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/p1_hess.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/m1_hess.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find imaginary Vector
    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index
    imaginary_vecotr = v[:, F]

    # Find X F element
    hess_plus_weighted_normal = np.transpose(curvature_vector) @ (hess_plus_weighted) @ (imaginary_vecotr)
    hess_minus_weighted_normal = np.transpose(curvature_vector) @ (hess_minus_weighted) @ (imaginary_vecotr)

    estimate_2 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    if abs(np.log(estimate_2 / estimate_1)) > 1 or ((estimate_1 > 0) != (estimate_2 > 0)):
        print("WARNING, ESTIMATES FOR f_{XFF} are quite different")

    # print("xff estiamtes", estimate_1, estimate_2)
    # return 0.5*(estimate_1+estimate_2)  #
    # return -0.5*(estimate_1-estimate_2)  # criegee  , methane can have plus/minus est_2
    return 0.5 * (estimate_1 + estimate_2)  # Nothing


def find_F_xfm(name, n_atoms, mass, rich_step_size, curvature_vector, m_vector):
    """
    There are two ways to estimate this.
    :return:
    """

    # ESTIMATE One  step along X, take element FM
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/X_plus.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/X_minus.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find imaginary Vector
    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index

    imaginary_vecotr = v[:, F]
    # m_vector = v[:, m]

    # Find element FF
    hess_plus_weighted_normal = np.transpose(m_vector) @ hess_plus_weighted @ imaginary_vecotr
    hess_minus_weighted_normal = np.transpose(m_vector) @ hess_minus_weighted @ imaginary_vecotr

    estimate_1 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    # ESTIMATE Two  step along F, take element XM
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/p1_hess.fchk", n_atoms)
    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/m1_hess.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find X F element
    hess_plus_weighted_normal = np.transpose(m_vector) @ (hess_plus_weighted) @ (curvature_vector)
    hess_minus_weighted_normal = np.transpose(m_vector) @ (hess_minus_weighted) @ (curvature_vector)

    estimate_2 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    if abs(np.log(abs(estimate_2 / estimate_1))) > 1 or ((estimate_1 > 0) != (estimate_2 > 0)):
        print("WARNING, ESTIMATES FOR f_{XFM} are quite different", estimate_1, estimate_2)

    return 0.5 * (estimate_1 + estimate_2)  # Nothing


def find_F_mff(name, n_atoms, mass, rich_step_size, curvature_vector, m_vector):
    """
    There are one ways to estimate this.
    :return:
    """

    # ESTIMATE Two  step along F, take element MF
    hess_plus = rg.find_hessian("checkpoint_files/" + name + "/p1_hess.fchk", n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian("checkpoint_files/" + name + "/m1_hess.fchk", n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find imaginary Vector
    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index
    imaginary_vecotr = v[:, F]
    # m_vector = v[:, m] #- np.dot(curvature_vector, v[:, m])*curvature_vector

    # Find X F element
    hess_plus_weighted_normal = np.transpose(m_vector) @ (hess_plus_weighted) @ (imaginary_vecotr)
    hess_minus_weighted_normal = np.transpose(m_vector) @ (hess_minus_weighted) @ (imaginary_vecotr)

    estimate_2 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)

    return estimate_2


def find_F_fff(name, n_atoms, mass, rich_step_size, curvature_vector):
    """
    There are one ways to estimate this.
    :return:
    """

    # ESTIMATE Two  step along F, take element FF
    hess_plus = rg.find_hessian(str("checkpoint_files/" + name + "/p1_hess.fchk"), n_atoms)
    hess_zero = rg.find_hessian("checkpoint_files/" + name + "_freq.fchk", n_atoms)
    hess_minus = rg.find_hessian(str("checkpoint_files/" + name + "/m1_hess.fchk"), n_atoms)

    # perform mass weighting
    hess_plus_weighted = (pf.find_mass_weighted_hessian(hess_plus, mass))
    hess_zero_weighted = pf.find_mass_weighted_hessian(hess_zero, mass)
    hess_minus_weighted = pf.find_mass_weighted_hessian(hess_minus, mass)

    # Find imaginary Vector
    w, v = LA.eig(hess_zero_weighted)
    imaginary_index = np.argmin(w)
    F = imaginary_index
    imaginary_vecotr = v[:, F]

    hess_plus_weighted_normal = np.transpose(imaginary_vecotr) @ (hess_plus_weighted) @ (imaginary_vecotr)
    hess_minus_weighted_normal = np.transpose(imaginary_vecotr) @ (hess_minus_weighted) @ (imaginary_vecotr)

    estimate_2 = (hess_plus_weighted_normal - hess_minus_weighted_normal) / (rich_step_size * 2) / (pc.amu ** 1.5)
    # print("fff is", estimate_2)
    return estimate_2  # uncat criegee, methane, 'obvious'
