"""
Timothy Burd 2017
Generate Gaussian Input Files for Richard Exptrapolation used in 1D SCTST.
Files produced are called m1.com, p1.com etc, and put in a file checkpoint_files/ts_name
These should be run, and the resulting fchk files put in the same folder
"""

import os

import numpy as np
from numpy import linalg as LA

import sctst


def find_normal_vectors(hessian, masses, linear=False):
    """
    Caculate the harmonic frequencies and eigenvectors  of a molecule given its hessian matrix and masses
    :param hessian: matrix of hessian elements from gaussian formatted checkpoint files
    :param masses: list of atom masses (atomic units)
    :return: list of frequenies
    """

    sqrt_masses = [mass ** (-0.5) for mass in masses]
    sqrt_mass_list = []
    for i in range(len(sqrt_masses)):
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])

    sqrt_masses = np.asarray(sqrt_mass_list)
    reduced_mass_matrix = np.outer(sqrt_mass_list, sqrt_mass_list)

    reduced_hessian = np.zeros(hessian.shape)

    for i in range(3 * len(masses)):
        for j in range(3 * len(masses)):
            reduced_hessian[i][j] = reduced_mass_matrix[i][j] * hessian[i][j]

    w, v = LA.eig(reduced_hessian)

    return [w, v]


def get_transition_frequencies(hessian, masses, transition_list=[], linear=False):
    """
    Return the frequencies of transition modes (specified in the inputs) from the hessian and masses

    :return: List of frequencies
    """

    [w, _] = find_normal_vectors(hessian, masses, linear=False)
    freqs = []
    for t in transition_list:
        freqs.append(w[t] ** 0.5)
    freqs = [value / (1822.888486192 ** 0.5) for value in freqs]  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )

    return freqs


def generate_curvature_hessians(ts_name, n_atoms, dq, dir_path, curvature_vector, unpaired_electron=True, method="mp2"):
    """
    Generate the two displaced hessians along the special mode (the curvature vector) inspired by truhlars SCT curvature vector.
    :param ts_name:
    :param n_atoms:
    :param dq:
    :return:
    """
    ts = sctst.Chemical(ts_name, n_atoms, [1], 1, high_level_energy=False)

    step = dq * curvature_vector

    step = np.reshape(step, (n_atoms, 3))  # Reshape to be compatible with coords matrix

    new_coords_plus = ts.coordinates + step
    new_coords_minus = ts.coordinates - step

    prepare_gaussian(ts_name, new_coords_plus, "X_plus", ts.mass, dir_path=dir_path, stepsize=str(dq), hessian=True, rd_file=False, unpaired_electron=unpaired_electron, method=method, active_mode=99999)
    prepare_gaussian(ts_name, new_coords_minus, "X_minus", ts.mass, dir_path=dir_path, stepsize=str(dq), hessian=True, rd_file=False, unpaired_electron=unpaired_electron, method=method, active_mode=99999)


def generate_energy_gaussian_files(ts_name, n_atoms, unpaired_electron, method, linear=False):
    """
    Genreate the _energy.com file from the geometry of the _freq.fchk file
    :return:
    """

    ts = sctst.Chemical(ts_name, n_atoms, [1], 1, high_level_energy=False, linear=linear)
    prepare_gaussian(ts_name, ts.coordinates, "energy", ts.mass, "home", active_mode=9999, stepsize="", hessian=False, rd_file=False, unpaired_electron=unpaired_electron, method=method)


def generate_anharm_gaussian_files(ts_name, n_atoms, dq, dir_path, active_mode=9999, hessian=False, rd_file=False, linear=False, unpaired_electron=True, method="mp2", n_steps=5, mass_list=None):
    """
    Generate .com files at geometries displaces from the stationary point by some distance dq in reduced mass units
    Also generate an "energy" .com file
    :param dq:  in bohr amu**0.5
    :param active_mode: mode to be shifted along. If default (9999) this chooses the imaginary mode. NOTE lower indicies correspond to higher frequencies
    :param hessian: If you want to do a freq calcualtion rather than a single point energy calcualtion, set as True
    :param rd_file: If using this funciton to do displacements along non-imaginary modes, can rediredt output to an RD_files directory
    :param linear: Is it a linear moldeule
    :param unpaired_electron: If true, then the spin state is set as doublet
    :return: None
    """
    # if linear:
    #     print("NOTE generate_anharm_gaussian_files is not currently set up for linear molecules - sorry")
    #     return 0
    ts = sctst.Chemical(ts_name, n_atoms, [1], 1, high_level_energy=False, linear=linear, mass_list=mass_list)

    [w, v] = find_normal_vectors(ts.hessian, ts.mass)

    # EITHER use np.argmin(w) to select an aimaginary mode OR sleect the mode manually (lower indicies correspond to higher frequencies)

    if active_mode > 9998:
        im_index = np.argmin(w)
    else:
        im_index = active_mode

    im_eigenvector = v[:, im_index]  # normlaised eigenvector IN REDUCED MASS CORRDINATES!
    im_eigenvector = np.reshape(im_eigenvector, (n_atoms, 3))  # Reshape to be compatible with coords matrix

    cartesian_eigenvector = np.zeros(im_eigenvector.shape)
    # print(cartesian_eigenvector, im_eigenvector, ts.mass)
    for i in range(len(im_eigenvector)):
        cartesian_eigenvector[i][0] = im_eigenvector[i][0] / ts.mass[i] ** 0.5  # Need to transform back to non-mass weighted coordinates
        cartesian_eigenvector[i][1] = im_eigenvector[i][1] / ts.mass[i] ** 0.5
        cartesian_eigenvector[i][2] = im_eigenvector[i][2] / ts.mass[i] ** 0.5

    for i in range(-n_steps, 0):
        step = 2 ** (-i - 1) * dq * cartesian_eigenvector
        new_coords = ts.coordinates - step
        prepare_gaussian(ts_name, new_coords, "m" + str(-i), ts.mass, dir_path, active_mode, stepsize=str(dq), hessian=hessian, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)
        prepare_gaussian(ts_name, new_coords, "m" + str(-i) + "_grad", ts.mass, dir_path, active_mode, stepsize=str(dq), grad=True, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)
        if i == -1:
            prepare_gaussian(ts_name, new_coords, "m" + str(-i) + "_hess", ts.mass, dir_path, active_mode, stepsize=str(dq), hessian=True, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)

    for i in range(1, n_steps):

        step = 2 ** (i - 1) * dq * cartesian_eigenvector
        new_coords = ts.coordinates + step
        prepare_gaussian(ts_name, new_coords, "p" + str(i), ts.mass, dir_path, active_mode, stepsize=str(dq), hessian=hessian, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)
        prepare_gaussian(ts_name, new_coords, "p" + str(i) + "_grad", ts.mass, dir_path, active_mode, stepsize=str(dq), grad=True, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)
        if i == 1:
            prepare_gaussian(ts_name, new_coords, "p" + str(i) + "_hess", ts.mass, dir_path, active_mode, stepsize=str(dq), hessian=True, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)
    prepare_gaussian(ts_name, ts.coordinates, "energy_mp2", ts.mass, dir_path, active_mode, stepsize=str(dq), hessian=hessian, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method)
    prepare_gaussian(ts_name, ts.coordinates, "irc", ts.mass, dir_path, active_mode, stepsize=str(dq), hessian=hessian, rd_file=rd_file, unpaired_electron=unpaired_electron, method=method, irc=True)


def prepare_gaussian(name, coords, index, mass_list, dir_path, active_mode, stepsize="", hessian=False, rd_file=False, unpaired_electron=True, method="mp2", grad=False, irc=False):
    """
    Write a gaussian input file and save its
    :param name: species name for file name
    :param coords: species coordinates
    :param index: e.g. m1? m2? m3? m4?
    :param mass_list: List of masses of atoms
    :param dir_path: path to checkpoint files dir
    :param active_mode: which mode is this along?
    :param stepsize:
    :param hessian: do you want a freq calc rather than just single point
    :param rd_file: can redirect output to RD_files directory if being used for a RD calcualtion
    :param unpaired_electron:   if True, spin state is 0 2, else 0 1
    :return: 0
    """

    bohr_to_angstroms = 0.529177208

    if method == "mp2":
        method = "ump2/Aug-CC-pVTZ"
    elif method == 'dft':
        method = "ub3lyp/cc-pVTZ"
    elif dir_path == 'home':  # assume this is an energy calcualtion in this case
        method = "uccsd(t,e4t)/" + method
    else:
        method = "ump2/" + method

    if not unpaired_electron:
        method = method[1:]  # dont need the u

    parallel = True  # set true if you want to execute the gaussian calculation on more than one processor (default is for 16)
    if unpaired_electron:
        multiplicity = "2"
    else:
        multiplicity = "1"
    # Convert mass list to atom list using dict
    atom_dict = {12.0: "C",
                 1.00782504: "H",
                 15.994914600000001: "O",
                 14.003074: "N",
                 30.9737634: "P",
                 18.9984032: "F",
                 31.9720718: "S",
                 34.9688527: "Cl",
                 2.01410179: "H(Iso=2)",
                 0.11398: "H(Iso=0.11398)",
                 2.02: "H(Iso=2.02)",
                 2.01: "H(Iso=2.01)",
                 2.01565008: "H(Iso=2.02)"
                 }

    if len(dir_path) > 0:
        dir_path += "/"
    if rd_file:
        file_name = "RD_files/" + name + "/" + str(index) + '_' + str(active_mode) + "_hess.com"

    else:
        file_name = "checkpoint_files/" + name + "/" + str(index) + ".com"

    if dir_path == 'home/':  # e.g. if creating a _energy.com file
        file_name = "checkpoint_files/" + name + "_" + str(index) + ".com"
    else:
        # ensure the anharmonic directory exists, if not, create it
        os.makedirs(os.path.dirname(file_name), exist_ok=True)

    # file_name = "checkpoint_files/" + name + "/" + str(index) + ".com"

    f = open(file_name, 'w')
    if parallel:
        f.write('%mem=14gb\n')
        f.write('%nprocshared=16\n')
    if rd_file:
        f.write('%chk=' + str(index) + '_' + str(active_mode) + '_hess.chk\n')
    else:
        f.write('%chk=' + str(index) + '.chk\n')
    if hessian:
        f.write('#p freq ' + method + '\n')
    elif grad:
        f.write('#p force ' + method + '\n')
    elif irc:
        f.write('#p  irc=(calcfc,maxpoints=10,stepsize=1) ' + method + '\n')
    else:
        f.write('#p ' + method + '\n')
        # f.write('#p  ub3lyp/cc-pvtz EmpiricalDispersion=GD3BJ\n')
        # f.write('#p 6-311+g(d,p) #nosymm integral=ultrafine m062x\n')
        # f.write('# b3lyp/cc-pvtz empiricaldispersion = gd3bj int = ultrafine')
        # f.write("#p integral=ultrafine m062x/cc-pvtz")
    f.write('#NoSymm\n')
    f.write('\n')
    f.write(stepsize + "_" + index + '\n')
    f.write('\n')
    f.write('0 ' + multiplicity + '\n')
    # f.write('1 1\n')

    for i in range(len(mass_list)):
        f.write(atom_dict[mass_list[i]] + '\t' + str(coords[i][0] * bohr_to_angstroms) + '\t' + str(coords[i][1] * bohr_to_angstroms) + '\t' + str(coords[i][2] * bohr_to_angstroms) + '\n')

    f.write('\n')
    f.close()

    return 0


if __name__ == '__main__':

    for i in range(18):
        generate_anharm_gaussian_files("ts", 8, 0.05, dir_path="", active_mode=i, rd_file=True, hessian=True, method='dft', unpaired_electron=False)
