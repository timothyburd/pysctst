"""
Timothy Burd 2017
Calcualte all properties for partition functions of chemical species
"""

import math
import numpy as np
from numpy import linalg as LA
import physical_constants as pc



def q_translational(masses, t):
    """
    Returns translational partiton function over the temps in t
    :param masses: list of masses in atomic units
    :param t: temperature list
    :return: list of q_trans
    """
    total_mass = sum(masses)
    q_trans = []

    for temp in t:
        q = ((2 * math.pi * total_mass * pc.au_to_kg * pc.kb * temp / (pc.h * pc.h)) ** (3 / 2))
        q_trans.append(q)

    return q_trans


def intermolecular_pf(frequencies, De, t_list):
    """
    Very soft intermolecular modes can have their partition functions approximated from a morese potenital since the frequency and dissociation energy are known.
    OLD, INCOMPLETE AND UNTESTED
    :param frequencies: 
    :param De: 
    :param t_list: 
    :return: partition function list
    """

    part_list = [1 for i in t_list]
    N_levels = 500
    for freq in frequencies:
        morse_levels = [freq * (i + 0.5) - ((freq * (i + 0.5)) ** 2 / (4 * De)) + (1 / 16) * (freq ** 2 / De) for i in range(N_levels)]  # energy levels of morse oscillator
        morse_levels = [morse_levels[i] for i in range(len(morse_levels) - 1) if (morse_levels[i] <= morse_levels[i + 1])]  # only select those before turning point!
        morse_levels = [m - morse_levels[0] for m in morse_levels]  # correct for ZPE
        pf_morse = [sum([math.exp(-m * pc.h * pc.c / (pc.kb * T)) for m in morse_levels]) for T in t_list]
        for i in range(len(t_list)):
            part_list[i] *= pf_morse[i]
    return part_list


def q_vib(frequencies, t_list, option="Harmonic", im=0, sigma=0, eigen_list=[], hr_index=0, vscf=False, name="Joanna", intermolecular_correction=False,
          intermolecular_modes=0, dissocation_energy=9 * 10 ** (30)):
    """
    Returns vibrations partiton function over the temps in t_list
    :param frequencies: 
    :param t_list: 
    :param option: hindered rotor treament
    :param im: reduced moment of inertia for rotating rotor
    :param sigma: symmetry number
            intermolecular_modes: Number of intermolecular modes
    :param vscf: if true, then a vscf calculation has been done, and the vib partition function must be multiplied by the pf multiplier
    : param name: needed to locate the file for the vscf multiplier
    :return: 
    """
    frequencies.sort()  # ensures they are in asscending order
    intra_modes = len(frequencies) - intermolecular_modes  # Numeber of intramoleculaar modes
    inter_freq = frequencies[:intermolecular_modes]
    frequencies = frequencies[-intra_modes:]  # selects just the modes labels as intra (i.e. the last inta_modes elements of this list)

    if len(inter_freq) > 0:
        pf_intermolecular = intermolecular_pf(inter_freq, dissocation_energy, t_list)
    else:
        pf_intermolecular = np.ones(len(t_list))

    vscf_multiplier = np.ones(len(t_list))
    if vscf:
        f = open(name + "_pf_multiplier.out", "r")
        for i, line in enumerate(f):
            if float(t_list[i]) != float(line.split(",")[0]):
                raise ValueError("Temperatures from VSCF caluclation should match the temperatures for the SCTST calculation!")
            else:
                vscf_multiplier[i] = (line.split(",")[1])
        if len(vscf_multiplier) != len(t_list):
            raise ValueError("The number of temeperatures from the VSCF calculation is not the same as the number in the SCTST calculation!")

    truncation_multiplier = np.ones(len(t_list))
    if intermolecular_correction:
        # Read in the correction from usinga  trunkated intermoelcular partition function from the repo truncated_partition_function
        g = open(name + "_intermolecular_pf.out")
        ratio = []
        g.readline()
        for i, line in enumerate(g):
            if float(t_list[i]) != float(line.split(",")[0]):
                raise ValueError("Temperatures from truncated partition functinn caluclation should match the temperatures for the SCTST calculation!")
            else:
                truncation_multiplier[i] = (float(line.split(",")[3].rstrip()))

        if len(truncation_multiplier) != len(t_list):
            raise ValueError("The number of temeperatures from the truncated partiton function calculation is not the same as the number in the SCTST calculation!")

    hr_list = ["Harmonic", "tanh", "eigenvalue_summation"]
    if option == "Harmonic":
        part_list = []
        sum_of_freq = sum(frequencies)
        for t in t_list:
            p_fun = 1
            for freq in frequencies:
                exponent = (-1) * (pc.h * pc.speed_of_light * freq) / (pc.kb * t)
                denominator = 1 - math.e ** exponent
                factor = denominator ** (-1)
                p_fun = p_fun * factor
            part_list.append(p_fun)
        return [part_list[i] * vscf_multiplier[i] * truncation_multiplier[i] * pf_intermolecular[i] for i in range(len(part_list))]

    if option == "tanh":
        if im == 0:
            raise ValueError("You need to give the reduced moment of inertia for the rotor")
        if sigma == 0:
            raise ValueError("You need to give the symmetry rotatiton number here")

        sorted_freq = sorted(frequencies)
        if sorted_freq[0] > sorted_freq[1]:
            raise ValueError("Sorting has not worked!")

        # hr_freq = sorted_freq.pop(0)   # Assumed hindered rotor mode is the lowest frequency
        hr_freq = sorted_freq.pop(hr_index)  # Default is zero i.e. the lowest frequency

        # Do harmonic treatment for non-hindered rotor degrees of freedom.
        part_list = []
        sum_of_freq = sum(sorted_freq)
        for t in t_list:
            p_fun = 1
            for freq in sorted_freq:
                exponent = (-1) * (pc.h * pc.speed_of_light * freq) / (pc.kb * t)
                denominator = 1 - math.e ** exponent
                factor = denominator ** (-1)
                p_fun = p_fun * factor

            p_fun = p_fun * tanh_function(hr_freq, im, t, sigma)
            part_list.append(p_fun)
        return [part_list[i] * vscf_multiplier[i] * truncation_multiplier[i] * pf_intermolecular[i] for i in range(len(part_list))]

    if option == "eigenvalue_summation":
        sorted_freq = sorted(frequencies)
        if sorted_freq[0] > sorted_freq[1]:
            raise ValueError("Sorting has not worked!")

        if len(eigen_list) != len(t_list):
            raise ValueError("1D rotor partition function wrong lenght!")

        hr_freq = sorted_freq.pop(0)

        if len(sorted_freq) != (len(frequencies) - 1):
            raise ValueError("Tim doesnt know how the .pop method works!")

        # Do harmonic treatment for non-hindered rotor degrees of freedom.
        part_list = []
        sum_of_freq = sum(sorted_freq)
        for t in t_list:
            p_fun = 1
            for freq in sorted_freq:
                exponent = (-1) * (pc.h * pc.speed_of_light * freq) / (pc.kb * t)
                denominator = 1 - math.e ** exponent
                factor = denominator ** (-1)
                p_fun = p_fun * factor

            part_list.append(p_fun)

        part_list_eigen = [part_list[i] * eigen_list[i] for i in range(len(t_list))]

        return [part_list_eigen[i] * vscf_multiplier[i] * truncation_multiplier[i] * pf_intermolecular[i] for i in range(len(part_list_eigen))]

    if option not in hr_list:
        raise ValueError(option, "is not an allowed hindered rotor treatment. Allowed treatments are:", hr_list)


def tanh_function(hr_freq, im, t, sigma=1):
    """
    Truhlars tanh formula for a hindered rotor (Truhlar Chuang 2000)
    Acts as a HO at low T and free rotor at high T
    :param hr_freq: 
    :param im: reduced rotational moment of inertia calculated using e.g Pitzers method
    :param t: list of temps
    :param sigma: symmetry number (not used)
    :return: 
    """

    q_ho = 1 / (1 - math.exp(-pc.h * pc.speed_of_light * hr_freq / (t * pc.kb)))

    q_fr = 2 * math.pi * ((2 * math.pi * im * pc.bohr_to_m * pc.bohr_to_m * pc.au_to_kg * pc.kb * t) ** 0.5) / (pc.h * 3)

    q_i = pc.kb * t / (pc.h * hr_freq * pc.speed_of_light)
    value = q_ho * math.tanh(q_fr / q_i)
    return value


def tanh_function_comparison(hr_freq, im, t, sigma=1):
    """
    Truhlars tanh formula for a hindered rotor (Truhlar Chuang 2000)
    Acts as a HO at low T and free rotor at high T
    :param hr_freq:
    :param im: reduced rotational moment of inertia calculated using e.g Pitzers method
    :param t: list of temps
    :param sigma: symmetry number (not used)
    :return:
    """

    q_ho = 1 / (1 - math.exp(-pc.h * pc.speed_of_light * hr_freq / (t * pc.kb)))

    q_fr = 2 * math.pi * ((2 * math.pi * im * pc.bohr_to_m * pc.bohr_to_m * pc.au_to_kg * pc.kb * t) ** 0.5) / (pc.h * 3)

    q_i = pc.kb * t / (pc.h * hr_freq * pc.speed_of_light)
    value = q_ho * math.tanh(q_fr / q_i)
    return [value, q_i, q_fr, q_ho]


def find_normal_vectors(hessian, masses, linear=False):
    """
    Caculated the harmonic frequencies and eigenvectors  of a molecule given its hessian matrix and masses
    :param hessian: matrix of hessian elements from gaussian formatted checkpoint files
    :param masses: list of atom masses (atomic units)
    :return: list of frequenies
    """

    sqrt_masses = [mass ** (-0.5) for mass in masses]
    sqrt_mass_list = []
    for i in range(len(sqrt_masses)):
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])

    sqrt_masses = np.asarray(sqrt_mass_list)
    reduced_mass_matrix = np.outer(sqrt_mass_list, sqrt_mass_list)

    reduced_hessian = np.zeros(hessian.shape)

    for i in range(3 * len(masses)):
        for j in range(3 * len(masses)):
            reduced_hessian[i][j] = reduced_mass_matrix[i][j] * hessian[i][j]

    w, v = LA.eig(reduced_hessian)

    return [w, v]


def find_mass_weighted_hessian(hessian, masses):
    sqrt_masses = [mass ** (-0.5) for mass in masses]
    sqrt_mass_list = []
    for i in range(len(sqrt_masses)):
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])

    sqrt_masses = np.asarray(sqrt_mass_list)
    reduced_mass_matrix = np.outer(sqrt_mass_list, sqrt_mass_list)  # units (amu)**-1

    reduced_hessian = np.zeros(hessian.shape)

    for i in range(3 * len(masses)):
        for j in range(3 * len(masses)):
            reduced_hessian[i][j] = reduced_mass_matrix[i][j] * hessian[i][j]  # Units (hartrees / (amu (bohr)**2) )

    return reduced_hessian


def find_frequencies(hessian, masses, linear=False):
    """
    Caculated the harmonic frequencies of a molecule given its hessian matrix and masses
    :param hessian: matrix of hessian elements from gaussian formatted checkpoint files (should be in units of hartrees / (bohr)**2 ) 
    :param masses: list of atom masses (atomic mass units)
    :return: list of frequenies (in cm-1)
    """

    sqrt_masses = [mass ** (-0.5) for mass in masses]
    sqrt_mass_list = []
    for i in range(len(sqrt_masses)):
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])

    sqrt_masses = np.asarray(sqrt_mass_list)
    reduced_mass_matrix = np.outer(sqrt_mass_list, sqrt_mass_list)  # units (amu)**-1

    reduced_hessian = np.zeros(hessian.shape)

    for i in range(3 * len(masses)):
        for j in range(3 * len(masses)):
            reduced_hessian[i][j] = reduced_mass_matrix[i][j] * hessian[i][j]  # Units (hartrees / (amu (bohr)**2) )

    w, v = LA.eig(reduced_hessian)

    w = np.sort(w)  # Eigenvalues have unit ( hartrees / (amu (bohr)**2) )

    # test for imaginary frequency
    if (w[0] < 0) and (w[0] ** 2 > (1 / 10000000)):
        # imaginary frequency present
        num_imaginary = 1
    else:
        num_imaginary = 0

    num_translational = 3  # number of trans degrees of freedom
    if linear:
        num_rot = 2
    else:
        num_rot = 3  # number of rotational degrees of freedom

    num_not_vib = num_rot + num_translational + num_imaginary

    for i in range(num_not_vib):
        w = np.delete(w, 0)  # Remove translational and rotational frequencies (nearly zero)

    frequencies = [value ** (0.5) for value in w]  # take square root to find frequencies   have unit ( hartrees**0.5 / (amu**0.5 (bohr)) )

    frequencies = [value / (1822.888486192 ** 0.5) for value in frequencies]  # converts to purely atomic units ( hartrees**0.5 / (me**0.5 (bohr)) )

    frequencies = [value * 219474.6306 for value in frequencies]  # converts to cm-1 from atomic units

    return frequencies


def find_imaginary_frequency(hessian, masses):
    """
    Finds and returns an imaginary frequency from the hessain. Only works for 1 or 0 imaginary frequencies, raises a 
    value error otherwise
    :param hessian: 
    :param masses: 
    :return: imaginary frequency in cm-1
    """
    sqrt_masses = [mass ** (-0.5) for mass in masses]
    sqrt_mass_list = []
    for i in range(len(sqrt_masses)):
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])

    sqrt_masses = np.asarray(sqrt_mass_list)
    reduced_mass_matrix = np.outer(sqrt_mass_list, sqrt_mass_list)

    reduced_hessian = np.zeros(hessian.shape)

    for i in range(3 * len(masses)):
        for j in range(3 * len(masses)):
            reduced_hessian[i][j] = reduced_mass_matrix[i][j] * hessian[i][j]

    w, v = LA.eig(reduced_hessian)

    w = np.sort(w)

    # test for imaginary frequency
    if (w[0] < 0) and (w[0] ** 2 > (1 / 1000000000)):
        freq_2 = -w[0]
    else:
        freq_2 = 0

    # Check there is only one imaginary frequency!
    if (w[1] < 0) and (w[1] ** 2 > (1 / 100000000)):
        print(w[1], w[1] ** 2)
        raise ValueError("Looks like there is more than one imaginary frequency here!")

    freq = freq_2 ** (0.5)
    freq = freq * 5140.52300803  # convert to cm -1

    return freq


def find_imaginary_vector(hessian, masses):
    """
    Finds and returns an imaginary frequency from the hessain. Only works for 1 or 0 imaginary frequencies, raises a
    value error otherwise
    :param hessian:
    :param masses:
    :return: imaginary frequency in cm-1
    """
    sqrt_masses = [mass ** (-0.5) for mass in masses]
    sqrt_mass_list = []
    for i in range(len(sqrt_masses)):
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])
        sqrt_mass_list.append(sqrt_masses[i])

    sqrt_masses = np.asarray(sqrt_mass_list)
    reduced_mass_matrix = np.outer(sqrt_mass_list, sqrt_mass_list)

    reduced_hessian = np.zeros(hessian.shape)

    for i in range(3 * len(masses)):
        for j in range(3 * len(masses)):
            reduced_hessian[i][j] = reduced_mass_matrix[i][j] * hessian[i][j]

    w, v = LA.eig(reduced_hessian)

    return v[:, np.argmin(w)]


def moment_of_intertia(coords, mass):
    """
    Calculates the 3 moment of inertias from the eigenvalues of the inertia matrix thing
    :return: list of length three
    """

    centre_of_mass = [0, 0, 0]
    for i in range(len(mass)):
        centre_of_mass = centre_of_mass + coords[i] * mass[i]
    centre_of_mass = centre_of_mass / (sum(mass))

    # move COM to origin
    for i in range(len(coords)):
        coords[i] = coords[i] - centre_of_mass

    # Set up inertia matrix

    i_mat = np.zeros((3, 3))

    i_mat[0][0] = sum((mass[i] * (coords[i][1] ** 2 + coords[i][2] ** 2)) for i in range(len(mass)))
    i_mat[1][1] = sum((mass[i] * (coords[i][2] ** 2 + coords[i][0] ** 2)) for i in range(len(mass)))
    i_mat[2][2] = sum((mass[i] * (coords[i][1] ** 2 + coords[i][0] ** 2)) for i in range(len(mass)))
    i_mat[0][1] = (-1) * sum((mass[i] * (coords[i][0] * coords[i][1])) for i in range(len(mass)))
    i_mat[1][0] = i_mat[0][1]
    i_mat[1][2] = (-1) * sum((mass[i] * (coords[i][2] * coords[i][1])) for i in range(len(mass)))
    i_mat[2][1] = i_mat[1][2]

    i_mat[0][2] = (-1) * sum((mass[i] * (coords[i][0] * coords[i][2])) for i in range(len(mass)))
    i_mat[2][0] = i_mat[0][2]
    w, v = LA.eig(i_mat)  # Eigen values are the moments of inertia

    w = w * pc.bohr_to_m * pc.bohr_to_m * pc.au_to_kg

    return w


def q_rotational(inertias, t_list):
    """
    HIgh temperature limit of rotational partiton function
    :param inertias: List of length 3, of the three moments of inertias
    :param t_list: list of temperatures
    :return: 
    """
    # if abs(inertias[0]) < 0.0000000000000000000001:
    if abs(inertias[0] / inertias[1]) < 1 / 10000000000:  # must be a linear molecule with one zero momont of inertia

        A = pc.hbar * pc.hbar / (2 * inertias[1] * pc.kb)
        p_fun = []
        for t in t_list:
            p_fun.append(t / (A))
    # elif abs(inertias[1]) < 0.0000000000000000000001:
    elif abs(inertias[1] / inertias[0]) < 1 / 10000000000:  # must be a linear molecule with one zero momont of inertia
        A = pc.hbar * pc.hbar / (2 * inertias[2] * pc.kb)
        p_fun = []
        for t in t_list:
            p_fun.append(t / (A))
    # elif abs(inertias[2]) < 0.0000000000000000000001:
    elif abs(inertias[2] / inertias[1]) < 1 / 1000000000000:  # must be a linear molecule with one zero momont of inertia
        A = pc.hbar * pc.hbar / (2 * inertias[1] * pc.kb)
        p_fun = []
        for t in t_list:
            p_fun.append(t / (A))
    else:
        # Rotational constnats
        A = pc.hbar * pc.hbar / (2 * inertias[0] * pc.kb)
        B = pc.hbar * pc.hbar / (2 * inertias[1] * pc.kb)
        C = pc.hbar * pc.hbar / (2 * inertias[2] * pc.kb)

        p_fun = []
        for t in t_list:
            p_fun.append(((math.pi * t ** 3) / (A * B * C)) ** 0.5)

    return p_fun


def zpe(freq_list, vscf=False, name="Joanna"):
    """
    Returns the zero point energy for a given list of frequencies in J
    :param freq_list: 
    :return: ZPE
    """
    zpe = pc.h * pc.speed_of_light * 0.5 * sum(freq_list)
    if not vscf:
        return zpe

    else:
        f = open(name + "_zpe.out")
        for i, line in enumerate(f):
            # Remember this may just be for a subset of the vibrations, so the ZPE from the VSCF calculation is NOT the totla zpe!
            # Note also the units in the .out file are atomic units, whilst the units of zpe are joules!
            if i == 0:
                harmonic_zpe = float(line.split()[1]) * hartree_to_joule
            if i == 1:
                peturbed_zpe = float(line.split()[1]) * hartree_to_joule
        f.close()

        new_zpe = zpe - harmonic_zpe + peturbed_zpe
        # Check this is a reasonable correction:

        print(-harmonic_zpe + peturbed_zpe)

        if ((new_zpe / zpe) > 5) or ((new_zpe / zpe) < 0.2):
            raise ValueError("This seems like an extreme adjustment!! CHECK PLEASE!")

        if new_zpe < 0:
            raise ValueError("The zpe cannot be below zero! Thats crazy!")

        return new_zpe


def reduced_inertia(coordinate_list, mass_list, hr_groups):
    """
    Gives the rotational reduced moment of inertia calculated using truhlars method
    :param coordinate_list: 
    :param mass_list: the list of masses of the atoms (obviously)
    :param hr_groups: first list is list of atoms in rotating group 1, second is list of atoms in rotating group 2, and 
            third list is the rotatiton axis
            NOTE ARRAYS INDEXING STARTS AT 0 (But in mathematica (and gaussview) they start at 1, so there is a difference to Xiao's origonal code)
    :return: 
    """
    axis = hr_groups[2]
    i1 = 0
    for atom in hr_groups[0]:
        if atom > len(mass_list) - 1:
            raise ValueError("This atoms index is alrger than the length of the mass list. Have you accounted for zero indexing in python?")
        i1 = i1 + mass_list[atom] * point_line_distance(axis, coordinate_list, atom) ** 2

    i2 = 0
    for atom in hr_groups[1]:
        if atom > len(mass_list) - 1:
            raise ValueError("This atoms index is alrger than the length of the mass list. Have you accounted for zero indexing in python?")

        i2 = i2 + mass_list[atom] * point_line_distance(axis, coordinate_list, atom) ** 2

    return i1 * i2 / (i1 + i2)


def point_line_distance(axis, coordinate_list, atom):
    x1 = np.asarray(coordinate_list[axis[0]])
    x2 = np.asarray(coordinate_list[axis[1]])

    x0 = np.asarray(coordinate_list[atom])

    d = np.linalg.norm(np.cross(x2 - x1, x1 - x0)) / np.linalg.norm(x2 - x1)

    return d


if __name__ == '__main__':
    # TODO unit tests

    pass
