// This code calculates the density of states of a system based on its anharmonic constants and harmonic frequencies.
// It is automatically called by the pySCTST code if required, and can be quite slow (~10 mins)
// It is based on the Wang Landau method (en.wikipedia.org/wiki/Wang_and_Landau_algorithm)
// Based on code written by Nguyen and Barker (http://pubs.acs.org/doi/pdf/10.1021/jp100132s)

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <assert.h>


#define Emax 0.30             // chosen to give 6000 steps
#define Estep 0.00005        // =11 cm-1
#define Niter 21              // normally 21
#define Ntrial 100000000  // original 10000000


int nvmax(unsigned int nv[], unsigned int k, double Eu, double anh[], double freq[], unsigned int n);
double nNRG(double *freq, double *anh, unsigned int *nv, unsigned int n);
int ckderiv(double *freq, double *anh, unsigned int *nv, unsigned int n); // returns 0 if ok, 1 if fail
double nCoup(double *coup, unsigned int *nv, unsigned int n);

int main(int argc, const char * argv[]) {


    // Specify the input file. This is generated automaically by the pySCTST code, by the transition_state method "write_states_input", which will generate a file called "FD_files/name_states_input.txt"
    FILE *inFile = fopen(argv[1], "r");  // Contains N bonds, frequencies, X_nF and X_ij


    // Read in the States_input file
    char *buf=NULL;
    size_t bufLen;
    getline(&buf, &bufLen, inFile);
    
    unsigned int nModes = 1;
    fscanf(inFile, "%d", &nModes);  // Read in number of modes

    
    fscanf(inFile, "%s", buf);
    getline(&buf, &bufLen, inFile);
    double freq[nModes];
    unsigned int i,j,k,l;
    for (i = 0; i < nModes; i++) {
        fscanf(inFile, "%lf,", &freq[i]);   // Read in frequency of modes
    }
    double coupling[nModes];
    fscanf(inFile, "%s", buf);
    getline(&buf, &bufLen, inFile);
    for (i=0; i < nModes; i++) {
        fscanf(inFile, "%lf,", &coupling[i]);   // Read in X_nF values (coupling to reaction coordinate)
    }


    double anh[nModes][nModes];
    fscanf(inFile, "%s", buf);
    getline(&buf, &bufLen, inFile);
    for (i=0; i < nModes; i++) {
        for (j=0; j < nModes; j++) {
            fscanf(inFile, "%lf,", &anh[i][j]);     // Read in X_ij values
        }
    }

    printf("Read in data:\n");
    printf("Number of Modes: %i\n", nModes);
    printf("Frequencies: ");
    for (int i=0; i<nModes; i++){printf("%f, ", freq[i]);}
    printf("\n Reaction coordinate coupling: ");
    for (int i=0; i<nModes; i++){printf("%f, ", coupling[i]);}
    printf("\n");
    printf("Anharmonic Constants: \n");
    for (int i=0; i<nModes; i++){
            for (j=0; j<nModes; j++){
                printf("%f, ", anh[i][j]);
            }
        printf("\n");
    }


    unsigned int nSteps = Emax / Estep;
    double g[nSteps];       // This is the (log of the) un-normalised density of states
    double TTF[nSteps];
    double Ev[nSteps];
    for (i = 0; i < nSteps; i++) {
        g[i] = 0;
        TTF[i] = 0;
        Ev[i] = 0;
    }
    double lnf = 1;

    // Calculate anharmonicaly corrected ZPE
    double ZPE = 0;
    for (i = 0; i < nModes; i++) {
        ZPE += freq[i] / 2;
        for (j = i; j < nModes; j++) {
            ZPE += anh[i][j] / 4;
        }
    }

    for (i=0; i<nModes; i++){printf("%f\n ", freq[i]);}
    
    time_t t;
    srand((unsigned) time(&t));
    rand(); // This randomly generated number will always be the same, but subsequent calls to rand() will generate random numbers
    
    // Initialize quantum numbers i.e. vibrational states of each of the modes
    unsigned int nvold[nModes];
    int ntest = 1;
    while (ntest == 1) {
        double Estart = (double)rand() / (double)RAND_MAX * Emax;    // Random energy between 0 and Emax

        // initialise states as 0
        for (i = 0; i < nModes; i++) {
            nvold[i] = 0;
        }

        for (l = 0; l < nModes; l++) {
            double Eu = Estart - nNRG(freq, (double *)anh, nvold, nModes);   // Calculates energy change
            if (Eu != 0) {
                int nstart = nvmax(nvold, l, Eu, (double *)anh, freq, nModes);  // Calculates the maximum allowed quantum number
                if (nstart == -1) {    // Happens if Eu <0
                    break;
                }
                nvold[l] = nstart * (double)rand() / (double)RAND_MAX;
            }
        }

        ntest = ckderiv(freq, (double *)anh, nvold, nModes); //  If derivative test passed, ntest =0 then escape this while loop
        if (ntest == 1) {

            for (t=0; t<nModes; t++){printf("%d ", nvold[t]);}

            int sum = 0;
            for (t=0; t<nModes; t++){sum = sum + nvold[t];}
            if (sum ==0){printf("Derivative test failed even in lowest energy state! Are your bonds too anharmonic, Tim?\n");}
            if (sum>0){printf("Failed derivative test\n");}



        }
    }
    double Eold = nNRG(freq, (double *)anh, nvold, nModes) - ZPE;
    unsigned int nold = Eold / Estep;    // I think this is the energy bin the state sits in
    
    double p = 1.0 / nModes;    // This is the probability any given mode will be changed up or down on a single iteration of the simulation.
    // Begin algorithm
    unsigned int H[nSteps];
    for (l = 0; l < nSteps; l++) {
        H[l] = 0;               // Initialise histogram at zero
    }


    for (i = 0; i < Niter; i++) {    // Niter is like about 20. This is the number of times we run the simulation, then reduce f -> sqrt(f).
            for (l = 0; l < nSteps; l++) {
            H[l] = 0;               // Initialise histogram at zero
            }

        printf("Iteration %u of %u\n", i + 1, Niter + 1);
        
        for (j = 0; j < Ntrial; j++) {    // Ntrial is about 100000000
            unsigned int nvnew[nModes];    // new state list
            int allZero = 1;
            for (k = 0; k < nModes; k++) {          // Go through each mode. With probability p, the mode will be shifted down, and with probability p the mode will be shifted up.
                double r1 = (double)rand() / (double)RAND_MAX;
                if (r1 <= p) {
                    int new = nvold[k] - 1;         // Shift Down (probability = p)
                    if (new < 0) {
                        new = 0;
                    }
                    nvnew[k] = new;
                }
                else if (r1 <= 2 * p) {
                    nvnew[k] = nvold[k] + 1;        // Shift Up (probability = p)
                }
                else {
                    nvnew[k] = nvold[k];            // Don't change  (probability = 1 - 2p)
                }
                if (allZero && nvnew[k] != 0) {
                    allZero = 0;
                }
            }
            double newE = nNRG(freq, (double *)anh, nvnew, nModes) - ZPE;               // Calculate the energy of the new vibrational state
            if (newE < Emax && ckderiv(freq, (double *)anh, nvnew, nModes) == 0) {      // TRUE if the state is bound
                unsigned int nnew = newE / Estep;                                       // New energy bin index
                double r2 = (double)rand() / (double)RAND_MAX;                          // Accept this new state with probability Min[g(E1)/g(E2), 1]. Here we actually use Min[exp(ln(g(E1)) - ln(g(E2))), 1]
                if (exp(g[nold] - g[nnew]) > r2) {
                    nold = nnew;                                                        // Accept the move
                    for (k = 0; k < nModes; k++) {
                        nvold[k] = nvnew[k];
                    }
                    Eold = newE;
                }
            }
            g[nold] += lnf;     //  Update the density of states of the bin [ g(e) -> f g(e) ], here we are actually doing [ln(g) -> ln(g) + ln(f) ]
            H[nold]++;                                                                   // Update the histogram



            if ((j % 500000) == 0){
                char itteration[5];
                sprintf(itteration, "histograms/%d_%d.csv", i, j/500000);
                FILE *outFile = fopen(itteration, "w");
                fprintf(outFile, "Estep = %lf; Niter = %u; Ntrial = %u\n", Estep, Niter, Ntrial);
                fprintf(outFile, "Energy,states,histogram\n");
                for (k = 0; k < nSteps; k++) {                                                    // Normalise the energies and couplings
                    fprintf(outFile, "%.7lf,%lf,%i\n", ZPE + k * Estep, exp(g[k] - g[0]), H[k]);
                }
                fclose(outFile);
            }

        }



        lnf *= 0.5;             // Reduce the value of f -> sqrt(f) [ here we are actually doing ln(f) -> 0.5 ln(f) ]





    }


    
    // Last iteration to compute energies (Ev) & couplings (TTF)
    printf("Starting final iteration\n");
//    unsigned int H[nSteps];
    for (l = 0; l < nSteps; l++) {
        H[l] = 0;               // Initialise histogram at zero
    }
    
    for (j = 0; j < Ntrial; j++) {
        unsigned int nvnew[nModes];
        for (k = 0; k < nModes; k++) {                              // Go through each mode. With probability p, the mode will be shifted down, and with probability p the mode will be shifted up.
            double r1 = (double)rand() / (double)RAND_MAX;
            if (r1 <= p) {
                int new = nvold[k] - 1;                             // Shift Down (probability = p)
                if (new < 0) {
                    new = 0;
                }
                nvnew[k] = new;
            }
            else if (r1 <= 2 * p) {
                nvnew[k] = nvold[k] + 1;                            // Shift Up (probability = p)
            }
            else {
                nvnew[k] = nvold[k];                                // Don't change  (probability = 1 - 2p)
            }
        }
        double newE = nNRG(freq, (double *)anh, nvnew, nModes) - ZPE;               // Calculate the energy of the new vibrational state
        if (newE < Emax && ckderiv(freq, (double *)anh, nvnew, nModes) == 0) {      // TRUE if the state is bound
            unsigned int nnew = newE / Estep;                                       // New energy bin index
            double r2 = (double)rand() / (double)RAND_MAX;
            if (exp(g[nold] - g[nnew]) > r2) {                                      // Accept this new state with probability Min[g(E1)/g(E2), 1]. Here we actually use Min[exp(ln(g(E1)) - ln(g(E2))), 1]
                nold = nnew;
                for (k = 0; k < nModes; k++) {
                    nvold[k] = nvnew[k];                                            // Accept the move
                }
                Eold = newE;
            }
        }
        H[nold]++;                                                                   // Update the histogram

        g[nold] += lnf;                                                              // Update the DoS
        Ev[nold] += Eold;
        TTF[nold] += nCoup(coupling, nvold, nModes);                                 // Update the average coupling terms from x_{nF} and quantum numbers
    }

    // Write output file for use in sctst.c
    FILE *outFile = fopen(argv[2], "w");
    fprintf(outFile, "Estep = %lf; Niter = %u; Ntrial = %u\n", Estep, Niter, Ntrial);
    fprintf(outFile, "Energy, # of states, average Ev, average coupling,histogram\n");
    for (i = 0; i < nSteps; i++) {                                                    // Normalise the energies and couplings
        if (H[i] > 0) {
            Ev[i] = Ev[i] / H[i];
            TTF[i] = TTF[i] / H[i];
        }
        else {
            Ev[i] = -ZPE;
            TTF[i] = -ZPE;
        }

        fprintf(outFile, "%.7lf,%lf,%.7lf,%.8lf,%i\n", ZPE + i * Estep, exp(g[i] - g[0]), Ev[i] + ZPE, TTF[i], H[i]);
    }
    
}

// Returns 1 if derivative of energy with respect to any quantum number is <0
int ckderiv(double *freq, double *anh, unsigned int *nv, unsigned int n) {
    int ntest = 0;
    unsigned int i,k;
    for (k = 0; k < n && ntest == 0; k++) {         // Itterate over all quantum numbers
        double sum = 0;
        for (i = 0; i < n; i++) {
            double inTerm = nv[i] + 0.5;
            sum += anh[k * n + i] * inTerm;
        }
        double knTerm = nv[k] + 0.5;
        double deriv = freq[k] + knTerm * anh[k * n + k] + sum; // eqtn 2.97 Sam's Thesis
        if (deriv < 0) {
//            printf("Issue with mode %i\n", k);
            ntest = 1;
        }
    }
    return ntest;
}

// Calculates energy from frequencies, anharmonic constants, and quantum numbers (VPT2 expression eqtn 2.88 Sam's Thesis)
double nNRG(double *freq, double *anh, unsigned int *nv, unsigned int n) {
    double result = 0;
    unsigned int i,j;
    for (i = 0; i < n; i++) {
        result += freq[i] * (nv[i] + 0.5);
        for (j = 0; j <= i; j++) {
            result += anh[j * n + i] * (nv[i] + 0.5) * (nv[j] + 0.5);
        }
    }
    return result;
}

// Calculates coupling terms from x_{nF} and quantum numbers
double nCoup(double *coup, unsigned int *nv, unsigned int n) {
    double result = 0;
    unsigned int i;
    for (i = 0; i < n; i++) {
        result += coup[i] * (nv[i] + 0.5);

    }
    return result;
}

// Calculates maximum allowed quantum number for a mode k given an energy Eu and a set of quantum numbers nv
int nvmax(unsigned int nv[], unsigned int k, double Eu, double anh[], double freq[], unsigned int n) {
    if (Eu < 0) {
        return -1;
    }
    if (anh[k * n + k] != 0) {
        double sum = 0;
        unsigned int i;
        for (i = 0; i < n; i++) {
            if (i != k) {
                sum += (nv[i] + 0.5) * anh[i * n + k];
            }
        }
        double anhkk = anh[k * n + k];
        double zpe = anhkk * 0.25 + freq[k] * 0.5 + sum * 0.5;
        double vd = (-freq[k] - sum) / (2 * anhkk) - 0.5;
        
        double anhksum = 0;
        for (i=0; i < n; i++) {
            if (i != k) {
                anhksum = anh[k * n + i];
            }
        }
        
        double Dk = -(freq[k] + sum) * (freq[k] + sum) / 4 / anhkk - zpe;
        
        unsigned int vmax = vd * (1 - sqrt(1 - Eu / Dk));
        if ((freq[k] + sum) > 0 && anhkk < 0) {
            if (Eu > Dk) {
                return vd;
            }
            else {
                return vmax;
            }
        }
        else {
            if ((freq[k] + sum) > 0 && anhkk > 0) {
                return vmax;
            }
            else {
                return -1;
            }
        }
    }
    
    return -1;
}
