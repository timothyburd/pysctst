# Written by T. Young. SCTST and exact results on analytic barriers

import matplotlib.pyplot as plt
from scipy import optimize
# from scipy.interpolate import spline
from scipy.integrate import quad
from scipy.optimize import brentq
from scipy.optimize import root

from asym_eckart_derivatives import *
from oneD_sctst_VTPn_data import *


class Eckart(object):
    """
    Class for the symmetric Eckart potential (V = V0 cosh^2(x/a). VPT0,2,4 results are from Stanton2016

    Requires initialisation with a mass, v0 and a parameters.
    """

    def __init__(self, mass, v0, a):
        """
        omgb is the absolute value of the imaginary frequency.
        omgcorr is the correction suggested in Stanton2016, will be generated if theta_vpt2corr is called.
        """

        self.mass = mass
        self.v0 = v0
        self.a = a
        self.omgb = np.sqrt(2.0 * v0 / (mass * a ** 2))

    def potential(self, x):
        return self.v0 / (np.power(np.cosh(x / self.a), 2))

    def theta_vpt0(self, energy):
        theta = (np.pi * self.v0 / self.omgb) * (1 - energy / self.v0)

        return theta

    def theta_vpt2(self, energy):
        theta = (2.0 * np.pi * self.v0 / self.omgb) * (1 -
                                                       np.sqrt(energy / self.v0 +
                                                               np.power(self.omgb, 2) / (16.0 * np.power(self.v0, 2)))
                                                       )

        g = -1.0 * self.omgb ** 2 / (16.0 * self.v0)
        w = self.omgb
        x = 4.0 * g

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_vpt4(self, energy):
        omgb_sq = np.power(self.omgb, 2)
        v0_sq = np.power(self.v0, 2)

        theta = ((2.0 * np.pi * self.v0) / self.omgb) * (1 -
                                                         (omgb_sq / (32.0 * v0_sq)) -
                                                         (np.sqrt(
                                                             energy / self.v0 +
                                                             np.power(omgb_sq, 2) / (1024.0 * np.power(v0_sq, 2))))
                                                         )

        g = -1.0 * self.omgb ** 2 / (16.0 * self.v0)
        w = self.omgb - self.omgb ** 3 / (32.0 * self.v0 ** 2)
        x = 4.0 * g

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_vpt2plus(self, energy):
        x = -4.0 * self.omgb ** 2 / (16.0 * self.v0)

        omgbcorr = self.omgb * np.sqrt(1 - (x / self.omgb) ** 2)

        theta = (2.0 * np.pi * self.v0 / omgbcorr) * (1 - np.sqrt(energy / self.v0 +
                                                                  omgbcorr ** 2 / (16.0 * np.power(self.v0, 2)))
                                                      )

        g = -1.0 * self.omgb ** 2 / (16.0 * self.v0)
        w = omgbcorr
        x = 4.0 * g

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_vpt2c(self, energy):
        x = -4.0 * self.omgb ** 2 / (16.0 * self.v0)

        omgbcorr = self.omgb * (1 - (x ** 2 / (2.0 * self.omgb ** 2)))

        g = -1.0 * self.omgb ** 2 / (16.0 * self.v0)
        w = omgbcorr
        x = 4.0 * g

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_exact(self, energy):
        omgb_sq = 2.0 * self.v0 / (self.mass * self.a ** 2)
        v0_sq = np.power(self.v0, 2)
        pi_sq = np.power(np.pi, 2)

        theta = np.log(np.cosh(np.sqrt(4.0 * pi_sq * (16.0 * v0_sq - omgb_sq) / (16.0 * omgb_sq))) /
                       np.sinh(np.sqrt(4.0 * pi_sq * self.v0 * energy / omgb_sq))
                       )

        return theta


class AsymmetricEckart(object):
    """
    Class for an Eckart potential shifted by a sigmoid b/(1 + exp{cx})
    VPT0, VPT2 and VPT4 expressions for theta are derived.
    """

    def __init__(self, mass, v1, a, v2, c, f_barrier=None, r_barrier=None):

        self.mass = mass
        self.v1 = v1
        self.a = a
        self.v2 = v2
        self.c = c
        self.v1_bar = v1 - v2 / 2.0
        self.f_barrier = f_barrier
        self.r_barrier = r_barrier

        # This value of omega_b is only valid when V2 ~ 0
        self.omgb = np.sqrt(2.0 * v1 / (mass * a ** 2))

        # Find the value of x that solves f'(x) = 0, or alternatively minimises the negative of the function
        self.x_max_v = optimize.fmin(lambda x: -self.potential(x), 0.0)[0]
        self.f1 = calc_f1(self.v1_bar, a, v2, c, self.x_max_v)
        if self.f1 > 1E-5:
            print('First derivative of the potential at x =', self.x_max_v, 'is', self.f1, 'It should be 0.')

        # If initialised with forward and reverse barriers specified, optimise v1 and v2 to reproduce them
        if f_barrier is not None and r_barrier is not None:
            self.v2 = r_barrier - f_barrier
            if f_barrier > r_barrier:
                def diff_v1_f_barrier(v1):
                    return (optimize.fmin(lambda x: -self.potential(x, v1=v1), 0.0, full_output=True)[1] + self.f_barrier) ** 2

                self.v1 = optimize.fmin(diff_v1_f_barrier, self.f_barrier)[0]

            if r_barrier > f_barrier:
                def diff_v1_r_barrier(v1):
                    return (optimize.fmin(lambda x: -self.potential(x, v1=v1), 0.0, full_output=True)[1] + self.r_barrier) ** 2

                self.v1 = optimize.fmin(diff_v1_r_barrier, self.r_barrier)[0]
                print(self.v1, self.v2)

    def potential(self, x, v1=None):
        if v1 is not None:
            return (v1 - self.v2 / 2.0) / (np.power(np.cosh(x / self.a), 2)) + self.v2 / (1.0 + np.exp(self.c * x))
        else:
            return self.v1_bar / (np.power(np.cosh(x / self.a), 2)) + self.v2 / (1.0 + np.exp(self.c * x))

    def theta_vpt0_smallb(self, energy):
        theta = (np.pi * self.v1 / self.omgb) * (1 - energy / self.v1)
        return theta

    def theta_vpt2_smallb(self, energy):

        a, b, c = self.a, self.v2, self.c
        v = self.v1
        m = self.mass

        g = -(18432.0 * v * v + 7.0 * a ** 6 * b ** 2 * c ** 6) / (147456 * a * a * m * v * v)
        w_abs = self.omgb
        x = -(6144.0 * v * v + 5.0 * a ** 6 * b ** 2 * c ** 6) / (12288 * a * a * m * v * v)

        # theta = np.pi * (-w_abs + np.sqrt(w_abs*w_abs + 4.0*x*(v + g - energy))) / (2.0*x)
        coeffs = [(v + g - energy), (-w_abs / np.pi), (-x / np.pi ** 2)]
        theta = np.roots(list(reversed(coeffs)))[1]

        return theta

    def theta_vpt4_smallb(self, energy):

        a, b, c = self.a, self.v2, self.c
        v = self.v1
        m = self.mass

        g = -(18432.0 * v * v + 7.0 * a ** 6 * b ** 2 * c ** 6) / (147456 * a * a * m * v * v)
        w_abs = (16.0 * a * a * m * v - 1) / (8.0 * a ** 4 * m * m * self.omgb)
        x = -(6144.0 * v * v + 5.0 * a ** 6 * b ** 2 * c ** 6) / (12288 * a * a * m * v * v)
        y_abs = (a ** 6 * b * b * c ** 6 * self.omgb * (235 * a ** 6 * b * b * c ** 6 -
                                                        12288.0 * (7.0 * a * a * c * c - 75.0) * v * v)) / \
                (905969664 * v ** 5 * a * a * m)

        coeffs = [(v + g - energy), (-w_abs / np.pi), (-x / np.pi ** 2), (y_abs / np.pi ** 3)]

        theta = np.roots(list(reversed(coeffs)))[2]

        return theta

    def theta_vpt0(self, energy):
        a, b, c = self.a, self.v2, self.c
        v = self.v1_bar
        omega_b = np.sqrt(-calc_f2(v, a, b, c, self.x_max_v) / self.mass)

        theta = (np.pi / omega_b) * (self.v1 - energy)

        return theta

    def theta_vpt2(self, energy):
        a, b, c = self.a, self.v2, self.c
        v = self.v1_bar
        m = self.mass
        nu_b = np.sqrt(-calc_f2(v, a, b, c, self.x_max_v) / m) * 1j

        f4 = calc_f4(v, a, b, c, self.x_max_v)
        f3 = calc_f3(v, a, b, c, self.x_max_v)

        phi4 = f4 / (m ** 2 * nu_b ** 2)
        phi3_sq = (f3 * f3) / (m ** 3 * nu_b ** 3)

        w_abs = np.abs(nu_b)
        x = phi4 / 16.0 - (5.0 * phi3_sq) / (48.0 * nu_b)
        g = phi4 / 64.0 - (7.0 * phi3_sq) / (576.0 * nu_b)

        coeffs = [(self.v1 + g - energy), (-w_abs / np.pi), (-x / np.pi ** 2)]
        theta = np.roots(list(reversed(coeffs)))[1]

        return theta

    def theta_vpt2plus(self, energy):
        a, b, c = self.a, self.v2, self.c
        v = self.v1_bar
        m = self.mass
        nu_b = np.sqrt(-calc_f2(v, a, b, c, self.x_max_v) / m) * 1j
        omgb = np.abs(nu_b)

        f4 = calc_f4(v, a, b, c, self.x_max_v)
        f3 = calc_f3(v, a, b, c, self.x_max_v)

        phi4 = f4 / (m ** 2 * nu_b ** 2)
        phi3_sq = (f3 * f3) / (m ** 3 * nu_b ** 3)

        x = phi4 / 16.0 - (5.0 * phi3_sq) / (48.0 * nu_b)
        g = phi4 / 64.0 - (7.0 * phi3_sq) / (576.0 * nu_b)

        w_abs = omgb * (1 - (x / omgb) ** 2) ** 0.5

        coeffs = [(self.v1 + g - energy), (-w_abs / np.pi), (-x / np.pi ** 2)]
        theta = np.roots(list(reversed(coeffs)))[1]

        return theta

    def theta_vpt4(self, energy):
        a, b, c = self.a, self.v2, self.c
        v = self.v1_bar
        m = self.mass
        nu_b = np.sqrt(-calc_f2(v, a, b, c, self.x_max_v) / m) * 1j

        f3 = calc_f3(v, a, b, c, self.x_max_v)
        f4 = calc_f4(v, a, b, c, self.x_max_v)
        f5 = calc_f5(v, a, b, c, self.x_max_v)
        f6 = calc_f6(v, a, b, c, self.x_max_v)

        phi3 = f3 / (m ** 1.5 * nu_b ** 1.5)
        phi4 = f4 / (m ** 2 * nu_b ** 2)
        phi5 = f5 / (m ** 2.5 * nu_b ** 2.5)
        phi6 = f6 / (m ** 3 * nu_b ** 3)

        w = nu_b - (67.0 * phi4 ** 2) / (9216.0 * nu_b) + (5.0 * phi6) / 1152.0
        y = phi6 / 288.0 - (7.0 * phi3 * phi5) / (288.0 * nu_b) - (17.0 * phi4 ** 2) / (2304.0 * nu_b) + \
            (25.0 * phi3 ** 2 * phi4) / (384.0 * nu_b ** 2) - (235.0 * phi3 ** 4) / (6912.0 * nu_b ** 3)

        w_abs = np.abs(w)
        y_abs = np.abs(y)
        x = phi4 / 16.0 - (5.0 * phi3 ** 2) / (48.0 * nu_b)
        g = phi4 / 64.0 - (7.0 * phi3 ** 2) / (576.0 * nu_b)

        coeffs = [(self.v1 + g - energy), (-w_abs / np.pi), (-x / np.pi ** 2), (y_abs / np.pi ** 3)]
        theta = np.roots(list(reversed(coeffs)))[2]

        return theta


class Gaussian(object):
    """
    Class for a gaussian barrier: V = V0 exp{-sigma x^2}. VPTn expressions are derived.
    """

    def __init__(self, mass, v0, sigma):
        self.mass = mass
        self.v0 = v0
        self.sigma = sigma

        self.omgb = np.sqrt(2.0 * v0 * sigma / mass)

    def potential(self, x):
        return self.v0 * np.exp(-(self.sigma * x * x))

    def theta_vpt0(self, energy):
        theta = (np.pi * self.v0 / self.omgb) * (1 - energy / self.v0)

        return theta

    def theta_vpt2(self, energy):
        w = self.omgb
        g = -(3.0 * self.sigma) / (32.0 * self.mass)
        x = 4.0 * g

        print(np.round(g * 10 ** 5, 3), np.round(w * 10 ** 3, 3), np.round(x * 10 ** 4, 3))

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_vpt2plus(self, energy):
        x = -(12.0 * self.sigma) / (32.0 * self.mass)
        w = self.omgb * np.sqrt(1 - (x / self.omgb) ** 2)

        g = -(3.0 * self.sigma) / (32.0 * self.mass)

        print(np.round(g * 10 ** 5, 3), np.round(w * 10 ** 3, 3), np.round(x * 10 ** 4, 3))

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_vpt2c(self, energy):
        x = -(12.0 * self.sigma) / (32.0 * self.mass)
        w = self.omgb * (1 - (x ** 2 / (2.0 * self.omgb ** 2)))

        g = -(3.0 * self.sigma) / (32.0 * self.mass)
        x = 4.0 * g

        theta = np.pi * (-w + np.sqrt(w * w + 4.0 * x * (self.v0 + g - energy))) / (2.0 * x)

        return theta

    def theta_vpt4(self, energy):
        m = self.mass
        s = self.sigma
        v = self.v0

        g = -(3.0 * s) / (32.0 * m)
        w_abs = (s * (1536.0 * m * v + s)) / (768.0 * m ** 2 * self.omgb) * 0.99
        x = 4.0 * g
        y_abs = (11.0 * self.omgb * s) / (384.0 * v * m)

        print(np.round(g * 10 ** 5, 3), np.round(w_abs * 10 ** 3, 3), np.round(x * 10 ** 4, 3), np.round(y_abs * 10 ** 6, 3))

        # E = V0 + G - |W|theta/pi - X theta^2/pi^2 + |Y| theta^3/pi^3
        coeffs = [(v + g - energy), (-w_abs / np.pi), (-x / np.pi ** 2), (y_abs / np.pi ** 3)]

        # Take the real root, which is the last defined from the cubic. np.roots finds all the roots of the cubic.
        theta = np.roots(list(reversed(coeffs)))[2]

        return np.real(theta)


class Step(object):
    """
    Class for a rectangular step potential.
    Only includes the potential, for 1Dqrs testing. VPTn expressions vanish.
    """

    def __init__(self, mass, v0, sigma, omega_b):
        self.mass = mass
        self.v0 = v0
        self.sigma = sigma
        self.omgb = omega_b

    def potential(self, x):
        v = 0.0
        if x < 0.0:
            v = self.v0
        return v


class JHEckart(object):

    def __init__(self, mass, dv1, dv2, L):
        self.mass = mass
        self.dv1 = dv1
        self.dv2 = dv2
        self.L = L

        self.a = self.dv1 - self.dv2
        self.b = (np.sqrt(dv1) + np.sqrt(dv2)) ** 2
        self.omgb = np.sqrt((self.b * np.pi ** 2) / (2.0 * L ** 2 * mass))

    def potential(self, x):
        y = np.exp(2.0 * np.pi * x / self.L)
        return self.a * y / (1.0 + y) + self.b * y / (1.0 + y) ** 2

    def p_exact(self, energy):
        """
        From: TUNNELLING CORRECTIONS FOR UNSYMMETRICAL ECKART POTENTIAL ENERGY BARRIERS,
        Harold S. Johnston and Julian Heicklen, 1962
        """
        xi = energy / self.dv1
        alpha1 = 2.0 * np.pi * self.dv1 / self.omgb
        alpha2 = 2.0 * np.pi * self.dv2 / self.omgb

        d = 2.0 * np.sqrt(alpha1 * alpha2 - np.pi ** 2 / 4)
        b = (2.0 * np.sqrt((1 + xi) * alpha1 - alpha2)) / (alpha1 ** -0.5 + alpha2 ** -0.5)
        a = (2.0 * np.sqrt(alpha1 * xi)) / (alpha1 ** -0.5 + alpha2 ** -0.5)

        if isinstance(d, complex):
            d = np.abs(d)

        tunneling_probability = 1 - (np.cosh(a - b) + np.cosh(d)) / (np.cosh(a + b) + np.cosh(d))

        return tunneling_probability


def calc_prob_from_coeffs(coeff_list):
    """
    From a list of coeffs obtained numerically return P(E) by solving the quartic under the condition that
    |A|^2 > |B|^2, i.e. the magnitude of the incoming scattering WF is greater than that of the reflected.

    :param coeff_list: list of coefficients. Returned from 1Dqrs.cpp
    :return: Tunneling probability P(E)
    """

    c_r, c_i, shift_r, shift_i = coeff_list

    def simil_eqns(variables):
        """
        Equations (=0) in vector form, required by scipy.fsolve.
        :return: np array of values for each of the equations
        """

        a, alpha, b, beta = variables

        values = np.array([(a + b) ** 2 + (beta - alpha) ** 2 - c_r ** 2,
                           (beta + alpha) ** 2 + (a - b) ** 2 - c_i ** 2,
                           (a + b) - shift_r * (beta - alpha),
                           (beta + alpha) - shift_i * (a - b)
                           ])
        return values

    '''
    Rather than finding all the roots systematically (i.e. with Mathematica) lets pick some initial values 
    for the solutions and chose the sensible one. The following is found to be flexible enough to find reasonable
    solutions. Other systems may require [i,k,k,l] arrays for i,j,k,l = +/- 1.
    '''

    ns = np.linspace(-5000, 5000, 10)
    ms = np.linspace(-50, 50, 1)
    os = np.linspace(-50, 50, 1)
    ps = np.linspace(-5000, 5000, 20)

    tunneling_prob = 0.0

    for n in ns:
        for m in ms:
            for o in os:
                for p in ps:
                    inital_conditions = np.array([n, m, o, p])
                    a, alpha, b, beta = root(simil_eqns, inital_conditions).x

                    mod_a_sq = a ** 2 + alpha ** 2  # PyCharm complains about alpha and beta being dicts, they're not
                    mod_b_sq = b ** 2 + beta ** 2
                    tunneling_prob = 1 - mod_b_sq / mod_a_sq

                    if tunneling_prob > 0.0:  # Take the value for which P(E) > 0.0. Only gives solutions +/- P
                        # print(tunneling_prob)
                        return tunneling_prob

    return tunneling_prob


def wkb_prob(potential, energy, kemble=False):
    """
    Calculate the tunneling probability using the WKB formula by numerical integration of the barrier
    penetration integral.
    :param kemble: Use the corrected WKB connection formula by Kemble
    :return: the tunneling probability
    """

    try:
        # Try and root find, might fail if there are no roots, in which case assign nan to integral bounds
        x_min = brentq(lambda x: potential.potential(x) - energy, -5.0, 0.0)
        x_max = -x_min

    except ValueError:
        if kemble:
            return 0.5
        else:
            return 1.0

    def integrand(x):
        return np.sqrt(2.0 * potential.mass * np.abs(energy - potential.potential(x)))

    if kemble:
        tunneling_prob = 1.0 / (1.0 + np.exp(2.0 * quad(integrand, x_min, x_max)[0]))
        return tunneling_prob

    else:
        tunneling_prob = np.exp(-2.0 * quad(integrand, x_min, x_max)[0])
        return tunneling_prob


def sctst_prob(energy, theta):
    """
    :param theta: a function for theta, takes the energy as the only variable
    :return: Tunneling probability P(E) using the SCTST expression
    """
    return 1.0 / (1 + np.exp(2.0 * theta(energy).real))


def plot_p_vs_energy_eckart(potential):
    energies = np.linspace(0.00001, 2.0, 500) * potential.v0

    p_vals_vpt0 = np.array([sctst_prob(energy, potential.theta_vpt0) for energy in energies])
    p_vals_vpt2 = np.array([sctst_prob(energy, potential.theta_vpt2) for energy in energies])
    p_vals_vpt2plus = np.array([sctst_prob(energy, potential.theta_vpt2plus) for energy in energies])
    p_vals_vpt2c = np.array([sctst_prob(energy, potential.theta_vpt2c) for energy in energies])
    p_vals_vpt4 = np.array([sctst_prob(energy, potential.theta_vpt4) for energy in energies])
    p_vals_wkb = np.array([wkb_prob(potential, energy, kemble=False) for energy in energies])
    p_vals_exact = np.array([sctst_prob(energy, potential.theta_exact) for energy in energies])
    # p_vals_exact_few = [p_k(energy, potential.theta_exact) for energy in energy_numeric_eckart]
    # plt.scatter(energy_numeric_eckart / potential.v0, p_vals_numeric_eckart / p_vals_exact_few,
    #             s=20.0, facecolors='none', edgecolors='k', label='Numeric')

    plt.plot(energies / potential.v0, p_vals_vpt0 / p_vals_exact, lw=1.0, label='VPT0', c='g')
    plt.plot(energies / potential.v0, p_vals_wkb / p_vals_exact, lw=1.0, label='WKB', c='y')
    plt.plot(energies / potential.v0, p_vals_vpt2 / p_vals_exact, lw=1.0, label='VPT2', c='r')
    plt.plot(energies / potential.v0, p_vals_vpt4 / p_vals_exact, lw=1.0, label='VPT4', c='b')
    plt.plot(energies / potential.v0, p_vals_vpt2plus / p_vals_exact, lw=1.0, label='VPT2+', c='m',
             linestyle='--', dashes=(5, 1))
    plt.plot(energies / potential.v0, p_vals_vpt2c / p_vals_exact, lw=1.0, label='VPT2c',
             linestyle='--', dashes=(5, 5), c='y')
    plt.plot(energies / potential.v0, [1 for x in energies], linestyle='dashed', lw=1.0, c='k', label='Exact')
    plt.ylim(ymin=0.0, ymax=5.0)
    # plt.xlim(xmin=0.0, xmax=0.3)
    plt.xlabel('E / V$_0$')
    plt.ylabel('P(E) / P(E)$_{exact}$')
    plt.tick_params(direction='in', bottom='on', top='on', left='on', right='on')
    plt.legend(prop={'size': 8})

    return plt.show()


def plot_p_vs_energy_jh(potential):
    energies = np.linspace(0.0, 0.01, 500)
    p_vals_exact = np.array([potential.p_exact(energy) for energy in energies])

    plt.plot(energies, p_vals_exact, linestyle='dashed', lw=1.0, c='k', label='Exact')
    plt.ylim(ymin=0.0, ymax=1.0)
    # plt.xlim(xmin=0.0, xmax=1.3)
    plt.xlabel('E')
    plt.ylabel('P(E)')
    plt.tick_params(direction='in', bottom='on', top='on', left='on', right='on')
    # plt.legend(prop={'size': 8})

    return plt.show()


def plot_p_vs_energy_gaussian(potential):
    energies = np.array(energy_numeric_gauss)

    # p_vals_vpt0 = np.array([p_k(energy, potential.theta_vpt0) for energy in energies])
    p_vals_vpt2 = np.array([sctst_prob(energy, potential.theta_vpt2) for energy in energies])
    p_vals_vpt2plus = np.array([sctst_prob(energy, potential.theta_vpt2plus) for energy in energies])
    p_vals_vpt2c = np.array([sctst_prob(energy, potential.theta_vpt2c) for energy in energies])
    p_vals_vpt4 = np.array([sctst_prob(energy, potential.theta_vpt4) for energy in energies])
    p_vals_wkb = np.array([wkb_prob(potential, energy, kemble=True) for energy in energies])

    more_energies = np.linspace(min(energies), max(energies), 1000)
    # smooth_vpt0 = spline(energies, (p_vals_vpt0 / p_vals_numeric_gaussian), more_energies)
    smooth_vpt2 = spline(energies, (p_vals_vpt2 / p_vals_numeric_gaussian), more_energies)
    smooth_vpt2_plus = spline(energies, (p_vals_vpt2plus / p_vals_numeric_gaussian), more_energies)
    smooth_vpt2c = spline(energies, (p_vals_vpt2c / p_vals_numeric_gaussian), more_energies)
    smooth_vpt4 = spline(energies, (p_vals_vpt4 / p_vals_numeric_gaussian), more_energies)
    smooth_wkb = spline(energies, (p_vals_wkb / p_vals_numeric_gaussian), more_energies)

    # plt.plot(more_energies / potential.v0, smooth_vpt0, lw=1.0, label='VPT0', c='g')
    plt.plot(more_energies / potential.v0, smooth_vpt2, lw=1.0, label='VPT2', c='r')
    plt.plot(more_energies / potential.v0, smooth_wkb, lw=1.0, label='WKB', c='y')
    plt.plot(more_energies / potential.v0, smooth_vpt2_plus, lw=1.0, label='VPT2+', c='m',
             linestyle='--', dashes=(5, 1))
    plt.plot(more_energies / potential.v0, smooth_vpt2c, lw=1.0, label='VPT2c',
             linestyle='--', dashes=(5, 5), c='y')
    plt.plot(more_energies / potential.v0, smooth_vpt4, lw=1.0, label='VPT4', c='b')
    # plt.scatter(energies / potential.v0, [1 for x in energies], s=20.0, facecolors='none', edgecolors='k')
    plt.plot(energies / potential.v0, [1 for x in energies], linestyle='dashed', lw=1.0, c='k', label='Numeric')
    plt.ylim(ymin=0.00, ymax=5.0)
    plt.xlim(xmin=0., xmax=0.30)
    plt.xlabel('E / V$_0$')
    plt.ylabel('P(E) / P(E)$_{exact}$')
    plt.tick_params(direction='in', bottom='on', top='on', left='on', right='on')
    plt.legend(prop={'size': 8})

    return plt.show()


def plot_p_vs_energy_asymeckart(potential):
    p_vals_numeric = [calc_prob_from_coeffs(l) for l in coeffs_asym_1amu_1c_neg_v2]

    energies = np.array(energies_asym_1amu_1c_neg_v2)
    # energies = np.linspace(0.00, 0.07, 200)

    p_vals_vpt0 = np.array([sctst_prob(energy, potential.theta_vpt0) for energy in energies])
    p_vals_vpt2 = np.array([sctst_prob(energy, potential.theta_vpt2) for energy in energies])
    p_vals_vpt2corr = np.array([sctst_prob(energy, potential.theta_vpt2plus) for energy in energies])
    p_vals_vpt4 = np.array([sctst_prob(energy, potential.theta_vpt4) for energy in energies])

    more_energies = np.linspace(energies.min(), energies.max(), 500)
    # smooth_vpt0 = spline(energies, (p_vals_vpt0 / p_vals_numeric), more_energies)
    smooth_vpt2 = spline(energies, (p_vals_vpt2 / p_vals_numeric), more_energies)
    smooth_vpt2corr = spline(energies, (p_vals_vpt2corr / p_vals_numeric), more_energies)
    smooth_vpt4 = spline(energies, (p_vals_vpt4 / p_vals_numeric), more_energies)

    def absolute_p(energies):
        # plt.plot(energies, p_vals_vpt0, lw=1.0, label='VPT0', c='g')

        if potential.v2 < 0:
            energies = energies + potential.v2

        plt.plot(energies, p_vals_vpt2, lw=1.0, label='VPT2', c='r')
        plt.plot(energies, p_vals_vpt2corr, lw=1.0, label='VPT2+', c='m',
                 linestyle='--', dashes=(5, 1))
        plt.plot(energies, p_vals_vpt4, lw=1.0, label='VPT4', c='b')
        plt.scatter(energies, p_vals_numeric, s=20.0,
                    facecolors='none', edgecolors='k', label='Numeric')
        # plt.ylim(ymin=0.0, ymax=0.002)
        # plt.xlim(xmin=0.002, xmax=0.006)
        plt.xlabel('E ')
        plt.ylabel('P(E)')
        plt.tick_params(direction='in', bottom='on', top='on', left='on', right='on')
        plt.legend(prop={'size': 8})
        plt.show()

    def rel_p():
        # plt.plot(more_energies, smooth_vpt0, lw=1.0, label='VPT0', c='g')
        plt.plot(more_energies, smooth_vpt2, lw=1.0, label='VPT2', c='r')
        plt.plot(more_energies, smooth_vpt2corr, lw=1.0, label='VPT2+', c='m',
                 linestyle='--', dashes=(5, 1))
        plt.plot(more_energies, smooth_vpt4, lw=1.0, label='VPT4', c='b')
        plt.plot(more_energies, [1 for x in more_energies], linestyle='dashed', lw=1.0, c='k')
        plt.ylim(ymin=0.0, ymax=5.0)
        plt.xlim(xmin=0.000, xmax=0.02)
        plt.xlabel('E')
        plt.ylabel('P(E) / P(E)$_{exact}$')
        plt.tick_params(direction='in', bottom='on', top='on', left='on', right='on')
        plt.legend(prop={'size': 8})
        plt.show()

    def rel_p_nosmooth():
        plt.plot(energies, p_vals_vpt2 / p_vals_numeric, lw=1.0, label='VPT2', c='r')
        plt.plot(energies, p_vals_vpt2corr / p_vals_numeric, lw=1.0, label='VPT2+', c='m',
                 linestyle='--', dashes=(5, 1))
        plt.plot(energies, p_vals_vpt4 / p_vals_numeric, lw=1.0, label='VPT4', c='b')
        plt.plot(energies, [1 for x in energies], linestyle='dashed', lw=1.0, c='k')
        plt.ylim(ymin=0.0, ymax=5.0)
        plt.xlim(xmin=0.000, xmax=0.02)
        plt.xlabel('E')
        plt.ylabel('P(E) / P(E)$_{exact}$')
        plt.tick_params(direction='in', bottom='on', top='on', left='on', right='on')
        plt.legend(prop={'size': 8})
        plt.show()

    absolute_p(energies)
    # rel_p_nosmooth()

    return 0


if __name__ == '__main__':
    H_H2_eckart = Eckart(mass=1061, v0=0.0156, a=0.734)
    plot_p_vs_energy_eckart(H_H2_eckart)

    # gaussian_1 = Gaussian(mass=1823, v0=0.0156, sigma=1.6562)
    # plot_p_vs_energy_gaussian(gaussian_1)

    # asym_eckart = AsymmetricEckart(mass=1000, v1=0.01, a=0.764, v2=0.005, c=2.0)
    # plot_p_vs_energy_asymeckart(asym_eckart)

    # jh_eckart = JHEckart(mass=1000, dv1=0.005, dv2=0.006, L=4.0)
    # plot_p_vs_energy_jh(jh_eckart)

    # H_CH4_asym_eckart = AsymmetricEckart(mass=(1.1417 * 1822.8885), v1=0.03116, a=0.9, v2=-0.0108, c=3.75)
    # asym_eckart = AsymmetricEckart(mass=1823, v1=0.0156, a=0.764, v2=-0.005, c=1.0)
    # asym_eckart_pos = AsymmetricEckart(mass=1823, v1=0.0156, a=0.764, v2=0.005, c=1.0)

    # plot_p_vs_energy_asymeckart(asym_eckart)
    exit()

    print(sctst_prob(0.00468, asym_eckart.theta_vpt2))
    print(sctst_prob(0.00468, asym_eckart.theta_vpt2plus))
    print(sctst_prob(0.00468, asym_eckart.theta_vpt4))
    print(calc_prob_from_coeffs([2203.490388, 423.896309, -0.7717633639, -0.7717645507]))  # Boost 5E9 -100
    print('\n')

    print(sctst_prob(0.00468 + 0.005, asym_eckart_pos.theta_vpt2))
    print(sctst_prob(0.00468 + 0.005, asym_eckart_pos.theta_vpt2plus))
    print(sctst_prob(0.00468 + 0.005, asym_eckart_pos.theta_vpt4))
    print(calc_prob_from_coeffs([15.99630527, 171.536708, -0.6286280682, -0.6278969886]))

    exit()

    x_tmp = np.linspace(-10, 10, 400)
    plt.plot(x_tmp, [asym_eckart.potential(x) for x in x_tmp], label='-0.005')
    plt.plot(x_tmp, [asym_eckart_pos.potential(x) for x in x_tmp], label='+0.005')
    plt.show()
