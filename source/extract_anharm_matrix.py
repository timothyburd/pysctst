import unittest

import numpy as np

import physical_constants as pc


def convert_to_float(number_string):
    """
    Convert a string in the format of a gaussian log file (-0.461848D+02) to a float
    """

    if number_string[0] == '-':
        first_bit = float(number_string[:9])
        exponant = float(number_string[-3:])
    else:
        first_bit = float(number_string[:8])
        exponant = float(number_string[-3:])

    return first_bit * 10 ** exponant


# def extract_derrivatives_matrix(filename):
#     f = open(filename, "r")
#
#
#     read = False
#
#     for i, line in enumerate(f):
#         if read:
#             pass
#     pass


def extract_matrix(filename, dof, fourths_only=False, thirds_only=False):
    """
    Extract the 'Total Anharmonic X Matrix' from a Gaussian09 .log file

    fourths_only and thirds_only allow for extraction of the fourth and third order derivative contributions to the matrix
    """
    x = np.zeros((dof, dof))  # x matrix wil be dof x dof, where dof is the total number of degrees of freedom

    # --------------------------------- READ THE LINES OF THE OUTPUT FILE ---------------------------------------------

    if fourths_only:
        key_line = '4th Deriv. contributions to X Matrix'
    elif thirds_only:
        key_line = '3rd Deriv. contributions to X Matrix'
    else:
        key_line = 'Total Anharmonic X Matrix (in cm^-1)'
    log_file_lines = []
    read = False
    with open(filename, "r") as log_file:
        for line in log_file:

            if len(line.strip()) == 0:  # If the line is blank stop reading
                read = False

            if read:
                log_file_lines.append(line)

            if key_line in line:
                log_file.readline()  # Skip the '-----------------------' line
                log_file.readline()  # Skip the      '1, 2, 3, 4, 5'      line
                read = True

    # ------------------------- PROCESS TO GET LISTS [block_number, index, values...] ---------------------------------
    block_lines = []
    block_number = 0

    for line in log_file_lines:
        try:  # Try and convert all the numbers in the line to integers
            [int(number) for number in line.split()]
            block_number += 1  # If all the numbers are integers a new block is started
        except ValueError:
            block_lines.append([block_number] + line.split())

    # ----------------------------- PROCESS TO A LIST OF LOWER LEFT MATRIX ROWS ---------------------------------

    lower_left_x_rows = []

    for line_a in block_lines:
        if int(line_a[1]) <= 5 and line_a[0] == 0:  # If the atom is .le. 5 and in the first block the line
            lower_left_x_rows.append(line_a[1:])  # does not extend over more than one block, append it

        extended_line = []
        for line_b in block_lines:  # Iterate over all the lines again checking for same atom no.
            # If the blocks are different and the atom number is the same, and we haven't appended it already then..
            if line_a[0] != line_b[0] and line_a[1] == line_b[1] and line_a[0] == 0:
                extended_line += line_b[2:]

        if len(extended_line) > 0:  # If the extended_line list has some elements
            lower_left_x_rows.append(line_a[1:] + extended_line)

    # ---------------------------------- POPULATE THE x MATRIX ----------------------------------------

    for row in lower_left_x_rows:
        atom_index = int(row[0]) - 1
        for j in range(len(row) - 1):
            value = float(row[j + 1].replace('D', 'E'))
            x[atom_index][j], x[j][atom_index] = value, value

    return x / pc.hartree_to_cm


def oldextract_matrix(filename, dof):
    matrix_size = dof
    f = open(filename, "r")
    matrix = False
    x = np.zeros((matrix_size, matrix_size))
    tmp = []
    for i, line in enumerate(f):
        if " Total Anharmonic X Matrix (in cm^-1)" in line:
            matrix = True
        if "Deperturbed" in line or 'Anharmonic Zero Point Energy' in line:
            matrix = False
        if "==" in line:
            matrix = False
        if matrix:
            if "Total" in line:
                pass
            if "----" in line:
                pass
            else:
                tmp.append(line.split())

    tmp.pop(0)
    tmp.pop(-1)

    for i in range(len(tmp)):
        if len(tmp[i]) > 0:
            tmp[i].pop(0)

    block1 = []
    block2 = []
    block3 = []
    block4 = []
    block5 = []
    block6 = []
    block7 = []

    tmp[:] = [item for item in tmp if item != '']

    tmp = [item for item in tmp if len(item) > 0]
    block = 0
    for i in range(len(tmp)):
        if tmp[i][0][0] in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            block = block + 1

        if block == 0:
            pass
        if block == 1:
            block1.append(tmp[i])
        if block == 2:
            block2.append(tmp[i])
        if block == 3:
            block3.append(tmp[i])
        if block == 4:
            block4.append(tmp[i])
        if block == 5:
            block5.append(tmp[i])
        if block == 6:
            block6.append(tmp[i])
        if block == 7:
            block7.append(tmp[i])

    block1.pop(0)
    for i in range(len(block1)):
        for j in range(len(block1[i])):
            x[i][j] = convert_to_float(block1[i][j])

    if len(block2) > 0:
        block2.pop(0)
        for i in range(len(block2)):
            for j in range(len(block2[i])):
                x[i + 5][j + len(block1[-1])] = convert_to_float(block2[i][j])
    if len(block3) > 0:
        block3.pop(0)
        for i in range(len(block3)):
            for j in range(len(block3[i])):
                x[i + 10][j + len(block2[-1]) + len(block1[-1])] = convert_to_float(block3[i][j])
    if len(block4) > 0:
        block4.pop(0)
        for i in range(len(block4)):
            for j in range(len(block4[i])):
                x[i + 15][j + len(block3[-1]) + len(block2[-1]) + len(block1[-1])] = convert_to_float(block4[i][j])
    if len(block5) > 0:
        block5.pop(0)
        for i in range(len(block5)):
            for j in range(len(block5[i])):
                x[i + 20][j + len(block4[-1]) + len(block3[-1]) + len(block2[-1]) + len(block1[-1])] = convert_to_float(block5[i][j])
    if len(block6) > 0:
        block6.pop(0)
        for i in range(len(block6)):
            for j in range(len(block6[i])):
                x[i + 25][j + len(block5[-1]) + len(block4[-1]) + len(block3[-1]) + len(block2[-1]) + len(block1[-1])] = convert_to_float(block6[i][j])

    for i in range(matrix_size):
        for j in range(i + 1, matrix_size):
            x[i][j] = x[j][i]
    x = x / 219474.63
    return x


