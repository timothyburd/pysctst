"""
Functions to calculate the one-dimensional semiclassical penetration integral, theta
"""

kb = 1.38064852 * (10 ** (-23))
hbar = 1.0545718 * (10 ** (-34))
h = 6.62607004 * (10 ** (-34))
au_to_kg = 1.66053892173 * (10 ** (-27))  # kg/au
speed_of_light = 2.99792 * (10 ** 10)  # ish
angstrom_to_m = 10 ** (-10)
hartree_to_joule = 4.35974 * 10 ** (-18)
avogadro = 6.022140857 * (10 ** 23)
bohr_to_m = 5.2917721067 * 10 ** (-11)

import cmath
import math

import numpy as np

import physical_constants as pc


def aDiff(numD, s):
    """
    Numerically differentiate using Richardson Extrappolation
    """
    outM = np.zeros((3, 3))

    for k in [0, 1, 2]:
        outM[0][k] = numD[2 - k]
        fac = 1
        for m in [1, 2]:
            if m <= k:
                fac = fac * s * s
                outM[m][k] = (outM[m - 1][k] * fac - outM[m - 1][k - 1]) / (fac - 1)

    return outM


def aDiff_matricies(numD, s):
    """
    Perform richardson exrapolation using matricies rather than just energy values
    """
    ns = len(numD)  # MIGHT only work for ns = 2...
    outM = np.zeros((ns, ns))

    for k in range(ns):
        outM[0][k] = numD[ns - 1 - k]
        fac = 1
        for m in [1, ns]:
            if m <= k:
                fac = fac * s * s

                outM[m][k] = (outM[m - 1][k] * fac - outM[m - 1][k - 1]) / (fac - 1)

    return outM


def anhCalc1D(energy_list, dQ, freq):
    """
    Perform richardson extrapolation for the reaction mode coordinate
    :param energy_list: list of MP2 energies along the reaction mode coordinate
    :param dQ: "step size" in Richardson extrapolation
    :param freq: imaginary frequency
    :return: xFF (Anharmonic constant along reaction mode coordinate)
    """
    freq = freq / pc.hartree_to_cm  # convert cm-1 to hartrees
    # convert to from normal mode coordinates to unites of (length mass*0.5) (4.19 of sam's thesis)
    # dq = dQ * (hbar / freq)**(0.5)
    dq = dQ * math.sqrt(pc.dalton_to_electronmass)  # conversion constant (dalton/electron mass -> 1 )**0.5

    # phi = [i - energy_list[4] for i in energy_list]
    phi = energy_list

    fxxx = []
    fxxx.append((phi[6] - 2 * phi[5] + 2 * phi[3] - phi[2]) / (2 * (dq ** 3)))
    fxxx.append((phi[7] - 2 * phi[6] + 2 * phi[2] - phi[1]) / (2 * ((2 * dq) ** 3)))
    fxxx.append((phi[8] - 2 * phi[7] + 2 * phi[1] - phi[0]) / (2 * ((4 * dq) ** 3)))

    fxxxx = []
    fxxxx.append((phi[6] - 4 * phi[5] + 6 * phi[4] - 4 * phi[3] + phi[2]) / (dq ** 4))
    fxxxx.append((phi[7] - 4 * phi[6] + 6 * phi[4] - 4 * phi[2] + phi[1]) / (((2 * dq) ** 4)))
    fxxxx.append((phi[8] - 4 * phi[7] + 6 * phi[4] - 4 * phi[1] + phi[0]) / (((4 * dq) ** 4)))

    richard_the_third = aDiff(fxxx, 2)[2][2]  # Select the bottom right hand corner componant
    richard_the_fourth = aDiff(fxxxx, 2)[2][2]  # Select the bottom right hand corner componant

    # NOTE this equation has dfferenct minus signs to thos in the papers/disseratitons. I think this is correct due to
    # the fact that here 'freq' is the MODULUS of the reaction mode frequency, whereas int he formulae in the papers it
    # is the imaginary frequency, and so anytime freq is squared, an extra minus sign appears!
    # J. Chem. Phys. 144, 244116 (2016) eq (23)

    xff = (-1 / (16 * freq * freq)) * (richard_the_fourth + (5 * richard_the_third * richard_the_third / (3 * freq * freq)))
    g0 = (richard_the_fourth / (-64 * freq * freq)) - ((5 / 576) * richard_the_third * richard_the_third / (freq ** 4))
    return [xff, g0, richard_the_third, richard_the_fourth]


def anhCalc1D_stable(energy_list, dQ, freq):
    """
    Performs richardson extrapolation for ANY STABLE mode coordinate
    :param energy_list: list of MP2 energies along the reaction mode coordinate
    :param dQ: "step size" in Richardson extrapolation
    :param freq: real frequency
    :return: xFF (Anharmonic constant along reaction mode coordinate)
    """
    freq = freq / pc.hartree_to_cm  # convert cm-1 to hartrees
    # convert to from normal mode coordinates to unites of (length mass*0.5) (4.19 of sam's thesis)
    # dq = dQ * (hbar / freq)**(0.5)
    dq = dQ * math.sqrt(pc.dalton_to_electronmass)  # conversion constant from xiaos code. (dalton/electron mass -> 1 )**0.5

    # phi = [i - energy_list[4] for i in energy_list]
    phi = energy_list

    fxxx = []
    fxxx.append((phi[6] - 2 * phi[5] + 2 * phi[3] - phi[2]) / (2 * (dq ** 3)))
    fxxx.append((phi[7] - 2 * phi[6] + 2 * phi[2] - phi[1]) / (2 * ((2 * dq) ** 3)))
    fxxx.append((phi[8] - 2 * phi[7] + 2 * phi[1] - phi[0]) / (2 * ((4 * dq) ** 3)))

    fxxxx = []
    fxxxx.append((phi[6] - 4 * phi[5] + 6 * phi[4] - 4 * phi[3] + phi[2]) / (dq ** 4))
    fxxxx.append((phi[7] - 4 * phi[6] + 6 * phi[4] - 4 * phi[2] + phi[1]) / (((2 * dq) ** 4)))
    fxxxx.append((phi[8] - 4 * phi[7] + 6 * phi[4] - 4 * phi[1] + phi[0]) / (((4 * dq) ** 4)))

    richard_the_third = aDiff(fxxx, 2)[2][2]  # Select the bottom right hand corner componant
    richard_the_fourth = aDiff(fxxxx, 2)[2][2]  # Select the bottom right hand corner componant

    # J. Chem. Phys. 144, 244116 (2016) eq (23)

    xff = (1 / (16 * freq * freq)) * (richard_the_fourth - (5 * richard_the_third * richard_the_third / (3 * freq * freq)))
    g0 = (richard_the_fourth / (64 * freq * freq)) - ((5 / 576) * richard_the_third * richard_the_third / (freq ** 4))
    return [xff, g0, richard_the_third, richard_the_fourth]


def anharmonic_barrier(r1, r2, ts):
    """
    Calculates the barrier including zpes AND the VPT2 correction G0 to the barrier
    :return: barrier height IN JOULES
    """
    dvf = ((ts.energy - r1.energy - r2.energy + ts.g0) * hartree_to_joule) + (ts.zpe - r1.zpe - r2.zpe)

    return dvf


def unimolecular_anharmonic_barrier(r1, ts):
    """
    Calculates the barrier including zpes AND the VPT2 correction G0 to the barrier
    :return: barrier height IN JOULES
    """
    dvf = ((ts.energy - r1.energy + ts.g0) * hartree_to_joule) + (ts.zpe - r1.zpe)
    return dvf


def BoundBar_composite(freq, bound_freq, xff, bar_height, rev_bar_height, s, V_1D_yx=0, reversed_flag=False, minimum=False):
    """
    Calculates the three piece potential for a DOUND ("double-well-ish") potentail
    :param freq: imaginary freq
    :param xff:
    :param bar_height: in joules
    :param rev_bar_height: in joules
    :param s: coordinate along potential (-100 -> 100) at which the potential should be evaluated
    """

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    if rev_bar_height != bar_height:
        raise ValueError("MUST BE SYMMETRIC REACTION")

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))  # units of freq*freq / energy
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5  # units of energy**0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * bar_height)) ** 0.5  # units of energy**0.5

    v1 = bar_height

    # new and improved (correct) coefficients
    a = Db - v1
    b = -2 * Db + v1
    c = Db + v1 * (alphab / alphar) ** 2
    polynomial_coeficients = [c, b, a]
    roots = np.roots(polynomial_coeficients)
    # print("Roots", roots)
    if minimum:
        A2 = min(roots.real)
    else:
        A2 = max(roots.real)
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    if cosh < 1:
        argument = 0
    else:
        argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # corss over point

    # once we know yx we can find yp
    tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    if cosh < 1:
        print("WARNING: Plot cosh < 1")
        if V_1D_yx == 0:
            raise ValueError("Cannot match the potentials! Cosh = ", cosh,
                             " and 1D potential value not given. Cannot perform correction!")
        print("Start adaption to 1D potential")
        yx = (2 / alphab) * np.arccosh(np.sqrt((Db) / (V_1D_yx - v1 + Db)))
        tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
        sech = np.sqrt(tmp)
        cosh = 1 / sech
        print("New cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = ((alphar * yx + 2 * argument) / alphar)
    yp = -yr

    # plot the potentials
    y = np.linspace(-200, 200, num=500)
    v_b = v1 - Db + Db * np.cosh(y * alphab / 2) ** -2
    v_r = + v1 - v1 * np.cosh((y - yr) * alphar / 2) ** -2
    v_p = + v1 - v1 * np.cosh((y - yp) * alphar / 2) ** -2

    Vp_s = + v1 - v1 * np.cosh((s - yr) * alphar / 2) ** -2

    Vr_s = + v1 - v1 * np.cosh((s - yp) * alphar / 2) ** -2

    Vb_s = v1 - Db + Db * np.cosh(s * alphab / 2) ** -2

    return [Vr_s, Vb_s, Vp_s, -yx, yx]


def BoundBar_composite_f(freq, bound_freq, xff, bar_height, rev_bar_height, s, V_1D_yx=0, reversed_flag=False, root=1, f=1):
    """
    Calculates the three piece potential for a DOUND ("double-well-ish") potentail
    :param freq: imaginary freq
    :param xff:
    :param bar_height: in joules
    :param rev_bar_height: in joules
    :param s: coordinate along potential (-100 -> 100) at which the potential should be evaluated
    """

    # f = 2

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    if rev_bar_height != bar_height:
        raise ValueError("MUST BE SYMMETRIC REACTION")

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))  # units of freq*freq / energy
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5  # units of energy**0.5
    v1 = bar_height
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * f * v1)) ** 0.5  # units of energy**0.5

    # new and improved (correct) coefficients
    a = (v1 - Db) + (f - 1) * (2 * v1) / (Db) * (v1 - Db) + (f - 1) ** 2 * ((v1 ** 3) / (Db ** 2) - (v1 ** 2) / (Db))
    b = 3 * Db - 2 * v1 + (f - 1) * 2 * v1 * (2 * Db - v1) / Db + (f - 1) ** 2 * v1 ** 2 / Db
    c = -3 * Db + v1 - f * v1 * ((alphab) / (alphar)) ** 2 - (f - 1) * 2 * v1
    d = Db + f * v1 * ((alphab) / (alphar)) ** 2

    polynomial_coeficients = [d, c, b, a]
    roots = np.roots(polynomial_coeficients)
    # print("Roots f", roots)

    real_roots = []
    for i in roots:
        if i.imag == 0 and i.real > 0:
            real_roots.append(i.real)
    # print("Real Roots: ", real_roots)

    if len(real_roots) == 3:
        if root == 3:
            A2 = min(real_roots)
        elif root == 2:
            A2 = real_roots[1]
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 2:
        if root == 3:
            raise ValueError("You selected root 3, but there are only 2 real and positive roots")
        elif root == 2:
            A2 = min(real_roots)
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 1:
        if root == 3:
            raise ValueError("You selected root 3, but there is only one real and positive root")
        elif root == 2:
            raise ValueError("You selected root 2, but there is only one real and positive root")
        elif root == 1:
            A2 = max(real_roots)

    # A2 = roots[1]
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    if cosh < 1:
        argument = 0
    else:
        argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # corss over point

    # once we know yx we can find yp
    tmp = (f - 1) / f + (Db * (1 - A2)) / (f * v1)
    if tmp < 0:
        raise ValueError("Cannot match the potentials! tmp = ", tmp)
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    if cosh < 1:
        print("WARNING: Plot cosh < 1")
        # if V_1D_yx == 0:
        raise ValueError("Cannot match the potentials! Cosh = ", cosh)
    argument = np.arccosh(cosh)

    yr = ((alphar * yx + 2 * argument) / alphar)
    yp = -yr

    # plot the potentials
    y = np.linspace(-200, 200, num=500)
    v_b = v1 - Db + Db * np.cosh(y * alphab / 2) ** -2
    v_r = + v1 - v1 * np.cosh((y - yr) * alphar / 2) ** -2
    v_p = + v1 - v1 * np.cosh((y - yp) * alphar / 2) ** -2

    Vp_s = + f * v1 - f * v1 * np.cosh((s - yr) * alphar / 2) ** -2

    Vr_s = + f * v1 - f * v1 * np.cosh((s - yp) * alphar / 2) ** -2

    Vb_s = v1 - Db + Db * np.cosh(s * alphab / 2) ** -2

    return [Vr_s, Vb_s, Vp_s, -yx, yx]


def BoundBar(freq, bound_freq, xff, bar_height, rev_bar_height, s, V_1D_yx=0, minimum=False):
    """
    Calculates the three piece potential for a DOUND ("double-well-ish") potentail
    :param freq: imaginary freq
    :param xff:
    :param bar_height: in joules
    :param rev_bar_height: in joules
    :param s: coordinate along potential (-100 -> 100) at which the potential should be evaluated
    :return:
    """

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    if rev_bar_height != bar_height:
        raise ValueError("MUST BE SYMMETRIC REACTION")

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))  # units of freq*freq / energy
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5  # units of energy**0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * bar_height)) ** 0.5  # units of energy**0.5
    v1 = bar_height

    # if v1 < 0.5 * Db:
    #     raise ValueError("BArrier issues")

    # Old, incorrect quadratic coefficients
    # a = -Db - v1
    # b = 2 * Db + v1
    # c = Db + v1 * (alphab / alphar) ** 2

    # new and improved (correct) coefficients
    a = Db - v1
    b = -2 * Db + v1
    c = Db + v1 * (alphab / alphar) ** 2

    polynomial_coeficients = [c, b, a]
    roots = np.roots(polynomial_coeficients)
    # print("Roots", roots)
    if minimum:
        A2 = min(roots.real)
    else:
        A2 = max(roots.real)
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point

    # once we know yx we can find yp
    tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    if cosh < 1:
        print("WARNING: cosh < 1")
        if V_1D_yx == 0:
            raise ValueError("Cannot match the potentials! Cosh = ", cosh, " and 1D potential value not given. Cannot perform correction!")
        print("Start adaption to 1D potential")
        yx = (2 / alphab) * np.arccosh(np.sqrt((Db) / (V_1D_yx - v1 + Db)))
        tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
        sech = np.sqrt(tmp)
        cosh = 1 / sech
        print("New cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = ((alphar * yx + 2 * argument) / alphar)
    yp = -yr

    Vp_s = + v1 - v1 * np.cosh((s - yr) * alphar / 2) ** -2

    Vr_s = + v1 - v1 * np.cosh((s - yp) * alphar / 2) ** -2

    Vb_s = v1 - Db + Db * np.cosh(s * alphab / 2) ** -2

    if s <= -yx:
        return Vr_s
    if (s > -yx) and (s < yx):
        return Vb_s
    if s >= yx:
        return Vp_s
    else:
        return 0  # Probably lots of NANs here due to a sqrt of a negative being used or somethign
        # raise Exception("Strange error here!")


def BoundBar_f(freq, bound_freq, xff, bar_height, rev_bar_height, s, V_1D_yx=0, root=1, f=1):
    """
    Calculates the three piece potential for a DOUND ("double-well-ish") potentail
    :param freq: imaginary freq
    :param xff:
    :param bar_height: in joules
    :param rev_bar_height: in joules
    :param s: coordinate along potential (-100 -> 100) at which the potential should be evaluated
    :return:
    """
    # f = 2
    # print("BoundBar_f f =", f)

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    if rev_bar_height != bar_height:
        raise ValueError("MUST BE SYMMETRIC REACTION")

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))  # units of freq*freq / energy
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5  # units of energy**0.5
    # maybe changes in here as well
    v1 = bar_height
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * f * v1)) ** 0.5  # units of energy**0.5
    # cause this changes! we scale V1!

    # new and improved (correct) coefficients
    a = (v1 - Db) + (f - 1) * (2 * v1) / (Db) * (v1 - Db) + (f - 1) ** 2 * ((v1 ** 3) / (Db ** 2) - (v1 ** 2) / (Db))
    b = 3 * Db - 2 * v1 + (f - 1) * 2 * v1 * (2 * Db - v1) / Db + (f - 1) ** 2 * v1 ** 2 / Db
    c = -3 * Db + v1 - f * v1 * ((alphab) / (alphar)) ** 2 - (f - 1) * 2 * v1
    d = Db + f * v1 * ((alphab) / (alphar)) ** 2

    polynomial_coeficients = [d, c, b, a]
    roots = np.roots(polynomial_coeficients)
    # print("Roots f", roots)
    # print(max(roots.real))

    # #Compare to old version wth f=1
    # a = Db - v1
    # b = -2 * Db + v1
    # c = Db + v1 * (alphab / alphar) ** 2
    #
    # polynomial_coeficients = [c, b, a]
    # roots = np.roots(polynomial_coeficients)
    # print("Roots old ", roots)
    # print(max(roots.real))

    real_roots = []
    for i in roots:
        if i.imag == 0 and i.real > 0:
            real_roots.append(i.real)
    # print("Real Roots: ", real_roots)

    if len(real_roots) == 3:
        if root == 3:
            A2 = min(real_roots)
        elif root == 2:
            A2 = real_roots[1]
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 2:
        if root == 3:
            raise ValueError("You selected root 3, but there are only 2 real and positive roots")
        elif root == 2:
            A2 = min(real_roots)
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 1:
        if root == 3:
            raise ValueError("You selected root 3, but there is only one real and positive root")
        elif root == 2:
            raise ValueError("You selected root 2, but there is only one real and positive root")
        elif root == 1:
            A2 = max(real_roots)

    # A2 = roots[1]
    # A2 = max(real_roots)
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)

    cosh = 1 / A
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point
    # correct until here

    # once we know yx we can find yp
    tmp = (f - 1) / f + (Db * (1 - A2)) / (f * v1)
    if tmp < 0:
        raise ValueError("Cannot match the potentials! tmp = ", tmp)
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    if cosh < 1:
        print("WARNING: cosh < 1")
        # if V_1D_yx == 0:
        raise ValueError("Cannot match the potentials! Cosh = ", cosh)
        # print("Start adaption to 1D potential")
        # yx = (2/alphab) * np.arccosh(np.sqrt(  (Db)/( V_1D_yx - v1 + Db )  ))
        # tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
        # sech = np.sqrt(tmp)
        # cosh = 1 / sech
        # print("New cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = ((alphar * yx + 2 * argument) / alphar)

    yp = -yr

    Vp_s = + f * v1 - f * v1 * np.cosh((s - yr) * alphar / 2) ** -2

    Vr_s = + f * v1 - f * v1 * np.cosh((s - yp) * alphar / 2) ** -2

    Vb_s = v1 - Db + Db * np.cosh(s * alphab / 2) ** -2

    if s <= -yx:
        return Vr_s
    if (s > -yx) and (s < yx):
        return Vb_s
    if s >= yx:
        return Vp_s
    else:
        return 0  # Probably lots of NANs here due to a sqrt of a negative being used or somethign
        # raise Exception("Strange error here!")


def get_roots_f(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False, minimum=1, f=1):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    THEN REUTNRS HTE CONTRIBUTIONS FROM THE CENTRAL AND EDGE BARRIER PARTS SEPERATLY, SO YOU CAN INSPECT THEM!
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    # f = 1.05

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    en = energy
    delVf = bar_height

    v1 = delVf

    rho = (rev_bar_height / bar_height) ** 0.5  # This should be one for a symmetric potential (whici everything should be!)
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:  # issues with the vpt2 expansion at the TS giving a negative effective barrier
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * f * bar_height)) ** 0.5  # units of energy**0.5
    if (Db / delVf) < 0:  # issues with the electronic structure calculations giving a negative barrier height
        return 0

    # new and improved (correct) coefficients
    a = (v1 - Db) + (f - 1) * (2 * v1) / (Db) * (v1 - Db) + (f - 1) ** 2 * ((v1 ** 3) / (Db ** 2) - (v1 ** 2) / (Db))
    b = 3 * Db - 2 * v1 + (f - 1) * 2 * v1 * (2 * Db - v1) / Db + (f - 1) ** 2 * v1 ** 2 / Db
    c = -3 * Db + v1 - f * v1 * ((alphab) / (alphar)) ** 2 - (f - 1) * 2 * v1
    d = Db + f * v1 * ((alphab) / (alphar)) ** 2

    polynomial_coeficients = [d, c, b, a]
    roots = np.roots(polynomial_coeficients)

    real_roots = []
    for i in roots:
        if i.imag == 0 and i.real > 0:
            real_roots.append(i.real)

    return roots, real_roots


def get_V_yx_1D(freq, bound_freq, xff, bar_height, rev_bar_height):
    """
    Get the 1D potential value at yx of 1D
    :param freq: imaginary freq
    :param xff:
    :param bar_height: in joules
    :param rev_bar_height: in joules
    :return:
    """

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    if rev_bar_height != bar_height:
        raise ValueError("MUST BE SYMMETRIC REACTION")

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))  # units of freq*freq / energy
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5  # units of energy**0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * bar_height)) ** 0.5  # units of energy**0.5
    v1 = bar_height

    # if v1 < 0.5 * Db:
    #     raise ValueError("BArrier issues")

    # Old, incorrect quadratic coefficients
    # a = -Db - v1
    # b = 2 * Db + v1
    # c = Db + v1 * (alphab / alphar) ** 2

    # new and improved (correct) coefficients
    a = Db - v1
    b = -2 * Db + v1
    c = Db + v1 * (alphab / alphar) ** 2

    polynomial_coeficients = [c, b, a]
    roots = np.roots(polynomial_coeficients)
    A2 = max(roots.real)
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    # print("Plot cosh 1: ", cosh)
    # if cosh < 1:
    #     raise ValueError("Cannot match the potentials! Cosh = ", cosh)
    #     #print("WARNING: Plot cosh 1 < 1")
    #     argument = 0
    # else:
    #     argument = np.arccosh(cosh)
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point
    # if s == -100:
    # print("y_x = ", yx)

    # if s == -100:
    #    print( (Db/v1) - (Db/v1) * np.arccosh((s * alphab)/(2))**2)

    # once we know yx we can find yp
    tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
    # print("tmp: ", tmp)
    sech = np.sqrt(tmp)
    # print("sech: ", sech)
    cosh = 1 / sech
    # print("cosh: ", cosh)
    # print("Plot cosh 2: ", cosh)
    if cosh < 1:
        raise ValueError("Cannot match the potentials! Cosh = ", cosh)
        # print("WARNING: Plot cosh < 1")
        # print("cosh is set to 1 and yx is re-evaluated")
        # yx = (2/alphab) * np.arccosh(np.sqrt(  (Db)/(Db-v1)  ))
        # print("New yx: ", yx)
        # print("-----Start test for correct yx------")
        # tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -v1
        # print("tmp: ", tmp)
        # sech = np.sqrt(tmp)
        # print("sech: ", sech)
        # cosh = 1 / sech
        # print("cosh: ", cosh)
        # print("-------End test for correct yx-------")
        # cosh = 1
        # yr =  82.8506880383455
    argument = np.arccosh(cosh)

    yr = ((alphar * yx + 2 * argument) / alphar)
    yp = -yr

    Vb = v1 - Db + Db * np.cosh(yx * alphab / 2) ** -2

    return Vb


def WagBar(freq, xff, bar_height, rev_bar_height, s, reversed_flag=False):
    """
    Calculates the three piece assymetric eckart potential
    :param freq: imaginary freq
    :param xff: 
    :param bar_height: in joules
    :param rev_bar_height: in joules
    :param s: coordinate along potential (-100 -> 100) at which the potential should be evaluated
    :return: 
    """

    infreq = freq
    inxff = xff
    inbar = bar_height
    inrev = rev_bar_height
    ins = s

    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))  # units of freq*freq / energy
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5  # units of energy**0.5

    f = (Db / bar_height) ** 0.5
    ur = (((rho * rho + rho + 1) ** 0.5) - (rho - 1)) / max(3 * f, 3)
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (f - rho * ((1 + f) ** 2) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (1 - rho) * f * f * f * ur * ur * ur + 105 * f * f * f * f * ur * ur * ur * ur)

    alphar = alphab * f * Rr
    umr1 = (((1 - rho) * (1 - rho) * (1 - Rr * f) * (1 - Rr * f) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) ** 0.5 + (1 - rho) * (1 - Rr * f)) / (2 * (1 - Rr * f * f))
    umr2 = (-1) * umr1

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab

    # --------------------- DEEP TUNELING ISSUES ------------------------------------------------------------------------------------------------------------------------------------------
    # The deep tunneling corrections have an issue, which is that for low barriers with high frequencies, the matching crieteria cannot be met, and you get a math error because it tries to log a
    # negative number!
    # It turns out that, in this situation, the reverse barrier can be constructed normally (e.g. by swapping the forward and reverse barriers)
    # The solution is thus simple. If this is an issue, we reverse the barrier, and calcualte the tunneling integrals as before
    # we then have to shift the energy by the difference in barrier heights.
    # NOTE this works because the tunneling integral is independent of whicih direction you are going!
    # this is done in the following line, where the test for an error is followed by calling the same function, with reversed barriers and a shifted energy

    # if 1 - f * umr <=0 :
    # if reversed_flag == False:
    #     return WagBar(infreq, inxff, inrev, inbar, -s, reversed_flag=True) - inrev/pc.hartree_to_joule + inbar/pc.hartree_to_joule

    dr = -ybr + math.log(((1 - f * umr) / (rho + f * umr))) / alphar

    ###################
    up = (((rho ** 2 + rho + 1) ** 0.5) + rho - 1) / max(3 * f, 3)
    Rp = (210 * rho * rho - 168 * (1 + f) * rho * (1 - rho) * up + 140 * (f - (1 + f) ** 2 * rho + f * rho ** 2) * up * up + 120 * f * (1 + f) * (1 - rho) * up ** 3 + 105 * f ** 2 * up ** 4) / (
                210 * rho ** 2 - 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho ** 2) * f ** 2 * up ** 2 + 240 * (1 - rho) * f ** 3 * up ** 3 + 105 * f ** 4 * up ** 4)

    alphap = alphab * f * Rp

    ump1 = (((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    ump2 = (-((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1

    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab

    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp

    Vr_s = bar_height * ((1 - rho ** 2) * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) +
                         (1 + rho) ** 2 * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) ** 2)

    Vp_s = bar_height * ((1 - rho ** 2) * math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) + (1 + rho) ** 2 *
                         math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) ** 2)

    Vb_s = bar_height - Db + Db * ((1 - rho ** 2) * math.exp(alphab * s) / (1 + math.exp(alphab * s)) + (1 + rho) ** 2 *
                                   math.exp(alphab * s) / (1 + math.exp(alphab * s)) ** 2)
    if s <= ybr:
        return Vr_s
    if (s > ybr) and (s < ybp):
        return Vb_s
    if s >= ybp:
        return Vp_s
    else:
        return 0  # Probably lots of NANs here due to a sqrt of a negative being used or somethign
        # raise Exception("Strange error here!")


def WagBar_composite(freq, xff, bar_height, rev_bar_height, s, reversed_flag=False):
    """
    Calculates the three piece assymetric eckart potential
    :param freq: imaginary freq
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param s: coordinate along potential (-100 -> 100) at which the potential should be evaluated
    :return:
    """

    infreq = freq
    inxff = xff
    inbar = bar_height
    inrev = rev_bar_height
    ins = s

    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5

    f = (Db / bar_height) ** 0.5
    ur = (((rho * rho + rho + 1) ** 0.5) - (rho - 1)) / max(3 * f, 3)
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (f - rho * ((1 + f) ** 2) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (1 - rho) * f * f * f * ur * ur * ur + 105 * f * f * f * f * ur * ur * ur * ur)

    alphar = alphab * f * Rr
    umr1 = (((1 - rho) * (1 - rho) * (1 - Rr * f) * (1 - Rr * f) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) ** 0.5 + (1 - rho) * (1 - Rr * f)) / (2 * (1 - Rr * f * f))
    umr2 = (-1) * umr1

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab

    # --------------------- DEEP TUNELING ISSUES ------------------------------------------------------------------------------------------------------------------------------------------
    # The deep tunneling corrections have an issue, which is that for low barriers with high frequencies, the matching crieteria cannot be met, and you get a math error because it tries to log a
    # negative number!
    # It turns out that, in this situation, the reverse barrier can be constructed normally (e.g. by swapping the forward and reverse barriers)
    # The solution is thus simple. If this is an issue, we reverse the barrier, and calcualte the tunneling integrals as before
    # we then have to shift the energy by the difference in barrier heights.
    # NOTE this works because the tunneling integral is independent of whicih direction you are going!
    # this is done in the following line, where the test for an error is followed by calling the same function, with reversed barriers and a shifted energy

    # if 1 - f * umr <=0 :
    # if reversed_flag == False:
    #     return WagBar_composite(infreq, inxff, inrev, inbar, -s, reversed_flag=True) - inrev/pc.hartree_to_joule + inbar/pc.hartree_to_joule

    dr = -ybr + math.log(((1 - f * umr) / (rho + f * umr))) / alphar

    ###################
    up = (((rho ** 2 + rho + 1) ** 0.5) + rho - 1) / max(3 * f, 3)
    Rp = (210 * rho * rho - 168 * (1 + f) * rho * (1 - rho) * up + 140 * (f - (1 + f) ** 2 * rho + f * rho ** 2) * up * up + 120 * f * (1 + f) * (1 - rho) * up ** 3 + 105 * f ** 2 * up ** 4) / (
                210 * rho ** 2 - 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho ** 2) * f ** 2 * up ** 2 + 240 * (1 - rho) * f ** 3 * up ** 3 + 105 * f ** 4 * up ** 4)

    alphap = alphab * f * Rp

    ump1 = (((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    ump2 = (-((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1

    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab

    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp

    Vr_s = bar_height * ((1 - rho ** 2) * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) +
                         (1 + rho) ** 2 * math.exp(alphar * (s + dr)) / (1 + math.exp(alphar * (s + dr))) ** 2)

    Vp_s = bar_height * ((1 - rho ** 2) * math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) + (1 + rho) ** 2 *
                         math.exp(alphap * (s + dp)) / (1 + math.exp(alphap * (s + dp))) ** 2)

    Vb_s = bar_height - Db + Db * ((1 - rho ** 2) * math.exp(alphab * s) / (1 + math.exp(alphab * s)) + (1 + rho) ** 2 *
                                   math.exp(alphab * s) / (1 + math.exp(alphab * s)) ** 2)

    return [Vr_s, Vb_s, Vp_s, ybr, ybp]


def TunnelProb(freq, xff, bar_height, rev_bar_height, energy, vpt4_correction=False):
    """ calculates tunneling probability WITHOUT deep tunneling corrections. For deerp tunnleing use 
    function WagProb()"""

    if energy == 0:
        return 0

    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees
    if bar_height > rev_bar_height:
        en = energy - (bar_height - rev_bar_height)
        delVf = rev_bar_height
        delVr = bar_height
    else:
        en = energy
        delVf = bar_height
        delVr = rev_bar_height

    deltaE = delVf - en
    tmp = freq * freq + 4 * xff * deltaE

    if tmp >= 0:
        prob = 1 / (1 + math.exp(2 * math.pi * (-freq + math.sqrt(tmp)) / (2 * xff)))  # eqtn 2.91/2.92 in Sam's thesis
    else:
        prob = 0

    """
    Stanton2016 gives an expression for theta to VPT4 for a symmetric Eckart potential. 
    omega_b = freq, V_0 = delVf
    """
    if vpt4_correction:

        '''
        This does not give the correct behaviour
        theta = (2.0 * np.pi * delVf / freq) * (1.0 -
                                                freq**2/(32.0 * delVf**2) -
                                                np.sqrt(en/delVf + freq**4/(1024.0*delVf**4)))
                                                
        '''

        w = freq * (1.0 - freq ** 3 / (32.0 * delVf ** 2))
        arg = w ** 2 + 4 * deltaE * xff
        if arg >= 0:
            theta = np.pi * (-w + np.sqrt(arg)) / (2.0 * xff)
            prob = 1.0 / (1.0 + np.exp(2.0 * theta))
        else:
            prob = 0

    return prob


def TunnelTheta(freq, xff, bar_height, rev_bar_height, energy, vpt4_correction=False):
    """ calculates tunneling probability WITHOUT deep tunneling corrections. For deerp tunnleing use
    function WagProb()"""

    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees
    if bar_height > rev_bar_height:
        en = energy - (bar_height - rev_bar_height)
        delVf = rev_bar_height
        delVr = bar_height
    else:
        en = energy
        delVf = bar_height
        delVr = rev_bar_height

    deltaE = delVf - en

    # deltaE = 0.718
    tmp = freq * freq + 4 * xff * deltaE

    if tmp < 0:
        return np.nan
    else:
        return math.pi * (-freq + math.sqrt(tmp)) / (2 * xff)


def WagProb(freq, xff, bar_height, rev_bar_height, energy, reversed_flag=False):
    """
    Calculates the probability of transmission at energy==energy, using the form of the potential that is calcualted
    in wagProb function. NOTE this is copied straight from Xiao's mathematica code. This includes deep tunneling corrections.
    :param freq: 
    :param xff: 
    :param bar_height: 
    :param rev_bar_height: 
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return: 
    """

    if energy == 0:
        return 0

    # These vairables jsut store the input values, which are changed units just after
    in_freq = freq
    in_bar_height = bar_height
    in_rev_bar_height = rev_bar_height
    in_energy = energy
    in_xff = xff

    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    # Originally, the code made sure it did its calcualtion in the exothermic direction.
    # This is fine, but for some reactions (high frequncy + small barrier) the matcing conditions in the compositie potential don't work!
    # In this case, you have to try doing it in the endothermic direciton.
    # In order for that to work, i have disabled the auto-exothermic snippet, but added a warning that you are looking at an endothermic reaciton

    # if bar_height > rev_bar_height:
    #     print("Warning: WagProb has been called with an endothermic potential. This is either because your reaction is endothermic, or your barrier is sufficiently weird that the matching "
    #           "conditions don't work! I am trying to use the reverse potential with a shifted energy to fix it!")
    #     en = energy - (bar_height - rev_bar_height)
    #     delVf = rev_bar_height
    #     delVr = bar_height
    # else:
    #     en = energy
    #     delVf = bar_height
    #     delVr = rev_bar_height
    en = energy
    delVf = bar_height
    delVr = rev_bar_height

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:
        # print("one")
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    if (Db / delVf) < 0:
        # print("two")
        return 0
    f = (Db / delVf) ** 0.5
    ur = (((rho * rho + rho + 1) ** 0.5) - (rho - 1)) / max(3 * f, 3)
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (f - rho * ((1 + f) ** 2) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (1 - rho) * f * f * f * ur * ur * ur + 105 * f * f * f * f * ur * ur * ur * ur)

    alphar = alphab * f * Rr
    umr1 = (((1 - rho) * (1 - rho) * (1 - Rr * f) * (1 - Rr * f) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) ** 0.5 + (1 - rho) * (1 - Rr * f)) / (
                2 * (1 - Rr * f * f))  # (S2.11) supp. info. umr is the corssover coordinate, and this eqtn requires the derivatives mathch here
    umr2 = (-1) * umr1

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab  # (S2.13)

    # --------------------- DEEP TUNELING ISSUES ------------------------------------------------------------------------------------------------------------------------------------------
    # The deep tunneling corrections have an issue, which is that for low barriers with high frequencies, the matching crieteria cannot be met, and you get a math error because it tries to log a
    # negative number!
    # It turns out that, in this situation, the reverse barrier can be constructed normally (e.g. by swapping the forward and reverse barriers)
    # The solution is thus simple. If this is an issue, we reverse the barrier, and calcualte the tunneling integrals as before
    # we then have to shift the energy by the difference in barrier heights.
    # NOTE this works because the tunneling integral is independent of whicih direction you are going!
    # this is done in the following line, where the test for an error is followed by calling the same function, with reversed barriers and a shifted energy

    # if ((1-f*umr) / (rho+f*umr) <= 0):  # important, since (1-f*umr) is about to be logged in the next line!
    # if reversed_flag == False:
    #     return WagProb(in_freq, in_xff, in_rev_bar_height, in_bar_height, in_energy + in_rev_bar_height - in_bar_height, reversed_flag=True)

    yrb = math.log(((1 - f * umr) / (rho + f * umr))) / alphar  # (S2.12) This is the value of y at which Vr matches Vb in value and slope

    # dr = -ybr + math.log(((1 - f * umr) / (rho + f * umr))) / alphar
    dr = -ybr + yrb  # same as above, but clearer        (S2.14)
    ###################
    up = (((rho ** 2 + rho + 1) ** 0.5) + rho - 1) / max(3 * f, 3)
    Rp = (210 * rho * rho - 168 * (1 + f) * rho * (1 - rho) * up + 140 * (f - (1 + f) ** 2 * rho + f * rho ** 2) * up * up + 120 * f * (1 + f) * (1 - rho) * up ** 3 + 105 * f ** 2 * up ** 4) / (
                210 * rho ** 2 - 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho ** 2) * f ** 2 * up ** 2 + 240 * (1 - rho) * f ** 3 * up ** 3 + 105 * f ** 4 * up ** 4)

    alphap = alphab * f * Rp

    ump1 = (((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    ump2 = (-((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1

    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab

    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp

    zrb = math.exp(alphar * (ybr + dr))
    zbr = math.exp(alphab * ybr)
    zpb = math.exp(alphap * (ybp + dp))
    zbp = math.exp(alphab * ybp)
    headb = 2 * Db / freq * rho / (1 + rho)
    headr = headb * delVf / Db / Rr
    headp = headr * Rr / Rp

    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    ep = thtaEn - delVf + Db

    eta = ep / Db

    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 assymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        thta = headb * thetaReg(2, zlo, zhi, eta, rho)
    else:  # The energy is below the VPT2 assymptote
        zlo = zbr
        zhi = zbp
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        thta = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)

    if zlo == zbr:
        eta = thtaEn / delVf
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)

        zlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zlo - 2 * sqrtdelld2a
        thta += headr * thetaReg(1, zlo, zrb, eta, rho)

        if zhi == zbp:
            thta += headp * thetaReg(3, zpb, zzhi, eta, rho)
    else:
        pass

    if en < delVf:
        thta = thta.real
        if thta > 30:
            return 0
        if thta > 20:
            return (1 + math.exp(2 * thta)) ** (-1)  # previosuly this said return 0, but this is clearly wrong! This gives the wrong tunneling at v low energies, which is important for low T
            # return 0
        else:
            return (1 + math.exp(2 * thta)) ** (-1)
    elif en < 2 * delVf:
        thta = thta.real
        if thta > 20:
            # print("four")
            # print(in_freq, in_xff, in_bar_height, in_rev_bar_height, in_energy)
            # print(TunnelProb(in_freq, in_xff, in_bar_height, in_rev_bar_height, in_energy))
            # return 1
            return 1
            # return TunnelProb(in_freq, in_xff, in_bar_height, in_rev_bar_height, in_energy)
            # return 0
        else:
            return 1 - ((1 + cmath.exp(2 * thta)) ** (-1))
    else:
        return 1


def BoundProb(freq, bound_freq, xff, bar_height, rev_bar_height, energy):
    """
    Calculate the probability of transmission thgouh Tim's unimolecular DT corrected barrier
    :return: Float
    """

    # if energy < bar_height:
    #     en = energy
    # elif energy < 2 * bar_height:
    #     en = 2 * bar_height - energy
    # else:
    #     return 1

    if energy < bar_height:
        en = energy
        theta = BoundTheta(freq, bound_freq, xff, bar_height, rev_bar_height, en)
        return (1 + math.exp(2 * theta)) ** (-1)
    elif energy < 2 * bar_height:
        en = 2 * bar_height - energy
        theta = BoundTheta(freq, bound_freq, xff, bar_height, rev_bar_height, en)
        if theta > 20:
            return 1
        else:
            return 1 - ((1 + cmath.exp(2 * theta)) ** (-1))
    else:
        return 1


def WagTheta(freq, xff, bar_height, rev_bar_height, energy, reversed_flag=False):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """

    if energy == 0:
        return 0

    # These vairables jsut store the input values, which are changed units just after
    in_freq = freq
    in_bar_height = bar_height
    in_rev_bar_height = rev_bar_height
    in_energy = energy
    in_xff = xff

    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    en = energy
    delVf = bar_height
    delVr = rev_bar_height

    rho = (rev_bar_height / bar_height) ** 0.5
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    if (Db / delVf) < 0:
        return 0
    f = (Db / delVf) ** 0.5
    ur = (((rho * rho + rho + 1) ** 0.5) - (rho - 1)) / max(3 * f, 3)
    Rr = (210 * rho * rho + 168 * (1 + f) * rho * (1 - rho) * ur + 140 * (f - rho * ((1 + f) ** 2) + f * rho * rho) * ur * ur - 120 * f * (1 + f) * (1 - rho) * ur * ur * ur + 105 * f * f * ur * ur * ur * ur) / (
                210 * rho * rho + 336 * rho * (1 - rho) * f * ur + 140 * (1 - 4 * rho + rho * rho) * f * f * ur * ur - 240 * (1 - rho) * f * f * f * ur * ur * ur + 105 * f * f * f * f * ur * ur * ur * ur)

    alphar = alphab * f * Rr
    umr1 = (((1 - rho) * (1 - rho) * (1 - Rr * f) * (1 - Rr * f) - 4 * rho * (1 - Rr * f * f) * (Rr - 1)) ** 0.5 + (1 - rho) * (1 - Rr * f)) / (
                2 * (1 - Rr * f * f))  # (S2.11) supp. info. umr is the corssover coordinate, and this eqtn requires the derivatives mathch here
    umr2 = (-1) * umr1

    if (umr1 < 0) or (umr1 > ur):
        umr = umr2
    else:
        umr = umr1

    ybr = math.log((1 - umr) / (rho + umr)) / alphab  # (S2.13)

    yrb = math.log(((1 - f * umr) / (rho + f * umr))) / alphar  # (S2.12) This is the value of y at which Vr matches Vb in value and slope

    dr = -ybr + yrb  # same as above, but clearer        (S2.14)
    ###################
    up = (((rho ** 2 + rho + 1) ** 0.5) + rho - 1) / max(3 * f, 3)
    Rp = (210 * rho * rho - 168 * (1 + f) * rho * (1 - rho) * up + 140 * (f - (1 + f) ** 2 * rho + f * rho ** 2) * up * up + 120 * f * (1 + f) * (1 - rho) * up ** 3 + 105 * f ** 2 * up ** 4) / (
                210 * rho ** 2 - 336 * rho * (1 - rho) * f * up + 140 * (1 - 4 * rho + rho ** 2) * f ** 2 * up ** 2 + 240 * (1 - rho) * f ** 3 * up ** 3 + 105 * f ** 4 * up ** 4)

    alphap = alphab * f * Rp

    ump1 = (((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    ump2 = (-((1 - rho) ** 2 * (1 - Rp * f) ** 2 - 4 * rho * (1 - Rp * f ** 2) * (Rp - 1)) ** 0.5 - (1 - rho) * (1 - Rp * f)) / (2 * (1 - Rp * f ** 2))

    if (ump1 < 0) or (ump1 > up):
        ump = ump2
    else:
        ump = ump1

    ybp = math.log((-1 - ump) / (-rho + ump)) / alphab

    dp = math.log((-1 - f * ump) / (-rho + f * ump)) / alphap - ybp

    zrb = math.exp(alphar * (ybr + dr))
    zbr = math.exp(alphab * ybr)
    zpb = math.exp(alphap * (ybp + dp))
    zbp = math.exp(alphab * ybp)
    headb = 2 * Db / freq * rho / (1 + rho)
    headr = headb * delVf / Db / Rr
    headp = headr * Rr / Rp

    thtaEn = en
    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    ep = thtaEn - delVf + Db

    eta = ep / Db

    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 assymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        thta = headb * thetaReg(2, zlo, zhi, eta, rho)
    else:  # The energy is below the VPT2 assymptote
        zlo = zbr
        zhi = zbp
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        thta = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)

    if zlo == zbr:
        eta = thtaEn / delVf
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)

        zlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zlo - 2 * sqrtdelld2a

        thta += headr * thetaReg(1, zlo, zrb, eta, rho)

        if zhi == zbp:
            thta += headp * thetaReg(3, zpb, zzhi, eta, rho)
    else:
        pass

    return thta.real


def BoundTheta_individual_contributions(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    THEN REUTNRS HTE CONTRIBUTIONS FROM THE CENTRAL AND EDGE BARRIER PARTS SEPERATLY, SO YOU CAN INSPECT THEM!
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    en = energy
    delVf = bar_height

    rho = (rev_bar_height / bar_height) ** 0.5  # This should be one for a symmetric potential (whici everything should be!)
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:  # issues with the vpt2 expansion at the TS giving a negative effective barrier
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * bar_height)) ** 0.5  # units of energy**0.5
    if (Db / delVf) < 0:  # issues with the electronic structure calculations giving a negative barrier height
        return 0

    # ------ Find crossing point and position of the upsidedown potentials
    # new and improved (correct) coefficients
    a = Db - delVf
    b = -2 * Db + delVf
    c = Db + delVf * (alphab / alphar) ** 2
    polynomial_coeficients = [c, b, a]
    roots = np.roots(polynomial_coeficients)
    # print("Roots: ", roots)
    A2 = max(roots.real)  # This is sech^2(yx alphab / 2)
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point on the reactant side

    # once we know yx we can find yp
    tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -delVf
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    adjust = False
    if cosh < 1:
        print("WARNING: Plot cosh < 1")
        if V_1D_yx == 0:
            raise ValueError("Cannot match the potentials! Cosh = ", cosh,
                             " and 1D potential value not given. Cannot perform correction!")

        print("Start adaption to 1D potential")
        adjust = True
        yx = (2 / alphab) * np.arccosh(np.sqrt((Db) / (V_1D_yx - delVf + Db)))
        tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -delVf
        sech = np.sqrt(tmp)
        cosh = 1 / sech
        print("New Plot cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = -((alphar * yx + 2 * argument) / alphar)
    yp = -yr  # positive number
    dr = -yr  # positive number
    ybr = -yx

    # energy at the crossing point
    en_yx = delVf - Db + Db * A2
    if not V_1D_yx == 0 and adjust:
        en_yx = V_1D_yx

    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #
    zrb = math.exp(alphar * (ybr + dr))  # cross over point reactant and central barrier in new reactant barrier coordinates
    zbr = math.exp(alphab * ybr)  # cross over point reactant and central barrier in new central barrier coordinates
    zbp = 1 / zbr
    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #

    Rr = (alphar / alphab) * math.sqrt(delVf / Db)  # S. 2.7. wagner
    Rp = Rr  # by symmetry
    headb = 2 * Db / freq * rho / (1 + rho)  # This is the prefactor in wagners S3.6 for the central integral
    headr = headb * delVf / Db / Rr  # This is the prefactor in wagners s3.6 for the first inragral

    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    ep = thtaEn - delVf + Db
    # Calculate eta for central barrier
    eta = ep / Db
    # ------------ CENTRAL BARRIER CONTRIBUTION ------------------------------------------------------------------------------------------------------
    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 assymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        central_cont = headb * thetaReg(2, zlo, zhi, eta, rho)
        thta = central_cont

    else:  # The energy is below the VPT2 assymptote
        zlo = zbr  # lower limit of integration is cross over point with reactant bit
        zhi = zbp  # upper limit of integration is cross over point with product  bit
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        central_cont = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)

        thta = central_cont
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------

    # ----------- EDGE BARRIER CONTRIBUTION --------------------------------------------------------------------------------------
    # Check if lower limit of central barrier integration coincides with matching point of potential. Only than we have edge contribution.
    if zlo == zbr:
        # - For me ----------------------

        # Now we have to find the z value for the given energy of the reactant potential
        epsilon = (delVf - thtaEn) / 4 / delVf  # 0.25 for thetEn = 0 (ZPE)
        if thtaEn > delVf:
            zlo = 1
        else:
            # solution to epsilon = - z /(1+z)^2
            zlo = (1 / 2 / epsilon) * (1 - 2 * epsilon + math.sqrt(1 - 4 * epsilon))  # should be larger than one

        # thtaEn = en_yx

        # eta = (delVf - thtaEn) / delVf          # this gives the inverted tunelling
        # adapted Eta for edge barrier
        eta = (delVf - en_yx) / delVf  # this gives the inverted tunelling at the correct energy

        # we might need to pretend that the segment is the middle one, because for the reactant side, it assuems zlo = -infinity
        edge_cont = 2 * headr * thetaReg(2, zlo, zrb, eta, rho)
        thta += edge_cont

    else:
        edge_cont = 0
        pass
    # ----------- ----------------------------- --------------------------------------------------------------------------------------
    return [energy, central_cont.real, edge_cont.real, thta.real]


def BoundTheta_individual_contributions_limits(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    THEN REUTNRS HTE CONTRIBUTIONS FROM THE CENTRAL AND EDGE BARRIER PARTS SEPERATLY, SO YOU CAN INSPECT THEM!
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    en = energy
    delVf = bar_height

    rho = (rev_bar_height / bar_height) ** 0.5  # This should be one for a symmetric potential (whici everything should be!)
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:  # issues with the vpt2 expansion at the TS giving a negative effective barrier
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * bar_height)) ** 0.5  # units of energy**0.5
    if (Db / delVf) < 0:  # issues with the electronic structure calculations giving a negative barrier height
        return 0

    # ------ Find crossing point and position of the upsidedown potentials
    # new and improved (correct) coefficients
    a = Db - delVf
    b = -2 * Db + delVf
    c = Db + delVf * (alphab / alphar) ** 2
    polynomial_coeficients = [c, b, a]
    roots = np.roots(polynomial_coeficients)
    # print("Roots: ", roots)
    A2 = max(roots.real)  # This is sech^2(yx alphab / 2)
    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point on the reactant side

    # once we know yx we can find yp
    tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -delVf
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    adjust = False
    if cosh < 1:
        print("WARNING: Plot cosh < 1")
        if V_1D_yx == 0:
            raise ValueError("Cannot match the potentials! Cosh = ", cosh,
                             " and 1D potential value not given. Cannot perform correction!")

        print("Start adaption to 1D potential")
        adjust = True
        yx = (2 / alphab) * np.arccosh(np.sqrt((Db) / (V_1D_yx - delVf + Db)))
        tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -delVf
        sech = np.sqrt(tmp)
        cosh = 1 / sech
        print("New Plot cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = -((alphar * yx + 2 * argument) / alphar)
    yp = -yr  # positive number
    dr = -yr  # positive number
    ybr = -yx

    # energy at the crossing point
    en_yx = delVf - Db + Db * A2
    if not V_1D_yx == 0 and adjust:
        en_yx = V_1D_yx

    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #
    zrb = math.exp(alphar * (ybr + dr))  # cross over point reactant and central barrier in new reactant barrier coordinates
    zbr = math.exp(alphab * ybr)  # cross over point reactant and central barrier in new central barrier coordinates
    zbp = 1 / zbr
    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #

    Rr = (alphar / alphab) * math.sqrt(delVf / Db)  # S. 2.7. wagner
    Rp = Rr  # by symmetry
    headb = 2 * Db / freq * rho / (1 + rho)  # This is the prefactor in wagners S3.6 for the central integral
    headr = headb * delVf / Db / Rr  # This is the prefactor in wagners s3.6 for the first inragral

    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    ep = thtaEn - delVf + Db
    # Calculate eta for central barrier
    eta = ep / Db
    # ------------ CENTRAL BARRIER CONTRIBUTION ------------------------------------------------------------------------------------------------------
    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 assymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        central_cont = headb * thetaReg(2, zlo, zhi, eta, rho)
        thta = central_cont

    else:  # The energy is below the VPT2 assymptote
        zlo = zbr  # lower limit of integration is cross over point with reactant bit
        zhi = zbp  # upper limit of integration is cross over point with product  bit
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        central_cont = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)
        thta = central_cont

    central_low = math.log(zlo) / alphab
    central_high = math.log(zhi) / alphab

    # ----------------------------------------------------------------------------------------------------------------------------------------------------------

    # ----------- EDGE BARRIER CONTRIBUTION --------------------------------------------------------------------------------------
    if zlo == zbr:  # Check if lower limit of central barrier integration coincides with matching point of potential. Only than we have edge contribution.
        # - For me ----------------------

        # Now we have to find the z value for the given energy of the reactant potential
        epsilon = (delVf - thtaEn) / 4 / delVf  # 0.25 for thetEn = 0 (ZPE)
        if thtaEn > delVf:
            zlo = 1
        else:
            # solution to epsilon = - z /(1+z)^2
            zlo = (1 / 2 / epsilon) * (1 - 2 * epsilon + math.sqrt(1 - 4 * epsilon))  # should be larger than one

        # thtaEn = en_yx

        # eta = (delVf - thtaEn) / delVf          # this gives the inverted tunelling
        # adapted Eta for edge barrier
        eta = (delVf - en_yx) / delVf  # this gives the inverted tunelling at the correct energy

        # we might need to pretend that the segment is the middle one, because for the reactant side, it assuems zlo = -infinity
        edge_cont = 2 * headr * thetaReg(2, zlo, zrb, eta, rho)
        thta += edge_cont

        edge_low = math.log(zrb) / alphar - dr
        edge_high = math.log(zlo) / alphar - dr

    else:
        edge_cont = 0
        pass
    # ----------- ----------------------------- --------------------------------------------------------------------------------------
    return [central_low, central_high, edge_low, edge_high]


def BoundTheta_individual_contributions_f(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False, root=1, f=1):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    THEN REUTNRS HTE CONTRIBUTIONS FROM THE CENTRAL AND EDGE BARRIER PARTS SEPERATLY, SO YOU CAN INSPECT THEM!
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    # f = 1.05

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    en = energy
    delVf = bar_height

    v1 = delVf

    rho = (rev_bar_height / bar_height) ** 0.5  # This should be one for a symmetric potential (whici everything should be!)
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:  # issues with the vpt2 expansion at the TS giving a negative effective barrier
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * f * bar_height)) ** 0.5  # units of energy**0.5
    if (Db / delVf) < 0:  # issues with the electronic structure calculations giving a negative barrier height
        return 0

    # new and improved (correct) coefficients
    a = (v1 - Db) + (f - 1) * (2 * v1) / (Db) * (v1 - Db) + (f - 1) ** 2 * ((v1 ** 3) / (Db ** 2) - (v1 ** 2) / (Db))
    b = 3 * Db - 2 * v1 + (f - 1) * 2 * v1 * (2 * Db - v1) / Db + (f - 1) ** 2 * v1 ** 2 / Db
    c = -3 * Db + v1 - f * v1 * ((alphab) / (alphar)) ** 2 - (f - 1) * 2 * v1
    d = Db + f * v1 * ((alphab) / (alphar)) ** 2

    polynomial_coeficients = [d, c, b, a]
    roots = np.roots(polynomial_coeficients)

    real_roots = []
    # print("Roots: ", roots)
    for i in roots:
        if i.imag == 0 and i.real > 0:
            real_roots.append(i.real)
    # print("Real roots: ", real_roots)
    # A2 = real_roots[2].real
    if len(real_roots) == 3:
        if root == 3:
            A2 = min(real_roots)
        elif root == 2:
            A2 = real_roots[1]
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 2:
        if root == 3:
            raise ValueError("You selected root 3, but there are only 2 real and positive roots")
        elif root == 2:
            A2 = min(real_roots)
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 1:
        if root == 3:
            raise ValueError("You selected root 3, but there is only one real and positive root")
        elif root == 2:
            raise ValueError("You selected root 2, but there is only one real and positive root")
        elif root == 1:
            A2 = max(real_roots)
    # print("Selected root: ", A2)
    # A2 = max(real_roots)    # This is sech^2(yx alphab / 2)

    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point on the reactant side

    # once we know yx we can find yp
    tmp = (f - 1) / f + (Db * (1 - A2)) / (f * v1)
    if tmp < 0:
        raise ValueError("Cannot match the potentials! tmp = ", tmp)
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    adjust = False
    if cosh < 1:
        # print("WARNING: Plot cosh < 1")
        # if V_1D_yx == 0:
        raise ValueError("Cannot match the potentials! Cosh = ", cosh)
        # print("Start adaption to 1D potential")
        # adjust = True
        # yx = (2 / alphab) * np.arccosh(np.sqrt((Db) / (V_1D_yx - delVf + Db)))
        # tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -delVf
        # sech = np.sqrt(tmp)
        # cosh = 1 / sech
        # print("New Plot cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = -((alphar * yx + 2 * argument) / alphar)
    yp = -yr  # positive number
    dr = -yr  # positive number
    ybr = -yx

    # energy at the crossing point - correct
    en_yx = delVf - Db + Db * A2

    # Assumption: Definitions of Wagner do not change
    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #
    # coordinate transformation - limits adaption
    zrb = math.exp(alphar * (ybr + dr))  # cross over point reactant and central barrier in new reactant barrier coordinates
    zbr = math.exp(alphab * ybr)  # cross over point reactant and central barrier in new central barrier coordinates
    zbp = 1 / zbr
    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #
    # Wagner's prefactors
    Rr = (alphar / alphab) * math.sqrt(f * delVf / Db)  # S. 2.7. wagner
    Rp = Rr  # by symmetry
    headb = 2 * Db / freq * rho / (1 + rho)  # This is the prefactor in wagners S3.6 for the central integral
    headr = headb * f * delVf / Db / Rr  # This is the prefactor in wagners s3.6 for the first inragral

    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    # ------------ CENTRAL BARRIER CONTRIBUTION ------------------------------------------------------------------------------------------------------
    # Calculate eta for central barrier
    ep = thtaEn - delVf + Db
    eta = ep / Db

    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 asymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        central_cont = headb * thetaReg(2, zlo, zhi, eta, rho)
        thta = central_cont

    else:  # The energy is below the VPT2 assymptote
        # Define limits
        zlo = zbr  # lower limit of integration is cross over point with reactant bit
        zhi = zbp  # upper limit of integration is cross over point with product  bit
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        central_cont = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)

        thta = central_cont
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------

    # ----------- EDGE BARRIER CONTRIBUTION --------------------------------------------------------------------------------------
    if zlo == zbr:  # Check if lower limit of central barrier integration coincides with matching point of potential. Only than we have edge contribution.
        # - For me ----------------------

        # Now we have to find the z value for the given energy of the reactant potential
        epsilon = (f * delVf - thtaEn) / 4 / (f * delVf)  # 0.25 for thetEn = 0 (ZPE)
        if thtaEn > delVf:
            zlo = 1
        else:
            # solution to epsilon = - z /(1+z)^2
            zlo = (1 / 2 / epsilon) * (1 - 2 * epsilon + math.sqrt(1 - 4 * epsilon))  # should be larger than one

        # thtaEn = en_yx

        # eta = (delVf - thtaEn) / delVf          # this gives the inverted tunelling
        # adapted Eta for edge barrier
        eta = (f * delVf - en_yx) / (f * delVf)  # this gives the inverted tunelling at the correct energy

        # we might need to pretend that the segment is the middle one, because for the reactant side, it assuems zlo = -infinity
        edge_cont = 2 * headr * thetaReg(2, zlo, zrb, eta, rho)
        thta += edge_cont

    else:
        edge_cont = 0
        pass
    # ----------- ----------------------------- --------------------------------------------------------------------------------------
    return [energy, central_cont.real, edge_cont.real, thta.real]


def BoundTheta_individual_contributions_f_limits(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False, root=1, f=1):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    THEN REUTNRS HTE CONTRIBUTIONS FROM THE CENTRAL AND EDGE BARRIER PARTS SEPERATLY, SO YOU CAN INSPECT THEM!
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    # f = 2

    bound_freq = bound_freq / pc.hartree_to_cm
    freq = freq / pc.hartree_to_cm  # Convert cm-1 to hartrees
    bar_height = bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    rev_bar_height = rev_bar_height / pc.hartree_to_joule  # Convert joules to hartrees
    energy = energy / pc.hartree_to_joule  # Convert joules to hartrees

    en = energy
    delVf = bar_height

    v1 = delVf

    rho = (rev_bar_height / bar_height) ** 0.5  # This should be one for a symmetric potential (whici everything should be!)
    Db = (-1) * (freq * freq / (4 * xff)) * (1 - (1 / rho) + (1 / (rho * rho)))
    if Db < 0:  # issues with the vpt2 expansion at the TS giving a negative effective barrier
        return 0
    alphab = freq * ((1 + rho) / rho) * (1 / (2 * Db)) ** 0.5
    alphar = bound_freq * ((1 + rho) / rho) * (1 / (2 * f * bar_height)) ** 0.5  # units of energy**0.5
    if (Db / delVf) < 0:  # issues with the electronic structure calculations giving a negative barrier height
        return 0

    # new and improved (correct) coefficients
    a = (v1 - Db) + (f - 1) * (2 * v1) / (Db) * (v1 - Db) + (f - 1) ** 2 * ((v1 ** 3) / (Db ** 2) - (v1 ** 2) / (Db))
    b = 3 * Db - 2 * v1 + (f - 1) * 2 * v1 * (2 * Db - v1) / Db + (f - 1) ** 2 * v1 ** 2 / Db
    c = -3 * Db + v1 - f * v1 * ((alphab) / (alphar)) ** 2 - (f - 1) * 2 * v1
    d = Db + f * v1 * ((alphab) / (alphar)) ** 2

    polynomial_coeficients = [d, c, b, a]
    roots = np.roots(polynomial_coeficients)

    real_roots = []
    for i in roots:
        if i.imag == 0 and i.real > 0:
            real_roots.append(i.real)
    # A2 = real_roots[0].real
    if len(real_roots) == 3:
        if root == 3:
            A2 = min(real_roots)
        elif root == 2:
            A2 = real_roots[1]
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 2:
        if root == 3:
            raise ValueError("You selected root 3, but there are only 2 real and positive roots")
        elif root == 2:
            A2 = min(real_roots)
        elif root == 1:
            A2 = max(real_roots)
    elif len(real_roots) == 1:
        if root == 3:
            raise ValueError("You selected root 3, but there is only one real and positive root")
        elif root == 2:
            raise ValueError("You selected root 2, but there is only one real and positive root")
        elif root == 1:
            A2 = max(real_roots)
    # A2 = max(real_roots).real    # This is sech^2(yx alphab / 2)

    A = np.sqrt(A2)  # This is sech(yx alphab / 2)
    cosh = 1 / A
    argument = np.arccosh(cosh)
    yx = argument * 2 / alphab  # cross over point on the reactant side

    # once we know yx we can find yp
    tmp = (f - 1) / f + (Db * (1 - A2)) / (f * v1)
    if tmp < 0:
        raise ValueError("Cannot match the potentials! tmp = ", tmp)
    sech = np.sqrt(tmp)
    cosh = 1 / sech
    adjust = False
    if cosh < 1:
        print("WARNING: Plot cosh < 1")
        # if V_1D_yx == 0:
        raise ValueError("Cannot match the potentials! Cosh = ", cosh)
        # print("Start adaption to 1D potential")
        # adjust = True
        # yx = (2 / alphab) * np.arccosh(np.sqrt((Db) / (V_1D_yx - delVf + Db)))
        # tmp = (-Db + Db * np.cosh(alphab * yx / 2) ** -2) / -delVf
        # sech = np.sqrt(tmp)
        # cosh = 1 / sech
        # print("New Plot cosh: ", cosh)
    argument = np.arccosh(cosh)

    yr = -((alphar * yx + 2 * argument) / alphar)
    yp = -yr  # positive number
    dr = -yr  # positive number
    ybr = -yx

    # energy at the crossing point - correct
    en_yx = delVf - Db + Db * A2

    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #
    # coordinate transformation - limits adaption
    zrb = math.exp(alphar * (ybr + dr))  # cross over point reactant and central barrier in new reactant barrier coordinates
    zbr = math.exp(alphab * ybr)  # cross over point reactant and central barrier in new central barrier coordinates
    zbp = 1 / zbr
    # --------------------------------- EDIT HERE ---------------------------------------------------------------------------------------------------------------------------------------------------------------------  #
    # Wagner's prefactors
    Rr = (alphar / alphab) * math.sqrt(f * delVf / Db)  # S. 2.7. wagner
    Rp = Rr  # by symmetry
    headb = 2 * Db / freq * rho / (1 + rho)  # This is the prefactor in wagners S3.6 for the central integral
    headr = headb * f * delVf / Db / Rr  # This is the prefactor in wagners s3.6 for the first inragral

    if en < delVf:
        thtaEn = en
    else:
        thtaEn = 2 * delVf - en

    # ------------ CENTRAL BARRIER CONTRIBUTION ------------------------------------------------------------------------------------------------------
    # Calculate eta for central barrier
    ep = thtaEn - delVf + Db
    eta = ep / Db

    if thtaEn > (delVf - Db):  # This means the energy is above the VPT2 asymptote
        sqrtdelld2a = (1 + rho) * (1 - eta) ** 0.5 / (1 - rho ** 2 - eta)
        zzlo = -(1 + rho - eta) / (1 - rho ** 2 - eta) + sqrtdelld2a
        zzhi = zzlo - 2 * sqrtdelld2a
        zlo = max(zbr, zzlo)
        zhi = min(zbp, zzhi)
        central_cont = headb * thetaReg(2, zlo, zhi, eta, rho)
        thta = central_cont

    else:  # The energy is below the VPT2 assymptote
        # Define limits
        zlo = zbr  # lower limit of integration is cross over point with reactant bit
        zhi = zbp  # upper limit of integration is cross over point with product  bit
        bb = 2 * Db * (1 + rho - eta)
        aa = Db * (1 - rho ** 2 - eta)
        cc = -ep
        central_cont = headb * thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc)

        thta = central_cont

    central_low = math.log(zlo) / alphab
    central_high = math.log(zhi) / alphab
    # ----------------------------------------------------------------------------------------------------------------------------------------------------------

    # ----------- EDGE BARRIER CONTRIBUTION --------------------------------------------------------------------------------------
    if zlo == zbr:  # Check if lower limit of central barrier integration coincides with matching point of potential. Only than we have edge contribution.
        # - For me ----------------------

        # Now we have to find the z value for the given energy of the reactant potential
        epsilon = (f * delVf - thtaEn) / 4 / (f * delVf)  # 0.25 for thetEn = 0 (ZPE)
        if thtaEn > delVf:
            zlo = 1
        else:
            # solution to epsilon = - z /(1+z)^2
            zlo = (1 / 2 / epsilon) * (1 - 2 * epsilon + math.sqrt(1 - 4 * epsilon))  # should be larger than one

        # thtaEn = en_yx

        # eta = (delVf - thtaEn) / delVf          # this gives the inverted tunelling
        # adapted Eta for edge barrier
        eta = (f * delVf - en_yx) / (f * delVf)  # this gives the inverted tunelling at the correct energy

        # we might need to pretend that the segment is the middle one, because for the reactant side, it assuems zlo = -infinity
        edge_cont = 2 * headr * thetaReg(2, zlo, zrb, eta, rho)
        thta += edge_cont

        edge_low = math.log(zrb) / alphar - dr
        edge_high = math.log(zlo) / alphar - dr

    else:
        edge_cont = 0
        pass

    # ----------- ----------------------------- --------------------------------------------------------------------------------------

    return [central_low, central_high, edge_low, edge_high]


def BoundTheta(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    # print("V_1D_yx", V_1D_yx)

    # The BoundTheta function calcualtes theta, and returns theta, the central barrier contribution to theta, the edge barrier contribution to theta as well
    # we only need total theta here, which is returned last
    [_, _, _, theta] = BoundTheta_individual_contributions(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx, reversed_flag)
    return theta


def BoundTheta_f(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx=0, reversed_flag=False, root=1, f=1):
    """
    Calculates E(theta) at energy==energy, using the form of the potential that is calcualted
    in wagProb function. This includes deep tunneling corrections.
    :param freq:
    :param xff:
    :param bar_height:
    :param rev_bar_height:
    :param energy:
    REVERSE_FLAG is TRUE if the function has been recalled using the reverse barrier
    :return:
    """
    # if energy == 0:
    #     return 0

    # print("V_1D_yx", V_1D_yx)

    # The BoundTheta function calcualtes theta, and returns theta, the central barrier contribution to theta, the edge barrier contribution to theta as well
    # we only need total theta here, which is returned last
    [_, _, _, theta] = BoundTheta_individual_contributions_f(freq, bound_freq, xff, bar_height, rev_bar_height, energy, V_1D_yx, reversed_flag, root=root, f=f)
    return theta


def thetaIrreg(zlo, zhi, eta, rho, aa, bb, cc):
    """

    :param zlo: 
    :param zhi: 
    :param eta: 
    :param rho: 
    :param aa: 
    :param bb: 
    :param cc: 
    :return: 
    """

    if math.isnan(zlo) or math.isnan(zhi) or math.isnan(eta) or math.isnan(rho) or math.isnan(aa) or math.isnan(bb) or math.isnan(cc):
        return 0

    c1 = (1 - eta) ** 0.5
    c2 = 1 - rho ** 2 - eta
    c3 = 1 + rho - eta

    if c1 == 0:
        c1 = 0.0000000001

    arg3h = max(-1, min(1, (rho * zhi - 1) / (1 + zhi) / c1))
    arg3l = max(-1, min(1, (rho * zlo - 1) / (1 + zlo) / c1))
    thta = (1 + rho) * (math.asin(arg3h) - math.asin(arg3l))

    rhi = max(0, cc + bb * zhi + aa * zhi ** 2)
    rlo = max(0, cc + bb * zlo + aa * zlo ** 2)
    arg1 = (2 * cc + bb * zhi + 2 * (cc * rhi) ** 0.5) / zhi
    arg2 = (2 * cc + bb * zlo + 2 * (cc * rlo) ** 0.5) / zlo

    thta -= (-eta) ** 0.5 * math.log(arg1 / arg2)
    # thta -= (abs(eta)) ** 0.5 * math.log(abs(arg1 / arg2))   # -------------------------------------------------------------------------------------------------------------------------------------------------------#

    if (-c2) >= 0:
        arg2h = max(-1, min(1, (c2 * zhi + c3) / (1 + rho) / c1))
        arg2l = max(-1, min(1, (c2 * zlo + c3) / (1 + rho) / c1))
        return thta + (-c2) ** 0.5 * (math.asin(arg2h) - math.asin(arg2l))

    else:
        arg1 = 2 * (aa * rhi) ** 0.5 + 2 * aa * zhi + bb
        arg2 = 2 * (aa * rlo) + 2 * aa * zlo + bb
        return thta + c2 ** 0.5 * math.log(arg1 / arg2)


def thetaReg(iseg, zlo, zhi, eta, rho):
    """
    Calculates the 'regular' theta, using just the VPT2 barrier. This is used when the energy is greated than the VPT2 assymptote
    :param iseg: 
    :param zlo: 
    :param zhi: 
    :param eta: 
    :param rho: 
    :return: 
    """

    c0 = rho ** 2 + eta - 1
    c3 = 1 + rho

    # c2 = c0**0.5
    c2 = cmath.sqrt(c0)

    # c1 = eta**0.5
    c1 = cmath.sqrt(eta)

    if eta > 1:
        return math.pi * (c3 - c2 - c1)
    else:
        c4 = (1 - eta) ** 0.5
        # c4 = cmath.sqrt(1-eta)
        c5 = c3 * c4
        c6 = c3 - eta

        if iseg >= 2:  # product segment
            arg1l = max(-1, min(1, (c6 * zlo - eta) / zlo / c5))
            arg2l = max(-1, min(1, (c6 - c0 * zlo) / c5))
            arg3l = max(-1, min(1, (rho * zlo - 1) / (1 + zlo) / c4))
            thtal = c3 * cmath.asin(arg3l) + c2 * cmath.asin(arg2l) - c1 * cmath.asin(arg1l)
        else:
            thtal = (-1) * math.pi / 2 * (c3 - c2 - c1)

        if iseg <= 2:  # reactant or barrier segment
            arg1h = max(-1, min(1, ((c6 * zhi - eta) / zhi / c5)))
            arg2h = max(-1, min(1, (c6 - c0 * zhi) / c5))
            arg3h = max(-1, min(1, (rho * zhi - 1) / (1 + zhi) / c4))
            thtah = c3 * cmath.asin(arg3h) + c2 * cmath.asin(arg2h) - c1 * cmath.asin(arg1h)
        else:
            thtah = math.pi / 2 * (c3 - c2 - c1)

        return thtah - thtal


if __name__ == '__main__':
    # z = thetaReg(1, 0.3819675540019771, 0.507923517507374, 0.8000014450240597, 1.0)
    # print(z)
    # import read_gaussian as rg
    # import matplotlib.pyplot as plt
    #
    # low_freq_energy_list = [rg.find_energy(str("h_plus_methanol/low_freq_anharmonicity/" + x + ".fchk")) for x in
    #                          ['m4', 'm3', 'm2', 'm1', "energy_mp2", 'p1', 'p2', 'p3', 'p4']]
    #
    # inverted_list = [-x for x in low_freq_energy_list]
    # low_freq = 102.009
    # [xff, g0, richard_the_third, richard_the_fourth] = anhCalc1D(inverted_list, 0.02, low_freq)
    # xff_cm = 219476 * xff
    #
    # print(xff_cm)   # -28147.0404641
    #
    # # This shows the normal mode approximation is NOT VALID for this mode, as the second order perturbation is big! xff = 28 * omega! For the reaction mode, xff = 0.3*omega.
    import matplotlib.pyplot as plt

    barrier = 1.26 * 10 ** -19
    energy = np.linspace(0, barrier / 20, num=100)

    crp = [WagProb(1931.0, -0.00143, barrier, barrier, e) for e in energy]
    crp_shallow = [TunnelProb(1469, -0.000336, barrier, barrier, e) for e in energy]

    plt.plot(energy / barrier, crp, label="Wag Prob")
    plt.plot(energy / barrier, crp_shallow, label="Tunnel Prob")
    # plt.ylim([0, 1])
    plt.legend()
    plt.show()
