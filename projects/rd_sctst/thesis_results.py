"""
This reproduces the Criegee RD SCTST calculations, described in Chapter 7 of the thesis, and in the paper:

Shan, X., Burd, T. A. H. & Clary, D. C. New Developments in Semiclassical Transition-State Theory. 
J. Phys. Chem. A 123, 4639–4657 (2019).

"""


import sctst
import numpy as np
import matplotlib.pyplot as plt
import math
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import md_sctst as md


# ---------------------Matplolib Rc ----------------------------------------

plt.rcParams.update({'font.size': 15})
plt.rcParams.update({'lines.linewidth': 2})
dir_path = ""

# ------------------- General Parameters ------------------------------------
rec_temps = np.linspace(0.5, 5, num=80)             # Defines the range of temperatures
temperatures = 1000 / rec_temps
n_quanta = 5                                        # This is the number of vibrational states per mode sampled over.
                                                    # This should be sufficiently large for convergence

# # ---------------------- Uncatalysed ------------------------------------------------------------------------------#
ts = sctst.TransitionState("ts_me", 11, temperatures, sigma=1, rich_step_size=0.05, vpt2_log=True, run_dos=False)
r = sctst.Chemical("r_me", 11, temperatures, sigma=1, vpt2_log=True, run_dos=False)
p = sctst.Chemical("p_me", 11, temperatures, vpt2_log=True, run_dos=False)

print("Calculate FD rate")
fulld_rate = sctst.FD_Unimolecular_Reaction(r, ts, p, temperatures, label="FD_criegee_compare", integration=False)

print("Calculate 1D rate")
oned_rate = sctst.UnimolecularReaction(r, ts, p, temperatures, label="1D_SCTST")

print("Calculate 2D rate")
# Calculate the 2D rate using the most strongly coupled mode, e.g. identified using the Two Hessian approximation
twod_rate = md.md_sctst([r, ts, p], [6], temperatures,  max_quanta=n_quanta,  spectator_coupling=True)

print("Calculate 3D rate")
# Calculate three d rate using the next-most strongly coupled mode. Expect the effect to be small
threed_rate = md.md_sctst([r, ts, p], [6,22], temperatures,  max_quanta=n_quanta, spectator_coupling=True)

print("Calculate 4D rate")
# Calculate four d rate using the next-most strongly coupled mode. Expect the effect to be small
fourd_rate = md.md_sctst([r, ts, p], [6,22,23], temperatures,  max_quanta=n_quanta, spectator_coupling=True)

print("Calculate 1D+ rate")
ts.xff = ts.fd_xff
oned_plus_rate = sctst.UnimolecularReaction(r, ts, p, temperatures, label="1D")



#
# ------- Plot the Results -----------------------------------------------------------------------------

# ------------------ Arrhenius Plot ------------------------------ #
fig = plt.figure(figsize=(6, 6))
ax = host_subplot(111, axes_class=AA.Axes)

dashed = True

line, = ax.plot(rec_temps, [math.log10(i) for i in oned_plus_rate.sctstRate], label="1D+")
line, = ax.plot(rec_temps, [math.log10(i) for i in twod_rate], label="2D")
line, = ax.plot(rec_temps, [math.log10(i) for i in oned_rate.rate_tst], label="TST")
line, = ax.plot(rec_temps, [math.log10(i) for i in threed_rate], label="3D")
line, = ax.plot(rec_temps, [math.log10(i) for i in fourd_rate], label="4D")
line, = ax.plot(rec_temps, [math.log10(i) for i in fulld_rate.rate_sctst], label="FD")
line, = ax.plot(rec_temps, [math.log10(i) for i in oned_rate.sctstRate], label="1D")

ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
elif max(rec_temps) < 1000 / 250:
    ax2.set_xticks([1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["300 K", "500 K", "1000 K"])

else:
    pass

ax2.axis["right"].major_ticklabels.set_visible(False)
ax2.axis["top"].major_ticklabels.set_visible(True)

plt.xlabel("$1000 \ \mathrm{K} / T$")
plt.ylabel("$\mathrm{log[k /  s^{-1}]}$")
plt.legend()

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    plt.xticks([1, 2, 3, 4, 5])
elif max(rec_temps) <= 1000 / 250:
    plt.xticks([1, 2, 3, 4])
else:
    pass
# plt.yticks([-28, -24, -20, -16, -12])
plt.draw()

fig.savefig("plots/compare_dimensions_3D.pdf")
plt.title("Effect of 1D, 1D, 3D and FD rates", y=1.1)
plt.show()
print("plots/compare_dimensions.pdf")


# ------------------ Ratio Plot -------------------------------------------
fig = plt.figure(figsize=(6, 6))
ax = host_subplot(111, axes_class=AA.Axes)

dashed = True
line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(fulld_rate.rate_sctst, fulld_rate.rate_sctst)], 'k--', label="Full Dimensional")
#
line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(oned_plus_rate.sctstRate, fulld_rate.rate_sctst)], 'k-', label="One Dimensional Plus")
# line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(afd_rate.sctstRate, fulld_rate.rate_sctst)], 'b-', label="One Dimensional AFD")

line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(twod_rate, fulld_rate.rate_sctst)], label="Two Dimensional")
#
line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(threed_rate, fulld_rate.rate_sctst)], label="Three Dimensional")
line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(fourd_rate, fulld_rate.rate_sctst)], label="Four Dimensional")
line, = ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(oned_rate.sctstRate, fulld_rate.rate_sctst)], label="One Dimensional")

ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
elif max(rec_temps) < 1000 / 250:
    ax2.set_xticks([1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["300 K", "500 K", "1000 K"])

else:
    pass

ax2.axis["right"].major_ticklabels.set_visible(False)
ax2.axis["top"].major_ticklabels.set_visible(True)

plt.xlabel("$1000 \ \mathrm{K} / T$")
plt.ylabel("$\mathrm{log[k / k_{FD}]}$")
plt.legend()

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    plt.xticks([1, 2, 3, 4, 5])
elif max(rec_temps) < 1000 / 250:
    plt.xticks([1, 2, 3, 4])
else:
    pass
# plt.yticks([-28, -24, -20, -16, -12])
plt.draw()
plt.tight_layout()
fig.savefig("plots/compare_dimensions_ratio.pdf")
plt.title("Effect of 1D, 3D and FD rates", y=1.1)
plt.show()

print("plots/compare_dimensions_ratio.pdf")

# ------------------------ Catalysed -------------------------------------------------------------------------------- #
r = sctst.Chemical("r_criegee", 8, temperatures, sigma=1, vpt2_log=True, run_dos=False)
p = sctst.Chemical("p_criegee", 8, temperatures, vpt2_log=True, run_dos=False)
ts = sctst.TransitionState("ts_water_h_1", 11, temperatures, sigma=1, rich_step_size=0.05, vpt2_log=True, run_dos=False)
water = sctst.Chemical("water", 3, temperatures, vpt2_log=True, run_dos=False)

ts_plus = sctst.TransitionState("ts_water_h_1", 11, temperatures, sigma=1, rich_step_size=0.05, vpt2_log=True,
                           run_dos=False)
ts_plus.xff = ts_plus.fd_xff


# Calculate 1D rate
print("Calculate 1D rate")
oned_rate_cat = sctst.Reaction(r, water, ts, water, p, temperatures, label="1D", deep_tunneling=False)

# Calculate 1D+ rate
print("Calculate 1D+ rate")
oned_plus_rate_cat = sctst.Reaction(r, water, ts_plus, water, p, temperatures, label="1D+", deep_tunneling=False)

# Calculate 2D rate
print("Calcuate 2D rate")
twod_rate_cat = md.md_sctst([r, water, ts, p, water], [5], temperatures, deep_tunneling=False, max_quanta=n_quanta)

# Calculate 3D rate
print("Calcuate 3D rate")
threed_rate_cat = md.md_sctst([r, water, ts, p, water], [5, 7], temperatures, max_quanta=n_quanta, deep_tunneling=False)

# Calculate 4D rate
print("Calcuate 4D rate")
fourd_rate_cat = md.md_sctst([r, water, ts, p, water], [7, 5, 12], temperatures, max_quanta=n_quanta, deep_tunneling=False)

# Calculate 5D rate
print("Calcuate 5D rate")
fived_rate_cat = md.md_sctst([r, water, ts, p, water], [7, 5, 12, 6], temperatures, max_quanta=n_quanta, deep_tunneling=False)

# Calculate 6D rate
print("Calcuate 6D rate")
sixd_rate_cat = md.md_sctst([r, water, ts, p, water], [7, 5, 12, 6, 13], temperatures, max_quanta=n_quanta, deep_tunneling=False)

# Calculate FD rate
print("Calculate FD rate")
fulld_rate_cat = sctst.FD_Bimolecular_Reaction(r, water, ts, p, water, temperatures, label='FD_water', integration=False, deep_tunneling=False)


# ----- Plot the Results -----------------------------------------------------------------------------

fig = plt.figure(figsize=(6, 6))
ax = host_subplot(111, axes_class=AA.Axes)
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(fulld_rate_cat.rate_sctst, fulld_rate_cat.rate_sctst)], 'k--', label="Full Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(oned_rate_cat.sctstRate, fulld_rate_cat.rate_sctst)], 'k-', label="One Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(twod_rate_cat, fulld_rate_cat.rate_sctst)], label="Two Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(threed_rate_cat, fulld_rate_cat.rate_sctst)], label="Three Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(fourd_rate_cat, fulld_rate_cat.rate_sctst)], label="Four Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(fived_rate_cat, fulld_rate_cat.rate_sctst)], label="Five Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(sixd_rate_cat, fulld_rate_cat.rate_sctst)], label="Six Dimensional")
ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
elif max(rec_temps) < 1000 / 250:
    ax2.set_xticks([1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["300 K", "500 K", "1000 K"])

else:
    pass

ax2.axis["right"].major_ticklabels.set_visible(False)
ax2.axis["top"].major_ticklabels.set_visible(True)

plt.xlabel("$1000 \ \mathrm{K} / T$")
plt.ylabel("$\mathrm{log[k / k_{FD}]}$")
plt.legend()

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    plt.xticks([1, 2, 3, 4, 5])
elif max(rec_temps) < 1000 / 250:
    plt.xticks([1, 2, 3, 4])
else:
    pass
plt.draw()
plt.tight_layout()
fig.savefig("plots/compare_dimensions_ratio_catalysed.pdf")
plt.show()
print("plots/compare_dimensions_ratio_catalysed.pdf")




