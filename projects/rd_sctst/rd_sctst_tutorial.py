"""
In this script, we show how to use the md_sctst package to perform RD SCTST calculations. 
We will use the unimolecular rearrangement of a carbene species (as described in Chapter 7) an an example. 
"""

import sctst            # THe main SCTST package
import md_sctst as md   # The RD SCTST package

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA
import math

# ----------- Define the plotting text font size and line thickness ------------------------------------------------------------------------------------------------------------- #
plt.rcParams.update({'font.size': 15})
plt.rcParams.update({'lines.linewidth': 2})

# ------------------- Basic parameters ------------------------------------------------------------------------------ #
# Define a list of temperatures (in kelvin) at which we want to know the rates.
rec_temps = np.linspace(1.5,10, num=85)
temperatures = 1000 / rec_temps
n_quanta = 5                # This is the maximum quntum number in each active mode that we sum over.
# Should be large enough to converge.

# ----------- Define Transition States  ----------------------------------------------------------------------------- #
ts = sctst.TransitionState("ts_carbene", 10, temperatures, rich_step_size=0.02,  high_level_energy=False, unpaired_electron=False, vpt2_log=True, run_dos=False)

# ----------- Define the chemicals  --------------------------------------------------------------------------------- #
p = sctst.Chemical("p_carbene", 10, temperatures, high_level_energy=False, vpt2_log=True, run_dos=False)
r = sctst.Chemical("r_carbene", 10, temperatures, high_level_energy=False, vpt2_log=True, run_dos=False)

# ----------- Calculate 1D rate ------------------------------------------------------------------------------------- #
print("Calculate 1D rate")
oned_rate = sctst.UnimolecularReaction(r, ts, p, temperatures, label="criegee_1D_pure")

# ----------- Calculate 2D rate ------------------------------------------------------------------------------------- #
# Here we use the md_sctst package, which returns a list of rates at each temperature.
# The transition modes are specified in a list as an input variable
# The sixth vibrational mode was identified as most significant
print("Calculate 2D rate")
twod_rate = md.md_sctst([r, ts, p], [6], temperatures, spectator_coupling=False, max_quanta=n_quanta, deep_tunneling=False)

# ----------- Calculate FD rate ------------------------------------------------------------------------------------- #
print("Calculate FD rate")
fulld_rate = sctst.FD_Unimolecular_Reaction(r, ts, p, temperatures, integration=False, label='FD-SCTST_carbene', deep_tunneling=False)


# ----------- Plot the Results -------------------------------------------------------------------------------------- #
fig = plt.figure(figsize=(6, 6))
ax = host_subplot(111, axes_class=AA.Axes)
ax.plot(rec_temps, np.log10(fulld_rate.rate_sctst), label="Full Dimensional")
ax.plot(rec_temps, np.log10(twod_rate), label="Two Dimensional")
ax.plot(rec_temps, np.log10(oned_rate.sctstRate), label="One Dimensional")
ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis
ax2.set_xticks([1000/100, 1000 / 200, 1000 / 300, 1000 / 500])
ax2.set_xticklabels(["100K", "200 K", "300 K", "500 K"])
ax2.axis["right"].major_ticklabels.set_visible(False)
ax2.axis["top"].major_ticklabels.set_visible(True)
plt.xlabel("$1000 \ \mathrm{K} / T$")
plt.ylabel("$\mathrm{log}[k / s^{-1}]$")
plt.legend()
plt.xticks([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
# plt.yticks([-28, -24, -20, -16, -12])
plt.draw()
plt.tight_layout()
fig.savefig("plots/rd_sctst_tutorial_plot.pdf")
plt.show()
print("plots/rd_sctst_tutorial_plot.pdf")

# ----------- Plot the Results as a Ratio --------------------------------------------------------------------------- #
fig = plt.figure(figsize=(6, 6))
ax = host_subplot(111, axes_class=AA.Axes)
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(fulld_rate.rate_sctst, fulld_rate.rate_sctst)], label="Full Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(twod_rate, fulld_rate.rate_sctst)], label="Two Dimensional")
ax.plot(rec_temps, [math.log10(i / j) for i, j in zip(oned_rate.sctstRate, fulld_rate.rate_sctst)], label="One Dimensional")
ax2 = ax.twin()  # ax2 is responsible for "top" axis and "right" axis
if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    ax2.set_xticks([1000 / 200, 1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["200 K", "300 K", "500 K", "1000 K"])
elif max(rec_temps) < 1000 / 250:
    ax2.set_xticks([1000 / 300, 1000 / 500, 1])
    ax2.set_xticklabels(["300 K", "500 K", "1000 K"])
else:
    pass
ax2.axis["right"].major_ticklabels.set_visible(False)
ax2.axis["top"].major_ticklabels.set_visible(True)
plt.xlabel("$1000 \ \mathrm{K} / T$")
plt.ylabel("$\mathrm{log[k / k_{FD}]}$")
plt.legend()

if max(rec_temps) > 1000 / 250 and max(rec_temps) < 1000 / 199:
    plt.xticks([1, 2, 3, 4, 5])
elif max(rec_temps) < 1000 / 250:
    plt.xticks([1, 2, 3, 4])
else:
    pass
plt.draw()
plt.tight_layout()
fig.savefig("plots/rd_sctst_tutorial_plot_ratio.pdf")
plt.show()
print("plots/rd_sctst_tutorial_plot_ratio.pdf")
