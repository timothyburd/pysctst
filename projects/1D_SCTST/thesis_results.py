"""
This program is currently set up for calculating CH3 + CH4 -> CH4 + CH3 as described in the paper:
Burd, Timothy AH, Xiao Shan, and David C. Clary. Tunnelling and the kinetic isotope effect in CH3  CH4 CH4 CH3:
An application of semiclassical transition state theory." Chemical Physics Letters (2018).

ab initio calcualtions are performed in gaussian, and the resultant cehckpoint files are kept in the directory "checkpoint_files"
The pySCTST code uses these files to calculate reaction rates, and has functions for plotting various quanties together easily
All resultant plots are saved in a directory "plots"

"""

import matplotlib.pyplot as plt
import numpy as np
import partiton_function as pf
from mpl_toolkits.axes_grid1 import host_subplot
import mpl_toolkits.axisartist as AA

import physical_constants as pc
import sctst  # This is the main pySCTST module

# ----------- Define the plotting text font size and line thickness ------------------------------------------------------------------------------------------------------------- #
plt.rcParams.update({'font.size': 15})
plt.rcParams.update({'lines.linewidth': 2})

# ----------- Define the temperature list  -------------------------------------------------------------------------------------------------------------------------------------- #
temperatures = np.linspace(250, 2000, num=36)
rec_temps = [1000 / temp for temp in temperatures]

# ----------- Define the chemicals  -------------------------------------------------------------------------------------------------------------------------------------- #
# This is how you set up reactant and product chemicals.
# It goes (name, n_atoms, temperature_list, rotational_symmetry_number)
# Running this code will read in the fchk files, and calculate all kinds of stuff like partition functions, energies, etc
# These are accessible as properties of the object e/g ch4.q_rot
# The "sigma" is the rotational symmetry number
ch4 = sctst.Chemical("ch4", 5, temperatures, sigma=12)
ch3 = sctst.Chemical("ch3", 4, temperatures, sigma=6)

# ----------- Define Isotopes  -------------------------------------------------------------------------------------------------------------------------------------- #
# Setting up isotopic substitutions is easy - you can use the same fchk files, but include a mass list with the new isotopic weights. This will produce a warning that you can ignore
# NOTE these will each produce a warning message - this can be ignored. The warning is because we are manually adjusting the masses of the atoms
# e.g. The energy and frequency structures for species ch3 are not the same!
cd3 = sctst.Chemical("ch3", 4, temperatures, sigma=6, mass_list=[2.01565008, 2.01565008, 2.01565008, 12])
chd3 = sctst.Chemical("ch4", 5, temperatures, sigma=3, mass_list=[12, 2.01565008, 2.01565008, 2.01565008, 1.00782504])
cd4 = sctst.Chemical("ch4", 5, temperatures, sigma=12, mass_list=[12, 2.01565008, 2.01565008, 2.01565008, 2.01565008])
cdh3 = sctst.Chemical("ch4", 5, temperatures, sigma=3, mass_list=[12, 2.01565008, 1.00782504, 1.00782504, 1.00782504])

# ----------- NOTE!  -------------------------------------------------------------------------------------------------------------------------------------- #
# In order to directly compare the rate of the three isotopic reactions, the CH3 + CDH3 rate should be multiplied by four to account for the fact that there is only one possible abstraction
# Rather than four for the other two reactions. Thea easiest way to do this is to multiply the rotational symmetry by four. This is artificial. IRL the reaction will not be multiplied
# by four, but here it allows us to compare the abstraction rate to the two others
cdh3.symmetry_number = cdh3.symmetry_number * 4

# ----------- Define Transition States  -------------------------------------------------------------------------------------------------------------------------------------- #
# Define a TS using the TransitionState class
# The gaussian input files for the Richardson Extrapolation calcualtion are generated automatically
# The first time you run the code, the richardson ab intio fchk files wont be there, so you will get an error (somerthing like "no files called ts/m4.fchk)
# Now look in the directory checkpoint_files/ts and you will see a load of .com files. The default step size is 0.02
# Run these .com files on gaussian and put the fchk files in this directory
# In this paper, we use three treatments of the internal rotational DOF in the TS - "harmonic", "tanh" and "eigenvalue summation" (see the paper for details), and so we define three TS objects
# The tanh method requires the hr_groups argument to tell it where the rotor is
# The eigenvalue _summation method requires the partition function of the HR at all the temperatures.
# This will not work if the temps do not allign with the PF temperatures, so don't fiddle with the temperatures!
ts_1_harmonic = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="Harmonic")
ts_1_tanh = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="tanh", hr_groups=[[0, 1, 2, 3], [4, 5, 6, 7], [0, 4]])

ts_1_eigen = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="eigenvalue_summation",
                                   eigen_list=[2.4169, 2.6495, 2.86343, 3.06254,
                                               3.24955, 3.4264, 3.59459, 3.75528,
                                               3.90939, 4.05766, 4.20071, 4.33906,
                                               4.47313, 4.60331, 4.72991, 4.85321,
                                               4.97346, 5.09087, 5.20564, 5.31793,
                                               5.4279, 5.53569, 5.64142, 5.7452,
                                               5.84715, 5.94735, 6.04589, 6.14285,
                                               6.2383, 6.33231, 6.42495, 6.51627,
                                               6.60634, 6.69519, 6.78287, 6.86944])

# We also define the TS for the isotopically substituted reactions
ts_2 = sctst.TransitionState("ts", 9, temperatures, sigma=6,
                             mass_list=[12, 1.00782504, 1.00782504, 1.00782504, 12, 1.00782504, 1.00782504, 1.00782504,
                                        2.01565008], hr_option="tanh", hr_groups=[[0, 1, 2, 3], [4, 5, 6, 7], [0, 4]])

ts_3 = sctst.TransitionState("ts", 9, temperatures, sigma=6,
                             mass_list=[12, 2.01565008, 2.01565008, 2.01565008, 12, 2.01565008, 2.01565008, 2.01565008,
                                        2.01565008], hr_option="tanh", hr_groups=[[0, 1, 2, 3], [4, 5, 6, 7], [0, 4]])

# Correct barrier
ts_1_harmonic.manual_set_energy(ts_1_harmonic.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)
ts_1_tanh.manual_set_energy(ts_1_tanh.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)
ts_1_eigen.manual_set_energy(ts_1_eigen.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)
ts_2.manual_set_energy(ts_2.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)
ts_3.manual_set_energy(ts_3.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)

# ----------- Define Reactions  -------------------------------------------------------------------------------------------------------------------------------------- #
# We now use the Reaction class (which is used for 1D-SCTST calculations). You ned to give the class the two reactants, the TS and two products, the temperatures that you want, and a label
# R1 - CH4 + CH3 -> CH3 + CH4
# R2 - CDH3 + CH3 -> CH3 + CDH3
# R3 - CD4 + CD3 -> CD3 + CD4

# HR tests
R1_harmonic = sctst.Reaction(ch3, ch4, ts_1_harmonic, ch3, ch4, temperatures, label="Harmonic")
R1_tanh = sctst.Reaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, label="tanh")
R1_eigen = sctst.Reaction(ch3, ch4, ts_1_eigen, ch3, ch4, temperatures, label="Eigenvalue Summation")

# Isotope Effect tests
R1 = sctst.Reaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, label="$\\mathrm{CH_3 + CH_4}$")
R2 = sctst.Reaction(ch3, cdh3, ts_2, ch3, cdh3, temperatures, label="$\\mathrm{CH_3 + CDH_3}$")
R3 = sctst.Reaction(cd3, cd4, ts_3, cd3, cd4, temperatures, label="$\\mathrm{CD_3 + CD_4}$")


# Add other rate data
remmert_temps = [250, 290, 350, 400, 500, 600, 800, 1000, 1500, 2000]
remmert_rates = [1.23*10**-24, 4.89*10**-23, 1.16*10**-21, 1.33*10**-20, 5.21*10**-19, 7.22*10**-18, 2.47*10**-16, 2.43*10**-15, 7.01*10**-14, 4.77*10**-13]
R1_harmonic.experimental_rates.append([remmert_temps, remmert_rates, "Remmert"])


# ----------- Define Series  -------------------------------------------------------------------------------------------------------------------------------------- #
# The Series class accepts a list of reaction objects, and makes it easier to compare reactions with one another in plots
hindered_testing = sctst.Series([R1_eigen, R1_tanh, R1_harmonic], "Hindered Rotor Treatments", rec_temps)
isotope_effect = sctst.Series([R1, R2, R3], "Isotope Effect", rec_temps)  # NOTE reaction R2 has a different symmetry number and so is too low in this plot!
just_r1 = sctst.Series([R1_eigen], "r1_only", rec_temps)
just_r1_harmonic = sctst.Series([R1_harmonic], "r1_only", rec_temps)


# ----------- Draw Plots  -------------------------------------------------------------------------------------------------------------------------------------- #
# All plots get saved in a folder called 'plots'
R1_eigen.compare_tst_sctst()
just_r1.plot_series(with_tst='Eigenvalue Summation')    # Fig. 3.8 (a) in thesis
just_r1_harmonic.plot_series(with_tst='Harmonic')       # Fig. 3.8 (b) in thesis
hindered_testing.plot_series()                          # Fig. 3.9 (a) in thesis
hindered_testing.plot_series_ratio()                    # Fig. 3.9 (b) in thesis
isotope_effect.plot_series()                            # Fig. 3.10 in thesis

ts_1_harmonic.plot_locality()                           # Locality of the reaction mode
isotope_effect.compare_barriers()                       # The barrier is much narrower when the tunneling atom is light


# HR compare ratio of partition functions (FIg. 3.7 of thesis)

temps_tanh = np.linspace(0, 250, num=400)
freq = ts_1_tanh.frequencies[0]
I = ts_1_tanh.reduced_moment_of_inertia

q_tanh = [pf.tanh_function_comparison(freq, I, t)[0] for t in temps_tanh]
q_i = [pf.tanh_function_comparison(freq, I, t)[1] for t in temps_tanh]
q_fr = [pf.tanh_function_comparison(freq, I, t)[2] for t in temps_tanh]
q_ho = [pf.tanh_function_comparison(freq, I, t)[3] for t in temps_tanh]

fig = plt.figure(figsize=(6, 6))
ax = host_subplot(111, axes_class=AA.Axes)
plt.plot(temps_tanh , (q_ho), label='Harmonic Oscillator', linestyle='--')
plt.plot(temps_tanh , (q_fr), label='Free Rotor', linestyle='--')
plt.plot(temps_tanh , (q_tanh), label='Hindered Rotor ', c='k')
plt.ylabel("$Q$")
plt.xlabel("$T / K$")
plt.legend(loc='best')
plt.savefig("plots/tanh_approximation_diagram.pdf")
plt.show()


# --------------------- Table 3.4  ---------------------------------------
f = open("kie.out", 'w')
f.write("Temperature (K),4 $k_2 / k_1$, $k_3 / k_1$\n")
for i in range(len(temperatures)):
    tmp = "{0:4.0f}& {1:1.2e}& {2:1.2e}\\\ ".format(temperatures[i], R2.sctstRate[i] / R1_eigen.sctstRate[i], R3.sctstRate[i] / R1_eigen.sctstRate[i])
    f.write(tmp + "\n")
f.close()



# --------------------- Export rate data to txt file for supp info ---------------------------------------
f = open("data.out", 'w')
f.write("Temperature (K),R2(TST), R2(Numerical),R2(tanh)\n")
for i in range(len(temperatures)):
    tmp = "{0:4.0f}& {1:1.2e}& {2:1.2e}& {3:1.2e}\\\ ".format(temperatures[i], R1.rate_tst[i], R1_eigen.sctstRate[i], R1_tanh.sctstRate[i])
    f.write(tmp + "\n")
f.write("Temperature (K),R2(Harmonic),R3(Numerical),R4(Numerical)\n")
for i in range(len(temperatures)):
    tmp = "{0:4.0f}& {1:1.2e}& {2:1.2e}& {3:1.2e} \\\ ".format(temperatures[i], R1_harmonic.sctstRate[i], R2.sctstRate[i], R3.sctstRate[i])
    f.write(tmp + "\n")

f.close()


