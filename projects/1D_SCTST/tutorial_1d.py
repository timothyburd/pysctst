"""
This is an example script of how to run a very simple, single reaction 1D-SCTST calculation

This tutorial is currently set up for calculating CH3 + CH4 -> CH4 + CH3 as described in the paper:
Burd, Timothy AH, Xiao Shan, and David C. Clary. Tunnelling and the kinetic isotope effect in CH3  CH4 CH4 CH3:
An application of semiclassical transition state theory." Chemical Physics Letters (2018).

ab initio calcualtions are performed in gaussian, and the resultant cehckpoint files are kept in the directory "checkpoint_files"
The pySCTST code uses these files to calculate reaction rates, and has functions for plotting various quanties together easily
All resultant plots are saved in a directory "plots"

Note these calculations are not being performed at the same level of theory as in the published work, so results will not align exactly
"""

import numpy as np

import sctst  # This is the main pySCTST module

# ----------- Define the temperature list  -------------------------------------------------------------------------------------------------------------------------------------- #
# Define a list of temperatures (in kelvin) at which we want to know the rates.
temperatures = np.linspace(250, 2000, num=400)
rec_temps = [1000 / temp for temp in temperatures]

# ----------- Define the chemicals  -------------------------------------------------------------------------------------------------------------------------------------- #
# This is how you set up reactant and product chemicals.
# It goes (name, n_atoms, temperature_list, rotational_symmetry_number)
# Running this code will read in the fchk files, and calculate all kinds of stuff like partition functions, energies
# These are accessible as properties of the object e/g ch4.q_rot
# The "sigma" is the rotational symmetry number
ch4 = sctst.Chemical("ch4", 5, temperatures, sigma=12)
ch3 = sctst.Chemical("ch3", 4, temperatures, sigma=6)

# ----------- Define Transition States  -------------------------------------------------------------------------------------------------------------------------------------- #
# Define a TS using the TransitionState class
# The gaussian input files for the Richardson Extrapolation calcualtion are generated automatically
# The first time you run the code, the richardson ab intio fchk files wont be there, so you will get an error (somerthing like "no files called ts/m4.fchk)
# Now look in the directory checkpoint_files/ts and you will see a load of .com files. The default step size is 0.02
# Run these .com files on gaussian and put the fchk files in this directory
# In this paper, we use three treatments of the internal rotational DOF in the TS - "harmonic", "tanh" and "eigenvalue summation" (see the paper for details), and so we define three TS objects
# The tanh method requires the hr_groups argument to tell it where the rotor is
# The eigenvalue _summation method requires the partition function of the HR at all the temperatures.
# This will not work if the temps do not allign with the PF temperatures, so don't fiddle with the temperatures!
ts_1 = sctst.TransitionState("ts", 9, temperatures, sigma=6)
# ----------- Define Reactions  -------------------------------------------------------------------------------------------------------------------------------------- #
# We now use the Reaction class (which is used for 1D-SCTST calculations). You ned to give the class the two reactants, the TS and two products, the temperatures that you want, and a label
# You can do unimolecular reactions using the UnimolecularReaction class
R1 = sctst.Reaction(ch3, ch4, ts_1, ch3, ch4, temperatures, label="sctst")

# ---- Experiment i.e. QRS results ---------
R1.experimental_rates.append([[250, 298, 350, 400, 500, 600, 800, 1000, 1500, 2000],
                              [1.23 * 10 ** -24, 4.89 * 10 ** -23, 1.16 * 10 ** -21, 1.33 * 10 ** -20, 5.21 * 10 ** -19, 7.22 * 10 ** -18, 2.47 * 10 ** -16, 2.43 * 10 ** -15, 7.01 * 10 ** -14, 4.77 * 10 ** -13],
                              "2D scattering"])

# ----------- Define Series  -------------------------------------------------------------------------------------------------------------------------------------- #
# The Series class accepts a list of reaction objects, and makes it easier to compare reactions with one another in plots
simple_series = sctst.Series([R1], "simple_reaction", rec_temps)

# ----------- Draw Plots  -------------------------------------------------------------------------------------------------------------------------------------- #
# All plots get saved in a folder called 'plots'
# The names of the saved files are printed in the console
simple_series.plot_series(with_tst='sctst')
simple_series.compare_barriers()
simple_series.compare_vibrational_prefactor()
simple_series.plot_series_tunneling_factor()
