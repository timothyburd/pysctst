"""
This program is for comparing various approaches to 1D rate calculations.
"""

import matplotlib.pyplot as plt
import numpy as np

import physical_constants as pc
import sctst  # This is the main pySCTST module

# ----------- Define the plotting text font size and line thickness ------------------------------------------------------------------------------------------------------------- #
plt.rcParams.update({'font.size': 15})
plt.rcParams.update({'lines.linewidth': 2})

# ----------- Define the temperature list  -------------------------------------------------------------------------------------------------------------------------------------- #
rec_temps = np.linspace(0.5, 4, num=80)
temperatures = [1000 / temp for temp in rec_temps]

# ----------- Define the chemicals  -------------------------------------------------------------------------------------------------------------------------------------- #
ch4 = sctst.Chemical("ch4", 5, temperatures, sigma=12)
ch3 = sctst.Chemical("ch3", 4, temperatures, sigma=6)

# ----------- Define Transition States  -------------------------------------------------------------------------------------------------------------------------------------- #
ts_1_harmonic = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="Harmonic")
ts_1_tanh = sctst.TransitionState("ts", 9, temperatures, sigma=6, hr_option="tanh", hr_groups=[[0, 1, 2, 3], [4, 5, 6, 7], [0, 4]])


# Correct barrier
ts_1_harmonic.manual_set_energy(ts_1_harmonic.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)
ts_1_tanh.manual_set_energy(ts_1_tanh.energy + (75.4 - 78.5) / pc.hartree_to_kj_per_mol)

# ----------- Define Reactions  -------------------------------------------------------------------------------------------------------------------------------------- #

DT = sctst.Reaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, label="Wagner")
VPT2 = sctst.Reaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, label="Pure VPT2", deep_tunneling=False)
parabolic = sctst.ParabolicReaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, "Parabolic")                                        # 1d reaction
eckart = sctst.AssymetricEckartReaction(ch3, ch4, ts_1_tanh, ch3, ch4, temperatures, "Assymetric Eckart")

# ----------- Define Series  -------------------------------------------------------------------------------------------------------------------------------------- #
compare_widths = sctst.Series([DT, parabolic,eckart], "widths", rec_temps)
compare_DT = sctst.Series([DT, VPT2], "deep_tunnelling", rec_temps)

# ----------- Draw Plots  -------------------------------------------------------------------------------------------------------------------------------------- #

compare_widths.plot_series()
compare_widths.compare_barriers()
compare_DT.plot_series()
compare_DT.compare_barriers()