"""
This is an example of a more complicated calculation

The tutorial is based on the Criegge Intermediate paper: "Catalysis and tunnelling in the unimolecular decay
of Criegee intermediates" (PCCP, 2018)

This includes a FD calculation. For FD calcualtions, need a VPT2 log file for each species. These are generated by uding the freq=anharmonic key word in gaussian
This produces a _vpt2.log file, which must be put in the FD_files directory
"""

import numpy as np
import matplotlib.pyplot as plt
import sctst
# ----------- Define the plotting text font size and line thickness ------------------------------------------------------------------------------------------------------------- #
plt.rcParams.update({'font.size': 15})
plt.rcParams.update({'lines.linewidth': 2})

# -------- Basic Parameters ---------------------------------------------------------------------------------------------------------------
temperatures = np.linspace(200, 1990, num=80)
rec_temps = 1000 / temperatures

# ---------- Define Reactants ----------------------------------------------------------------------------------------------------------
print("Read in Reactants")
# Set VPT2_log to True, so a FD calculation can be done
# For FD calculations, a DOS calculation must be done on each species. This is quite slow (minuites), but only needs to be done once, since the results are outputted in a States_Output file.
# You can set run_dos = False after you have done it once to skip this step in future.
r = sctst.Chemical("r", 8, temperatures, sigma=1, vpt2_log=True, run_dos=False)
water = sctst.Chemical("water", 3, temperatures, sigma=2, vpt2_log=True, run_dos=False)

# ---------- Define Transition States ----------------------------------------------------------------------------------------------------------
print("Read in TS")
ts = sctst.TransitionState("ts", 8, temperatures, sigma=0.5, rich_step_size=0.05, vpt2_log=True, run_dos=False)
ts_water = sctst.TransitionState("ts_water", 11, temperatures, sigma=0.5, rich_step_size=0.05, vpt2_log=True, run_dos=False)
ts_water_better_xff = sctst.TransitionState("ts_water", 11, temperatures, sigma=0.5, rich_step_size=0.05, vpt2_log=True, run_dos=False)
ts_water_better_xff.xff = -0.000524

# ---------- Define Products ----------------------------------------------------------------------------------------------------------
print("Read in Products")
p = sctst.Chemical("p", 8, temperatures, sigma=1, vpt2_log=True, run_dos=False)

# ---------- Define Reactions ----------------------------------------------------------------------------------------------------------
# For FD reactions, we need to use the FD_Unimolecular_Reaction class, or the FD_Bimolecular_Reaction class
# For 1D unimolecular reactions we use cd the UnimolecularReaction class
# For 1D bimolecular reactions we use the Reaction class
# For the FD reaction classes, there is a slow step of numerically summing over the energy states of the TS. This is done by a C program, and can take a minuite or two.
# Similar to the running the DOS bit, this only needs to be done once, since this code outputs a CRP file.
# If you have done this once, you can safely set integration=False, and it just reads the output file.

# Uncatalysed
uncatalysed_1d = sctst.UnimolecularReaction(r, ts, p, temperatures, label="1D SCTST")
uncatalysed_fd = sctst.FD_Unimolecular_Reaction(r, ts, p, temperatures, label="FD_SCTST", integration=False)

# Catalysed
catalysed_1d = sctst.Reaction(water, r, ts_water, p, water, temperatures, label="1D SCTST (Catalysed reaction)")
catalysed_1d_plus = sctst.Reaction(water, r, ts_water_better_xff, p, water, temperatures, label="1D+ SCTST (Catalysed reaction)")
catalysed_fd = sctst.FD_Bimolecular_Reaction(r, water, ts_water, p, water, temperatures, label="FD_SCTST(Catalysed reaction)", integration=False)


# ---------------- Define Series for comparison of reactions ---------------------------------
compare_uncatalysed = sctst.Series([uncatalysed_fd, uncatalysed_1d], "uncatalysed", rec_temps)
compare_catalysed = sctst.Series([catalysed_fd, catalysed_1d, catalysed_1d_plus], "catalysed", rec_temps)

# ----- Plots ----------
compare_uncatalysed.plot_series(with_tst='1D SCTST')
compare_uncatalysed.plot_series_ratio()
compare_catalysed.plot_series(with_tst="1D SCTST (Catalysed reaction)")
compare_catalysed.plot_series_ratio()
