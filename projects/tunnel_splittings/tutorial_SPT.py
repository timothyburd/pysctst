"""
This script reproduces the SCTST tunnelling splitting values for Malonaldehyde at the aug-cc-pQTZ level of theory.
All ab initio calculations are included in the checkpoint_files and FD_files directory

Tim Burd 2019
"""

# --------------- Imports -------------------------------------------------------------------------------- #
import matplotlib.pyplot as plt

import physical_constants as pc
import sctst  # This is the main SCTST module that contains all the classes we need.

# --------------- Define Temperatures -------------------------------------------------------------------------------- #
# For splittings (as opposed to rates), we can use an empty list for the temperatures, since it is not a thermal quantity
temperatures = []

# --------------- Define Species -------------------------------------------------------------------------------- #
r = sctst.Chemical("malonaldehyde", 9, temperatures, high_level_energy=True, vpt2_log=True, run_dos=False)
ts = sctst.TransitionState("ts", 9, temperatures, high_level_energy=True, unpaired_electron=False, vpt2_log=True, run_dos=False)

# Below are the deuterium substituted species
r_D = sctst.Chemical("D-malonaldehyde", 9, temperatures, high_level_energy=True, vpt2_log=True, run_dos=False)
ts_D = sctst.TransitionState("D-ts", 9, temperatures, high_level_energy=True, unpaired_electron=False, vpt2_log=True, run_dos=False)

# --------------- Define Splitting -------------------------------------------------------------------------------- #
# We also need to specify the frequency of the lowest frequency reactant mode with the same symmetry as the imaginary TS mode. Here that is about 280 cm
# Initiating these classes automatically spits out the pure-VPT2 splitting and also the splitting calculated using the Wagner Deep tunnelling corrections
# It also prints out the barrier (kJ/mol), the ZPE corrected barrier (kJ/mol), the imaginary frequency (cm-1) and the anharmonic constant(cm-1)

oneD = sctst.SplittingWell(r, ts, r, 289.65, label='1D Malonaldehyde with aug-cc-pvqz')
fullD = sctst.SplittingWell(r, ts, r, 289.65, label='FD Malonaldehyde with aug-cc-pvqz', full_dimensional=True, reactant_mode=-2)

D_oneD = sctst.SplittingWell(r_D, ts_D, r_D, 279.99, label='1D D-Malonaldehyde with aug-cc-pvqz')
D_fullD = sctst.SplittingWell(r_D, ts_D, r_D, 279.99, label='FD D-Malonaldehyde with aug-cc-pvqz', full_dimensional=True, reactant_mode=-2)

# --------------- Plot the effective morse barriers -------------------------------------------------------------------------------- #
fig = plt.figure(figsize=(6, 6))
plt.axhline(y=0, c='k', linestyle=":", linewidth=1)
plt.plot(fullD.reaction_coordinates, [i * pc.hartree_to_kj_per_mol for i in oneD.vpt2_potential], label="1D (H)")
plt.plot(fullD.reaction_coordinates, [i * pc.hartree_to_kj_per_mol for i in fullD.vpt2_potential], label="FD (H)")
plt.plot(fullD.reaction_coordinates, [i * pc.hartree_to_kj_per_mol for i in D_oneD.vpt2_potential], label="1D (D)")
plt.plot(fullD.reaction_coordinates, [i * pc.hartree_to_kj_per_mol for i in D_fullD.vpt2_potential], label="FD (D)")
plt.ylim([-2, 10.5])
plt.xlim([-50, 35])
plt.legend()
plt.xlabel("Reaction Coordinate")
plt.ylabel("Energy / (kJ /mol)")
plt.show()
