(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     29494,        768]
NotebookOptionsPosition[     29177,        753]
NotebookOutlinePosition[     29553,        769]
CellTagsIndexPosition[     29510,        766]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  StyleBox["(*", "Title"], 
  StyleBox[" ", "Title"], 
  RowBox[{
   StyleBox[
    RowBox[{
    "Splitting", " ", "in", " ", "a", " ", "model", " ", "double", " ", 
     "well", " ", "potential"}], "Title"], 
   StyleBox[",", "Title"], 
   StyleBox[" ", "Title"], 
   RowBox[{
    StyleBox["using", "Title"], 
    StyleBox[" ", "Title"], 
    StyleBox["SPT", "Title"], 
    StyleBox["\[IndentingNewLine]", "Title"], 
    StyleBox["Timothy", "Subtitle"], 
    StyleBox[" ", "Subtitle"], 
    StyleBox["Burd", "Subtitle"]}], 
   StyleBox[",", "Subtitle"], 
   StyleBox["  ", "Subtitle"], 
   StyleBox[
    RowBox[{"2019", "\[IndentingNewLine]", 
     RowBox[{"timothy", ".", 
      RowBox[{"burd", "@", "chem"}], ".", "ox", ".", "ac", ".", "uk"}]}], 
    "Subtitle"]}], 
  StyleBox["\[IndentingNewLine]", "Subtitle"], 
  StyleBox["*)", "Subtitle"]}]], "Input",
 CellChangeTimes->{{3.7790839244670305`*^9, 3.779083972268727*^9}, 
   3.779087767185652*^9, {3.779107536107182*^9, 3.7791075655282393`*^9}, {
   3.7830869307333016`*^9, 3.7830869695252733`*^9}, 3.7830872844513426`*^9}],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  StyleBox[
   RowBox[{"(*", " ", 
    RowBox[{
    "Set", " ", "up", " ", "the", " ", "Double", " ", "Well", " ", 
     "potential"}], " ", "*)"}], "Section"], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"V0", " ", "=", " ", "1"}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"This", " ", "is", " ", "the", " ", "barrier", " ", "height"}], 
    " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"x0", " ", "=", 
     RowBox[{"5", " ", "*", " ", 
      SqrtBox["V0"]}]}], ";"}], "    ", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"This", " ", "fixes", " ", "the", " ", "harmonic", " ", "ZPE"}], 
     ",", " ", 
     RowBox[{
     "even", " ", "when", " ", "the", " ", "barrier", " ", "height", " ", 
      "changes"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"f", "[", "x_", "]"}], " ", ":=", " ", 
    RowBox[{"V0", " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        FractionBox[
         SuperscriptBox["x", "2"], 
         SuperscriptBox["x0", "2"]], " ", "-", "1"}], ")"}], "2"]}]}], "  ", 
   RowBox[{"(*", " ", 
    RowBox[{
    "This", " ", "is", " ", "the", " ", "functional", " ", "form", " ", "of", 
     " ", "the", " ", "double", " ", "well", " ", "potential"}], " ", "*)"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "Now", " ", "determine", " ", "basic", " ", "parameters", " ", "of", " ",
       "the", " ", "double", " ", "well", " ", "potential"}], " ", "*)"}], 
    "Section"], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"omega", " ", "=", 
     SqrtBox[
      RowBox[{" ", 
       RowBox[{"-", 
        RowBox[{
         RowBox[{"f", "''"}], "[", "0", "]"}]}]}]]}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
     "Imaginary", " ", "frequency", " ", "at", " ", "the", " ", "top", " ", 
      "of", " ", "the", " ", 
      RowBox[{"barrier", ".", " ", "Because"}], " ", "of", " ", "the", " ", 
      "minus", " ", "sign", " ", "this", " ", "is", " ", "actually", " ", 
      "the", " ", "magnitude", " ", "of", " ", "the", " ", "frequency"}], ",",
      " ", 
     RowBox[{"and", " ", "is", " ", "pure", " ", 
      RowBox[{"real", "."}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"omegaR", " ", "=", " ", 
     SqrtBox[
      RowBox[{" ", 
       RowBox[{
        RowBox[{"f", "''"}], "[", "x0", "]"}]}]]}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{
    "Reactant", " ", "frequency", " ", "in", " ", "the", " ", "well"}], " ", 
    "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{"VPT2", " ", "Parameters"}], " ", "*)"}], "Section"], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"xff", " ", "=", " ", 
     RowBox[{
      FractionBox[
       RowBox[{"-", "1"}], 
       RowBox[{"16", " ", "*", " ", 
        SuperscriptBox["omega", "2"]}]], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"f", "''''"}], "[", "0", "]"}], " ", "+", " ", 
        FractionBox[
         RowBox[{"5", " ", 
          SuperscriptBox[
           RowBox[{
            RowBox[{"f", "'''"}], "[", "0", "]"}], "2"]}], 
         RowBox[{"3", " ", 
          SuperscriptBox["omega", "2"]}]]}], ")"}]}]}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Anharmonic", " ", "constant", " ", "of", " ", "the", " ", "TS"}],
     " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"g0", " ", "=", " ", 
     RowBox[{
      FractionBox["1", 
       RowBox[{"64", " "}]], 
      RowBox[{"(", 
       RowBox[{
        FractionBox[
         RowBox[{"-", 
          RowBox[{
           RowBox[{"f", "''''"}], "[", "0", "]"}], " "}], 
         SuperscriptBox["omega", "2"]], "-", " ", 
        FractionBox[
         RowBox[{"5", " ", 
          SuperscriptBox[
           RowBox[{
            RowBox[{"f", "'''"}], "[", "0", "]"}], "2"]}], 
         RowBox[{"9", " ", 
          SuperscriptBox["omega", "4"]}]]}], ")"}]}]}], ";"}], " ", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"x00", " ", "=", " ", 
     RowBox[{
      FractionBox["1", 
       RowBox[{"16", " ", "*", " ", 
        SuperscriptBox["omegaR", "2"]}]], 
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         RowBox[{"f", "''''"}], "[", "x0", "]"}], " ", "-", " ", 
        FractionBox[
         RowBox[{"5", " ", 
          SuperscriptBox[
           RowBox[{
            RowBox[{"f", "'''"}], "[", "x0", "]"}], "2"]}], 
         RowBox[{"3", " ", 
          SuperscriptBox["omegaR", "2"]}]]}], ")"}]}]}], " ", ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{
    "Anharmonic", " ", "constant", " ", "of", " ", "the", " ", "reactant", 
     " ", "well"}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"gr", " ", "=", " ", 
     RowBox[{
      FractionBox["1", 
       RowBox[{"64", " "}]], 
      RowBox[{"(", 
       RowBox[{
        FractionBox[
         RowBox[{
          RowBox[{
           RowBox[{"f", "''''"}], "[", "x0", "]"}], " "}], 
         SuperscriptBox["omegaR", "2"]], "-", " ", 
        FractionBox[
         RowBox[{"5", " ", 
          SuperscriptBox[
           RowBox[{
            RowBox[{"f", "'''"}], "[", "x0", "]"}], "2"]}], 
         RowBox[{"9", " ", 
          SuperscriptBox["omegaR", "4"]}]]}], ")"}]}]}], ";"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "Calcualte", " ", "the", " ", "ZPE", " ", "corrected", " ", "barrier"}], 
     " ", "*)"}], "Section"], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"v0", " ", "=", " ", 
     RowBox[{
      RowBox[{"f", "[", "0", "]"}], " ", "+", " ", "g0", " ", "-", " ", 
      RowBox[{"0.5", " ", "*", " ", "omegaR"}], " ", "-", " ", 
      RowBox[{"0.25", " ", "*", " ", "x00"}], " ", "-", " ", "gr"}]}], ";"}], 
   "  ", 
   RowBox[{"(*", " ", 
    RowBox[{"Anharmonic", " ", "ZPE", " ", "corrected", " ", "barrier"}], " ",
     "*)"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"En", " ", "=", " ", "0"}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
     "The", " ", "energy", " ", "at", " ", "which", " ", "we", " ", 
      "evaluate", " ", "theta", " ", "is", " ", "at", " ", "the", " ", "ZPE", 
      " ", "of", " ", "the", " ", "reactant", " ", "state"}], ",", " ", "or", 
     ",", " ", 
     RowBox[{
     "if", " ", "we", " ", "are", " ", "using", " ", "the", " ", "ZPE", " ", 
      "corrected", " ", "barrier", " ", "height"}], ",", " ", 
     RowBox[{"at", " ", "zero", " ", "energy"}]}], " ", "*)"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "Find", " ", "Theta", " ", "and", " ", "the", " ", "splitting", " ", 
      "using", " ", "VPT2"}], " ", "*)"}], "Section"], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"theta", " ", "=", " ", 
     RowBox[{"\[Pi]", " ", "*", " ", 
      FractionBox[
       RowBox[{
        RowBox[{"-", " ", "omega"}], " ", "+", " ", 
        SqrtBox[
         RowBox[{
          SuperscriptBox["omega", "2"], " ", "+", " ", 
          RowBox[{"4", " ", "*", " ", "xff", " ", "*", 
           RowBox[{"(", 
            RowBox[{"v0", " ", "-", " ", "En"}], ")"}], " "}]}]]}], 
       RowBox[{"2", " ", "*", " ", "xff"}]]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"delta", " ", "=", 
     RowBox[{
      FractionBox["omegaR", 
       SqrtBox[
        RowBox[{"\[Pi]", "*", "\[ExponentialE]"}]]], " ", "*", " ", 
      RowBox[{"Exp", "[", 
       RowBox[{"-", " ", "theta"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{
    "\"\<The VPT2 tunnelling splitting for the well with V0 = `1` is `2`\>\"",
      ",", "V0", ",", "delta"}], "]"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{"Now", " ", "do", " ", "a", " ", "VPT4", " ", "treatment"}], " ",
      "*)"}], "Section"], "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{"Based", " ", "on", " ", 
     RowBox[{"Stanton", "'"}], "s", " ", 
     RowBox[{"work", ":", " ", "DOI", ":", 
      RowBox[{
       RowBox[{"10.1021", "/", 
        RowBox[{"acs", ".", "jpclett"}]}], ".6", "b01239"}]}]}], " ", "*)"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"phi4", " ", "=", " ", 
     FractionBox[
      RowBox[{
       RowBox[{
        RowBox[{"f", "''''"}], "[", "0", "]"}], " "}], 
      SuperscriptBox["omega", "2"]]}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "8", " ", "of", " ", "Stanton"}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"phi4R", " ", "=", " ", 
     FractionBox[
      RowBox[{
       RowBox[{
        RowBox[{"f", "''''"}], "[", "x0", "]"}], " "}], 
      SuperscriptBox["omegaR", "2"]]}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "8", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"phi3R", " ", "=", " ", 
     FractionBox[
      RowBox[{
       RowBox[{
        RowBox[{"f", "'''"}], "[", "x0", "]"}], " "}], 
      SuperscriptBox["omegaR", "1.5"]]}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "8", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"W", " ", "=", " ", 
     RowBox[{"omega", " ", "-", " ", 
      FractionBox[
       RowBox[{"67", " ", "*", " ", 
        SuperscriptBox["phi4", "2"]}], 
       RowBox[{"9216", " ", "*", " ", "omega"}]]}]}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "4", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Y", " ", "=", " ", 
     FractionBox[
      RowBox[{
       RowBox[{"-", "17"}], " ", "*", " ", 
       SuperscriptBox["phi4", "2"]}], 
      RowBox[{"2304", " ", "*", " ", "omega"}]]}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"Equation", " ", "5", " ", "of", " ", "Stanton"}], " ", ",", " ", 
     RowBox[{
     "note", " ", "that", " ", "phi3", " ", "at", " ", "the", " ", "TS", " ", 
      "is", " ", "zero", " ", "by", " ", "symmetry"}]}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"X", " ", "=", " ", "xff"}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "6", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"G", " ", "=", " ", "g0"}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "7", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Wr", " ", "=", " ", 
     RowBox[{"omegaR", " ", "-", " ", 
      FractionBox[
       RowBox[{"67", " ", "*", " ", 
        SuperscriptBox["phi4R", "2"]}], 
       RowBox[{"9216", " ", "*", " ", "omegaR"}]]}]}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "4", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"YR", " ", "=", " ", 
     RowBox[{
      FractionBox[
       RowBox[{
        RowBox[{"-", "17"}], " ", "*", " ", 
        SuperscriptBox["phi4R", "2"]}], 
       RowBox[{"2304", " ", "*", " ", "omegaR"}]], " ", "+", " ", 
      FractionBox[
       RowBox[{"25", "*", " ", 
        SuperscriptBox["phi3R", "2"], " ", "*", "phi4R"}], 
       RowBox[{"384", " ", "*", " ", 
        SuperscriptBox["omegaR", "2"]}]], " ", "-", " ", 
      FractionBox[
       RowBox[{"235", " ", "*", " ", 
        SuperscriptBox["phi3R", "4"]}], 
       RowBox[{"6912", " ", 
        SuperscriptBox["omegaR", "3"]}]]}]}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "5", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Xr", " ", "=", " ", "x00"}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "6", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Gr", " ", "=", " ", "gr"}], ";"}], 
   RowBox[{"(*", " ", 
    RowBox[{"Equation", " ", "7", " ", "of", " ", "Stanton"}], "  ", "*)"}], 
   "\[IndentingNewLine]", "\[IndentingNewLine]", "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"ZPE", " ", "=", " ", 
     RowBox[{
      RowBox[{"0.5", " ", "*", " ", "Wr"}], " ", "+", " ", 
      RowBox[{"0.25", " ", "*", " ", "Xr"}], " ", "+", " ", 
      RowBox[{"YR", " ", "*", " ", 
       RowBox[{"(", 
        FractionBox["1", 
         SuperscriptBox["2", "3"]], ")"}]}], " ", "+", " ", "gr"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"zpeBarrier", " ", "=", " ", 
     RowBox[{"V0", " ", "+", " ", "G", " ", "-", " ", "ZPE"}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"thetas", " ", "=", " ", 
     RowBox[{"Roots", "[", 
      RowBox[{
       RowBox[{
        RowBox[{"zpeBarrier", " ", "-", " ", 
         RowBox[{
          FractionBox["W", "\[Pi]"], "thetaVPT4"}], " ", "-", " ", 
         RowBox[{
          FractionBox[
           RowBox[{"X", "  "}], 
           SuperscriptBox["\[Pi]", "2"]], 
          SuperscriptBox["thetaVPT4", "2"]}], " ", "-", " ", 
         RowBox[{
          FractionBox["Y", 
           SuperscriptBox["\[Pi]", "3"]], 
          SuperscriptBox["thetaVPT4", "3"]}]}], " ", "\[Equal]", " ", "0"}], 
       " ", ",", " ", "thetaVPT4"}], "]"}]}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{"Need", " ", "to", " ", "solve", " ", "this", " ", 
      RowBox[{"cubic", "!"}], " ", "Here", " ", "we", " ", "do", " ", "it", 
      " ", "numerically"}], ",", " ", 
     RowBox[{"giving", " ", "three", " ", "roots"}]}], " ", "*)"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"thetasList", " ", "=", " ", 
     RowBox[{"Sort", "@", 
      RowBox[{
       RowBox[{"Level", "[", 
        RowBox[{"thetas", ",", "1", ",", 
         RowBox[{"Heads", "\[Rule]", "False"}]}], "]"}], "[", 
       RowBox[{"[", 
        RowBox[{"All", ",", "2"}], "]"}], "]"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"theta4", " ", "=", " ", 
     RowBox[{"thetasList", "[", 
      RowBox[{"[", "2", "]"}], "]"}]}], ";"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"delta4", " ", "=", 
     RowBox[{
      FractionBox["omegaR", 
       SqrtBox[
        RowBox[{"\[Pi]", "*", "\[ExponentialE]"}]]], " ", "*", " ", 
      RowBox[{"Exp", "[", 
       RowBox[{"-", " ", "theta4"}], "]"}]}]}], ";"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{
     RowBox[{
     "Calculate", " ", "Delta", " ", "using", " ", "all", " ", "three", " ", 
      "roots"}], " ", "-", " ", 
     RowBox[{
     "only", " ", "one", " ", "will", " ", "give", " ", "a", " ", "sensible", 
      " ", "answer"}]}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{
    "\"\<The VPT4 tunnelling splitting for the well with V0 = `1` is `2`. \
\>\"", ",", "V0", ",", "delta4"}], "]"}], 
   StyleBox["\[IndentingNewLine]", "Section"], 
   StyleBox["\[IndentingNewLine]", "Section"], 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "We", " ", "can", " ", "also", " ", "do", " ", "a", " ", "VPT0", " ", 
      RowBox[{"(", "Parabolic", ")"}], "  ", "Treatment"}], " ", "*)"}], 
    "Section"], 
   StyleBox["\[IndentingNewLine]", "Section"], 
   RowBox[{
    RowBox[{"harmonicZPE", " ", "=", " ", 
     RowBox[{"0.5", " ", "*", "omegaR"}]}], " ", ";"}], 
   StyleBox["\[IndentingNewLine]", "Section"], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"theta0", " ", "=", " ", 
     FractionBox[
      RowBox[{"\[Pi]", " ", 
       RowBox[{"(", 
        RowBox[{"V0", " ", "-", "harmonicZPE"}], ")"}]}], "omega"]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"delta0", " ", "=", 
     RowBox[{
      FractionBox["omegaR", 
       SqrtBox[
        RowBox[{"\[Pi]", "*", "\[ExponentialE]"}]]], " ", "*", " ", 
      RowBox[{"Exp", "[", 
       RowBox[{"-", " ", "theta0"}], "]"}]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{
    "\"\<The VPT0 tunnelling splitting for the well with V0 = `1` is `2`. \
Expected to overestimate the splitting as the barrier is approximated as far \
too narrow.\>\"", ",", "V0", ",", 
     RowBox[{"N", "[", "delta0", "]"}]}], "]"}], 
   StyleBox["\[IndentingNewLine]", "Section"], "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
      RowBox[{
      "We", " ", "can", " ", "also", " ", "do", " ", "an", " ", "analytic", 
       " ", "Instanton", " ", "Treatment"}], " ", ",", " ", 
      RowBox[{"See", " ", "Garg", " ", 
       RowBox[{"(", "2000", ")"}]}]}], "*)"}], "Section"], 
   StyleBox["\[IndentingNewLine]", "Section"], 
   RowBox[{
    RowBox[{"S", " ", "=", " ", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"16", "/", "3"}], ")"}], " ", "*", " ", 
      RowBox[{"V0", " ", "/", " ", "omegaR"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"detlaWKB", " ", "=", " ", 
     RowBox[{"4", " ", 
      SqrtBox["3"], " ", "omegaR", " ", 
      SqrtBox[
       FractionBox["S", 
        RowBox[{"2", "\[Pi]"}]]], 
      SuperscriptBox["\[ExponentialE]", 
       RowBox[{"-", "S"}]]}]}], ";"}], "\[IndentingNewLine]", 
   RowBox[{"StringForm", "[", 
    RowBox[{
    "\"\<The analytical WKB tunnelling splitting for the well with V0 = `1` \
is `2`.\>\"", ",", "V0", ",", 
     RowBox[{"N", "[", "detlaWKB", "]"}]}], "]"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{
   "StringForm", "[", "\"\<The double well potential looks like this:\>\"", 
    "]"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "We", " ", "can", " ", "plot", " ", "the", " ", "double", " ", "well", 
      " ", "potential"}], " ", "*)"}], "Section"], "\[IndentingNewLine]", 
   RowBox[{"Plot", "[", 
    RowBox[{
     RowBox[{"f", "[", "x", "]"}], ",", " ", 
     RowBox[{"{", 
      RowBox[{"x", ",", " ", 
       RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
     RowBox[{"PlotStyle", "\[Rule]", 
      RowBox[{"{", 
       RowBox[{
        RowBox[{"RGBColor", "[", 
         RowBox[{
          RowBox[{"27", "/", "255"}], ",", 
          RowBox[{"158", "/", "255"}], ",", 
          RowBox[{"119", "/", "255"}]}], "]"}], ",", " ", 
        RowBox[{"Thickness", "[", "0.01", "]"}]}], "}"}]}]}], "]"}], " ", 
   RowBox[{"(*", " ", 
    RowBox[{
    "Plot", " ", "of", " ", "the", " ", "double", " ", "well", " ", 
     "potential"}], " ", "*)"}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "Find", " ", "the", " ", "equivalent", " ", "Morse", " ", "barrier", " ",
       "parameters", " ", "to", " ", "compare", " ", "effective", " ", 
      "barrier", " ", "shapes"}], " ", "*)"}], "Section"], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"De", " ", "=", " ", 
     RowBox[{"0.25", " ", "*", " ", 
      RowBox[{
       SuperscriptBox["omega", "2"], "  ", "/", " ", "xff"}]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"a", " ", "=", " ", 
     RowBox[{"omega", " ", "/", " ", 
      SqrtBox[
       RowBox[{
        RowBox[{"-", "2"}], " ", "*", " ", "De"}]]}]}], ";"}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Morse", "[", "x_", "]"}], " ", ":=", " ", 
    RowBox[{
     RowBox[{"De", " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{"1", " ", "-", " ", 
         RowBox[{"Exp", "[", 
          RowBox[{"a", " ", "*", "x"}], "]"}]}], ")"}], "2"]}], " ", "+", " ",
      "V0"}]}], "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{"(*", " ", 
     RowBox[{
     "Plot", " ", "the", " ", "effective", " ", "barrier", " ", "with", " ", 
      "the", " ", "double", " ", "well", " ", "potential"}], " ", "*)"}], 
    "Section"], "\[IndentingNewLine]", 
   RowBox[{
   "StringForm", "[", "\"\<The effective VPT2 barrier looks like this:\>\"", 
    "]"}], "\[IndentingNewLine]", 
   RowBox[{"Show", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Plot", "[", 
      RowBox[{
       RowBox[{"f", "[", "x", "]"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", 
         RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
       RowBox[{"PlotStyle", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"RGBColor", "[", 
           RowBox[{
            RowBox[{"27", "/", "255"}], ",", 
            RowBox[{"158", "/", "255"}], ",", 
            RowBox[{"119", "/", "255"}]}], "]"}], ",", " ", 
          RowBox[{"Thickness", "[", "0.01", "]"}]}], "}"}]}]}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Plot", "[", 
      RowBox[{
       RowBox[{"Morse", "[", 
        RowBox[{"x", "-", "0.9"}], "]"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", 
         RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
       RowBox[{"PlotStyle", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"RGBColor", "[", 
           RowBox[{
            RowBox[{"217", "/", "255"}], ",", 
            RowBox[{"95", "/", "255"}], ",", 
            RowBox[{"2", "/", "255"}]}], "]"}], ",", " ", ",", " ", 
          RowBox[{"Thickness", "[", "0.01", "]"}]}], "}"}]}]}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Plot", "[", 
      RowBox[{"ZPE", ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", 
         RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
       RowBox[{"PlotStyle", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{"Dashed", ",", " ", 
          RowBox[{"RGBColor", "[", 
           RowBox[{
            RowBox[{"117", "/", "255"}], ",", 
            RowBox[{"112", "/", "255"}], ",", 
            RowBox[{"179", "/", "255"}]}], "]"}], ",", " ", ",", " ", 
          RowBox[{"Thickness", "[", "0.005", "]"}]}], "}"}]}]}], "]"}]}], 
    "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{"(*", " ", 
    RowBox[{
    "We", " ", "can", " ", "plot", " ", "the", " ", "effective", " ", "VPT0", 
     " ", "barrier", " ", "too"}], " ", "*)"}], "\[IndentingNewLine]", 
   RowBox[{
   "StringForm", "[", "\"\<The Effective VPT0 barrier looks like this:\>\"", 
    "]"}], "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"Parabola", "[", "x_", "]"}], " ", ":=", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "0.5"}], " ", "*", " ", "x", " ", "*", " ", "x", " ", "*", 
      " ", 
      SuperscriptBox["omega", "2"]}], "+", " ", "V0"}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"Show", "[", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"Plot", "[", 
      RowBox[{
       RowBox[{"f", "[", "x", "]"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", 
         RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
       RowBox[{"PlotStyle", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"RGBColor", "[", 
           RowBox[{
            RowBox[{"27", "/", "255"}], ",", 
            RowBox[{"158", "/", "255"}], ",", 
            RowBox[{"119", "/", "255"}]}], "]"}], ",", " ", 
          RowBox[{"Thickness", "[", "0.010", "]"}]}], "}"}]}]}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Plot", "[", 
      RowBox[{
       RowBox[{"Parabola", "[", "x", "]"}], ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", 
         RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
       RowBox[{"PlotStyle", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{
          RowBox[{"RGBColor", "[", 
           RowBox[{
            RowBox[{"117", "/", "255"}], ",", 
            RowBox[{"112", "/", "255"}], ",", 
            RowBox[{"179", "/", "255"}]}], "]"}], ",", " ", ",", " ", 
          RowBox[{"Thickness", "[", "0.010", "]"}]}], "}"}]}]}], "]"}], ",", 
     "\[IndentingNewLine]", 
     RowBox[{"Plot", "[", 
      RowBox[{"ZPE", ",", " ", 
       RowBox[{"{", 
        RowBox[{"x", ",", " ", 
         RowBox[{"-", "7"}], ",", " ", "7"}], "}"}], ",", " ", 
       RowBox[{"PlotStyle", "\[Rule]", 
        RowBox[{"{", 
         RowBox[{"Dashed", ",", " ", 
          RowBox[{"RGBColor", "[", 
           RowBox[{
            RowBox[{"117", "/", "255"}], ",", 
            RowBox[{"112", "/", "255"}], ",", 
            RowBox[{"179", "/", "255"}]}], "]"}], ",", " ", ",", " ", 
          RowBox[{"Thickness", "[", "0.005", "]"}]}], "}"}]}]}], "]"}]}], 
    "\[IndentingNewLine]", "]"}], "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.7730427752724743`*^9, 3.773042873279869*^9}, {
   3.7730430995533032`*^9, 3.7730431786413937`*^9}, {3.773043215021755*^9, 
   3.773043215093748*^9}, 3.7730444007790003`*^9, {3.7730458596470003`*^9, 
   3.77304588288*^9}, {3.775537375866*^9, 3.775537471908*^9}, 
   3.7755377786359997`*^9, {3.7755378362609997`*^9, 3.775537836652*^9}, {
   3.775537869116*^9, 3.775537869259*^9}, {3.775537972061*^9, 
   3.775538031499*^9}, {3.775545946826*^9, 3.775545947002*^9}, {
   3.775545994291*^9, 3.7755462218900003`*^9}, {3.7755467426029997`*^9, 
   3.775546763314*^9}, {3.777193726636832*^9, 3.777193733259832*^9}, {
   3.7771937686378317`*^9, 3.777193771565832*^9}, {3.7771942896119013`*^9, 
   3.7771942921230993`*^9}, 3.7771943361406755`*^9, {3.7771944659964027`*^9, 
   3.7771945043479795`*^9}, {3.7771946057813187`*^9, 3.777194614755113*^9}, {
   3.777194709738658*^9, 3.777194711211658*^9}, {3.777194854835573*^9, 
   3.7771948596909704`*^9}, {3.779083921924247*^9, 3.779083922080246*^9}, {
   3.7790839851654444`*^9, 3.779084168634472*^9}, {3.7790843747309017`*^9, 
   3.7790843758696947`*^9}, {3.7790844084910765`*^9, 3.779084452830634*^9}, {
   3.7790844841864367`*^9, 3.779084652691437*^9}, {3.7790847991393566`*^9, 
   3.779084848660853*^9}, {3.7790849625797114`*^9, 3.779084968718872*^9}, {
   3.779085258633014*^9, 3.779085282064064*^9}, {3.7790866838633413`*^9, 
   3.779086829575341*^9}, {3.779087106613864*^9, 3.779087126754891*^9}, {
   3.7790872878757257`*^9, 3.7790872964695845`*^9}, {3.779087385999537*^9, 
   3.7790874097119083`*^9}, {3.7790874461565523`*^9, 3.779087465070443*^9}, 
   3.779087572968232*^9, {3.779087623877322*^9, 3.7790876505509896`*^9}, {
   3.7790876918521194`*^9, 3.7790877535632896`*^9}, {3.779087824257341*^9, 
   3.7790878250883408`*^9}, {3.779087988556341*^9, 3.779087990400341*^9}, {
   3.779097925488*^9, 3.779098117033*^9}, {3.779098153241*^9, 
   3.7790981749849997`*^9}, {3.7790983090620003`*^9, 3.779098405664*^9}, {
   3.779098506475*^9, 3.7790985157060003`*^9}, {3.779098571601*^9, 
   3.779098608329*^9}, {3.7790987385480003`*^9, 3.779098776825*^9}, {
   3.779098816916338*^9, 3.7790990646295643`*^9}, {3.779099197374*^9, 
   3.779099235988*^9}, {3.779099269139*^9, 3.779099379795*^9}, {
   3.779099572046*^9, 3.779099572156*^9}, {3.77909963431*^9, 
   3.779099634899*^9}, {3.7790998078450003`*^9, 3.7790998092209997`*^9}, 
   3.779099999407*^9, {3.77910012059*^9, 3.779100235941*^9}, {
   3.7791002902939997`*^9, 3.7791003227019997`*^9}, {3.779100354292*^9, 
   3.779100375229*^9}, {3.779107503077485*^9, 3.7791075257842145`*^9}, {
   3.779512416292339*^9, 3.779512434426339*^9}, {3.7800339343238096`*^9, 
   3.7800339507728095`*^9}, {3.7800340759288096`*^9, 
   3.7800342987188096`*^9}, {3.780208517498*^9, 3.7802085175620003`*^9}, {
   3.780208594692*^9, 3.780208653421*^9}, {3.7802086856029997`*^9, 
   3.780208685708*^9}, {3.7802087312530003`*^9, 3.7802087313719997`*^9}, 
   3.7830869288243017`*^9, {3.7830869595728936`*^9, 3.783087163102542*^9}, 
   3.7830872500663443`*^9}]
},
WindowSize->{1381, 992},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
Magnification:>0.9 Inherited,
FrontEndVersion->"11.0 for Microsoft Windows (64-bit) (September 21, 2016)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1102, 30, 128, "Input"],
Cell[1663, 52, 27510, 699, 2227, "Input"]
}
]
*)

