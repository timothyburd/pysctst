"""
This file contains tests to ensure the code is in the correct state to reproduce the results of Tim Burd's Thesis
"""

import unittest
import numpy as np
import sctst  # This is the main pySCTST module
import os
class TestFiles(unittest.TestCase):
    def setUp(self):

        # Make list of required files for the tutorials/thesis results examples
        required_species = ["malonaldehyde", "D-malonaldehyde"]
        required_ts = ["ts", "D-ts"]

        self.required_files = ["checkpoint_files/" + i + "_freq.fchk" for i in required_species + required_ts]
        self.required_files = ["checkpoint_files/" + i + "_energy.fchk" for i in required_species + required_ts]
        self.required_files += ["checkpoint_files/" + i + "/m" + str(j) + ".fchk" for i in required_ts for j in range(1, 5)]
        self.required_files += ["checkpoint_files/" + i + "/p" + str(j) + ".fchk" for i in required_ts for j in range(1, 5)]
        self.required_files += ["FD_files/" + i + "_vpt2.log" for i in required_species + required_ts]
        self.required_files += ["FD_files/States_Output_" + i + ".txt" for i in required_species + required_ts]

    def test_files_present(self):
        """
        Check if all files in required_files lis are present
        :return: 
        """

        self.file_present = [os.path.exists(file) for file in self.required_files]
        missing_files = []
        for index, value in enumerate(self.file_present):
            if not value:
                missing_files.append(self.required_files[index])


        self.assertTrue(all(self.file_present), "You are missing a required file for this project." + str(missing_files))


class TestSplittings(unittest.TestCase):

    def setUp(self):
        temperatures = []

        r = sctst.Chemical("malonaldehyde", 9, temperatures, high_level_energy=True, vpt2_log=True, run_dos=False)
        ts = sctst.TransitionState("ts", 9, temperatures, high_level_energy=True, unpaired_electron=False, vpt2_log=True,
                               run_dos=False)

        self.oneD = sctst.SplittingWell(r, ts, r, 289.65, label='1D Malonaldehyde with aug-cc-pvqz')
        self.fullD = sctst.SplittingWell(r, ts, r, 289.65, label='FD Malonaldehyde with aug-cc-pvqz', full_dimensional=True, reactant_mode=-2)

    def test_deltaFD(self):
        self.assertEqual(self.fullD.delta_LH, 18.686020756882272)

    def test_delta0(self):
        self.assertEqual(self.oneD.delta_LH, 22.227691306857441)



if __name__ == '__main__':
    unittest.main()

